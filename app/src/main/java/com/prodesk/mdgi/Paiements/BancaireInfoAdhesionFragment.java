package com.prodesk.mdgi.Paiements;
import android.os.Bundle;import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;import android.view.LayoutInflater;
import android.view.View;import android.view.ViewGroup;import com.prodesk.mdgi.R;
//** Sam 17.08.2019 // 1.46pm **//
public class BancaireInfoAdhesionFragment extends Fragment
{
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_bancaire_info_adhesion, container, false);
        return view;
    }
}