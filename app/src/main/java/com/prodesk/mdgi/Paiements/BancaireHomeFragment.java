package com.prodesk.mdgi.Paiements;
import android.content.Intent;import android.os.Bundle;
import android.support.annotation.Nullable;import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;import android.view.LayoutInflater;
import android.view.View;import android.view.ViewGroup;
import android.widget.LinearLayout;import com.prodesk.mdgi.R;
import com.prodesk.mdgi.WebViewer.WebActivity;
//** Sam 17.08.2019 // 1.46pm **//
public class BancaireHomeFragment extends Fragment
{
    Fragment frInfoAdhesion = new BancaireInfoAdhesionFragment();Fragment frInfoConnexion = new BancaireInfoConnexionFragment();
    public BancaireHomeFragment() {}
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        final android.support.v4.app.FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        View view = inflater.inflate(R.layout.fragment_bancaire_home, container, false);
        LinearLayout gotoAdhesion = (LinearLayout)view.findViewById(R.id.bancaire_adhesion);
        gotoAdhesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openbancairewebconnect(getString(R.string.txt_bancaire_adhesion_title),getString(R.string.txt_bancaire_adhesion_link),"website");
            }
        });
        LinearLayout gotoConnection = (LinearLayout)view.findViewById(R.id.bancaire_connexion);
        gotoConnection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openbancairewebconnect(getString(R.string.txt_bancaire_connexion_title),getString(R.string.txt_bancaire_connexion_link),"website");
            }
        });
        LinearLayout gotoInfoAdhesion = (LinearLayout)view.findViewById(R.id.bancaire_info_adhesion);
        gotoInfoAdhesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                fragmentTransaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
                fragmentManager.beginTransaction().replace(R.id.bancaire_fragment_container, frInfoAdhesion).addToBackStack(null).commit();
            }
        });
        LinearLayout gotoInfoConnexion = (LinearLayout)view.findViewById(R.id.bancaire_info_connexion);
        gotoInfoConnexion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                fragmentTransaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
                fragmentManager.beginTransaction().replace(R.id.bancaire_fragment_container, frInfoConnexion).addToBackStack(null).commit();
            }
        });
        LinearLayout gotoInfoPaiement = (LinearLayout)view.findViewById(R.id.bancaire_info_paiement);
        gotoInfoPaiement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openbancairewebconnect(getString(R.string.txt_bancaire_info_paiement),getString(R.string.txt_bancaire_paiement_link),"website");
            }
        });return view;
    }
    void openbancairewebconnect(String title, String link, String icon)
    {
        Intent intPourWeb = new Intent(getActivity(), WebActivity.class);intPourWeb.putExtra("dataWebTitle", title);intPourWeb.putExtra("dataWebLink", link);
        intPourWeb.putExtra("dataWebIcon", icon);startActivity(intPourWeb);
    }
}