package com.prodesk.mdgi.Paiements;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import com.prodesk.mdgi.R;
//** Sam 17.08.2019 // 1.47pm **//
public class PaiementMobile extends AppCompatActivity
{
    String encHash = Uri.encode("#");String ussdjai;
    Animation hautEtbas;Animation zoomDedans, zoomDehors;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);setContentView(R.layout.paiement_mobile);
        TextView gotoOrangeMoney = (TextView) findViewById(R.id.mobile_orange_money);
        TextView gotoFloozMoney = (TextView)findViewById(R.id.mobile_flooz_money);
        TextView gotoMtnMoney = (TextView)findViewById(R.id.mobile_mtn_money);
        final ImageView mainBackground = (ImageView)findViewById(R.id.paiment_mobile_img);
        // charger aimation
        /*zoomDedans = AnimationUtils.loadAnimation(this,R.anim.zoomin);
        zoomDehors = AnimationUtils.loadAnimation(this,R.anim.zoomout);
        zoomDedans.setAnimationListener(new Animation.AnimationListener()
        {
            @Override
            public void onAnimationStart(Animation arg0) {}
            @Override
            public void onAnimationRepeat(Animation arg0) {}
            @Override
            public void onAnimationEnd(Animation arg0) {
                mainBackground.startAnimation(zoomDehors);
            }
        });
        zoomDehors.setAnimationListener(new Animation.AnimationListener()
        {
            @Override
            public void onAnimationStart(Animation arg0) {}
            @Override
            public void onAnimationRepeat(Animation arg0) {}
            @Override
            public void onAnimationEnd(Animation arg0) {
                mainBackground.startAnimation(zoomDedans);
            }
        });
        hautEtbas = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.haut_bas);
        mainBackground.startAnimation(zoomDedans);*/
        //mainBackground.startAnimation(zoomDedans);
        // onclick for them
        gotoFloozMoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intFlooz = new Intent(PaiementMobile.this, PaiementMobileFlooz.class);
                startActivity(intFlooz);
            }
        });
        gotoMtnMoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intMtn = new Intent(PaiementMobile.this, PaiementMobileMtn.class);
                startActivity(intMtn);
            }
        });
        gotoOrangeMoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intOrange = new Intent(PaiementMobile.this, PaiementMobileOrange.class);
                startActivity(intOrange);
            }
        });
    }
    @Override
    public void finish() {
        super.finish();
    }
}