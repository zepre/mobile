package com.prodesk.mdgi.Paiements;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import com.prodesk.mdgi.R;
//** Sam 17.08.2019 // 1.47pm **//
public class PaiementBancaire extends AppCompatActivity
{
    Fragment frBancaireHome = new BancaireHomeFragment();
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.paiement_bancaire);
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (findViewById(R.id.bancaire_fragment_container) != null) {
            if (savedInstanceState != null) {
                return;
            }
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.add(R.id.bancaire_fragment_container, frBancaireHome, null);
            fragmentTransaction.commit();
        }
    }
    public Fragment currentFragment()
    {
        return getSupportFragmentManager().findFragmentById(R.id.bancaire_fragment_container);
    }
    @Override
    public void onBackPressed()
    {
        if (currentFragment() instanceof BancaireHomeFragment) {
            finish();
        } else {
            FragmentManager fragmentManager = getSupportFragmentManager();
            if (currentFragment() instanceof BancaireInfoAdhesionFragment) {
                fragmentManager.beginTransaction().replace(R.id.bancaire_fragment_container, frBancaireHome).commit();
            }
            if (currentFragment() instanceof BancaireInfoConnexionFragment) {
                fragmentManager.beginTransaction().replace(R.id.bancaire_fragment_container, frBancaireHome).commit();
            }
        }
    }
    @Override
    public void finish()
    {
        super.finish();
    }
}