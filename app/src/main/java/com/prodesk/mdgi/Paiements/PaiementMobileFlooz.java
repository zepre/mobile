package com.prodesk.mdgi.Paiements;
import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import com.prodesk.mdgi.R;
//** Lun 30.09.2019 // 10.43pm **//
public class PaiementMobileFlooz extends AppCompatActivity
{
    String encHash = Uri.encode("#");String ussdjai;
    Animation hautEtbas;Animation zoomDedans, zoomDehors;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);setContentView(R.layout.paiement_mobile_flooz);
        TextView floozPaiementSynthetique = (TextView) findViewById(R.id.flooz_paiement_synthetique);
        TextView floozPaiementEimpot = (TextView) findViewById(R.id.flooz_paiement_eimpot);
        TextView floozPaiementConsultationAvis = (TextView) findViewById(R.id.flooz_consultation_avis);
        TextView floozPaiementConsultationPaiement = (TextView) findViewById(R.id.flooz_consultation_paiement);
        final ImageView mainBackground = (ImageView)findViewById(R.id.paiment_mobile_img_flooz);
        zoomDedans = AnimationUtils.loadAnimation(this,R.anim.zoomin);
        zoomDehors = AnimationUtils.loadAnimation(this,R.anim.zoomout);
        zoomDedans.setAnimationListener(new Animation.AnimationListener()
        {
            @Override
            public void onAnimationStart(Animation arg0) {}
            @Override
            public void onAnimationRepeat(Animation arg0) {}
            @Override
            public void onAnimationEnd(Animation arg0) {
                mainBackground.startAnimation(zoomDehors);
            }
        });
        zoomDehors.setAnimationListener(new Animation.AnimationListener()
        {
            @Override
            public void onAnimationStart(Animation arg0) {}
            @Override
            public void onAnimationRepeat(Animation arg0) {}
            @Override
            public void onAnimationEnd(Animation arg0) {
                mainBackground.startAnimation(zoomDedans);
            }
        });
        hautEtbas = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.haut_bas);
        mainBackground.startAnimation(zoomDedans);
        floozPaiementSynthetique.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ussdjai = "*155*4*6*1*1"+encHash;
                executerFloozService(ussdjai);
            }
        });
        floozPaiementEimpot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ussdjai = "*155*4*6*1*2"+encHash;
                executerFloozService(ussdjai);
            }
        });
        floozPaiementConsultationAvis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ussdjai = "*155*4*6*1*3"+encHash;
                executerFloozService(ussdjai);
            }
        });
        floozPaiementConsultationPaiement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ussdjai = "*155*4*6*1*4"+encHash;
                executerFloozService(ussdjai);
            }
        });
    }
    public void executerFloozService(String code)
    {
        try {
            if(Build.VERSION.SDK_INT > 22) {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, 101);
                    return;
                }
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + code));
                startActivity(callIntent);
            }
            else {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + code));
                startActivity(callIntent);
            }
        }
        catch (Exception ex) {}
    }
    @Override
    public void finish()
    {
        super.finish();
    }
}