package com.prodesk.mdgi.DonneesLocal;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import com.prodesk.mdgi.R;
/**
 * Created by Candidell on 05/07/2019.
 */
public class WeasDonneesLocal extends SQLiteOpenHelper
{
    public static final  String NOM_DONNEELOCAL = "Mdgici.db";public static final  String NOM_TABLE = "communiques_local";
    public static final  String TABLE_ID = "COM_ID";public static final  int DATA_VERSION = 1;
    public static final  String TABLE_TITRE = "COM_TITRE";public static final  String TABLE_CONTENU = "COM_CONTENU";
    public static final  String TABLE_DATE = "COM_DATE";public static final  String TABLE_HEURE = "COM_HEURE";
    public static final  String TABLE_IMAGE = "COM_IMAGE";public static final  String TABLE_AUTEUR = "COM_AUTEUR";public static final  String TABLE_FOLDERIMAGE = "COM_FOLDERIMAGE";
    private static final String CREATION_TABLE = "CREATE TABLE " + NOM_TABLE + "("
            + TABLE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + TABLE_TITRE + " TEXT,"
            + TABLE_CONTENU + " TEXT,"
            + TABLE_DATE + " TEXT,"
            + TABLE_HEURE + " TEXT,"
            + TABLE_IMAGE + " TEXT,"
            + TABLE_AUTEUR + " TEXT,"
            + TABLE_FOLDERIMAGE + " TEXT" + ")";

    private Context context;
    private myDataHelper mdgiciDb;
    private SQLiteDatabase db;

    public WeasDonneesLocal(Context context)
    {
        super(context,NOM_DONNEELOCAL, null, 1);
        mdgiciDb = new myDataHelper(context);
    }
    // Open the database connection.
    public WeasDonneesLocal open()
    {
        db = mdgiciDb.getWritableDatabase();
        return this;
    }
    /*@Override
    public void onCreate(SQLiteDatabase mdgiDb)
    {
        mdgiDb.execSQL("create table "+NOM_TABLE_COMMUNIQUES+"(COM_ID INTEGER PRIMARY KEY AUTOINCREMENT, COM_TITRE TEXT, COM_CONTENU TEXT, COM_DATE TEXT, COM_HEURE TEXT, COM_IMAGE TEXT, COM_AUTEUR TEXT, COM_FOLDERIMAGE TEXT)");
    }
    @Override
    public void onUpgrade(SQLiteDatabase mdgiDb, int oldVersion, int newVersion)
    {
        mdgiDb.execSQL("DROP TABLE IF EXISTS "+NOM_TABLE_COMMUNIQUES);
        onCreate(mdgiDb);
    }*/
    // obtient le nombres de communiques enregistres
    public long nombresCommuniquesLocal()
    {
        SQLiteDatabase mdgiDb = this.getWritableDatabase();
        long count = DatabaseUtils.queryNumEntries(mdgiDb, NOM_TABLE);
        mdgiDb.close();
        return count;
    }
    // efface les données
    public void effacerDonneesLocal()
    {
        SQLiteDatabase mdgiDb = this.getWritableDatabase();
        mdgiDb.delete(NOM_TABLE,null,null);
        mdgiDb.close();
        Log.e("dat cleared","toute les lignes effacées!");
    }
    // enregistre les communiques
    public boolean enregistreCommuniques(String comtitre, String comcontenu, String comdate, String comheure, String comimage, String comauteur, String comfolder)
    {
        SQLiteDatabase localdb = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(TABLE_TITRE,comtitre);
        values.put(TABLE_CONTENU,comcontenu);
        values.put(TABLE_DATE,comdate);
        values.put(TABLE_HEURE,comheure);
        values.put(TABLE_IMAGE,comimage);
        values.put(TABLE_AUTEUR,comauteur);
        values.put(TABLE_FOLDERIMAGE,comfolder);
        long result = localdb.insert(NOM_TABLE,null, values);
        if (result == -1)
        {
            return false;
        } else
            return true;
    }
    // lister les infos locals
    public Cursor afficherSlideCommuniques()
    {
        SQLiteDatabase mdgiDB = this.getWritableDatabase();
        Cursor cur = mdgiDB.rawQuery("select * from "+NOM_TABLE+String.valueOf(R.string.txt_query_slide_limit),null);
        return cur;
        /*String[] requete = {TABLE_ID,TABLE_TITRE,TABLE_CONTENU,TABLE_DATE,TABLE_HEURE,TABLE_IMAGE,TABLE_AUTEUR,TABLE_FOLDERIMAGE};
        Cursor cur = mdgiDb.query(NOM_TABLE_COMMUNIQUES,requete,null,null,null,null,null);
        return cur;*/
    }
    //
    public String[] afficheSlideCom1()
    {
        String[] result = new String[2];
        Cursor cur = db.rawQuery("select * from "+NOM_TABLE+String.valueOf(R.string.txt_query_slide_limit1),null);
        result[0] = cur.getString(1);
        result[1] = cur.getString(5);
        return result;
    }
    //
    public String[] afficheSlideCom2()
    {
        String[] result = new String[2];
        Cursor cur = db.rawQuery("select * from "+NOM_TABLE+String.valueOf(R.string.txt_query_slide_limit2),null);
        result[0] = cur.getString(1);
        result[1] = cur.getString(5);
        return result;
    }
    //
    public String[] afficheSlideCom3()
    {
        String[] result = new String[2];
        Cursor cur = db.rawQuery("select * from "+NOM_TABLE+String.valueOf(R.string.txt_query_slide_limit3),null);
        result[0] = cur.getString(1);
        result[1] = cur.getString(5);
        return result;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    // local constructor
    public static class myDataHelper extends SQLiteOpenHelper
    {
        myDataHelper(Context context)
        {
            super(context, NOM_DONNEELOCAL, null, DATA_VERSION);
        }
        @Override
        public void onCreate(SQLiteDatabase _db)
        {
            _db.execSQL(CREATION_TABLE);
        }
        @Override
        public void onUpgrade(SQLiteDatabase _db, int oldVersion, int newVersion)
        {
            // Destroy old database:
            _db.execSQL("DROP TABLE IF EXISTS " + NOM_TABLE);
            // Recreate new database:
            onCreate(_db);
        }
    }
}