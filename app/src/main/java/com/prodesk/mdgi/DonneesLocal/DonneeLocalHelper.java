package com.prodesk.mdgi.DonneesLocal;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import java.io.ByteArrayOutputStream;
//** Sam 17.08.2019 // 1.41pm **//
public class DonneeLocalHelper extends SQLiteOpenHelper
{
    public static final  String NOM_DONNEELOCAL = "Mdgici.db";public static final  String NOM_TABLE_COMMUNIQUES = "communiques_local";
    public static final  String TABLE_ID = "COM_ID";public static final  int DATA_VERSION = 1;
    public static final  String TABLE_TITRE = "COM_TITRE";public static final  String TABLE_CONTENU = "COM_CONTENU";
    public static final  String TABLE_DATE = "COM_DATE";public static final  String TABLE_HEURE = "COM_HEURE";
    public static final  String TABLE_IMAGE = "COM_IMAGE";public static final  String TABLE_AUTEUR = "COM_AUTEUR";
    public static final  String TABLE_FOLDERIMAGE = "COM_FOLDERIMAGE";public static final  String TABLE_IMAGEBIT = "COM_IMAGEBIT";
    public static final  String NOM_TABLE_DIRECTIONS = "direction_general";public static final  String TABLE_DIR_ID = "DIR_ID";
    public static final  String TABLE_DIR_TITRE = "DIR_TITRE";public static final  String TABLE_DIR_CONTACT = "DIR_CONTACT";
    public static final  String TABLE_DIR_ADRESSE = "DIR_ADRESSE";public static final  String TABLE_DIR_CATEGORIE = "DIR_CATEGORIE";
    public static final  String TABLE_DIR_LOCALITE = "DIR_LOCALITE";public static final  String TABLE_DIR_LOCALITE_PRINCIPALE = "DIR_LOCALITE_PRINCIPALE";public static final  String TABLE_DIR_CLASS = "DIR_CLASS";
    private static final String CREATION_TABLE_COMMUNIQUES = "CREATE TABLE " + NOM_TABLE_COMMUNIQUES + "("
        + TABLE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + TABLE_TITRE + " TEXT," + TABLE_CONTENU + " TEXT,"
        + TABLE_DATE + " TEXT," + TABLE_HEURE + " TEXT," + TABLE_IMAGE + " TEXT," + TABLE_AUTEUR + " TEXT,"
        + TABLE_FOLDERIMAGE + " TEXT," + TABLE_IMAGEBIT + " BLOB" + ")";
    private static final String CREATION_TABLE_DIRECTIONS = "CREATE TABLE " + NOM_TABLE_DIRECTIONS + "("
        + TABLE_DIR_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + TABLE_DIR_TITRE + " TEXT,"
        + TABLE_DIR_CONTACT + " TEXT," + TABLE_DIR_ADRESSE + " TEXT," + TABLE_DIR_CATEGORIE + " TEXT," + TABLE_DIR_LOCALITE + " TEXT,"
        + TABLE_DIR_LOCALITE_PRINCIPALE + " TEXT," + TABLE_DIR_CLASS + " TEXT" + ")";
    public DonneeLocalHelper(Context context)
    {
        super(context, NOM_DONNEELOCAL, null, DATA_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase mdgDb)
    {
        mdgDb.execSQL(CREATION_TABLE_COMMUNIQUES);mdgDb.execSQL(CREATION_TABLE_DIRECTIONS);
    }
    @Override
    public void onUpgrade(SQLiteDatabase mdgDb, int oldVersion, int newVersion)
    {
        mdgDb.execSQL("DROP TABLE IF EXISTS " + NOM_TABLE_COMMUNIQUES);mdgDb.execSQL("DROP TABLE IF EXISTS " + NOM_TABLE_DIRECTIONS);onCreate(mdgDb);
    }
    public long nombresCommuniquesLocal()
    {
        SQLiteDatabase mdgiDb = this.getWritableDatabase();long count = DatabaseUtils.queryNumEntries(mdgiDb, NOM_TABLE_COMMUNIQUES);
        mdgiDb.close();return count;
    }
    public long nombresDirectionsLocal()
    {
        long cnt = 0;SQLiteDatabase mdgiDb = this.getWritableDatabase();
        long count = DatabaseUtils.queryNumEntries(mdgiDb, NOM_TABLE_DIRECTIONS);
        mdgiDb.close();return count;
    }
    public void effacerCommuniquesLocal()
    {
        SQLiteDatabase mdgiDb = this.getWritableDatabase();mdgiDb.delete(NOM_TABLE_COMMUNIQUES,null,null);mdgiDb.close();
    }
    public void effacerDirectionsLocal()
    {
        SQLiteDatabase mdgiDb = this.getWritableDatabase();mdgiDb.delete(NOM_TABLE_DIRECTIONS,null,null);mdgiDb.close();
    }
    public void enregistreCommuniques(String comtitre, String comcontenu, String comdate, String comimage, Bitmap comimagebit)
    {
        ByteArrayOutputStream out = new ByteArrayOutputStream();comimagebit.compress(Bitmap.CompressFormat.PNG, 100, out);
        byte[] buff = out.toByteArray();SQLiteDatabase mdgiDb = this.getWritableDatabase();
        mdgiDb.beginTransaction();ContentValues values;
        try {
            values = new ContentValues();values.put(TABLE_TITRE,comtitre);
            values.put(TABLE_CONTENU,comcontenu);values.put(TABLE_DATE,comdate);
            values.put(TABLE_IMAGE,comimage);values.put(TABLE_IMAGEBIT,buff);
            long result = mdgiDb.insert(NOM_TABLE_COMMUNIQUES,null, values);
            mdgiDb.setTransactionSuccessful();
        } catch (SQLiteException se) {} finally {
            mdgiDb.endTransaction();mdgiDb.close();
        }
    }
    public void enregistreDirection(String dirtitre, String dircontact, String diradresse, String dircategorie, String dirlocalite, String dirlocaliteprincipale, String dirclassjava)
    {
        SQLiteDatabase mdgiDb = this.getWritableDatabase();mdgiDb.beginTransaction();
        ContentValues values;
        try {
            values = new ContentValues();values.put(TABLE_DIR_TITRE,dirtitre);
            values.put(TABLE_DIR_CONTACT,dircontact);values.put(TABLE_DIR_ADRESSE,diradresse);
            values.put(TABLE_DIR_CATEGORIE,dircategorie);values.put(TABLE_DIR_LOCALITE,dirlocalite);
            values.put(TABLE_DIR_LOCALITE_PRINCIPALE,dirlocaliteprincipale);values.put(TABLE_DIR_CLASS,dirclassjava);
            long result = mdgiDb.insert(NOM_TABLE_DIRECTIONS,null, values);mdgiDb.setTransactionSuccessful();
        } catch (SQLiteException se) {} finally {
            mdgiDb.endTransaction();mdgiDb.close();
        }
    }
    public String afficheSlideCom1()
    {
        SQLiteDatabase mdgiDb = this.getWritableDatabase();String result = "";
        String limit = " order by "+TABLE_ID+" desc LIMIT 1";Cursor cur = mdgiDb.rawQuery("select * from "+ NOM_TABLE_COMMUNIQUES +limit,null);
        while (cur.moveToNext()) {
            result = cur.getString(1);
        }return result;
    }
    public String afficheSlideCom2()
    {
        SQLiteDatabase mdgiDb = this.getWritableDatabase();String result = "";
        String limit = " order by "+TABLE_ID+" desc LIMIT 1,1";Cursor cur = mdgiDb.rawQuery("select * from "+ NOM_TABLE_COMMUNIQUES +limit,null);
        while (cur.moveToNext()) {
            result = cur.getString(1);
        }return result;
    }
    public String afficheSlideCom3()
    {
        SQLiteDatabase mdgiDb = this.getWritableDatabase();String result = "";String limit = " order by "+TABLE_ID+" desc LIMIT 2,1";
        Cursor cur = mdgiDb.rawQuery("select * from "+ NOM_TABLE_COMMUNIQUES +limit,null);
        while (cur.moveToNext()) {
            result = cur.getString(1);
        }return result;
    }
    public String afficheIdCom1()
    {
        SQLiteDatabase mdgiDb = this.getWritableDatabase();String result = "";
        String req = "select * from "+ NOM_TABLE_COMMUNIQUES +" order by "+TABLE_ID+" desc limit 1";Cursor cur = mdgiDb.rawQuery(req,null);
        while (cur.moveToNext()) {
            result = cur.getString(0);
        }return result;
    }
    public String afficheIdCom2()
    {
        SQLiteDatabase mdgiDb = this.getWritableDatabase();String result = "";
        String req = "select * from "+ NOM_TABLE_COMMUNIQUES +" order by "+TABLE_ID+" desc limit 1,1";Cursor cur = mdgiDb.rawQuery(req,null);
        while (cur.moveToNext()) {
            result = cur.getString(0);
        }return result;
    }
    public String afficheIdCom3()
    {
        SQLiteDatabase mdgiDb = this.getWritableDatabase();String result = "";
        String req = "select * from "+ NOM_TABLE_COMMUNIQUES +" order by "+TABLE_ID+" desc limit 2,1";Cursor cur = mdgiDb.rawQuery(req,null);
        while (cur.moveToNext()) {
            result = cur.getString(0);
        }return result;
    }
    public String afficheMainComTitre()
    {
        SQLiteDatabase mdgiDb = this.getWritableDatabase();String result = "";String req = "select * from "+ NOM_TABLE_COMMUNIQUES +" order by "+TABLE_ID+" desc limit 1";Cursor cur = mdgiDb.rawQuery(req,null);
        while (cur.moveToNext()) {
            result = cur.getString(1);
        }return result;
    }
    public String afficheMainComImage()
    {
        SQLiteDatabase mdgiDb = this.getWritableDatabase();String result = "";
        String req = "select * from "+ NOM_TABLE_COMMUNIQUES +" order by "+TABLE_ID+" desc limit 1";Cursor cur = mdgiDb.rawQuery(req,null);
        while (cur.moveToNext()) {
            result = cur.getString(5);
        }return result;
    }
    public String afficheMainComDate()
    {
        SQLiteDatabase mdgiDb = this.getWritableDatabase();String result = "";
        String req = "select * from "+ NOM_TABLE_COMMUNIQUES +" order by "+TABLE_ID+" desc limit 1";Cursor cur = mdgiDb.rawQuery(req,null);
        while (cur.moveToNext()) {
            result = cur.getString(3);
        }return result;
    }
    public String afficheMainComId()
    {
        SQLiteDatabase mdgiDb = this.getWritableDatabase();String result = "";
        String req = "select * from "+ NOM_TABLE_COMMUNIQUES +" order by "+TABLE_ID+" desc limit 1";Cursor cur = mdgiDb.rawQuery(req,null);
        while (cur.moveToNext())
        {
            result = cur.getString(0);
        }return result;
    }
    public String afficheMainComContenu()
    {
        SQLiteDatabase mdgiDb = this.getWritableDatabase();
        String result = "";String req = "select * from "+ NOM_TABLE_COMMUNIQUES +" order by "+TABLE_ID+" desc limit 1";Cursor cur = mdgiDb.rawQuery(req,null);
        while (cur.moveToNext())
        {
            result = cur.getString(2);
        }return result;
    }
    public Cursor afficheTotalCommuniques(String limit)
    {
        SQLiteDatabase mdgiDb = this.getWritableDatabase();String req = "select * from "+ NOM_TABLE_COMMUNIQUES +" order by "+TABLE_ID+" asc limit 1,"+limit  ;Cursor cur = mdgiDb.rawQuery(req,null);return cur;
    }
    public Cursor afficheTotalDirections(String limit)
    {
        SQLiteDatabase mdgiDb = this.getWritableDatabase();String req = "select * from "+ NOM_TABLE_DIRECTIONS +" limit 1,"+limit;Cursor cur = mdgiDb.rawQuery(req,null);return cur;
    }
    public Bitmap afficheImagesHorsLigne(int id)
    {
        Bitmap imagebit = null;SQLiteDatabase mdgiDb = this.getReadableDatabase();
        mdgiDb.beginTransaction();
        try {
            String req = "select * from "+ NOM_TABLE_COMMUNIQUES +" where "+TABLE_ID+" = "+id;Cursor cur = mdgiDb.rawQuery(req,null);
            if (cur.getCount() > 0) {
                while (cur.moveToNext()) {
                    byte[] blob = cur.getBlob(cur.getColumnIndex(TABLE_IMAGEBIT));imagebit = BitmapFactory.decodeByteArray(blob,0,blob.length);
                }
            }
            mdgiDb.setTransactionSuccessful();
        } catch (SQLiteException se) { } finally {
            mdgiDb.endTransaction();mdgiDb.close();
        }return imagebit;
    }
}