package com.prodesk.mdgi;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.prodesk.mdgi.DirectionsCentrales.DirCentraleMap;

//** Jeud 05.09.2019 // 8.58pm **//
public class DirCentrale extends AppCompatActivity
{
    String txt_dc_abrev, txt_dc_titre, txt_dc_contact1, txt_dc_contact2, txt_dc_nbcontact, txt_dc_adresse, txt_dc_email, txt_dc_lat, txt_dc_long, txt_contacts;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);setContentView(R.layout.activity_dircentrale);
        Toolbar toolbar = (Toolbar) findViewById(R.id.dircentrale_toolbar_read);
        Intent intDircentrale = getIntent();
        txt_dc_abrev = intDircentrale.getStringExtra("dataDcAbrev");
        txt_dc_titre = intDircentrale.getStringExtra("dataDcTitre");
        txt_dc_contact1 = intDircentrale.getStringExtra("dataDcContact1");
        txt_dc_contact2 = intDircentrale.getStringExtra("dataDcContact2");
        txt_dc_nbcontact = intDircentrale.getStringExtra("dataDcNbContact");
        txt_dc_adresse = intDircentrale.getStringExtra("dataDcAdresse");
        txt_dc_email = intDircentrale.getStringExtra("dataDcEmail");
        txt_dc_lat = intDircentrale.getStringExtra("dataDcLat");
        txt_dc_long = intDircentrale.getStringExtra("dataDcLong");
        TextView txt_abrev = (TextView)findViewById(R.id.dircentrale_toolbar_title);
        TextView txt_title = (TextView)findViewById(R.id.dircentrale_title);
        TextView txt_contact = (TextView)findViewById(R.id.dircentrale_contact);
        TextView txt_adresse = (TextView)findViewById(R.id.dircentrale_adresse);
        txt_abrev.setText(txt_dc_abrev);
        txt_title.setText(txt_dc_titre);
        if (txt_dc_nbcontact == "2") {
            txt_contacts = txt_dc_contact1+" / "+txt_dc_contact2;
        } else {
            txt_contacts = txt_dc_contact1;
        }
        txt_contact.setText(txt_contacts);
        txt_adresse.setText(txt_dc_adresse);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        Button btn_envoi_suggest = (Button) findViewById(R.id.dircentrale_ecrire_mail);
        btn_envoi_suggest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ecrireDirCentrale(txt_dc_titre,"DIRECTION CENTRALE", txt_dc_email);
            }
        });
        LinearLayout goback = (LinearLayout) findViewById(R.id.dircentrale_retour);
        goback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        FragmentManager fragmentManager = getSupportFragmentManager();FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Fragment fragment = DirCentraleMap.newInstance(txt_dc_lat, txt_dc_long, txt_dc_titre);
        fragmentTransaction.add(R.id.dircentrale_map_content, fragment);fragmentTransaction.commit();
    }
    public void ecrireDirCentrale(String title, String categorie, String destinataire)
    {
        Intent intEcrireAuDr = new Intent(DirCentrale.this, ContactEmail.class);
        intEcrireAuDr.putExtra("dataContactTitle", title);intEcrireAuDr.putExtra("dataContactCategorie", categorie);
        intEcrireAuDr.putExtra("dataContactDestinataire", destinataire);startActivity(intEcrireAuDr);
    }
}