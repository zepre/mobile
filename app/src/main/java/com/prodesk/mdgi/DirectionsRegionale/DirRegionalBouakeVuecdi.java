package com.prodesk.mdgi.DirectionsRegionale;
import android.Manifest;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;
import com.prodesk.mdgi.ContactEmail;
import com.prodesk.mdgi.R;
//** Jeud 05.09.2019 // 10.18pm **//
public class DirRegionalBouakeVuecdi extends Fragment
{
    public String dr_bouake_num;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_dr_bouake_vuecdi, container, false);Button call_dr_bouake = (Button)view.findViewById(R.id.dr_bouake_call);
        call_dr_bouake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_bouake_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_bouake_num = getString(R.string.txt_dr_bouake_contact);apellerDrBouake(dr_bouake_num);
                }
            }
        });
        ImageView call_dr_bouake_cdi_bouakei = (ImageView)view.findViewById(R.id.dr_bouake_cdi_bouakei_call);
        call_dr_bouake_cdi_bouakei.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_bouake_cdi_bouakei_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_bouake_num = getString(R.string.txt_dr_bouake_cdi_bouakei_contact);apellerDrBouake(dr_bouake_num);
                }
            }
        });
        ImageView call_dr_bouake_cdi_bouakeii = (ImageView)view.findViewById(R.id.dr_bouake_cdi_bouakeii_call);
        call_dr_bouake_cdi_bouakeii.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_bouake_cdi_bouakeii_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_bouake_num = getString(R.string.txt_dr_bouake_cdi_bouakeii_contact);apellerDrBouake(dr_bouake_num);
                }
            }
        });
        ImageView call_dr_bouake_cdi_beoumi = (ImageView)view.findViewById(R.id.dr_bouake_cdi_beoumi_call);
        call_dr_bouake_cdi_beoumi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_bouake_cdi_beoumi_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_bouake_num = getString(R.string.txt_dr_bouake_cdi_beoumi_contact);apellerDrBouake(dr_bouake_num);
                }
            }
        });
        ImageView call_dr_bouake_cdi_dabakala = (ImageView)view.findViewById(R.id.dr_bouake_cdi_dabakala_call);
        call_dr_bouake_cdi_dabakala.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_bouake_cdi_dabakala_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_bouake_num = getString(R.string.txt_dr_bouake_cdi_dabakala_contact);apellerDrBouake(dr_bouake_num);
                }
            }
        });
        ImageView call_dr_bouake_cdi_katiola = (ImageView)view.findViewById(R.id.dr_bouake_cdi_katiola_call);
        call_dr_bouake_cdi_katiola.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_bouake_cdi_katiola_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_bouake_num = getString(R.string.txt_dr_bouake_cdi_katiola_contact);apellerDrBouake(dr_bouake_num);
                }
            }
        });
        Button envoi_mail_dr_bouake = (Button)view.findViewById(R.id.dr_bouake_message);
        envoi_mail_dr_bouake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                ecrireDirGenerale(getString(R.string.txt_dr_bouake_titre),"DIRECTION REGIONALE");
            }
        });return view;
    }
    public void apellerDrBouake(String dr_bouake_appel)
    {
        try {
            if(Build.VERSION.SDK_INT > 22) {
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE}, 101);return;
                }
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + dr_bouake_appel));startActivity(callIntent);
            } else {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + dr_bouake_appel));startActivity(callIntent);
            }
        } catch (Exception ex) {}
    }
    public void ecrireDirGenerale(String title, String categorie)
    {
        Intent intEcrireAuDr = new Intent(getContext(), ContactEmail.class);intEcrireAuDr.putExtra("dataContactTitle", title);intEcrireAuDr.putExtra("dataContactCategorie", categorie);
        intEcrireAuDr.putExtra("dataContactDestinataire", getString(R.string.txt_contact_destinataire_bouake));startActivity(intEcrireAuDr);
    }
}