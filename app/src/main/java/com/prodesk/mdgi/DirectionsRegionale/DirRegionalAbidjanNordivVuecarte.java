package com.prodesk.mdgi.DirectionsRegionale;
import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Color;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.prodesk.mdgi.R;
//** Sam 17.08.2019 // 12.36pm **//
public class DirRegionalAbidjanNordivVuecarte extends Fragment implements OnMapReadyCallback
{
    Double mapLat,mapLong;GoogleMap drAbidjanNordivMap;MapView drAbidjanNordivMapView;MarkerOptions cdiYopougoni;LocationManager locationManager;
    boolean statusOfGPS;FragmentManager fragmentManager;SupportMapFragment mFragment;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_dr_abidjannordiv_vuecarte, container, false);
        if (!isGooglePlayServicesAvailable()) {
            Toast.makeText(getActivity(), "Service Google Play inaccessible !", Toast.LENGTH_SHORT).show();
        }
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        statusOfGPS = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        return view;
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        drAbidjanNordivMapView = (MapView)view.findViewById(R.id.dr_abidjannordiv_map);
        if (drAbidjanNordivMapView != null) {
            drAbidjanNordivMapView.onCreate(null);drAbidjanNordivMapView.onResume();
            drAbidjanNordivMapView.getMapAsync(this);
        }
    }
    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        mapLat = Double.parseDouble(getString(R.string.txt_dr_abidjannordiv_lat));mapLong = Double.parseDouble(getString(R.string.txt_dr_abidjannordiv_long));
        MapsInitializer.initialize(getContext());drAbidjanNordivMap = googleMap;
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.getUiSettings().setZoomControlsEnabled(true);googleMap.getUiSettings().setZoomGesturesEnabled(true);googleMap.getUiSettings().setCompassEnabled(true);
        LatLng ptDrAbNiv = new LatLng(mapLat,mapLong);
        LatLng ptcdiYopougon = new LatLng(Double.parseDouble(getString(R.string.txt_dr_abidjannordiv_cdi_yopougon_lat)),Double.parseDouble(getString(R.string.txt_dr_abidjannordiv_cdi_yopougon_long)));
        cdiYopougoni = new MarkerOptions();cdiYopougoni.position(ptcdiYopougon).icon(setIconColor("#c86331"));cdiYopougoni.title(getString(R.string.txt_dr_abidjannordiv_cdi_yopougon_title));
        cdiYopougoni.snippet(getString(R.string.txt_dr_abidjannordiv_cdi_yopougoni_adresse));googleMap.addMarker(cdiYopougoni);
        CameraPosition drAbNivPos = CameraPosition.builder().target(ptDrAbNiv).zoom(13).bearing(0).tilt(45).build();
        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(drAbNivPos));
    }
    private boolean isGooglePlayServicesAvailable()
    {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            GooglePlayServicesUtil.getErrorDialog(status, getActivity(), 0).show();
            return false;
        }
    }
    public BitmapDescriptor setIconColor(String kol)
    {
        float[] hsv = new float[3];Color.colorToHSV(Color.parseColor(kol), hsv);
        return BitmapDescriptorFactory.defaultMarker(hsv[0]);
    }
}