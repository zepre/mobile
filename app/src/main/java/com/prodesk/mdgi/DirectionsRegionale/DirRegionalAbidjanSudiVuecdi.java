package com.prodesk.mdgi.DirectionsRegionale;
import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;
import com.prodesk.mdgi.ContactEmail;
import com.prodesk.mdgi.R;
//** Jeud 05.09.2019 // 10.07pm **//
public class DirRegionalAbidjanSudiVuecdi extends Fragment
{
    String dr_abidjansudi_num;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_dr_abidjansudi_vuecdi, container, false);
        Button call_dr_abidjansudi = (Button)view.findViewById(R.id.dr_abidjansudi_call);
        call_dr_abidjansudi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                dr_abidjansudi_num = getString(R.string.txt_dr_abidjansudi_contact);apellerDrAbidjanSudi(dr_abidjansudi_num);
            }
        });
        ImageView call_dr_abidjansudi_cdi_portbouetvridi = (ImageView)view.findViewById(R.id.dr_abidjansudi_cdi_portbouetvridi_call);
        call_dr_abidjansudi_cdi_portbouetvridi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_abidjansudi_cdi_portbouetvridi_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_abidjansudi_num = getString(R.string.txt_dr_abidjansudi_cdi_portbouetvridi_contact);apellerDrAbidjanSudi(dr_abidjansudi_num);
                }
            }
        });
        ImageView call_dr_abidjansudi_treichvillei = (ImageView)view.findViewById(R.id.dr_abidjansudi_cdi_treichvillei_call);
        call_dr_abidjansudi_treichvillei.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_abidjansudi_cdi_treichvillei_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    AlertDialog.Builder buildCall = new AlertDialog.Builder(getContext());
                    buildCall.setMessage("Veuillez choisir le numéro à appeler !");
                    buildCall.setCancelable(true);
                    buildCall.setPositiveButton(
                            getString(R.string.txt_dr_abidjansudi_cdi_treichvillei_contact1),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dr_abidjansudi_num = getString(R.string.txt_dr_abidjansudi_cdi_treichvillei_contact1);apellerDrAbidjanSudi(dr_abidjansudi_num);
                                }
                            });

                    buildCall.setNegativeButton(
                            getString(R.string.txt_dr_abidjansudi_cdi_treichvillei_contact2),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dr_abidjansudi_num = getString(R.string.txt_dr_abidjansudi_cdi_treichvillei_contact2);apellerDrAbidjanSudi(dr_abidjansudi_num);
                                }
                            });
                    AlertDialog alert11 = buildCall.create();alert11.show();
                }
            }
        });
        ImageView call_dr_abidjansudi_treichvilleii = (ImageView)view.findViewById(R.id.dr_abidjansudi_cdi_treichvilleii_call);
        call_dr_abidjansudi_treichvilleii.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_abidjansudi_cdi_treichvilleii_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    AlertDialog.Builder buildCall = new AlertDialog.Builder(getContext());
                    buildCall.setMessage("Veuillez choisir le numéro à appeler !");
                    buildCall.setCancelable(true);
                    buildCall.setPositiveButton(
                            getString(R.string.txt_dr_abidjansudi_cdi_treichvilleii_contact1),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dr_abidjansudi_num = getString(R.string.txt_dr_abidjansudi_cdi_treichvilleii_contact1);apellerDrAbidjanSudi(dr_abidjansudi_num);
                                }
                            });

                    buildCall.setNegativeButton(
                            getString(R.string.txt_dr_abidjansudi_cdi_treichvilleii_contact2),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dr_abidjansudi_num = getString(R.string.txt_dr_abidjansudi_cdi_treichvilleii_contact2);apellerDrAbidjanSudi(dr_abidjansudi_num);
                                }
                            });
                    AlertDialog alert11 = buildCall.create();alert11.show();
                }
            }
        });
        Button envoi_mail_dr_abidjansudi = (Button)view.findViewById(R.id.dr_abidjansudi_message);
        envoi_mail_dr_abidjansudi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                ecrireDirGenerale(getString(R.string.txt_dr_abidjansudi_titre),"DIRECTION REGIONALE");
            }
        });
        return view;
    }
    public void apellerDrAbidjanSudi(String dr_abidjansudi_appel)
    {
        try {
            if(Build.VERSION.SDK_INT > 22) {
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE}, 101);return;
                }
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + dr_abidjansudi_appel));startActivity(callIntent);
            } else {
                Intent callIntent = new Intent(Intent.ACTION_CALL);callIntent.setData(Uri.parse("tel:" + dr_abidjansudi_appel));startActivity(callIntent);
            }
        } catch (Exception ex) {}
    }
    public void ecrireDirGenerale(String title, String categorie)
    {
        Intent intEcrireAuDr = new Intent(getContext(), ContactEmail.class);intEcrireAuDr.putExtra("dataContactTitle", title);
        intEcrireAuDr.putExtra("dataContactCategorie", categorie);intEcrireAuDr.putExtra("dataContactDestinataire", getString(R.string.txt_contact_destinataire_dras1));startActivity(intEcrireAuDr);
    }
}