package com.prodesk.mdgi.DirectionsRegionale;
import android.app.FragmentManager;import android.content.Context;
import android.graphics.Color;import android.location.LocationManager;
import android.os.Bundle;import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;import android.view.LayoutInflater;
import android.view.View;import android.view.ViewGroup;
import android.widget.Toast;import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;import com.google.android.gms.maps.model.MarkerOptions;import com.prodesk.mdgi.R;
//** Sam 17.08.2019 // 1.35pm **//
public class DirRegionalOdieneVuecarte extends Fragment implements OnMapReadyCallback
{
    Double mapLat,mapLong;GoogleMap drOdieneMap;MapView drOdieneMapView;MarkerOptions cdiOdiene, cdiTouba;LocationManager locationManager;boolean statusOfGPS;
    FragmentManager fragmentManager;SupportMapFragment mFragment;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_dr_odiene_vuecarte, container, false);
        if (!isGooglePlayServicesAvailable()) {
            Toast.makeText(getActivity(), "Service Google Play inaccessible !", Toast.LENGTH_SHORT).show();
        }
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        statusOfGPS = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);return view;
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        drOdieneMapView = (MapView)view.findViewById(R.id.dr_odiene_map);
        if (drOdieneMapView != null) {
            drOdieneMapView.onCreate(null);drOdieneMapView.onResume();
            drOdieneMapView.getMapAsync(this);
        }
    }
    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        mapLat = Double.parseDouble(getString(R.string.txt_dr_odiene_point_lat));mapLong = Double.parseDouble(getString(R.string.txt_dr_odiene_point_long));MapsInitializer.initialize(getContext());
        drOdieneMap = googleMap;
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.getUiSettings().setZoomGesturesEnabled(true);googleMap.getUiSettings().setCompassEnabled(true);
        LatLng ptDrOdiene = new LatLng(mapLat,mapLong);
        LatLng ptcdiOdiene = new LatLng(Double.parseDouble(getString(R.string.txt_dr_odiene_cdi_odiene_lat)),Double.parseDouble(getString(R.string.txt_dr_odiene_cdi_odiene_long)));
        cdiOdiene = new MarkerOptions();cdiOdiene.position(ptcdiOdiene).icon(setIconColor("#c86331"));
        cdiOdiene.title(getString(R.string.txt_dr_odiene_cdi_odiene_titre));cdiOdiene.snippet(getString(R.string.txt_dr_odiene_cdi_odiene_adresse));googleMap.addMarker(cdiOdiene);
        LatLng ptcdiTouba = new LatLng(Double.parseDouble(getString(R.string.txt_dr_odiene_cdi_touba_lat)),Double.parseDouble(getString(R.string.txt_dr_odiene_cdi_touba_long)));
        cdiTouba = new MarkerOptions();cdiTouba.position(ptcdiTouba).icon(setIconColor("#c86331"));
        cdiTouba.title(getString(R.string.txt_dr_odiene_cdi_touba_titre));cdiTouba.snippet(getString(R.string.txt_dr_odiene_cdi_touba_adresse));googleMap.addMarker(cdiTouba);
        CameraPosition drOdienePos = CameraPosition.builder().target(ptDrOdiene).zoom(8).bearing(0).tilt(45).build();googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(drOdienePos));
    }
    private boolean isGooglePlayServicesAvailable()
    {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            GooglePlayServicesUtil.getErrorDialog(status, getActivity(), 0).show();return false;
        }
    }
    public BitmapDescriptor setIconColor(String kol)
    {
        float[] hsv = new float[3];Color.colorToHSV(Color.parseColor(kol), hsv);
        return BitmapDescriptorFactory.defaultMarker(hsv[0]);
    }
}