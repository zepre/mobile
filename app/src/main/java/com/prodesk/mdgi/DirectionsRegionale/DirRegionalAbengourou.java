package com.prodesk.mdgi.DirectionsRegionale;
import android.content.pm.ActivityInfo;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import com.prodesk.mdgi.R;
//** Sam 17.08.2019 // 12.19pm **//
public class DirRegionalAbengourou extends AppCompatActivity
{
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener()
    {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item)
        {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            switch (item.getItemId())
            {
                case R.id.nav_directionsregionales_vuecdi:
                    transaction.replace(R.id.frame_dr_abengourou, new DirRegionalAbengourouVuecdi()).commit();
                return true;
                case R.id.nav_directionsregionales_vuecarte:
                    transaction.replace(R.id.frame_dr_abengourou, new DirRegionalAbengourouVuecarte()).commit();
                return true;
            }
            return false;
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dr_abengourou);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.nav_dr_abengourou);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.frame_dr_abengourou, new DirRegionalAbengourouVuecdi()).commit();
    }
}