package com.prodesk.mdgi.DirectionsRegionale;
import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Color;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.prodesk.mdgi.R;
//** Sam 17.08.2019 // 12.29pm **//
public class DirRegionalAbidjanNordiiVuecarte extends Fragment implements OnMapReadyCallback
{
    Double mapLat,mapLong;GoogleMap drAbidjanNordiiMap;MapView drAbidjanNordiiMapView;
    MarkerOptions cdiAboboi, cdiAlepe, cdiAnyama;LocationManager locationManager;
    boolean statusOfGPS;FragmentManager fragmentManager;SupportMapFragment mFragment;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_dr_abidjannordii_vuecarte, container, false);
        if (!isGooglePlayServicesAvailable()) {
            Toast.makeText(getActivity(), "Service Google Play inaccessible !", Toast.LENGTH_SHORT).show();
        }
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        statusOfGPS = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        return view;
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        drAbidjanNordiiMapView = (MapView)view.findViewById(R.id.dr_abidjannordii_map);
        if (drAbidjanNordiiMapView != null) {
            drAbidjanNordiiMapView.onCreate(null);drAbidjanNordiiMapView.onResume();
            drAbidjanNordiiMapView.getMapAsync(this);
        }
    }
    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        mapLat = Double.parseDouble(getString(R.string.txt_dr_abidjannordii_point_lat));mapLong = Double.parseDouble(getString(R.string.txt_dr_abidjannordii_point_long));MapsInitializer.initialize(getContext());
        drAbidjanNordiiMap = googleMap;
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.getUiSettings().setZoomControlsEnabled(true);googleMap.getUiSettings().setZoomGesturesEnabled(true);googleMap.getUiSettings().setCompassEnabled(true);
        LatLng ptDrAbNii = new LatLng(mapLat,mapLong);
        LatLng ptcdiAboboi = new LatLng(Double.parseDouble(getString(R.string.txt_dr_abidjannordii_cdi_aboboi_lat)),Double.parseDouble(getString(R.string.txt_dr_abidjannordii_cdi_aboboi_long)));
        cdiAboboi = new MarkerOptions();
        cdiAboboi.position(ptcdiAboboi).icon(setIconColor("#c86331"));cdiAboboi.title(getString(R.string.txt_dr_abidjannordii_cdi_abobo_title));
        cdiAboboi.snippet(getString(R.string.txt_dr_abidjannordii_cdi_aboboi_adresse));googleMap.addMarker(cdiAboboi);
        LatLng ptcdiAlepe = new LatLng(Double.parseDouble(getString(R.string.txt_dr_abidjannordii_cdi_alepe_lat)),Double.parseDouble(getString(R.string.txt_dr_abidjannordii_cdi_alepe_long)));
        cdiAlepe = new MarkerOptions();
        cdiAlepe.position(ptcdiAlepe).icon(setIconColor("#c86331"));cdiAlepe.title(getString(R.string.txt_dr_abidjannordii_cdi_alepe_titre));
        cdiAlepe.snippet(getString(R.string.txt_dr_abidjannordii_cdi_alepe_adresse));googleMap.addMarker(cdiAlepe);
        LatLng ptcdiAnyama = new LatLng(Double.parseDouble(getString(R.string.txt_dr_abidjannordii_cdi_anyama_lat)),Double.parseDouble(getString(R.string.txt_dr_abidjannordii_cdi_anyama_long)));
        cdiAnyama = new MarkerOptions();
        cdiAnyama.position(ptcdiAnyama).icon(setIconColor("#c86331"));cdiAnyama.title(getString(R.string.txt_dr_abidjannordii_cdi_anyama_titre));
        cdiAnyama.snippet(getString(R.string.txt_dr_abidjannordii_cdi_anyama_adresse));googleMap.addMarker(cdiAnyama);
        CameraPosition drAbNiiPos = CameraPosition.builder().target(ptDrAbNii).zoom(9).bearing(0).tilt(45).build();
        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(drAbNiiPos));
    }
    private boolean isGooglePlayServicesAvailable()
    {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            GooglePlayServicesUtil.getErrorDialog(status, getActivity(), 0).show();
            return false;
        }
    }
    public BitmapDescriptor setIconColor(String kol)
    {
        float[] hsv = new float[3];Color.colorToHSV(Color.parseColor(kol), hsv);
        return BitmapDescriptorFactory.defaultMarker(hsv[0]);
    }
}