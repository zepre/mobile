package com.prodesk.mdgi.DirectionsRegionale;
import android.app.FragmentManager;import android.content.Context;
import android.graphics.Color;import android.location.LocationManager;
import android.os.Bundle;import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;import android.view.LayoutInflater;
import android.view.View;import android.view.ViewGroup;
import android.widget.Toast;import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;import com.google.android.gms.maps.model.MarkerOptions;import com.prodesk.mdgi.R;
//** Sam 17.08.2019 // 1.39pm **//
public class DirRegionalYamoussoukroVuecarte extends Fragment implements OnMapReadyCallback
{
    Double mapLat,mapLong;GoogleMap drYamoussoukroMap;MapView drYamoussoukroMapView;MarkerOptions cdiBonon, cdiBouafle, cdiSinfra, cdiTiebissou, cdiToumodi, cdiYamoussoukro, cdiZuenoula;
    LocationManager locationManager;boolean statusOfGPS;FragmentManager fragmentManager;SupportMapFragment mFragment;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_dr_yamoussoukro_vuecarte, container, false);
        if (!isGooglePlayServicesAvailable()) {
            Toast.makeText(getActivity(), "Service Google Play inaccessible !", Toast.LENGTH_SHORT).show();
        }
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        statusOfGPS = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);return view;
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        drYamoussoukroMapView = (MapView)view.findViewById(R.id.dr_yamoussoukro_map);
        if (drYamoussoukroMapView != null) {
            drYamoussoukroMapView.onCreate(null);drYamoussoukroMapView.onResume();
            drYamoussoukroMapView.getMapAsync(this);
        }
    }
    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        mapLat = Double.parseDouble(getString(R.string.txt_dr_yamoussoukro_lat));mapLong = Double.parseDouble(getString(R.string.txt_dr_yamoussoukro_long));MapsInitializer.initialize(getContext());
        drYamoussoukroMap = googleMap;
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.getUiSettings().setZoomGesturesEnabled(true);googleMap.getUiSettings().setCompassEnabled(true);
        LatLng ptDrYamoussoukro  = new LatLng(mapLat,mapLong);
        LatLng ptcdiBonon = new LatLng(Double.parseDouble(getString(R.string.txt_dr_yamoussoukro_cdi_bonon_lat)),Double.parseDouble(getString(R.string.txt_dr_yamoussoukro_cdi_bonon_long)));
        cdiBonon = new MarkerOptions();cdiBonon.position(ptcdiBonon).icon(setIconColor("#c86331"));
        cdiBonon.title(getString(R.string.txt_dr_yamoussoukro_cdi_bonon_titre));cdiBonon.snippet(getString(R.string.txt_dr_yamoussoukro_cdi_bonon_adresse));googleMap.addMarker(cdiBonon);
        LatLng ptcdiBouafle = new LatLng(Double.parseDouble(getString(R.string.txt_dr_yamoussoukro_cdi_bouafle_lat)),Double.parseDouble(getString(R.string.txt_dr_yamoussoukro_cdi_bouafle_long)));
        cdiBouafle = new MarkerOptions();cdiBouafle.position(ptcdiBouafle).icon(setIconColor("#c86331"));
        cdiBouafle.title(getString(R.string.txt_dr_yamoussoukro_cdi_bouafle_titre));cdiBouafle.snippet(getString(R.string.txt_dr_yamoussoukro_cdi_bouafle_adresse));googleMap.addMarker(cdiBouafle);
        LatLng ptcdiSinfra = new LatLng(Double.parseDouble(getString(R.string.txt_dr_yamoussoukro_cdi_sinfra_lat)),Double.parseDouble(getString(R.string.txt_dr_yamoussoukro_cdi_sinfra_long)));
        cdiSinfra = new MarkerOptions();cdiSinfra.position(ptcdiSinfra).icon(setIconColor("#c86331"));
        cdiSinfra.title(getString(R.string.txt_dr_yamoussoukro_cdi_sinfra_titre));cdiSinfra.snippet(getString(R.string.txt_dr_yamoussoukro_cdi_sinfra_adresse));googleMap.addMarker(cdiSinfra);
        LatLng ptcdiTiebissou = new LatLng(Double.parseDouble(getString(R.string.txt_dr_yamoussoukro_cdi_tiebissou_lat)),Double.parseDouble(getString(R.string.txt_dr_yamoussoukro_cdi_tiebissou_long)));
        cdiTiebissou = new MarkerOptions();cdiTiebissou.position(ptcdiTiebissou).icon(setIconColor("#c86331"));
        cdiTiebissou.title(getString(R.string.txt_dr_yamoussoukro_cdi_tiebissou_titre));cdiTiebissou.snippet(getString(R.string.txt_dr_yamoussoukro_cdi_tiebissou_adresse));googleMap.addMarker(cdiTiebissou);
        LatLng ptcdiToumodi = new LatLng(Double.parseDouble(getString(R.string.txt_dr_yamoussoukro_cdi_toumodi_lat)),Double.parseDouble(getString(R.string.txt_dr_yamoussoukro_cdi_toumodi_long)));
        cdiToumodi = new MarkerOptions();cdiToumodi.position(ptcdiToumodi).icon(setIconColor("#c86331"));
        cdiToumodi.title(getString(R.string.txt_dr_yamoussoukro_cdi_toumodi_titre));cdiToumodi.snippet(getString(R.string.txt_dr_yamoussoukro_cdi_toumodi_adresse));googleMap.addMarker(cdiToumodi);
        LatLng ptcdiYamoussoukro = new LatLng(Double.parseDouble(getString(R.string.txt_dr_yamoussoukro_cdi_yamoussoukro_lat)),Double.parseDouble(getString(R.string.txt_dr_yamoussoukro_cdi_yamoussoukro_long)));
        cdiYamoussoukro = new MarkerOptions();cdiYamoussoukro.position(ptcdiYamoussoukro).icon(setIconColor("#c86331"));
        cdiYamoussoukro.title(getString(R.string.txt_dr_yamoussoukro_cdi_yamoussoukro_titre));cdiYamoussoukro.snippet(getString(R.string.txt_dr_yamoussoukro_cdi_yamoussoukro_adresse));googleMap.addMarker(cdiYamoussoukro);
        LatLng ptcdiZuenoula = new LatLng(Double.parseDouble(getString(R.string.txt_dr_yamoussoukro_cdi_zuenoula_lat)),Double.parseDouble(getString(R.string.txt_dr_yamoussoukro_cdi_zuenoula_long)));
        cdiZuenoula = new MarkerOptions();cdiZuenoula.position(ptcdiZuenoula).icon(setIconColor("#c86331"));
        cdiZuenoula.title(getString(R.string.txt_dr_yamoussoukro_cdi_zuenoula_titre));cdiZuenoula.snippet(getString(R.string.txt_dr_yamoussoukro_cdi_zuenoula_adresse));googleMap.addMarker(cdiZuenoula);
        CameraPosition drYamoussoukroPos = CameraPosition.builder().target(ptDrYamoussoukro).zoom(8).bearing(0).tilt(45).build();googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(drYamoussoukroPos));
    }
    private boolean isGooglePlayServicesAvailable()
    {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            GooglePlayServicesUtil.getErrorDialog(status, getActivity(), 0).show();return false;
        }
    }
    public BitmapDescriptor setIconColor(String kol)
    {
        float[] hsv = new float[3];Color.colorToHSV(Color.parseColor(kol), hsv);
        return BitmapDescriptorFactory.defaultMarker(hsv[0]);
    }
}