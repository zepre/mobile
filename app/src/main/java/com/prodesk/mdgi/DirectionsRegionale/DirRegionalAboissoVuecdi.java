package com.prodesk.mdgi.DirectionsRegionale;
import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;
import com.prodesk.mdgi.ContactEmail;
import com.prodesk.mdgi.R;
//** Jeud 05.09.2019 // 10.09pm **//
public class DirRegionalAboissoVuecdi extends Fragment
{
    String dr_aboisso_num;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_dr_aboisso_vuecdi, container, false);
        Button call_dr_aboisso = (Button)view.findViewById(R.id.dr_aboisso_call);
        call_dr_aboisso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                dr_aboisso_num = getString(R.string.txt_dr_aboisso_contact);apellerDrAboisso(dr_aboisso_num);
            }
        });
        ImageView call_dr_aboisso_cdi_bonoua = (ImageView)view.findViewById(R.id.dr_aboisso_cdi_bonoua_call);
        call_dr_aboisso_cdi_bonoua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_aboisso_cdi_bonoua_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_aboisso_num = getString(R.string.txt_dr_aboisso_cdi_bonoua_contact);apellerDrAboisso(dr_aboisso_num);
                }
            }
        });
        ImageView call_dr_aboisso_cdi_aboisso = (ImageView)view.findViewById(R.id.dr_aboisso_cdi_aboisso_call);
        call_dr_aboisso_cdi_aboisso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_aboisso_cdi_aboisso_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_aboisso_num = getString(R.string.txt_dr_aboisso_cdi_aboisso_contact);apellerDrAboisso(dr_aboisso_num);
                }
            }
        });
        ImageView call_dr_aboisso_cdi_adiake = (ImageView)view.findViewById(R.id.dr_aboisso_cdi_adiake_call);
        call_dr_aboisso_cdi_adiake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_aboisso_cdi_adiake_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_aboisso_num = getString(R.string.txt_dr_aboisso_cdi_adiake_contact);apellerDrAboisso(dr_aboisso_num);
                }
            }
        });
        ImageView call_dr_aboisso_cdi_grandbassam = (ImageView)view.findViewById(R.id.dr_aboisso_cdi_grandbassam_call);
        call_dr_aboisso_cdi_grandbassam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_aboisso_cdi_grandbassam_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_aboisso_num = getString(R.string.txt_dr_aboisso_cdi_grandbassam_contact);apellerDrAboisso(dr_aboisso_num);
                }
            }
        });
        ImageView call_dr_aboisso_cdi_tiapoum = (ImageView)view.findViewById(R.id.dr_aboisso_cdi_tiapoum_call);
        call_dr_aboisso_cdi_tiapoum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_aboisso_cdi_tiapoum_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_aboisso_num = getString(R.string.txt_dr_aboisso_cdi_tiapoum_contact);apellerDrAboisso(dr_aboisso_num);
                }
            }
        });
        Button envoi_mail_dr_aboisso = (Button)view.findViewById(R.id.dr_aboisso_message);
        envoi_mail_dr_aboisso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                ecrireDirGenerale(getString(R.string.txt_dr_aboisso_titre),"DIRECTION REGIONALE");
            }
        });
        return view;
    }
    public void apellerDrAboisso(String dr_aboisso_appel)
    {
        try {
            if(Build.VERSION.SDK_INT > 22) {
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE}, 101);
                    return;
                }
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + dr_aboisso_appel));startActivity(callIntent);
            } else {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + dr_aboisso_appel));startActivity(callIntent);
            }
        } catch (Exception ex) {}
    }
    public void ecrireDirGenerale(String title, String categorie)
    {
        Intent intEcrireAuDr = new Intent(getContext(), ContactEmail.class);intEcrireAuDr.putExtra("dataContactTitle", title);intEcrireAuDr.putExtra("dataContactCategorie", categorie);
        intEcrireAuDr.putExtra("dataContactDestinataire", getString(R.string.txt_contact_destinataire_aboisso));startActivity(intEcrireAuDr);
    }
}