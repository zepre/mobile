package com.prodesk.mdgi.DirectionsRegionale;
import android.Manifest;import android.content.Intent;import android.content.pm.PackageManager;
import android.net.Uri;import android.os.Build;import android.os.Bundle;import android.support.v4.app.ActivityCompat;import android.support.v4.app.Fragment;
import android.view.LayoutInflater;import android.view.View;import android.view.ViewGroup;
import android.widget.Button;import android.widget.ImageView;
import android.widget.Toast;import com.prodesk.mdgi.ContactEmail;import com.prodesk.mdgi.R;
//** Jeud 05.09.2019 // 10.30pm **//
public class DirRegionalKorhogoVuecdi extends Fragment
{
    String dr_korhogo_num;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_dr_korhogo_vuecdi, container, false);Button call_dr_guiglo = (Button)view.findViewById(R.id.dr_korhogo_call);
        call_dr_guiglo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_korhogo_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_korhogo_num = getString(R.string.txt_dr_korhogo_contact);apellerDrKorhogo(dr_korhogo_num);
                }
            }
        });
        ImageView call_dr_korhogo_cdi_boundiali = (ImageView)view.findViewById(R.id.dr_korhogo_cdi_boundiali_call);
        call_dr_korhogo_cdi_boundiali.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_korhogo_cdi_boundiali_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_korhogo_num = getString(R.string.txt_dr_korhogo_cdi_boundiali_contact);apellerDrKorhogo(dr_korhogo_num);
                }
            }
        });
        ImageView call_dr_korhogo_cdi_ferkessedougou = (ImageView)view.findViewById(R.id.dr_korhogo_cdi_ferkessedougou_call);
        call_dr_korhogo_cdi_ferkessedougou.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_korhogo_cdi_ferkessedougou_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_korhogo_num = getString(R.string.txt_dr_korhogo_cdi_ferkessedougou_contact);apellerDrKorhogo(dr_korhogo_num);
                }
            }
        });
        ImageView call_dr_korhogo_cdi_kong = (ImageView)view.findViewById(R.id.dr_korhogo_cdi_kong_call);
        call_dr_korhogo_cdi_kong.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_korhogo_cdi_kong_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_korhogo_num = getString(R.string.txt_dr_korhogo_cdi_kong_contact);apellerDrKorhogo(dr_korhogo_num);
                }
            }
        });
        ImageView call_dr_korhogo_cdi_korhogo = (ImageView)view.findViewById(R.id.dr_korhogo_cdi_korhogo_call);
        call_dr_korhogo_cdi_korhogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_korhogo_cdi_korhogo_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_korhogo_num = getString(R.string.txt_dr_korhogo_cdi_korhogo_contact);apellerDrKorhogo(dr_korhogo_num);
                }
            }
        });
        ImageView call_dr_korhogo_cdi_mbengue = (ImageView)view.findViewById(R.id.dr_korhogo_cdi_mbengue_call);
        call_dr_korhogo_cdi_mbengue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_korhogo_cdi_mbengue_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_korhogo_num = getString(R.string.txt_dr_korhogo_cdi_mbengue_contact);apellerDrKorhogo(dr_korhogo_num);
                }
            }
        });
        ImageView call_dr_korhogo_cdi_ouangolodougou = (ImageView)view.findViewById(R.id.dr_korhogo_cdi_ouangolodougou_call);
        call_dr_korhogo_cdi_ouangolodougou.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_korhogo_cdi_ouangolodougou_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_korhogo_num = getString(R.string.txt_dr_korhogo_cdi_ouangolodougou_contact);apellerDrKorhogo(dr_korhogo_num);
                }
            }
        });
        ImageView call_dr_korhogo_cdi_tingrela = (ImageView)view.findViewById(R.id.dr_korhogo_cdi_tingrela_call);
        call_dr_korhogo_cdi_tingrela.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_korhogo_cdi_tingrela_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_korhogo_num = getString(R.string.txt_dr_korhogo_cdi_tingrela_contact);apellerDrKorhogo(dr_korhogo_num);
                }
            }
        });
        Button envoi_mail_dr_korhogo = (Button)view.findViewById(R.id.dr_korhogo_message);
        envoi_mail_dr_korhogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                ecrireDirGenerale(getString(R.string.txt_dr_korhogo_titre),"DIRECTION REGIONALE");
            }
        });return view;
    }
    public void apellerDrKorhogo(String dr_korhogo_appel)
    {
        try {
            if(Build.VERSION.SDK_INT > 22) {
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE}, 101);return;
                }
                Intent callIntent = new Intent(Intent.ACTION_CALL);callIntent.setData(Uri.parse("tel:" + dr_korhogo_appel));startActivity(callIntent);
            } else {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + dr_korhogo_appel));startActivity(callIntent);
            }
        } catch (Exception ex) { }
    }
    public void ecrireDirGenerale(String title, String categorie)
    {
        Intent intEcrireAuDr = new Intent(getContext(), ContactEmail.class);
        intEcrireAuDr.putExtra("dataContactTitle", title);intEcrireAuDr.putExtra("dataContactCategorie", categorie);
        intEcrireAuDr.putExtra("dataContactDestinataire", getString(R.string.txt_contact_destinataire_korhogo));startActivity(intEcrireAuDr);
    }
}