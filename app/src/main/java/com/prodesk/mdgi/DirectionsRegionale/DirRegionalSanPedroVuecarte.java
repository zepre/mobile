package com.prodesk.mdgi.DirectionsRegionale;
import android.app.FragmentManager;import android.content.Context;
import android.graphics.Color;import android.location.LocationManager;
import android.os.Bundle;import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;import android.view.LayoutInflater;
import android.view.View;import android.view.ViewGroup;
import android.widget.Toast;import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;import com.google.android.gms.maps.model.MarkerOptions;import com.prodesk.mdgi.R;
//** Sam 17.08.2019 // 1.37pm **//
public class DirRegionalSanPedroVuecarte extends Fragment implements OnMapReadyCallback
{
    Double mapLat,mapLong;GoogleMap drSanPedroMap;MapView drSanPedroMapView;MarkerOptions cdiFresco,  cdiMeagui, cdiSanPedro, cdiSassandra, cdisoubre, cdiTabou;
    LocationManager locationManager;boolean statusOfGPS;FragmentManager fragmentManager;SupportMapFragment mFragment;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_dr_sanpedro_vuecarte, container, false);
        if (!isGooglePlayServicesAvailable()) {
            Toast.makeText(getActivity(), "Service Google Play inaccessible !", Toast.LENGTH_SHORT).show();
        }
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        statusOfGPS = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);return view;
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        drSanPedroMapView = (MapView)view.findViewById(R.id.dr_sanpedro_map);
        if (drSanPedroMapView != null) {
            drSanPedroMapView.onCreate(null);drSanPedroMapView.onResume();
            drSanPedroMapView.getMapAsync(this);
        }
    }
    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        mapLat = Double.parseDouble(getString(R.string.txt_dr_sanpedro_point_lat));mapLong = Double.parseDouble(getString(R.string.txt_dr_sanpedro_point_long));MapsInitializer.initialize(getContext());
        drSanPedroMap = googleMap;
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.getUiSettings().setZoomGesturesEnabled(true);googleMap.getUiSettings().setCompassEnabled(true);
        LatLng ptDrSanPedro  = new LatLng(mapLat,mapLong);
        LatLng ptcdiFresco = new LatLng(Double.parseDouble(getString(R.string.txt_dr_sanpedro_cdi_fresco_lat)),Double.parseDouble(getString(R.string.txt_dr_sanpedro_cdi_fresco_long)));
        cdiFresco = new MarkerOptions();cdiFresco.position(ptcdiFresco).icon(setIconColor("#c86331"));
        cdiFresco.title(getString(R.string.txt_dr_sanpedro_cdi_fresco_titre));cdiFresco.snippet(getString(R.string.txt_dr_sanpedro_cdi_fresco_adresse));googleMap.addMarker(cdiFresco);
        LatLng ptcdiMeagui = new LatLng(Double.parseDouble(getString(R.string.txt_dr_sanpedro_cdi_meagui_lat)),Double.parseDouble(getString(R.string.txt_dr_sanpedro_cdi_meagui_long)));
        cdiMeagui = new MarkerOptions();cdiMeagui.position(ptcdiMeagui).icon(setIconColor("#c86331"));
        cdiMeagui.title(getString(R.string.txt_dr_sanpedro_cdi_meagui_titre));cdiMeagui.snippet(getString(R.string.txt_dr_sanpedro_cdi_meagui_adresse));googleMap.addMarker(cdiMeagui);
        LatLng ptcdiSanpedro = new LatLng(Double.parseDouble(getString(R.string.txt_dr_sanpedro_cdi_sanpedro_lat)),Double.parseDouble(getString(R.string.txt_dr_sanpedro_cdi_sanpedro_long)));
        cdiSanPedro = new MarkerOptions();cdiSanPedro.position(ptcdiSanpedro).icon(setIconColor("#c86331"));
        cdiSanPedro.title(getString(R.string.txt_dr_sanpedro_cdi_sanpedro_titre));cdiSanPedro.snippet(getString(R.string.txt_dr_sanpedro_cdi_sanpedro_adresse));googleMap.addMarker(cdiSanPedro);
        LatLng ptcdiSassandra = new LatLng(Double.parseDouble(getString(R.string.txt_dr_sanpedro_cdi_sassandra_lat)),Double.parseDouble(getString(R.string.txt_dr_sanpedro_cdi_sassandra_long)));
        cdiSassandra = new MarkerOptions();cdiSassandra.position(ptcdiSassandra).icon(setIconColor("#c86331"));
        cdiSassandra.title(getString(R.string.txt_dr_sanpedro_cdi_sassandra_titre));cdiSassandra.snippet(getString(R.string.txt_dr_sanpedro_cdi_sassandra_adresse));googleMap.addMarker(cdiSassandra);
        LatLng ptcdiSoubre = new LatLng(Double.parseDouble(getString(R.string.txt_dr_sanpedro_cdi_soubre_lat)),Double.parseDouble(getString(R.string.txt_dr_sanpedro_cdi_soubre_long)));
        cdisoubre = new MarkerOptions();cdisoubre.position(ptcdiSoubre).icon(setIconColor("#c86331"));cdisoubre.title(getString(R.string.txt_dr_sanpedro_cdi_soubre_titre));
        cdisoubre.snippet(getString(R.string.txt_dr_sanpedro_cdi_soubre_adresse));googleMap.addMarker(cdisoubre);
        LatLng ptcdiTabou = new LatLng(Double.parseDouble(getString(R.string.txt_dr_sanpedro_cdi_tabou_lat)),Double.parseDouble(getString(R.string.txt_dr_sanpedro_cdi_tabou_long)));
        cdiTabou = new MarkerOptions();cdiTabou.position(ptcdiTabou).icon(setIconColor("#c86331"));
        cdiTabou.title(getString(R.string.txt_dr_sanpedro_cdi_tabou_titre));cdiTabou.snippet(getString(R.string.txt_dr_sanpedro_cdi_tabou_adresse));googleMap.addMarker(cdiTabou);
        CameraPosition drSanPedroPos = CameraPosition.builder().target(ptDrSanPedro).zoom(6).bearing(0).tilt(45).build();googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(drSanPedroPos));
    }
    private boolean isGooglePlayServicesAvailable()
    {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            GooglePlayServicesUtil.getErrorDialog(status, getActivity(), 0).show();return false;
        }
    }
    public BitmapDescriptor setIconColor(String kol)
    {
        float[] hsv = new float[3];Color.colorToHSV(Color.parseColor(kol), hsv);
        return BitmapDescriptorFactory.defaultMarker(hsv[0]);
    }
}