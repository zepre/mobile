package com.prodesk.mdgi.DirectionsRegionale;
import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;
import com.prodesk.mdgi.ContactEmail;
import com.prodesk.mdgi.R;
//** Jeud 05.09.2019 // 10.13pm **//
public class DirRegionalBondoukouVuecdi extends Fragment
{
    public String dr_bondoukou_num;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_dr_bondoukou_vuecdi, container, false);
        Button call_dr_bondoukou = (Button)view.findViewById(R.id.dr_bondoukou_call);
        call_dr_bondoukou.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                dr_bondoukou_num = getString(R.string.txt_dr_bondoukou_contact);apellerDrBondoukou(dr_bondoukou_num);
            }
        });
        ImageView call_dr_bondoukou_cdi_bondoukou = (ImageView)view.findViewById(R.id.dr_bondoukou_cdi_bondoukou_call);
        call_dr_bondoukou_cdi_bondoukou.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_bondoukou_cdi_bondoukou_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_bondoukou_num = getString(R.string.txt_dr_bondoukou_cdi_bondoukou_contact);apellerDrBondoukou(dr_bondoukou_num);
                }
            }
        });
        ImageView call_dr_bondoukou_cdi_doropo = (ImageView)view.findViewById(R.id.dr_bondoukou_cdi_doropo_call);
        call_dr_bondoukou_cdi_doropo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_bondoukou_cdi_doropo_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_bondoukou_num = getString(R.string.txt_dr_bondoukou_cdi_doropo_contact);apellerDrBondoukou(dr_bondoukou_num);
                }
            }
        });
        ImageView call_dr_bondoukou_cdi_kounfao = (ImageView)view.findViewById(R.id.dr_bondoukou_cdi_kounfao_call);
        call_dr_bondoukou_cdi_kounfao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_bondoukou_cdi_kounfao_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_bondoukou_num = getString(R.string.txt_dr_bondoukou_cdi_kounfao_contact);apellerDrBondoukou(dr_bondoukou_num);
                }
            }
        });
        ImageView call_dr_bondoukou_cdi_kouassidatekro = (ImageView)view.findViewById(R.id.dr_bondoukou_cdi_kouassidatekro_call);
        call_dr_bondoukou_cdi_kouassidatekro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_bondoukou_cdi_kouassidatekro_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_bondoukou_num = getString(R.string.txt_dr_bondoukou_cdi_kouassidatekro_contact);apellerDrBondoukou(dr_bondoukou_num);
                }
            }
        });
        ImageView call_dr_bondoukou_cdi_nassian = (ImageView)view.findViewById(R.id.dr_bondoukou_cdi_nassian_call);
        call_dr_bondoukou_cdi_nassian.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_bondoukou_cdi_nassian_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_bondoukou_num = getString(R.string.txt_dr_bondoukou_cdi_nassian_contact);apellerDrBondoukou(dr_bondoukou_num);
                }
            }
        });
        ImageView call_dr_bondoukou_cdi_tanda = (ImageView)view.findViewById(R.id.dr_bondoukou_cdi_tanda_call);
        call_dr_bondoukou_cdi_tanda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_bondoukou_cdi_tanda_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_bondoukou_num = getString(R.string.txt_dr_bondoukou_cdi_tanda_contact);apellerDrBondoukou(dr_bondoukou_num);
                }
            }
        });
        Button envoi_mail_dr_bondoukou = (Button)view.findViewById(R.id.dr_bondoukou_message);
        envoi_mail_dr_bondoukou.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                ecrireDirGenerale(getString(R.string.txt_dr_bondoukou_titre),"DIRECTION REGIONALE");
            }
        });return view;
    }
    public void apellerDrBondoukou(String dr_bondoukou_appel)
    {
        try {
            if(Build.VERSION.SDK_INT > 22) {
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE}, 101);return;
                }
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + dr_bondoukou_appel));startActivity(callIntent);
            } else {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + dr_bondoukou_appel));startActivity(callIntent);
            }
        } catch (Exception ex) {}
    }
    public void ecrireDirGenerale(String title, String categorie)
    {
        Intent intEcrireAuDr = new Intent(getContext(), ContactEmail.class);intEcrireAuDr.putExtra("dataContactTitle", title);intEcrireAuDr.putExtra("dataContactCategorie", categorie);
        intEcrireAuDr.putExtra("dataContactDestinataire", getString(R.string.txt_contact_destinataire_bondoukou));startActivity(intEcrireAuDr);
    }
}