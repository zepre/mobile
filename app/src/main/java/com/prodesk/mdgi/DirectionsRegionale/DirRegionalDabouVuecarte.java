package com.prodesk.mdgi.DirectionsRegionale;
import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Color;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.prodesk.mdgi.R;
//** Sam 17.08.2019 // 1.09pm **//
public class DirRegionalDabouVuecarte extends Fragment implements OnMapReadyCallback
{
    Double mapLat,mapLong;GoogleMap drDabouMap;MapView drDabouMapView;MarkerOptions cdiDabou, cdiGrandLahou, cdiJacqueville, cdiSikensi;LocationManager locationManager;
    boolean statusOfGPS;FragmentManager fragmentManager;SupportMapFragment mFragment;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_dr_dabou_vuecarte, container, false);
        if (!isGooglePlayServicesAvailable()) {
            Toast.makeText(getActivity(), "Service Google Play inaccessible !", Toast.LENGTH_SHORT).show();
        }
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        statusOfGPS = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);return view;
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        drDabouMapView = (MapView)view.findViewById(R.id.dr_dabou_map);
        if (drDabouMapView != null) {
            drDabouMapView.onCreate(null);drDabouMapView.onResume();
            drDabouMapView.getMapAsync(this);
        }
    }
    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        mapLat = Double.parseDouble(getString(R.string.txt_dr_dabou_point_lat));mapLong = Double.parseDouble(getString(R.string.txt_dr_dabou_point_long));
        MapsInitializer.initialize(getContext());drDabouMap = googleMap;
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.getUiSettings().setZoomGesturesEnabled(true);googleMap.getUiSettings().setCompassEnabled(true);
        LatLng ptDrDabou  = new LatLng(mapLat,mapLong);
        LatLng ptcdiDabou = new LatLng(Double.parseDouble(getString(R.string.txt_dr_dabou_cdi_dabou_lat)),Double.parseDouble(getString(R.string.txt_dr_dabou_cdi_dabou_long)));
        cdiDabou = new MarkerOptions();cdiDabou.position(ptcdiDabou).icon(setIconColor("#c86331"));cdiDabou.title(getString(R.string.txt_dr_dabou_cdi_dabou_titre));
        cdiDabou.snippet(getString(R.string.txt_dr_dabou_cdi_dabou_adresse));
        googleMap.addMarker(cdiDabou);
        LatLng ptcdiGrandLahou = new LatLng(Double.parseDouble(getString(R.string.txt_dr_dabou_cdi_grandlahou_lat)),Double.parseDouble(getString(R.string.txt_dr_dabou_cdi_grandlahou_long)));
        cdiGrandLahou = new MarkerOptions();cdiGrandLahou.position(ptcdiGrandLahou).icon(setIconColor("#c86331"));
        cdiGrandLahou.title(getString(R.string.txt_dr_dabou_cdi_grandlahou_titre));cdiGrandLahou.snippet(getString(R.string.txt_dr_dabou_cdi_grandlahou_adresse));googleMap.addMarker(cdiGrandLahou);
        LatLng ptcdiJacqueville = new LatLng(Double.parseDouble(getString(R.string.txt_dr_dabou_cdi_jacqueville_lat)),Double.parseDouble(getString(R.string.txt_dr_dabou_cdi_jacqueville_long)));
        cdiJacqueville = new MarkerOptions();cdiJacqueville.position(ptcdiJacqueville).icon(setIconColor("#c86331"));cdiJacqueville.title(getString(R.string.txt_dr_dabou_cdi_jacqueville_titre));
        cdiJacqueville.snippet(getString(R.string.txt_dr_dabou_cdi_jacqueville_adresse));googleMap.addMarker(cdiJacqueville);
        LatLng ptcdiSikensi = new LatLng(Double.parseDouble(getString(R.string.txt_dr_dabou_cdi_sikensi_lat)),Double.parseDouble(getString(R.string.txt_dr_dabou_cdi_sikensi_long)));
        cdiSikensi = new MarkerOptions();cdiSikensi.position(ptcdiSikensi).icon(setIconColor("#c86331"));
        cdiSikensi.title(getString(R.string.txt_dr_dabou_cdi_sikensi_titre));cdiSikensi.snippet(getString(R.string.txt_dr_dabou_cdi_sikensi_adresse));googleMap.addMarker(cdiSikensi);
        CameraPosition drDabouPos = CameraPosition.builder().target(ptDrDabou).zoom(8).bearing(0).tilt(45).build();
        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(drDabouPos));
    }
    private boolean isGooglePlayServicesAvailable()
    {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            GooglePlayServicesUtil.getErrorDialog(status, getActivity(), 0).show();
            return false;
        }
    }
    public BitmapDescriptor setIconColor(String kol)
    {
        float[] hsv = new float[3];Color.colorToHSV(Color.parseColor(kol), hsv);
        return BitmapDescriptorFactory.defaultMarker(hsv[0]);
    }
}