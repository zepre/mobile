package com.prodesk.mdgi.DirectionsRegionale;
import android.Manifest;import android.content.Intent;
import android.content.pm.PackageManager;import android.net.Uri;
import android.os.Build;import android.os.Bundle;
import android.support.v4.app.ActivityCompat;import android.support.v4.app.Fragment;
import android.view.LayoutInflater;import android.view.View;
import android.view.ViewGroup;import android.widget.Button;
import android.widget.ImageView;import android.widget.Toast;
import com.prodesk.mdgi.ContactEmail;import com.prodesk.mdgi.R;
//** Jeud 05.09.2019 // 10.27pm **//
public class DirRegionalGagnoaVuecdi extends Fragment
{
    String dr_gagnoa_num;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_dr_gagnoa_vuecdi, container, false);Button call_dr_gagnoa = (Button)view.findViewById(R.id.dr_gagnoa_call);
        call_dr_gagnoa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_gagnoa_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_gagnoa_num = getString(R.string.txt_dr_gagnoa_contact);apellerDrGagnoa(dr_gagnoa_num);
                }
            }
        });
        ImageView call_dr_gagnoa_cdi_divo = (ImageView)view.findViewById(R.id.dr_gagnoa_cdi_divo_call);
        call_dr_gagnoa_cdi_divo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_gagnoa_cdi_divo_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_gagnoa_num = getString(R.string.txt_dr_gagnoa_cdi_divo_contact);apellerDrGagnoa(dr_gagnoa_num);
                }
            }
        });
        ImageView call_dr_gagnoa_cdi_gagnoa = (ImageView)view.findViewById(R.id.dr_gagnoa_cdi_gagnoa_call);
        call_dr_gagnoa_cdi_gagnoa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_gagnoa_cdi_gagnoa_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_gagnoa_num = getString(R.string.txt_dr_gagnoa_cdi_gagnoa_contact);apellerDrGagnoa(dr_gagnoa_num);
                }
            }
        });
        // apeller dr Lakota
        ImageView call_dr_gagnoa_cdi_lakota = (ImageView)view.findViewById(R.id.dr_gagnoa_cdi_lakota_call);
        call_dr_gagnoa_cdi_lakota.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_gagnoa_cdi_lakota_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_gagnoa_num = getString(R.string.txt_dr_gagnoa_cdi_lakota_contact);apellerDrGagnoa(dr_gagnoa_num);
                }
            }
        });
        ImageView call_dr_gagnoa_cdi_oume = (ImageView)view.findViewById(R.id.dr_gagnoa_cdi_oume_call);
        call_dr_gagnoa_cdi_oume.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_gagnoa_cdi_oume_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_gagnoa_num = getString(R.string.txt_dr_gagnoa_cdi_oume_contact);apellerDrGagnoa(dr_gagnoa_num);
                }
            }
        });
        Button envoi_mail_dr_gagnoa = (Button)view.findViewById(R.id.dr_gagnoa_message);
        envoi_mail_dr_gagnoa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                ecrireDirGenerale(getString(R.string.txt_dr_gagnoa_titre),"DIRECTION REGIONALE");
            }
        });return view;
    }
    public void apellerDrGagnoa(String dr_gagnoa_appel)
    {
        try {
            if(Build.VERSION.SDK_INT > 22) {
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE}, 101);
                    return;
                }
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + dr_gagnoa_appel));startActivity(callIntent);
            } else {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + dr_gagnoa_appel));startActivity(callIntent);
            }
        } catch (Exception ex) {}
    }
    public void ecrireDirGenerale(String title, String categorie)
    {
        Intent intEcrireAuDr = new Intent(getContext(), ContactEmail.class);intEcrireAuDr.putExtra("dataContactTitle", title);intEcrireAuDr.putExtra("dataContactCategorie", categorie);
        intEcrireAuDr.putExtra("dataContactDestinataire", getString(R.string.txt_contact_destinataire_gagnoa));startActivity(intEcrireAuDr);
    }
}