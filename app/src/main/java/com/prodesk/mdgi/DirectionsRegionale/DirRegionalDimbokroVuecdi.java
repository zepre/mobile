package com.prodesk.mdgi.DirectionsRegionale;
import android.Manifest;import android.content.Intent;
import android.content.pm.PackageManager;import android.net.Uri;
import android.os.Build;import android.os.Bundle;
import android.support.v4.app.ActivityCompat;import android.support.v4.app.Fragment;
import android.view.LayoutInflater;import android.view.View;
import android.view.ViewGroup;import android.widget.Button;
import android.widget.ImageView;import android.widget.Toast;import com.prodesk.mdgi.ContactEmail;import com.prodesk.mdgi.R;
//** Jeud 05.09.2019 // 10.26pm **//
public class DirRegionalDimbokroVuecdi extends Fragment
{
    String dr_dimbokro_num;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_dr_dimbokro_vuecdi, container, false);Button call_dr_dimbokro = (Button)view.findViewById(R.id.dr_dimbokro_call);
        call_dr_dimbokro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_dimbokro_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_dimbokro_num = getString(R.string.txt_dr_dimbokro_contact);apellerDrDimbokro(dr_dimbokro_num);
                }
            }
        });
        ImageView call_dr_dimbokro_cdi_arrah = (ImageView)view.findViewById(R.id.dr_dimbokro_cdi_arrah_call);
        call_dr_dimbokro_cdi_arrah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_dimbokro_cdi_arrah_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_dimbokro_num = getString(R.string.txt_dr_dimbokro_cdi_arrah_contact);apellerDrDimbokro(dr_dimbokro_num);
                }
            }
        });
        ImageView call_dr_dimbokro_cdi_bocanda = (ImageView)view.findViewById(R.id.dr_dimbokro_cdi_bocanda_call);
        call_dr_dimbokro_cdi_bocanda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_dimbokro_cdi_bocanda_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_dimbokro_num = getString(R.string.txt_dr_dimbokro_cdi_bocanda_contact);apellerDrDimbokro(dr_dimbokro_num);
                }
            }
        });
        ImageView call_dr_dimbokro_cdi_bongouanou = (ImageView)view.findViewById(R.id.dr_dimbokro_cdi_bongouanou_call);
        call_dr_dimbokro_cdi_bongouanou.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_dimbokro_cdi_bongouanou_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_dimbokro_num = getString(R.string.txt_dr_dimbokro_cdi_bongouanou_contact);apellerDrDimbokro(dr_dimbokro_num);
                }
            }
        });
        ImageView call_dr_dimbokro_cdi_dimbokro = (ImageView)view.findViewById(R.id.dr_dimbokro_cdi_dimbokro_call);
        call_dr_dimbokro_cdi_dimbokro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_dimbokro_cdi_dimbokro_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_dimbokro_num = getString(R.string.txt_dr_dimbokro_cdi_dimbokro_contact);apellerDrDimbokro(dr_dimbokro_num);
                }
            }
        });
        ImageView call_dr_dimbokro_cdi_daoukro = (ImageView)view.findViewById(R.id.dr_dimbokro_cdi_daoukro_call);
        call_dr_dimbokro_cdi_daoukro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_dimbokro_cdi_daoukro_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_dimbokro_num = getString(R.string.txt_dr_dimbokro_cdi_daoukro_contact);apellerDrDimbokro(dr_dimbokro_num);
                }
            }
        });
        ImageView call_dr_dimbokro_cdi_mbatto = (ImageView)view.findViewById(R.id.dr_dimbokro_cdi_mbatto_call);
        call_dr_dimbokro_cdi_mbatto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_dimbokro_cdi_mbatto_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_dimbokro_num = getString(R.string.txt_dr_dimbokro_cdi_mbatto_contact);apellerDrDimbokro(dr_dimbokro_num);
                }
            }
        });
        Button envoi_mail_dr_dimbokro = (Button)view.findViewById(R.id.dr_dimbokro_message);
        envoi_mail_dr_dimbokro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                ecrireDirGenerale(getString(R.string.txt_dr_dimbokro_titre),"DIRECTION REGIONALE");
            }
        });return view;
    }
    public void apellerDrDimbokro(String dr_dimbokro_appel)
    {
        try {
            if(Build.VERSION.SDK_INT > 22) {
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE}, 101);return;
                }
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + dr_dimbokro_appel));startActivity(callIntent);
            } else {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + dr_dimbokro_appel));startActivity(callIntent);
            }
        } catch (Exception ex) {}
    }
    public void ecrireDirGenerale(String title, String categorie)
    {
        Intent intEcrireAuDr = new Intent(getContext(), ContactEmail.class);intEcrireAuDr.putExtra("dataContactTitle", title);intEcrireAuDr.putExtra("dataContactCategorie", categorie);
        intEcrireAuDr.putExtra("dataContactDestinataire", getString(R.string.txt_contact_destinataire_dimbokro));startActivity(intEcrireAuDr);
    }
}