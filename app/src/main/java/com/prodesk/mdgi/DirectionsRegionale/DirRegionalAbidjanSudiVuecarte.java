package com.prodesk.mdgi.DirectionsRegionale;
import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Color;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.prodesk.mdgi.R;
//** Sam 17.08.2019 // 12.53pm **//
public class DirRegionalAbidjanSudiVuecarte extends Fragment implements OnMapReadyCallback
{
    Double mapLat,mapLong;GoogleMap drAbidjanSudiMap;MapView drAbidjanSudiMapView;MarkerOptions cdiPortBouetVridi, cdiTreichville;LocationManager locationManager;
    boolean statusOfGPS;FragmentManager fragmentManager;SupportMapFragment mFragment;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_dr_abidjansudi_vuecarte, container, false);
        if (!isGooglePlayServicesAvailable()) {
            Toast.makeText(getActivity(), "Service Google Play inaccessible !", Toast.LENGTH_SHORT).show();
        }
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        statusOfGPS = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);return view;
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        drAbidjanSudiMapView = (MapView)view.findViewById(R.id.dr_abidjansudi_map);
        if (drAbidjanSudiMapView != null) {
            drAbidjanSudiMapView.onCreate(null);drAbidjanSudiMapView.onResume();
            drAbidjanSudiMapView.getMapAsync(this);
        }
    }
    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        mapLat = Double.parseDouble(getString(R.string.txt_dr_abidjansudi_point_lat));mapLong = Double.parseDouble(getString(R.string.txt_dr_abidjansudi_point_long));
        MapsInitializer.initialize(getContext());drAbidjanSudiMap = googleMap;
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.getUiSettings().setZoomGesturesEnabled(true);googleMap.getUiSettings().setCompassEnabled(true);
        LatLng ptDrAbSi = new LatLng(mapLat,mapLong);
        LatLng ptcdiPortbouetvridi = new LatLng(Double.parseDouble(getString(R.string.txt_dr_abidjansudi_cdi_portbouetvridi_lat)),Double.parseDouble(getString(R.string.txt_dr_abidjansudi_cdi_portbouetvridi_long)));
        cdiPortBouetVridi = new MarkerOptions();cdiPortBouetVridi.position(ptcdiPortbouetvridi).icon(setIconColor("#c86331"));
        cdiPortBouetVridi.title(getString(R.string.txt_dr_abidjansudi_cdi_portbouetvridi_titre));cdiPortBouetVridi.snippet(getString(R.string.txt_dr_abidjansudi_cdi_portbouetvridi_adresse));
        googleMap.addMarker(cdiPortBouetVridi);
        LatLng ptcdiTreichville = new LatLng(Double.parseDouble(getString(R.string.txt_dr_abidjansudi_cdi_treichville_lat)),Double.parseDouble(getString(R.string.txt_dr_abidjansudi_cdi_treichville_long)));
        cdiTreichville = new MarkerOptions();cdiTreichville.position(ptcdiTreichville).icon(setIconColor("#c86331"));cdiTreichville.title(getString(R.string.txt_dr_abidjansudi_cdi_treichville_title));
        cdiTreichville.snippet(getString(R.string.txt_dr_abidjansudi_cdi_treichvillei_adresse));googleMap.addMarker(cdiTreichville);
        CameraPosition drAbSiPos = CameraPosition.builder().target(ptDrAbSi).zoom(13).bearing(0).tilt(45).build();
        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(drAbSiPos));
    }
    private boolean isGooglePlayServicesAvailable()
    {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            GooglePlayServicesUtil.getErrorDialog(status, getActivity(), 0).show();
            return false;
        }
    }
    public BitmapDescriptor setIconColor(String kol)
    {
        float[] hsv = new float[3];Color.colorToHSV(Color.parseColor(kol), hsv);
        return BitmapDescriptorFactory.defaultMarker(hsv[0]);
    }
}