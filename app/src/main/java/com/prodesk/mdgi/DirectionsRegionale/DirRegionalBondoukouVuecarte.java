package com.prodesk.mdgi.DirectionsRegionale;
import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Color;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.prodesk.mdgi.R;
//** Sam 17.08.2019 // 1.02pm **//
public class DirRegionalBondoukouVuecarte extends Fragment implements OnMapReadyCallback
{
    Double mapLat,mapLong;GoogleMap drBondoukouMap;MapView drBondoukouMapView;MarkerOptions cdiBondoukou, cdiDoropo, cdiKounFao, cdiKouassiDatekro, cdiNassian, cdiTanda;
    LocationManager locationManager;boolean statusOfGPS;FragmentManager fragmentManager;SupportMapFragment mFragment;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_dr_bondoukou_vuecarte, container, false);
        if (!isGooglePlayServicesAvailable()) {
            Toast.makeText(getActivity(), "Service Google Play inaccessible !", Toast.LENGTH_SHORT).show();
        }
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        statusOfGPS = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);return view;
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        drBondoukouMapView = (MapView)view.findViewById(R.id.dr_bondoukou_map);
        if (drBondoukouMapView != null) {
            drBondoukouMapView.onCreate(null);drBondoukouMapView.onResume();
            drBondoukouMapView.getMapAsync(this);
        }
    }
    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        mapLat = Double.parseDouble(getString(R.string.txt_dr_bondoukou_point_lat));mapLong = Double.parseDouble(getString(R.string.txt_dr_bondoukou_point_long));MapsInitializer.initialize(getContext());drBondoukouMap = googleMap;
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.getUiSettings().setZoomGesturesEnabled(true);googleMap.getUiSettings().setCompassEnabled(true);
        LatLng ptDrBondoukou  = new LatLng(mapLat,mapLong);
        LatLng ptcdiBondoukou = new LatLng(Double.parseDouble(getString(R.string.txt_dr_bondoukou_cdi_bondoukou_lat)),Double.parseDouble(getString(R.string.txt_dr_bondoukou_cdi_bondoukou_long)));
        cdiBondoukou = new MarkerOptions();cdiBondoukou.position(ptcdiBondoukou).icon(setIconColor("#c86331"));cdiBondoukou.title(getString(R.string.txt_dr_bondoukou_cdi_bondoukou_titre));
        cdiBondoukou.snippet(getString(R.string.txt_dr_bondoukou_cdi_bondoukou_adresse));googleMap.addMarker(cdiBondoukou);
        LatLng ptcdiDoropo = new LatLng(Double.parseDouble(getString(R.string.txt_dr_bondoukou_cdi_doropo_lat)),Double.parseDouble(getString(R.string.txt_dr_bondoukou_cdi_doropo_long)));
        cdiDoropo = new MarkerOptions();cdiDoropo.position(ptcdiDoropo).icon(setIconColor("#c86331"));cdiDoropo.title(getString(R.string.txt_dr_bondoukou_cdi_doropo_titre));
        cdiDoropo.snippet(getString(R.string.txt_dr_bondoukou_cdi_doropo_adresse));googleMap.addMarker(cdiDoropo);
        LatLng ptcdiKounFao = new LatLng(Double.parseDouble(getString(R.string.txt_dr_bondoukou_cdi_kounfao_lat)),Double.parseDouble(getString(R.string.txt_dr_bondoukou_cdi_kounfao_long)));
        cdiKounFao = new MarkerOptions();cdiKounFao.position(ptcdiKounFao).icon(setIconColor("#c86331"));cdiKounFao.title(getString(R.string.txt_dr_bondoukou_cdi_kounfao_titre));
        cdiKounFao.snippet(getString(R.string.txt_dr_bondoukou_cdi_kounfao_adresse));googleMap.addMarker(cdiKounFao);
        LatLng ptcdiKouassiDatekro = new LatLng(Double.parseDouble(getString(R.string.txt_dr_bondoukou_cdi_kouassidatekro_lat)),Double.parseDouble(getString(R.string.txt_dr_bondoukou_cdi_kouassidatekro_long)));
        cdiKouassiDatekro = new MarkerOptions();cdiKouassiDatekro.position(ptcdiKouassiDatekro).icon(setIconColor("#c86331"));
        cdiKouassiDatekro.title(getString(R.string.txt_dr_bondoukou_cdi_kouassidatekro_titre));cdiKouassiDatekro.snippet(getString(R.string.txt_dr_bondoukou_cdi_kouassidatekro_adresse));googleMap.addMarker(cdiKouassiDatekro);
        LatLng ptcdiNassian = new LatLng(Double.parseDouble(getString(R.string.txt_dr_bondoukou_cdi_nassian_lat)),Double.parseDouble(getString(R.string.txt_dr_bondoukou_cdi_nassian_long)));
        cdiNassian = new MarkerOptions();cdiNassian.position(ptcdiNassian).icon(setIconColor("#c86331"));cdiNassian.title(getString(R.string.txt_dr_bondoukou_cdi_nassian_titre));
        cdiNassian.snippet(getString(R.string.txt_dr_bondoukou_cdi_nassian_adresse));googleMap.addMarker(cdiNassian);
        LatLng ptcdiTanda = new LatLng(Double.parseDouble(getString(R.string.txt_dr_bondoukou_cdi_tanda_lat)),Double.parseDouble(getString(R.string.txt_dr_bondoukou_cdi_tanda_long)));
        cdiTanda = new MarkerOptions();cdiTanda.position(ptcdiTanda).icon(setIconColor("#c86331"));cdiTanda.title(getString(R.string.txt_dr_bondoukou_cdi_tanda_titre));
        cdiTanda.snippet(getString(R.string.txt_dr_bondoukou_cdi_tanda_adresse));googleMap.addMarker(cdiTanda);
        CameraPosition drAgbovillePos = CameraPosition.builder().target(ptDrBondoukou).zoom(8).bearing(0).tilt(45).build();
        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(drAgbovillePos));
    }
    private boolean isGooglePlayServicesAvailable()
    {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            GooglePlayServicesUtil.getErrorDialog(status, getActivity(), 0).show();
            return false;
        }
    }
    public BitmapDescriptor setIconColor(String kol)
    {
        float[] hsv = new float[3];Color.colorToHSV(Color.parseColor(kol), hsv);
        return BitmapDescriptorFactory.defaultMarker(hsv[0]);
    }
}