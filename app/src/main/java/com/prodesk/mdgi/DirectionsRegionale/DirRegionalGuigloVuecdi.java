package com.prodesk.mdgi.DirectionsRegionale;
import android.Manifest;import android.content.Intent;
import android.content.pm.PackageManager;import android.net.Uri;
import android.os.Build;import android.os.Bundle;
import android.support.v4.app.ActivityCompat;import android.support.v4.app.Fragment;
import android.view.LayoutInflater;import android.view.View;
import android.view.ViewGroup;import android.widget.Button;import android.widget.ImageView;
import android.widget.Toast;import com.prodesk.mdgi.ContactEmail;import com.prodesk.mdgi.R;
//** Jeud 05.09.2019 // 10.28pm **//
public class DirRegionalGuigloVuecdi extends Fragment
{
    String dr_guiglo_num;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_dr_guiglo_vuecdi, container, false);Button call_dr_guiglo = (Button)view.findViewById(R.id.dr_guiglo_call);
        call_dr_guiglo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_guiglo_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_guiglo_num = getString(R.string.txt_dr_guiglo_contact);apellerDrGuiglo(dr_guiglo_num);
                }
            }
        });
        ImageView call_dr_guiglo_cdi_blolequin = (ImageView)view.findViewById(R.id.dr_guiglo_cdi_blolequin_call);
        call_dr_guiglo_cdi_blolequin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_guiglo_cdi_blolequin_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_guiglo_num = getString(R.string.txt_dr_guiglo_cdi_blolequin_contact);apellerDrGuiglo(dr_guiglo_num);
                }
            }
        });
        ImageView call_dr_guiglo_cdi_duekoue = (ImageView)view.findViewById(R.id.dr_guiglo_cdi_duekoue_call);
        call_dr_guiglo_cdi_duekoue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_guiglo_cdi_duekoue_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_guiglo_num = getString(R.string.txt_dr_guiglo_cdi_duekoue_contact);apellerDrGuiglo(dr_guiglo_num);
                }
            }
        });
        ImageView call_dr_guiglo_cdi_guiglo = (ImageView)view.findViewById(R.id.dr_guiglo_cdi_guiglo_call);
        call_dr_guiglo_cdi_guiglo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_guiglo_cdi_guiglo_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_guiglo_num = getString(R.string.txt_dr_guiglo_cdi_guiglo_contact);apellerDrGuiglo(dr_guiglo_num);
                }
            }
        });
        ImageView call_dr_guiglo_cdi_toulepleu = (ImageView)view.findViewById(R.id.dr_guiglo_cdi_toulepleu_call);
        call_dr_guiglo_cdi_toulepleu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_guiglo_cdi_toulepleu_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_guiglo_num = getString(R.string.txt_dr_guiglo_cdi_toulepleu_contact);apellerDrGuiglo(dr_guiglo_num);
                }
            }
        });
        Button envoi_mail_dr_guiglo = (Button)view.findViewById(R.id.dr_guiglo_message);
        envoi_mail_dr_guiglo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                ecrireDirGenerale(getString(R.string.txt_dr_guiglo_titre),"DIRECTION REGIONALE");
            }
        });return view;
    }
    public void apellerDrGuiglo(String dr_guiglo_appel)
    {
        try {
            if(Build.VERSION.SDK_INT > 22) {
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE}, 101);return;
                }
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + dr_guiglo_appel));startActivity(callIntent);
            } else {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + dr_guiglo_appel));startActivity(callIntent);
            }
        } catch (Exception ex) {}
    }
    public void ecrireDirGenerale(String title, String categorie)
    {
        Intent intEcrireAuDr = new Intent(getContext(), ContactEmail.class);intEcrireAuDr.putExtra("dataContactTitle", title);intEcrireAuDr.putExtra("dataContactCategorie", categorie);
        intEcrireAuDr.putExtra("dataContactDestinataire", getString(R.string.txt_contact_destinataire_guiglo));startActivity(intEcrireAuDr);
    }
}