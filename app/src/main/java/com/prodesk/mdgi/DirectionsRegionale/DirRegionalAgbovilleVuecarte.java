package com.prodesk.mdgi.DirectionsRegionale;
import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Color;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.prodesk.mdgi.R;
//** Sam 17.08.2019 // 12.59pm **//
public class DirRegionalAgbovilleVuecarte extends Fragment implements OnMapReadyCallback
{
    Double mapLat,mapLong;GoogleMap drAgbovilleMap;MapView drAgbovilleMapView;
    MarkerOptions cdiAdzope, cdiAgboville, cdiAkoupe, cdiTaabo, cdiTiassale, cdiYakasse;LocationManager locationManager;
    boolean statusOfGPS;FragmentManager fragmentManager;SupportMapFragment mFragment;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_dr_agboville_vuecarte, container, false);
        if (!isGooglePlayServicesAvailable()) {
            Toast.makeText(getActivity(), "Service Google Play inaccessible !", Toast.LENGTH_SHORT).show();
        }
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        statusOfGPS = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        return view;
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        drAgbovilleMapView = (MapView)view.findViewById(R.id.dr_agboville_map);
        if (drAgbovilleMapView != null) {
            drAgbovilleMapView.onCreate(null);drAgbovilleMapView.onResume();
            drAgbovilleMapView.getMapAsync(this);
        }
    }
    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        mapLat = Double.parseDouble(getString(R.string.txt_dr_agboville_point_lat));mapLong = Double.parseDouble(getString(R.string.txt_dr_agboville_point_long));MapsInitializer.initialize(getContext());
        drAgbovilleMap = googleMap;googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.getUiSettings().setZoomControlsEnabled(true);googleMap.getUiSettings().setZoomGesturesEnabled(true);googleMap.getUiSettings().setCompassEnabled(true);
        LatLng ptDrAgboville = new LatLng(mapLat,mapLong);
        LatLng ptcdiAdzope = new LatLng(Double.parseDouble(getString(R.string.txt_dr_agboville_cdi_adzope_lat)),Double.parseDouble(getString(R.string.txt_dr_agboville_cdi_adzope_long)));
        cdiAdzope = new MarkerOptions();cdiAdzope.position(ptcdiAdzope).icon(setIconColor("#c86331"));
        cdiAdzope.title(getString(R.string.txt_dr_agboville_cdi_adzope_titre));cdiAdzope.snippet(getString(R.string.txt_dr_agboville_cdi_adzope_adresse));googleMap.addMarker(cdiAdzope);
        LatLng ptcdiAgboville = new LatLng(Double.parseDouble(getString(R.string.txt_dr_agboville_cdi_agboville_lat)),Double.parseDouble(getString(R.string.txt_dr_agboville_cdi_agboville_long)));
        cdiAgboville = new MarkerOptions();cdiAgboville.position(ptcdiAgboville).icon(setIconColor("#c86331"));
        cdiAgboville.title(getString(R.string.txt_dr_agboville_cdi_agboville_titre));cdiAgboville.snippet(getString(R.string.txt_dr_agboville_cdi_agboville_adresse));googleMap.addMarker(cdiAgboville);
        LatLng ptcdiAkoupe = new LatLng(Double.parseDouble(getString(R.string.txt_dr_agboville_cdi_akoupe_lat)),Double.parseDouble(getString(R.string.txt_dr_agboville_cdi_akoupe_long)));
        cdiAkoupe = new MarkerOptions();cdiAkoupe.position(ptcdiAkoupe).icon(setIconColor("#c86331"));
        cdiAkoupe.title(getString(R.string.txt_dr_agboville_cdi_akoupe_titre));cdiAkoupe.snippet(getString(R.string.txt_dr_agboville_cdi_akoupe_adresse));googleMap.addMarker(cdiAkoupe);
        LatLng ptcdiTaabo = new LatLng(Double.parseDouble(getString(R.string.txt_dr_agboville_cdi_taabo_lat)),Double.parseDouble(getString(R.string.txt_dr_agboville_cdi_taabo_long)));
        cdiTaabo = new MarkerOptions();cdiTaabo.position(ptcdiTaabo).icon(setIconColor("#c86331"));
        cdiTaabo.title(getString(R.string.txt_dr_agboville_cdi_taabo_titre));cdiTaabo.snippet(getString(R.string.txt_dr_agboville_cdi_taabo_adresse));googleMap.addMarker(cdiTaabo);
        LatLng ptcdiTiassale = new LatLng(Double.parseDouble(getString(R.string.txt_dr_agboville_cdi_tiassale_lat)),Double.parseDouble(getString(R.string.txt_dr_agboville_cdi_tiassale_long)));
        cdiTiassale = new MarkerOptions();cdiTiassale.position(ptcdiTiassale).icon(setIconColor("#c86331"));
        cdiTiassale.title(getString(R.string.txt_dr_agboville_cdi_tiassale_titre));cdiTiassale.snippet(getString(R.string.txt_dr_agboville_cdi_tiassale_adresse));googleMap.addMarker(cdiTiassale);
        LatLng ptcdiYakasse = new LatLng(Double.parseDouble(getString(R.string.txt_dr_agboville_cdi_yakasse_lat)),Double.parseDouble(getString(R.string.txt_dr_agboville_cdi_yakasse_long)));
        cdiYakasse = new MarkerOptions();cdiYakasse.position(ptcdiYakasse).icon(setIconColor("#c86331"));
        cdiYakasse.title(getString(R.string.txt_dr_agboville_cdi_yakasse_titre));cdiYakasse.snippet(getString(R.string.txt_dr_agboville_cdi_yakasse_adresse));
        googleMap.addMarker(cdiYakasse);CameraPosition drAgbovillePos = CameraPosition.builder().target(ptDrAgboville).zoom(7).bearing(0).tilt(45).build();
        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(drAgbovillePos));
    }
    private boolean isGooglePlayServicesAvailable()
    {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            GooglePlayServicesUtil.getErrorDialog(status, getActivity(), 0).show();
            return false;
        }
    }
    public BitmapDescriptor setIconColor(String kol)
    {
        float[] hsv = new float[3];Color.colorToHSV(Color.parseColor(kol), hsv);
        return BitmapDescriptorFactory.defaultMarker(hsv[0]);
    }
}