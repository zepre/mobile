package com.prodesk.mdgi.DirectionsRegionale;
import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Color;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.prodesk.mdgi.R;
//** Sam 17.08.2019 // 1.25pm **//
public class DirRegionalGagnoaVuecarte extends Fragment implements OnMapReadyCallback
{
    Double mapLat,mapLong;GoogleMap drGagnoaMap;MapView drGagnoaMapView;
    MarkerOptions cdiDivo, cdiGagnoa, cdiLakota, cdiOume;LocationManager locationManager;boolean statusOfGPS;
    FragmentManager fragmentManager;SupportMapFragment mFragment;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_dr_gagnoa_vuecarte, container, false);
        if (!isGooglePlayServicesAvailable()) {
            Toast.makeText(getActivity(), "Service Google Play inaccessible !", Toast.LENGTH_SHORT).show();
        }
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        statusOfGPS = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        return view;
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        drGagnoaMapView = (MapView)view.findViewById(R.id.dr_gagnoa_map);
        if (drGagnoaMapView != null) {
            drGagnoaMapView.onCreate(null);
            drGagnoaMapView.onResume();
            drGagnoaMapView.getMapAsync(this);
        }
    }
    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        mapLat = Double.parseDouble(getString(R.string.txt_dr_gagnoa_point_lat));mapLong = Double.parseDouble(getString(R.string.txt_dr_gagnoa_point_long));MapsInitializer.initialize(getContext());drGagnoaMap = googleMap;
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.getUiSettings().setZoomGesturesEnabled(true);googleMap.getUiSettings().setCompassEnabled(true);
        LatLng ptDrGagnoa  = new LatLng(mapLat,mapLong);
        LatLng ptcdiDivo = new LatLng(Double.parseDouble(getString(R.string.txt_dr_gagnoa_cdi_divo_lat)),Double.parseDouble(getString(R.string.txt_dr_gagnoa_cdi_divo_long)));
        cdiDivo = new MarkerOptions();cdiDivo.position(ptcdiDivo).icon(setIconColor("#c86331"));
        cdiDivo.title(getString(R.string.txt_dr_gagnoa_cdi_divo_titre));cdiDivo.snippet(getString(R.string.txt_dr_gagnoa_cdi_divo_adresse));googleMap.addMarker(cdiDivo);
        LatLng ptcdiGagnoa = new LatLng(Double.parseDouble(getString(R.string.txt_dr_gagnoa_cdi_gagnoa_lat)),Double.parseDouble(getString(R.string.txt_dr_gagnoa_cdi_gagnoa_long)));
        cdiGagnoa = new MarkerOptions();cdiGagnoa.position(ptcdiGagnoa).icon(setIconColor("#c86331"));
        cdiGagnoa.title(getString(R.string.txt_dr_gagnoa_cdi_gagnoa_titre));cdiGagnoa.snippet(getString(R.string.txt_dr_gagnoa_cdi_gagnoa_adresse));googleMap.addMarker(cdiGagnoa);
        LatLng ptcdiLakota = new LatLng(Double.parseDouble(getString(R.string.txt_dr_gagnoa_cdi_lakota_lat)),Double.parseDouble(getString(R.string.txt_dr_gagnoa_cdi_lakota_long)));
        cdiLakota = new MarkerOptions();cdiLakota.position(ptcdiLakota).icon(setIconColor("#c86331"));
        cdiLakota.title(getString(R.string.txt_dr_gagnoa_cdi_lakota_titre));cdiLakota.snippet(getString(R.string.txt_dr_gagnoa_cdi_lakota_adresse));googleMap.addMarker(cdiLakota);
        LatLng ptcdiOume = new LatLng(Double.parseDouble(getString(R.string.txt_dr_gagnoa_cdi_oume_lat)),Double.parseDouble(getString(R.string.txt_dr_gagnoa_cdi_oume_long)));
        cdiOume = new MarkerOptions();cdiOume.position(ptcdiOume).icon(setIconColor("#c86331"));
        cdiOume.title(getString(R.string.txt_dr_gagnoa_cdi_oume_titre));cdiOume.snippet(getString(R.string.txt_dr_gagnoa_cdi_oume_adresse));googleMap.addMarker(cdiOume);
        CameraPosition drGagnoaPos = CameraPosition.builder().target(ptDrGagnoa).zoom(8).bearing(0).tilt(45).build();
        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(drGagnoaPos));
    }
    private boolean isGooglePlayServicesAvailable()
    {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            GooglePlayServicesUtil.getErrorDialog(status, getActivity(), 0).show();
            return false;
        }
    }
    public BitmapDescriptor setIconColor(String kol)
    {
        float[] hsv = new float[3];Color.colorToHSV(Color.parseColor(kol), hsv);return BitmapDescriptorFactory.defaultMarker(hsv[0]);
    }
}