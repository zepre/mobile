package com.prodesk.mdgi.DirectionsRegionale;
import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;
import com.prodesk.mdgi.ContactEmail;
import com.prodesk.mdgi.R;
//** Jeud 05.09.2019 // 9.33pm **//
public class DirRegionalAbidjanNordiiiVuecdi extends Fragment
{
    String dr_abidjannordiii_num;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_dr_abidjannordiii_vuecdi, container, false);
        Button call_dr_abidjannordiii = (Button)view.findViewById(R.id.dr_abidjannordiii_call);
        call_dr_abidjannordiii.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                dr_abidjannordiii_num = getString(R.string.txt_dr_abidjannordiii_contact);
                apellerDrAbidjanNordiii(dr_abidjannordiii_num);
            }
        });
        ImageView call_dr_abidjannordiii_cdi_adjamei = (ImageView)view.findViewById(R.id.dr_abidjannordiii_cdi_adjamei_call);
        call_dr_abidjannordiii_cdi_adjamei.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_abidjannordiii_cdi_adjamei_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    AlertDialog.Builder buildCall = new AlertDialog.Builder(getContext());
                    buildCall.setMessage("Veuillez choisir le numéro à appeler !");
                    buildCall.setCancelable(true);
                    buildCall.setPositiveButton(
                            getString(R.string.txt_dr_abidjannordiii_cdi_adjamei_contact1),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dr_abidjannordiii_num = getString(R.string.txt_dr_abidjannordiii_cdi_adjamei_contact1);
                                    apellerDrAbidjanNordiii(dr_abidjannordiii_num);
                                }
                            });

                    buildCall.setNegativeButton(
                            getString(R.string.txt_dr_abidjannordiii_cdi_adjamei_contact2),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dr_abidjannordiii_num = getString(R.string.txt_dr_abidjannordiii_cdi_adjamei_contact2);
                                    apellerDrAbidjanNordiii(dr_abidjannordiii_num);
                                }
                            });
                    AlertDialog alert11 = buildCall.create();
                    alert11.show();
                    return;
                }
            }
        });
        ImageView call_dr_abidjannordiii_cdi_adjameii = (ImageView)view.findViewById(R.id.dr_abidjannordiii_cdi_adjameii_call);
        call_dr_abidjannordiii_cdi_adjameii.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_abidjannordiii_cdi_adjamiiattcb_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    AlertDialog.Builder buildCall = new AlertDialog.Builder(getContext());
                    buildCall.setMessage("Veuillez choisir le numéro à appeler !");
                    buildCall.setCancelable(true);
                    buildCall.setPositiveButton(
                            getString(R.string.txt_dr_abidjannordiii_cdi_adjamiiattcb_contact1),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dr_abidjannordiii_num = getString(R.string.txt_dr_abidjannordiii_cdi_adjamiiattcb_contact1);
                                    apellerDrAbidjanNordiii(dr_abidjannordiii_num);
                                }
                            });

                    buildCall.setNegativeButton(
                            getString(R.string.txt_dr_abidjannordiii_cdi_adjamiiattcb_contact2),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dr_abidjannordiii_num = getString(R.string.txt_dr_abidjannordiii_cdi_adjamiiattcb_contact2);
                                    apellerDrAbidjanNordiii(dr_abidjannordiii_num);
                                }
                            });
                    AlertDialog alert11 = buildCall.create();
                    alert11.show();
                    return;
                }
            }
        });
        ImageView call_dr_abidjannordiii_cdi_adjameiii = (ImageView)view.findViewById(R.id.dr_abidjannordiii_cdi_adjameiii_call);
        call_dr_abidjannordiii_cdi_adjameiii.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_abidjannordiii_cdi_adjamiiiwillims_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    AlertDialog.Builder buildCall = new AlertDialog.Builder(getContext());
                    buildCall.setMessage("Veuillez choisir le numéro à appeler !");
                    buildCall.setCancelable(true);
                    buildCall.setPositiveButton(
                            getString(R.string.txt_dr_abidjannordiii_cdi_adjamiiiwillims_contact1),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dr_abidjannordiii_num = getString(R.string.txt_dr_abidjannordiii_cdi_adjamiiiwillims_contact1);
                                    apellerDrAbidjanNordiii(dr_abidjannordiii_num);
                                }
                            });

                    buildCall.setNegativeButton(
                            getString(R.string.txt_dr_abidjannordiii_cdi_adjamiiiwillims_contact2),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dr_abidjannordiii_num = getString(R.string.txt_dr_abidjannordiii_cdi_adjamiiiwillims_contact2);
                                    apellerDrAbidjanNordiii(dr_abidjannordiii_num);
                                }
                            });
                    AlertDialog alert11 = buildCall.create();
                    alert11.show();
                    return;
                }
            }
        });
        ImageView call_dr_abidjannordiii_cdi_plateaui = (ImageView)view.findViewById(R.id.dr_abidjannordiii_cdi_plateau_i_call);
        call_dr_abidjannordiii_cdi_plateaui.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_abidjannordiii_cdi_plat_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_abidjannordiii_num = getString(R.string.txt_dr_abidjannordiii_cdi_plat_contact);
                    apellerDrAbidjanNordiii(dr_abidjannordiii_num);
                }
            }
        });
        ImageView call_dr_abidjannordiii_cdi_plateauii = (ImageView)view.findViewById(R.id.dr_abidjannordiii_cdi_plateau_ii_call);
        call_dr_abidjannordiii_cdi_plateauii.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_abidjannordiii_cdi_plat_ii_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_abidjannordiii_num = getString(R.string.txt_dr_abidjannordiii_cdi_plat_ii_contact);
                    apellerDrAbidjanNordiii(dr_abidjannordiii_num);
                }
            }
        });
        ImageView call_dr_abidjannordiii_cdi_cme = (ImageView)view.findViewById(R.id.dr_abidjannordiii_cdi_cme_call);
        call_dr_abidjannordiii_cdi_cme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_abidjannordiii_cdi_centremoyenterp_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_abidjannordiii_num = getString(R.string.txt_dr_abidjannordiii_cdi_centremoyenterp_contact);
                    apellerDrAbidjanNordiii(dr_abidjannordiii_num);
                }
            }
        });
        Button envoi_mail_dr_abidjannordiii = (Button)view.findViewById(R.id.dr_abidjannordiii_message);
        envoi_mail_dr_abidjannordiii.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                ecrireDirGenerale(getString(R.string.txt_dr_abidjannordiii_titre),"DIRECTION REGIONALE");
            }
        });
        return view;
    }
    public void apellerDrAbidjanNordiii(String dr_abidjannordiii_appel)
    {
        try {
            if(Build.VERSION.SDK_INT > 22) {
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE}, 101);
                    return;
                }
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + dr_abidjannordiii_appel));
                startActivity(callIntent);
            }
            else {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + dr_abidjannordiii_appel));
                startActivity(callIntent);
            }
        }
        catch (Exception ex) {}
    }
    public void ecrireDirGenerale(String title, String categorie)
    {
        Intent intEcrireAuDr = new Intent(getContext(), ContactEmail.class);
        intEcrireAuDr.putExtra("dataContactTitle", title);intEcrireAuDr.putExtra("dataContactCategorie", categorie);
        intEcrireAuDr.putExtra("dataContactDestinataire", getString(R.string.txt_contact_destinataire_dran3));startActivity(intEcrireAuDr);
    }
}