package com.prodesk.mdgi.DirectionsRegionale;
import android.app.FragmentManager;import android.content.Context;
import android.graphics.Color;import android.location.LocationManager;
import android.os.Bundle;import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;import android.view.LayoutInflater;
import android.view.View;import android.view.ViewGroup;
import android.widget.Toast;import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;import com.google.android.gms.maps.model.MarkerOptions;import com.prodesk.mdgi.R;
//** Sam 17.08.2019 // 1.14pm **//
public class DirRegionalDimbokroVuecarte extends Fragment implements OnMapReadyCallback
{
    Double mapLat,mapLong;GoogleMap drDimbokroMap;MapView drDimbokroMapView;MarkerOptions cdiArrah, cdiBocanda, cdiBongouanou, cdiDimbokro, cdiDaoukro, cdiMbatto;LocationManager locationManager;
    boolean statusOfGPS;FragmentManager fragmentManager;SupportMapFragment mFragment;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_dr_dimbokro_vuecarte, container, false);
        if (!isGooglePlayServicesAvailable()) {
            Toast.makeText(getActivity(), "Service Google Play inaccessible !", Toast.LENGTH_SHORT).show();
        }
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        statusOfGPS = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);return view;
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        drDimbokroMapView = (MapView)view.findViewById(R.id.dr_dimbokro_map);
        if (drDimbokroMapView != null) {
            drDimbokroMapView.onCreate(null);drDimbokroMapView.onResume();
            drDimbokroMapView.getMapAsync(this);
        }
    }
    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        mapLat = Double.parseDouble(getString(R.string.txt_dr_dimbokro_point_lat));mapLong = Double.parseDouble(getString(R.string.txt_dr_dimbokro_point_long));MapsInitializer.initialize(getContext());
        drDimbokroMap = googleMap;
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.getUiSettings().setZoomGesturesEnabled(true);googleMap.getUiSettings().setCompassEnabled(true);LatLng ptDrDimbokro  = new LatLng(mapLat,mapLong);
        LatLng ptcdiArrah = new LatLng(Double.parseDouble(getString(R.string.txt_dr_dimbokro_cdi_arrah_lat)),Double.parseDouble(getString(R.string.txt_dr_dimbokro_cdi_arrah_long)));
        cdiArrah = new MarkerOptions();cdiArrah.position(ptcdiArrah).icon(setIconColor("#c86331"));
        cdiArrah.title(getString(R.string.txt_dr_dimbokro_cdi_arrah_titre));cdiArrah.snippet(getString(R.string.txt_dr_dimbokro_cdi_arrah_adresse));googleMap.addMarker(cdiArrah);
        LatLng ptcdiBocanda = new LatLng(Double.parseDouble(getString(R.string.txt_dr_dimbokro_cdi_bocanda_lat)),Double.parseDouble(getString(R.string.txt_dr_dimbokro_cdi_bocanda_long)));
        cdiBocanda = new MarkerOptions();cdiBocanda.position(ptcdiBocanda).icon(setIconColor("#c86331"));
        cdiBocanda.title(getString(R.string.txt_dr_dimbokro_cdi_bocanda_titre));cdiBocanda.snippet(getString(R.string.txt_dr_dimbokro_cdi_bocanda_adresse));googleMap.addMarker(cdiBocanda);
        LatLng ptcdiBongouanou = new LatLng(Double.parseDouble(getString(R.string.txt_dr_dimbokro_cdi_bongouanou_lat)),Double.parseDouble(getString(R.string.txt_dr_dimbokro_cdi_bongouanou_long)));
        cdiBongouanou = new MarkerOptions();cdiBongouanou.position(ptcdiBongouanou).icon(setIconColor("#c86331"));
        cdiBongouanou.title(getString(R.string.txt_dr_dimbokro_cdi_bongouanou_titre));cdiBongouanou.snippet(getString(R.string.txt_dr_dimbokro_cdi_bongouanou_adresse));googleMap.addMarker(cdiBongouanou);
        LatLng ptcdiDimbokro = new LatLng(Double.parseDouble(getString(R.string.txt_dr_dimbokro_cdi_dimbokro_lat)),Double.parseDouble(getString(R.string.txt_dr_dimbokro_cdi_dimbokro_long)));
        cdiDimbokro = new MarkerOptions();cdiDimbokro.position(ptcdiDimbokro).icon(setIconColor("#c86331"));
        cdiDimbokro.title(getString(R.string.txt_dr_dimbokro_cdi_dimbokro_titre));cdiDimbokro.snippet(getString(R.string.txt_dr_dimbokro_cdi_dimbokro_adresse));googleMap.addMarker(cdiDimbokro);
        LatLng ptcdiDaoukro = new LatLng(Double.parseDouble(getString(R.string.txt_dr_dimbokro_cdi_daoukro_lat)),Double.parseDouble(getString(R.string.txt_dr_dimbokro_cdi_daoukro_long)));
        cdiDaoukro.position(ptcdiDaoukro).icon(setIconColor("#c86331"));cdiDaoukro.title(getString(R.string.txt_dr_dimbokro_cdi_daoukro_titre));
        cdiDaoukro.snippet(getString(R.string.txt_dr_dimbokro_cdi_daoukro_adresse));googleMap.addMarker(cdiDaoukro);
        LatLng ptcdiMbatto = new LatLng(Double.parseDouble(getString(R.string.txt_dr_dimbokro_cdi_mbatto_lat)),Double.parseDouble(getString(R.string.txt_dr_dimbokro_cdi_mbatto_long)));
        cdiMbatto = new MarkerOptions();cdiMbatto.position(ptcdiMbatto).icon(setIconColor("#c86331"));
        cdiMbatto.title(getString(R.string.txt_dr_dimbokro_cdi_mbatto_titre));cdiMbatto.snippet(getString(R.string.txt_dr_dimbokro_cdi_mbatto_adresse));
        googleMap.addMarker(cdiMbatto);CameraPosition drDimbokroPos = CameraPosition.builder().target(ptDrDimbokro).zoom(8).bearing(0).tilt(45).build();googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(drDimbokroPos));
    }
    private boolean isGooglePlayServicesAvailable()
    {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            GooglePlayServicesUtil.getErrorDialog(status, getActivity(), 0).show();
            return false;
        }
    }
    public BitmapDescriptor setIconColor(String kol)
    {
        float[] hsv = new float[3];Color.colorToHSV(Color.parseColor(kol), hsv);
        return BitmapDescriptorFactory.defaultMarker(hsv[0]);
    }
}