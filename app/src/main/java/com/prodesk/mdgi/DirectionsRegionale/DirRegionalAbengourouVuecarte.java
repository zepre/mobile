package com.prodesk.mdgi.DirectionsRegionale;
import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Color;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.prodesk.mdgi.R;
//** Sam 17.08.2019 // 12.21pm **//
public class DirRegionalAbengourouVuecarte extends Fragment implements OnMapReadyCallback
{
    Double mapLat,mapLong;GoogleMap drAbengourouMap;MapView drAbengourouMapView;MarkerOptions cdiBetie, cdiAbengourou, cdiAgnibilekro, cdiNiable;
    LocationManager locationManager;boolean statusOfGPS;FragmentManager fragmentManager;SupportMapFragment mFragment;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_dr_abengourou_vuecarte, container, false);
        if (!isGooglePlayServicesAvailable()) {
            Toast.makeText(getActivity(), "Service Google Play inaccessible !", Toast.LENGTH_SHORT).show();
        }
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        statusOfGPS = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        return view;
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        drAbengourouMapView = (MapView)view.findViewById(R.id.dr_abengourou_map);
        if (drAbengourouMapView != null) {
            drAbengourouMapView.onCreate(null);
            drAbengourouMapView.onResume();drAbengourouMapView.getMapAsync(this);
        }
    }
    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        mapLat = Double.parseDouble(getString(R.string.txt_dr_abengourou_point_lat));mapLong = Double.parseDouble(getString(R.string.txt_dr_abengourou_point_long));MapsInitializer.initialize(getContext());
        drAbengourouMap = googleMap;
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.getUiSettings().setZoomGesturesEnabled(true);googleMap.getUiSettings().setCompassEnabled(true);
        LatLng ptDrAbengourou = new LatLng(mapLat,mapLong);
        LatLng ptcdiBetie = new LatLng(Double.parseDouble(getString(R.string.txt_dr_abengourou_cdi_betie_lat)),Double.parseDouble(getString(R.string.txt_dr_abengourou_cdi_betie_long)));
        cdiBetie = new MarkerOptions();cdiBetie.position(ptcdiBetie).icon(setIconColor("#c86331"));cdiBetie.title(getString(R.string.txt_dr_abengourou_cdi_betie_titre));
        cdiBetie.snippet(getString(R.string.txt_dr_abengourou_cdi_betie_adresse));googleMap.addMarker(cdiBetie);
        LatLng ptcdiAbengourou = new LatLng(Double.parseDouble(getString(R.string.txt_dr_abengourou_cdi_abengourou_lat)),Double.parseDouble(getString(R.string.txt_dr_abengourou_cdi_abengourou_long)));
        cdiAbengourou = new MarkerOptions();cdiAbengourou.position(ptcdiAbengourou).icon(setIconColor("#c86331"));cdiAbengourou.title(getString(R.string.txt_dr_abengourou_cdi_abengourou_titre));
        cdiAbengourou.snippet(getString(R.string.txt_dr_abengourou_cdi_abengourou_adresse));googleMap.addMarker(cdiAbengourou);
        LatLng ptcdiAgnibilekro = new LatLng(Double.parseDouble(getString(R.string.txt_dr_abengourou_cdi_agnibilekro_lat)),Double.parseDouble(getString(R.string.txt_dr_abengourou_cdi_agnibilekro_long)));
        cdiAgnibilekro = new MarkerOptions();cdiAgnibilekro.position(ptcdiAgnibilekro).icon(setIconColor("#c86331"));cdiAgnibilekro.title(getString(R.string.txt_dr_abengourou_cdi_agnibilekro_titre));
        cdiAgnibilekro.snippet(getString(R.string.txt_dr_abengourou_cdi_agnibilekro_adresse));googleMap.addMarker(cdiAgnibilekro);
        LatLng ptcdiNiable = new LatLng(Double.parseDouble(getString(R.string.txt_dr_abengourou_cdi_niable_lat)),Double.parseDouble(getString(R.string.txt_dr_abengourou_cdi_niable_long)));
        cdiNiable = new MarkerOptions();cdiNiable.position(ptcdiNiable).icon(setIconColor("#c86331"));
        cdiNiable.title(getString(R.string.txt_dr_abengourou_cdi_niable_titre));cdiNiable.snippet(getString(R.string.txt_dr_abengourou_cdi_niable_adresse));
        googleMap.addMarker(cdiNiable);
        CameraPosition drAbengourouPos = CameraPosition.builder().target(ptDrAbengourou).zoom(8).bearing(0).tilt(45).build();
        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(drAbengourouPos));
    }
    private boolean isGooglePlayServicesAvailable()
    {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            GooglePlayServicesUtil.getErrorDialog(status, getActivity(), 0).show();
            return false;
        }
    }
    public BitmapDescriptor setIconColor(String kol)
    {
        float[] hsv = new float[3];Color.colorToHSV(Color.parseColor(kol), hsv);
        return BitmapDescriptorFactory.defaultMarker(hsv[0]);
    }
}