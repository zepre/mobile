package com.prodesk.mdgi.DirectionsRegionale;
import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;
import com.prodesk.mdgi.ContactEmail;
import com.prodesk.mdgi.R;
//** Jeud 05.09.2019 // 9.34pm **//
public class DirRegionalAbidjanNordiiVuecdi extends Fragment
{
    String dr_abidjannordii_num;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_dr_abidjannordii_vuecdi, container, false);
        Button call_dr_abidjannordii = (Button)view.findViewById(R.id.dr_abidjannordii_call);
        call_dr_abidjannordii.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_abidjannordii_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_abidjannordii_num = getString(R.string.txt_dr_abidjannordii_contact);
                    apellerDrAbidjanNordii(dr_abidjannordii_num);
                }
            }
        });
        ImageView call_dr_abidjannordii_cdi_aboboi = (ImageView)view.findViewById(R.id.dr_abidjannordii_cdi_aboboi_call);
        call_dr_abidjannordii_cdi_aboboi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_abidjannordii_cdi_aboboi_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_abidjannordii_num = getString(R.string.txt_dr_abidjannordii_cdi_aboboi_contact);
                    apellerDrAbidjanNordii(dr_abidjannordii_num);
                }
            }
        });
        ImageView call_dr_abidjannordii_cdi_aboboii = (ImageView)view.findViewById(R.id.dr_abidjannordii_cdi_aboboii_call);
        call_dr_abidjannordii_cdi_aboboii.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_abidjannordii_cdi_aboboii_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_abidjannordii_num = getString(R.string.txt_dr_abidjannordii_cdi_aboboii_contact);
                    apellerDrAbidjanNordii(dr_abidjannordii_num);
                }
            }
        });
        ImageView call_dr_abidjannordii_cdi_aboboiii = (ImageView)view.findViewById(R.id.dr_abidjannordii_cdi_aboboiii_call);
        call_dr_abidjannordii_cdi_aboboiii.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_abidjannordii_cdi_aboboiii_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_abidjannordii_num = getString(R.string.txt_dr_abidjannordii_cdi_aboboiii_contact);
                    apellerDrAbidjanNordii(dr_abidjannordii_num);
                }
            }
        });
        ImageView call_dr_abidjannordii_cdi_alepe = (ImageView)view.findViewById(R.id.dr_abidjannordii_cdi_alepe_call);
        call_dr_abidjannordii_cdi_alepe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_abidjannordii_cdi_alepe_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_abidjannordii_num = getString(R.string.txt_dr_abidjannordii_cdi_alepe_contact);
                    apellerDrAbidjanNordii(dr_abidjannordii_num);
                }
            }
        });
        ImageView call_dr_abidjannordii_cdi_anyama = (ImageView)view.findViewById(R.id.dr_abidjannordii_cdi_anyama_call);
        call_dr_abidjannordii_cdi_anyama.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_abidjannordii_cdi_anyama_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_abidjannordii_num = getString(R.string.txt_dr_abidjannordii_cdi_anyama_contact);
                    apellerDrAbidjanNordii(dr_abidjannordii_num);
                }
            }
        });
        Button envoi_mail_dr_abidjannordii = (Button)view.findViewById(R.id.dr_abidjannordii_message);
        envoi_mail_dr_abidjannordii.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                ecrireDirGenerale(getString(R.string.txt_dr_abidjannordii_titre),"DIRECTION REGIONALE");
            }
        });
        return view;
    }
    public void apellerDrAbidjanNordii(String dr_abidjannordii_appel)
    {
        try {
            if(Build.VERSION.SDK_INT > 22) {
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE}, 101);
                    return;
                }
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + dr_abidjannordii_appel));
                startActivity(callIntent);
            }
            else {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + dr_abidjannordii_appel));
                startActivity(callIntent);
            }
        }
        catch (Exception ex) {}
    }
    public void ecrireDirGenerale(String title, String categorie)
    {
        Intent intEcrireAuDr = new Intent(getContext(), ContactEmail.class);
        intEcrireAuDr.putExtra("dataContactTitle", title);intEcrireAuDr.putExtra("dataContactCategorie", categorie);
        intEcrireAuDr.putExtra("dataContactDestinataire", getString(R.string.txt_contact_destinataire_dran2));startActivity(intEcrireAuDr);
    }
}