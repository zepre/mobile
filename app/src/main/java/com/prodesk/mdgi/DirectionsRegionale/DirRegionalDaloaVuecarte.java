package com.prodesk.mdgi.DirectionsRegionale;
import android.app.FragmentManager;import android.content.Context;
import android.graphics.Color;import android.location.LocationManager;
import android.os.Bundle;import android.support.annotation.Nullable;import android.support.v4.app.Fragment;
import android.view.LayoutInflater;import android.view.View;
import android.view.ViewGroup;import android.widget.Toast;
import com.google.android.gms.common.ConnectionResult;import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;import com.prodesk.mdgi.R;
//** Sam 17.08.2019 // 1.12pm **//
public class DirRegionalDaloaVuecarte extends Fragment implements OnMapReadyCallback
{
    Double mapLat,mapLong;GoogleMap drDaloaMap;MapView drDaloaMapView;MarkerOptions cdiDaloa, cdiDaloai, cdiIssia, cdiSeguela;LocationManager locationManager;
    boolean statusOfGPS;FragmentManager fragmentManager;SupportMapFragment mFragment;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_dr_daloa_vuecarte, container, false);
        if (!isGooglePlayServicesAvailable()) {
            Toast.makeText(getActivity(), "Service Google Play inaccessible !", Toast.LENGTH_SHORT).show();
        }
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        statusOfGPS = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);return view;
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        drDaloaMapView = (MapView)view.findViewById(R.id.dr_daloa_map);
        if (drDaloaMapView != null) {
            drDaloaMapView.onCreate(null);drDaloaMapView.onResume();drDaloaMapView.getMapAsync(this);
        }
    }
    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        mapLat = Double.parseDouble(getString(R.string.txt_dr_daloa_point_lat));mapLong = Double.parseDouble(getString(R.string.txt_dr_daloa_point_long));MapsInitializer.initialize(getContext());drDaloaMap = googleMap;
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.getUiSettings().setZoomGesturesEnabled(true);googleMap.getUiSettings().setCompassEnabled(true);
        LatLng ptDrDaloa  = new LatLng(mapLat,mapLong);
        LatLng ptcdiDaloa = new LatLng(Double.parseDouble(getString(R.string.txt_dr_daloa_cdi_daloa_lat)),Double.parseDouble(getString(R.string.txt_dr_daloa_cdi_daloa_long)));
        cdiDaloa = new MarkerOptions();cdiDaloa.position(ptcdiDaloa).icon(setIconColor("#c86331"));cdiDaloa.title(getString(R.string.txt_dr_daloa_cdi_daloa_titre));
        cdiDaloa.snippet(getString(R.string.txt_dr_daloa_cdi_daloa_adresse));googleMap.addMarker(cdiDaloa);
        LatLng ptcdiDaloai = new LatLng(Double.parseDouble(getString(R.string.txt_dr_daloa_cdi_daloai_lat)),Double.parseDouble(getString(R.string.txt_dr_daloa_cdi_daloai_long)));
        cdiDaloai = new MarkerOptions();cdiDaloai.position(ptcdiDaloai).icon(setIconColor("#c86331"));cdiDaloai.title(getString(R.string.txt_dr_daloa_cdi_daloai_title));
        cdiDaloai.snippet(getString(R.string.txt_dr_daloa_cdi_daloai_adresse));googleMap.addMarker(cdiDaloai);
        LatLng ptcdiIssia = new LatLng(Double.parseDouble(getString(R.string.txt_dr_daloa_cdi_issia_lat)),Double.parseDouble(getString(R.string.txt_dr_daloa_cdi_issia_long)));
        cdiIssia = new MarkerOptions();cdiIssia.position(ptcdiIssia).icon(setIconColor("#c86331"));
        cdiIssia.title(getString(R.string.txt_dr_daloa_cdi_issia_titre));cdiIssia.snippet(getString(R.string.txt_dr_daloa_cdi_issia_adresse));googleMap.addMarker(cdiIssia);
        LatLng ptcdiSeguela = new LatLng(Double.parseDouble(getString(R.string.txt_dr_daloa_cdi_seguela_lat)),Double.parseDouble(getString(R.string.txt_dr_daloa_cdi_seguela_long)));
        cdiSeguela = new MarkerOptions();cdiSeguela.position(ptcdiSeguela).icon(setIconColor("#c86331"));cdiSeguela.title(getString(R.string.txt_dr_daloa_cdi_seguela_titre));
        cdiSeguela.snippet(getString(R.string.txt_dr_daloa_cdi_seguela_adresse));googleMap.addMarker(cdiSeguela);
        CameraPosition drDaloaPos = CameraPosition.builder().target(ptDrDaloa).zoom(8).bearing(0).tilt(45).build();
        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(drDaloaPos));
    }
    private boolean isGooglePlayServicesAvailable()
    {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            GooglePlayServicesUtil.getErrorDialog(status, getActivity(), 0).show();
            return false;
        }
    }
    public BitmapDescriptor setIconColor(String kol)
    {
        float[] hsv = new float[3];Color.colorToHSV(Color.parseColor(kol), hsv);
        return BitmapDescriptorFactory.defaultMarker(hsv[0]);
    }
}