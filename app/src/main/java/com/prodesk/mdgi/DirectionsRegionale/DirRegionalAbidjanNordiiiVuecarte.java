package com.prodesk.mdgi.DirectionsRegionale;
import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Color;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.prodesk.mdgi.R;
//** Sam 17.08.2019 // 12.27pm **//
public class DirRegionalAbidjanNordiiiVuecarte extends Fragment implements OnMapReadyCallback
{
    Double mapLat,mapLong;GoogleMap drAbidjanNordiiiMap;MapView drAbidjanNordiiiMapView;
    MarkerOptions cdiAdjame, cdiPlateau, cdiCme;LocationManager locationManager;
    boolean statusOfGPS;FragmentManager fragmentManager;SupportMapFragment mFragment;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_dr_abidjannordiii_vuecarte, container, false);
        if (!isGooglePlayServicesAvailable()) {
            Toast.makeText(getActivity(), "Service Google Play inaccessible !", Toast.LENGTH_SHORT).show();
        }
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        statusOfGPS = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        return view;
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        drAbidjanNordiiiMapView = (MapView)view.findViewById(R.id.dr_abidjannordiii_map);
        if (drAbidjanNordiiiMapView != null) {
            drAbidjanNordiiiMapView.onCreate(null);
            drAbidjanNordiiiMapView.onResume();drAbidjanNordiiiMapView.getMapAsync(this);
        }
    }
    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        mapLat = Double.parseDouble(getString(R.string.txt_dr_abidjannordiii_point_lat));mapLong = Double.parseDouble(getString(R.string.txt_dr_abidjannordiii_point_long));MapsInitializer.initialize(getContext());
        drAbidjanNordiiiMap = googleMap;
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.getUiSettings().setZoomGesturesEnabled(true);googleMap.getUiSettings().setCompassEnabled(true);
        LatLng ptDrAbNiii = new LatLng(mapLat,mapLong);
        LatLng ptcdiAdjame = new LatLng(Double.parseDouble(getString(R.string.txt_dr_abidjannordiii_cdi_adjame_lat)),Double.parseDouble(getString(R.string.txt_dr_abidjannordiii_cdi_adjame_long)));
        cdiAdjame = new MarkerOptions();cdiAdjame.position(ptcdiAdjame).icon(setIconColor("#c86331"));
        cdiAdjame.title(getString(R.string.txt_dr_abidjannordiii_cdi_adjame_title));
        cdiAdjame.snippet(getString(R.string.txt_dr_abidjannordiii_cdi_adjamei_adresse));googleMap.addMarker(cdiAdjame);
        LatLng ptcdiPlateau = new LatLng(Double.parseDouble(getString(R.string.txt_dr_abidjannordiii_cdi_plateau_lat)),Double.parseDouble(getString(R.string.txt_dr_abidjannordiii_cdi_plateau_long)));
        cdiPlateau = new MarkerOptions();
        cdiPlateau.position(ptcdiPlateau).icon(setIconColor("#c86331"));cdiPlateau.title(getString(R.string.txt_dr_abidjannordiii_cdi_plateau_title));
        cdiPlateau.snippet(getString(R.string.txt_dr_abidjannordiii_cdi_plat_adresse));googleMap.addMarker(cdiPlateau);
        LatLng ptcdiCme = new LatLng(Double.parseDouble(getString(R.string.txt_dr_abidjannordiii_cdi_cme_lat)),Double.parseDouble(getString(R.string.txt_dr_abidjannordiii_cdi_cme_long)));
        cdiCme = new MarkerOptions();cdiCme.position(ptcdiCme).icon(setIconColor("#c86331"));cdiCme.title(getString(R.string.txt_dr_abidjannordiii_cdi_centremoyenterp_titre));
        cdiCme.snippet(getString(R.string.txt_dr_abidjannordiii_cdi_centremoyenterp_adresse));googleMap.addMarker(cdiCme);
        CameraPosition drAbNiiiPos = CameraPosition.builder().target(ptDrAbNiii).zoom(14).bearing(0).tilt(45).build();
        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(drAbNiiiPos));
    }
    private boolean isGooglePlayServicesAvailable()
    {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            GooglePlayServicesUtil.getErrorDialog(status, getActivity(), 0).show();
            return false;
        }
    }
    public BitmapDescriptor setIconColor(String kol)
    {
        float[] hsv = new float[3];
        Color.colorToHSV(Color.parseColor(kol), hsv);
        return BitmapDescriptorFactory.defaultMarker(hsv[0]);
    }
}