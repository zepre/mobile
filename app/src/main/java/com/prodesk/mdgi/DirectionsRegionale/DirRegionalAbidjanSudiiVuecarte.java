package com.prodesk.mdgi.DirectionsRegionale;
import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Color;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.prodesk.mdgi.R;
//** Sam 17.08.2019 // 12.50pm **//
public class DirRegionalAbidjanSudiiVuecarte extends Fragment implements OnMapReadyCallback
{
    Double mapLat,mapLong;GoogleMap drAbidjanSudiiMap;MapView drAbidjanSudiiMapView;MarkerOptions cdiBietry, cdiKoumassi, cdiMarcory, cdiCme;LocationManager locationManager;
    boolean statusOfGPS;FragmentManager fragmentManager;SupportMapFragment mFragment;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_dr_abidjansudii_vuecarte, container, false);
        if (!isGooglePlayServicesAvailable()) {
            Toast.makeText(getActivity(), "Service Google Play inaccessible !", Toast.LENGTH_SHORT).show();
        }
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        statusOfGPS = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        return view;
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        drAbidjanSudiiMapView = (MapView)view.findViewById(R.id.dr_abidjansudii_map);
        if (drAbidjanSudiiMapView != null) {
            drAbidjanSudiiMapView.onCreate(null);drAbidjanSudiiMapView.onResume();
            drAbidjanSudiiMapView.getMapAsync(this);
        }
    }
    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        mapLat = Double.parseDouble(getString(R.string.txt_dr_abidjansudii_point_lat));mapLong = Double.parseDouble(getString(R.string.txt_dr_abidjansudii_point_long));
        MapsInitializer.initialize(getContext());drAbidjanSudiiMap = googleMap;
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.getUiSettings().setZoomGesturesEnabled(true);googleMap.getUiSettings().setCompassEnabled(true);
        LatLng ptDrAbSii = new LatLng(mapLat,mapLong);
        LatLng ptcdiBietry = new LatLng(Double.parseDouble(getString(R.string.txt_dr_abidjansudii_cdi_bietry_lat)),Double.parseDouble(getString(R.string.txt_dr_abidjansudii_cdi_bietry_long)));
        cdiBietry = new MarkerOptions();cdiBietry.position(ptcdiBietry).icon(setIconColor("#c86331"));cdiBietry.title(getString(R.string.txt_dr_abidjansudii_cdi_bietry_title));
        cdiBietry.snippet(getString(R.string.txt_dr_abidjansudii_cdi_bietry_adresse));googleMap.addMarker(cdiBietry);
        LatLng ptcdiKoumassi = new LatLng(Double.parseDouble(getString(R.string.txt_dr_abidjansudii_cdi_koumassi_lat)),Double.parseDouble(getString(R.string.txt_dr_abidjansudii_cdi_koumassi_long)));
        cdiKoumassi = new MarkerOptions();cdiKoumassi.position(ptcdiKoumassi).icon(setIconColor("#c86331"));cdiKoumassi.title(getString(R.string.txt_dr_abidjansudii_cdi_koumassi_title));
        cdiKoumassi.snippet(getString(R.string.txt_dr_abidjansudii_cdi_koumassii_adresse));googleMap.addMarker(cdiKoumassi);
        LatLng ptcdiMarcory = new LatLng(Double.parseDouble(getString(R.string.txt_dr_abidjansudii_cdi_marcory_lat)),Double.parseDouble(getString(R.string.txt_dr_abidjansudii_cdi_marcory_long)));
        cdiMarcory = new MarkerOptions();cdiMarcory.position(ptcdiMarcory).icon(setIconColor("#c86331"));
        cdiMarcory.title(getString(R.string.txt_dr_abidjansudii_cdi_marcory_title));cdiMarcory.snippet(getString(R.string.txt_dr_abidjansudii_cdi_marcoryi_adresse));
        googleMap.addMarker(cdiMarcory);
        LatLng ptcdiCme = new LatLng(Double.parseDouble(getString(R.string.txt_dr_abidjansudii_cdi_cme_lat)),Double.parseDouble(getString(R.string.txt_dr_abidjansudii_cdi_cme_long)));
        cdiCme = new MarkerOptions();cdiCme.position(ptcdiCme).icon(setIconColor("#c86331"));
        cdiCme.title(getString(R.string.txt_dr_abidjansudii_cdi_centremoyentrep_titre));cdiCme.snippet(getString(R.string.txt_dr_abidjansudii_cdi_centremoyentrep_adresse));
        googleMap.addMarker(cdiCme);CameraPosition drAbSiiPos = CameraPosition.builder().target(ptDrAbSii).zoom(13).bearing(0).tilt(45).build();
        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(drAbSiiPos));
    }
    private boolean isGooglePlayServicesAvailable()
    {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            GooglePlayServicesUtil.getErrorDialog(status, getActivity(), 0).show();
            return false;
        }
    }
    public BitmapDescriptor setIconColor(String kol)
    {
        float[] hsv = new float[3];Color.colorToHSV(Color.parseColor(kol), hsv);
        return BitmapDescriptorFactory.defaultMarker(hsv[0]);
    }
}