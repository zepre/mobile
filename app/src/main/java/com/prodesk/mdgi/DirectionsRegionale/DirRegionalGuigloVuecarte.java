package com.prodesk.mdgi.DirectionsRegionale;
import android.app.FragmentManager;import android.content.Context;
import android.graphics.Color;import android.location.LocationManager;
import android.os.Bundle;import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;import android.view.LayoutInflater;
import android.view.View;import android.view.ViewGroup;
import android.widget.Toast;import com.google.android.gms.common.ConnectionResult;import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;import com.prodesk.mdgi.R;
//** Sam 17.08.2019 // 1.27pm **//
public class DirRegionalGuigloVuecarte extends Fragment implements OnMapReadyCallback
{
    Double mapLat,mapLong;GoogleMap drGuigloMap;MapView drGuigloMapView;MarkerOptions cdiBlolequin, cdiDuekoue, cdiGuiglo, cdiToulepleu;LocationManager locationManager;boolean statusOfGPS;
    FragmentManager fragmentManager;SupportMapFragment mFragment;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_dr_guiglo_vuecarte, container, false);
        if (!isGooglePlayServicesAvailable()) {
            Toast.makeText(getActivity(), "Service Google Play inaccessible !", Toast.LENGTH_SHORT).show();
        }
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        statusOfGPS = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);return view;
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        drGuigloMapView = (MapView)view.findViewById(R.id.dr_guiglo_map);
        if (drGuigloMapView != null) {
            drGuigloMapView.onCreate(null);drGuigloMapView.onResume();
            drGuigloMapView.getMapAsync(this);
        }
    }
    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        mapLat = Double.parseDouble(getString(R.string.txt_dr_guiglo_point_lat));mapLong = Double.parseDouble(getString(R.string.txt_dr_guiglo_point_long));MapsInitializer.initialize(getContext());
        drGuigloMap = googleMap;googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.getUiSettings().setZoomControlsEnabled(true);googleMap.getUiSettings().setZoomGesturesEnabled(true);googleMap.getUiSettings().setCompassEnabled(true);
        LatLng ptDrGuiglo = new LatLng(mapLat,mapLong);
        LatLng ptcdiBlolequin = new LatLng(Double.parseDouble(getString(R.string.txt_dr_guiglo_cdi_blolequin_lat)),Double.parseDouble(getString(R.string.txt_dr_guiglo_cdi_blolequin_long)));
        cdiBlolequin = new MarkerOptions();cdiBlolequin.position(ptcdiBlolequin).icon(setIconColor("#c86331"));
        cdiBlolequin.title(getString(R.string.txt_dr_guiglo_cdi_blolequin_titre));cdiBlolequin.snippet(getString(R.string.txt_dr_guiglo_cdi_blolequin_adresse));googleMap.addMarker(cdiBlolequin);
        LatLng ptcdiDuekoue = new LatLng(Double.parseDouble(getString(R.string.txt_dr_guiglo_cdi_duekoue_lat)),Double.parseDouble(getString(R.string.txt_dr_guiglo_cdi_duekoue_long)));
        cdiDuekoue = new MarkerOptions();cdiDuekoue.position(ptcdiDuekoue).icon(setIconColor("#c86331"));cdiDuekoue.title(getString(R.string.txt_dr_guiglo_cdi_duekoue_titre));
        cdiDuekoue.snippet(getString(R.string.txt_dr_guiglo_cdi_duekoue_adresse));googleMap.addMarker(cdiDuekoue);
        LatLng ptcdiGuiglo = new LatLng(Double.parseDouble(getString(R.string.txt_dr_guiglo_cdi_guiglo_lat)),Double.parseDouble(getString(R.string.txt_dr_guiglo_cdi_guiglo_long)));
        cdiGuiglo = new MarkerOptions();cdiGuiglo.position(ptcdiGuiglo).icon(setIconColor("#c86331"));
        cdiGuiglo.title(getString(R.string.txt_dr_guiglo_cdi_guiglo_titre));cdiGuiglo.snippet(getString(R.string.txt_dr_guiglo_cdi_guiglo_adresse));googleMap.addMarker(cdiGuiglo);
        LatLng ptcdiToulepleu = new LatLng(Double.parseDouble(getString(R.string.txt_dr_guiglo_cdi_toulepleu_lat)),Double.parseDouble(getString(R.string.txt_dr_guiglo_cdi_toulepleu_long)));
        cdiToulepleu = new MarkerOptions();cdiToulepleu.position(ptcdiToulepleu).icon(setIconColor("#c86331"));cdiToulepleu.title(getString(R.string.txt_dr_guiglo_cdi_toulepleu_titre));
        cdiToulepleu.snippet(getString(R.string.txt_dr_guiglo_cdi_toulepleu_adresse));googleMap.addMarker(cdiToulepleu);
        CameraPosition drGuigloPos = CameraPosition.builder().target(ptDrGuiglo).zoom(8).bearing(0).tilt(45).build();googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(drGuigloPos));
    }
    private boolean isGooglePlayServicesAvailable()
    {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            GooglePlayServicesUtil.getErrorDialog(status, getActivity(), 0).show();return false;
        }
    }
    public BitmapDescriptor setIconColor(String kol)
    {
        float[] hsv = new float[3];Color.colorToHSV(Color.parseColor(kol), hsv);return BitmapDescriptorFactory.defaultMarker(hsv[0]);
    }
}