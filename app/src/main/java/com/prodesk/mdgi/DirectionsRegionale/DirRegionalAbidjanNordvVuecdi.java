package com.prodesk.mdgi.DirectionsRegionale;
import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;
import com.prodesk.mdgi.ContactEmail;
import com.prodesk.mdgi.R;
//** Jeud 05.09.2019 // 10.02pm **//
public class DirRegionalAbidjanNordvVuecdi extends Fragment
{
    String dr_abidjannordv_num;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_dr_abidjannordv_vuecdi, container, false);
        Button call_dr_abidjannordv = (Button)view.findViewById(R.id.dr_abidjannordv_call);
        call_dr_abidjannordv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                dr_abidjannordv_num = getString(R.string.txt_dr_abidjannordv_contact);
                apellerDrAbidjanNordv(dr_abidjannordv_num);
            }
        });
        ImageView call_dr_abidjannordv_cdi_yopiii = (ImageView)view.findViewById(R.id.dr_abidjannordv_cdi_yopiii_call);
        call_dr_abidjannordv_cdi_yopiii.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_abidjannordv_cdi_yopiii_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_abidjannordv_num = getString(R.string.txt_dr_abidjannordv_cdi_yopiii_contact);
                    apellerDrAbidjanNordv(dr_abidjannordv_num);
                }
            }
        });
        ImageView call_dr_abidjannordv_cdi_yopiv = (ImageView)view.findViewById(R.id.dr_abidjannordv_cdi_yopiv_call);
        call_dr_abidjannordv_cdi_yopiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_abidjannordv_cdi_yopiv_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_abidjannordv_num = getString(R.string.txt_dr_abidjannordv_cdi_yopiv_contact);
                    apellerDrAbidjanNordv(dr_abidjannordv_num);
                }
            }
        });
        ImageView call_dr_abidjannordv_cdi_yopv = (ImageView)view.findViewById(R.id.dr_abidjannordv_cdi_yopv_call);
        call_dr_abidjannordv_cdi_yopv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_abidjannordv_cdi_yopv_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_abidjannordv_num = getString(R.string.txt_dr_abidjannordv_cdi_yopv_contact);
                    apellerDrAbidjanNordv(dr_abidjannordv_num);
                }
            }
        });
        Button envoi_mail_dr_abidjannordv = (Button)view.findViewById(R.id.dr_abidjannordv_message);
        envoi_mail_dr_abidjannordv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                ecrireDirGenerale(getString(R.string.txt_dr_abidjannordv_titre),"DIRECTION REGIONALE");
            }
        });
        return view;
    }
    public void apellerDrAbidjanNordv(String dr_abidjannordv_appel)
    {
        try {
            if(Build.VERSION.SDK_INT > 22) {
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE}, 101);
                    return;
                }
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + dr_abidjannordv_appel));startActivity(callIntent);
            }
            else {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + dr_abidjannordv_appel));startActivity(callIntent);
            }
        }
        catch (Exception ex) {}
    }
    public void ecrireDirGenerale(String title, String categorie)
    {
        Intent intEcrireAuDr = new Intent(getContext(), ContactEmail.class);
        intEcrireAuDr.putExtra("dataContactTitle", title);intEcrireAuDr.putExtra("dataContactCategorie", categorie);
        intEcrireAuDr.putExtra("dataContactDestinataire", getString(R.string.txt_contact_destinataire_dran5));startActivity(intEcrireAuDr);
    }
}