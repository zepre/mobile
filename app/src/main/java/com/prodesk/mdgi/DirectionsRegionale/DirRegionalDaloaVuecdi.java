package com.prodesk.mdgi.DirectionsRegionale;
import android.Manifest;import android.content.DialogInterface;
import android.content.Intent;import android.content.pm.PackageManager;
import android.net.Uri;import android.os.Build;
import android.os.Bundle;import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;import android.view.View;
import android.view.ViewGroup;import android.widget.Button;
import android.widget.ImageView;import android.widget.Toast;
import com.prodesk.mdgi.ContactEmail;import com.prodesk.mdgi.R;
//** Jeud 05.09.2019 // 10.25pm **//
public class DirRegionalDaloaVuecdi extends Fragment
{
    public String dr_daloa_num;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_dr_daloa_vuecdi, container, false);Button call_dr_daloa = (Button)view.findViewById(R.id.dr_daloa_call);
        call_dr_daloa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_daloa_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    AlertDialog.Builder buildCall = new AlertDialog.Builder(getContext());
                    buildCall.setMessage("Veuillez choisir le numéro à appeler !");buildCall.setCancelable(true);
                    buildCall.setPositiveButton(
                            getString(R.string.txt_dr_daloa_contact1),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dr_daloa_num = getString(R.string.txt_dr_daloa_contact1);apellerDrDaloa(dr_daloa_num);
                                }
                            });
                    buildCall.setNegativeButton(
                            getString(R.string.txt_dr_daloa_contact2),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dr_daloa_num = getString(R.string.txt_dr_daloa_contact2);apellerDrDaloa(dr_daloa_num);
                                }
                            });
                    AlertDialog alert11 = buildCall.create();alert11.show();
                }
            }
        });
        ImageView call_dr_daloa_cdi_daloa = (ImageView)view.findViewById(R.id.dr_daloa_cdi_daloa_call);
        call_dr_daloa_cdi_daloa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_daloa_cdi_daloa_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_daloa_num = getString(R.string.txt_dr_daloa_cdi_daloa_contact);apellerDrDaloa(dr_daloa_num);
                }
            }
        });
        ImageView call_dr_daloa_cdi_daloai = (ImageView)view.findViewById(R.id.dr_daloa_cdi_daloai_call);
        call_dr_daloa_cdi_daloai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_daloa_cdi_daloai_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_daloa_num = getString(R.string.txt_dr_daloa_cdi_daloai_contact);apellerDrDaloa(dr_daloa_num);
                }
            }
        });
        ImageView call_dr_daloa_cdi_daloaii = (ImageView)view.findViewById(R.id.dr_daloa_cdi_daloaii_call);
        call_dr_daloa_cdi_daloaii.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_daloa_cdi_daloaii_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_daloa_num = getString(R.string.txt_dr_daloa_cdi_daloaii_contact);apellerDrDaloa(dr_daloa_num);
                }
            }
        });
        ImageView call_dr_daloa_cdi_issia = (ImageView)view.findViewById(R.id.dr_daloa_cdi_issia_call);
        call_dr_daloa_cdi_issia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_daloa_cdi_issia_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_daloa_num = getString(R.string.txt_dr_daloa_cdi_issia_contact);apellerDrDaloa(dr_daloa_num);
                }
            }
        });
        ImageView call_dr_daloa_cdi_seguela = (ImageView)view.findViewById(R.id.dr_daloa_cdi_seguela_call);
        call_dr_daloa_cdi_seguela.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_daloa_cdi_seguela_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_daloa_num = getString(R.string.txt_dr_daloa_cdi_seguela_contact);apellerDrDaloa(dr_daloa_num);
                }
            }
        });
        Button envoi_mail_dr_daloa = (Button)view.findViewById(R.id.dr_daloa_message);
        envoi_mail_dr_daloa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                ecrireDirGenerale(getString(R.string.txt_dr_daloa_titre),"DIRECTION REGIONALE");
            }
        });return view;
    }
    public void apellerDrDaloa(String dr_daloa_appel)
    {
        try {
            if(Build.VERSION.SDK_INT > 22) {
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE}, 101);return;
                }
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + dr_daloa_appel));startActivity(callIntent);
            } else {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + dr_daloa_appel));startActivity(callIntent);
            }
        } catch (Exception ex) {}
    }
    public void ecrireDirGenerale(String title, String categorie)
    {
        Intent intEcrireAuDr = new Intent(getContext(), ContactEmail.class);
        intEcrireAuDr.putExtra("dataContactTitle", title);intEcrireAuDr.putExtra("dataContactCategorie", categorie);intEcrireAuDr.putExtra("dataContactDestinataire", getString(R.string.txt_contact_destinataire_daloa));startActivity(intEcrireAuDr);
    }
}