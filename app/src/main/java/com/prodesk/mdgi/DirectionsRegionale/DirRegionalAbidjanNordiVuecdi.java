package com.prodesk.mdgi.DirectionsRegionale;
import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;
import com.prodesk.mdgi.ContactEmail;
import com.prodesk.mdgi.R;
//** Jeud 05.09.2019 // 9.37pm **//
public class DirRegionalAbidjanNordiVuecdi extends Fragment
{
    String dr_abidjannordi_num;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_dr_abidjannordi_vuecdi, container, false);
        Button call_dr_abidjannordi = (Button)view.findViewById(R.id.dr_abidjannordi_call);
        call_dr_abidjannordi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                dr_abidjannordi_num = getString(R.string.txt_dr_abidjannordi_contact);apellerDrAbidjanNordi(dr_abidjannordi_num);
            }
        });
        ImageView call_dr_abidjannordi_cdi_iiplatii = (ImageView)view.findViewById(R.id.dr_abidjannordi_cdi_iiplat_ii_call);
        call_dr_abidjannordi_cdi_iiplatii.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_abidjannordi_cdi_iiplat_ii_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    AlertDialog.Builder buildCall = new AlertDialog.Builder(getContext());
                    buildCall.setMessage("Veuillez choisir le numéro à appeler !");
                    buildCall.setCancelable(true);
                    buildCall.setPositiveButton(
                            getString(R.string.txt_dr_abidjannordi_cdi_iiplat_ii_contact1),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dr_abidjannordi_num = getString(R.string.txt_dr_abidjannordi_cdi_iiplat_ii_contact1);
                                    apellerDrAbidjanNordi(dr_abidjannordi_num);
                                }
                            });

                    buildCall.setNegativeButton(
                            getString(R.string.txt_dr_abidjannordi_cdi_iiplat_ii_contact2),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dr_abidjannordi_num = getString(R.string.txt_dr_abidjannordi_cdi_iiplat_ii_contact2);
                                    apellerDrAbidjanNordi(dr_abidjannordi_num);
                                }
                            });
                    AlertDialog alert11 = buildCall.create();
                    alert11.show();
                    return;
                }
            }
        });
        ImageView call_dr_abidjannordi_cdi_cocody = (ImageView)view.findViewById(R.id.dr_abidjannordi_cdi_cocody_call);
        call_dr_abidjannordi_cdi_cocody.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_abidjannordi_cdi_cocody_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_abidjannordi_num = getString(R.string.txt_dr_abidjannordi_cdi_cocody_contact);
                    apellerDrAbidjanNordi(dr_abidjannordi_num);
                }
            }
        });
        ImageView call_dr_abidjannordi_cdi_iiplatdjibi = (ImageView)view.findViewById(R.id.dr_abidjannordi_cdi_iiplatdjibi_call);
        call_dr_abidjannordi_cdi_iiplatdjibi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_abidjannordi_cdi_iiplatdjibi_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_abidjannordi_num = getString(R.string.txt_dr_abidjannordi_cdi_iiplatdjibi_contact);
                    apellerDrAbidjanNordi(dr_abidjannordi_num);
                }
            }
        });
        ImageView call_dr_abidjannordi_cdi_iiplati = (ImageView)view.findViewById(R.id.dr_abidjannordi_cdi_iiplati_call);
        call_dr_abidjannordi_cdi_iiplati.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_abidjannordi_cdi_iiplati_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_abidjannordi_num = getString(R.string.txt_dr_abidjannordi_cdi_iiplati_contact);
                    apellerDrAbidjanNordi(dr_abidjannordi_num);
                }
            }
        });
        ImageView call_dr_abidjannordi_cdi_iiplatiii = (ImageView)view.findViewById(R.id.dr_abidjannordi_cdi_iiplatiii_call);
        call_dr_abidjannordi_cdi_iiplatiii.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_abidjannordi_cdi_iiplatiii_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_abidjannordi_num = getString(R.string.txt_dr_abidjannordi_cdi_iiplatiii_contact);
                    apellerDrAbidjanNordi(dr_abidjannordi_num);
                }
            }
        });
        Button envoi_mail_dr_abidjannordi = (Button)view.findViewById(R.id.dr_abidjannordi_message);
        envoi_mail_dr_abidjannordi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                ecrireDirGenerale(getString(R.string.txt_dr_abidjannordi_titre),"DIRECTION REGIONALE");
            }
        });
        return view;
    }
    public void apellerDrAbidjanNordi(String dr_abidjannordi_appel)
    {
        try {
            if(Build.VERSION.SDK_INT > 22) {
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE}, 101);
                    return;
                }
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + dr_abidjannordi_appel));
                startActivity(callIntent);
            }
            else {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + dr_abidjannordi_appel));
                startActivity(callIntent);
            }
        }
        catch (Exception ex) {}
    }
    public void ecrireDirGenerale(String title, String categorie)
    {
        Intent intEcrireAuDr = new Intent(getContext(), ContactEmail.class);
        intEcrireAuDr.putExtra("dataContactTitle", title);intEcrireAuDr.putExtra("dataContactCategorie", categorie);
        intEcrireAuDr.putExtra("dataContactDestinataire", getString(R.string.txt_contact_destinataire_dran1));startActivity(intEcrireAuDr);
    }
}