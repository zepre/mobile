package com.prodesk.mdgi.DirectionsRegionale;
import android.Manifest;import android.content.Intent;
import android.content.pm.PackageManager;import android.net.Uri;
import android.os.Build;import android.os.Bundle;
import android.support.v4.app.ActivityCompat;import android.support.v4.app.Fragment;
import android.view.LayoutInflater;import android.view.View;
import android.view.ViewGroup;import android.widget.Button;
import android.widget.ImageView;import android.widget.Toast;
import com.prodesk.mdgi.ContactEmail;import com.prodesk.mdgi.R;
//** Jeud 05.09.2019 // 11.06pm **//
public class DirRegionalSanPedroVuecdi extends Fragment
{
    String dr_sanpedro_num;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_dr_sanpedro_vuecdi, container, false);Button call_dr_sanpedro = (Button)view.findViewById(R.id.dr_sanpedro_call);
        call_dr_sanpedro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_sanpedro_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_sanpedro_num = getString(R.string.txt_dr_sanpedro_contact);apellerDrSanPedro(dr_sanpedro_num);
                }
            }
        });
        ImageView call_dr_sanpedro_cdi_fresco = (ImageView)view.findViewById(R.id.dr_sanpedro_cdi_fresco_call);
        call_dr_sanpedro_cdi_fresco.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_sanpedro_cdi_fresco_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_sanpedro_num = getString(R.string.txt_dr_sanpedro_cdi_fresco_contact);apellerDrSanPedro(dr_sanpedro_num);
                }
            }
        });
        ImageView call_dr_sanpedro_cdi_meagui = (ImageView)view.findViewById(R.id.dr_sanpedro_cdi_meagui_call);
        call_dr_sanpedro_cdi_meagui.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_sanpedro_cdi_meagui_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_sanpedro_num = getString(R.string.txt_dr_sanpedro_cdi_meagui_contact);apellerDrSanPedro(dr_sanpedro_num);
                }
            }
        });
        ImageView call_dr_sanpedro_cdi_sanpedro = (ImageView)view.findViewById(R.id.dr_sanpedro_cdi_sanpedro_call);
        call_dr_sanpedro_cdi_sanpedro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_sanpedro_cdi_sanpedro_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_sanpedro_num = getString(R.string.txt_dr_sanpedro_cdi_sanpedro_contact);apellerDrSanPedro(dr_sanpedro_num);
                }
            }
        });
        ImageView call_dr_sanpedro_cdi_sassandra = (ImageView)view.findViewById(R.id.dr_sanpedro_cdi_sassandra_call);
        call_dr_sanpedro_cdi_sassandra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_sanpedro_cdi_sassandra_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_sanpedro_num = getString(R.string.txt_dr_sanpedro_cdi_sassandra_contact);apellerDrSanPedro(dr_sanpedro_num);
                }
            }
        });
        ImageView call_dr_sanpedro_cdi_soubre = (ImageView)view.findViewById(R.id.dr_sanpedro_cdi_soubre_call);
        call_dr_sanpedro_cdi_soubre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_sanpedro_cdi_soubre_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_sanpedro_num = getString(R.string.txt_dr_sanpedro_cdi_soubre_contact);apellerDrSanPedro(dr_sanpedro_num);
                }
            }
        });
        ImageView call_dr_sanpedro_cdi_tabou = (ImageView)view.findViewById(R.id.dr_sanpedro_cdi_tabou_call);
        call_dr_sanpedro_cdi_tabou.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_sanpedro_cdi_tabou_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_sanpedro_num = getString(R.string.txt_dr_sanpedro_cdi_tabou_contact);apellerDrSanPedro(dr_sanpedro_num);
                }
            }
        });
        Button envoi_mail_dr_sanpedro = (Button)view.findViewById(R.id.dr_sanpedro_message);
        envoi_mail_dr_sanpedro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                ecrireDirGenerale(getString(R.string.txt_dr_sanpedro_titre),"DIRECTION REGIONALE");
            }
        });return view;
    }
    public void apellerDrSanPedro(String dr_sanpedro_appel)
    {
        try {
            if(Build.VERSION.SDK_INT > 22) {
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE}, 101);return;
                }
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + dr_sanpedro_appel));startActivity(callIntent);
            } else {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + dr_sanpedro_appel));startActivity(callIntent);
            }
        } catch (Exception ex) {}
    }
    public void ecrireDirGenerale(String title, String categorie)
    {
        Intent intEcrireAuDr = new Intent(getContext(), ContactEmail.class);intEcrireAuDr.putExtra("dataContactTitle", title);
        intEcrireAuDr.putExtra("dataContactCategorie", categorie);intEcrireAuDr.putExtra("dataContactDestinataire", getString(R.string.txt_contact_destinataire_sanpedro));startActivity(intEcrireAuDr);
    }
}