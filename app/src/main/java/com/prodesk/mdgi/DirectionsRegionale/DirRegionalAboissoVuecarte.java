package com.prodesk.mdgi.DirectionsRegionale;
import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Color;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.prodesk.mdgi.R;
//** Sam 17.08.2019 // 12.56pm **//
public class DirRegionalAboissoVuecarte extends Fragment implements OnMapReadyCallback
{
    Double mapLat,mapLong;GoogleMap drAboissoMap;MapView drAboissoMapView;MarkerOptions cdiAboisso, cdiAdiake, cdiBonoua, cdiGrandBassam, cdiTiapoum;
    LocationManager locationManager;boolean statusOfGPS;FragmentManager fragmentManager;SupportMapFragment mFragment;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_dr_aboisso_vuecarte, container, false);
        if (!isGooglePlayServicesAvailable()) {
            Toast.makeText(getActivity(), "Service Google Play inaccessible !", Toast.LENGTH_SHORT).show();
        }
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        statusOfGPS = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);return view;
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        drAboissoMapView = (MapView)view.findViewById(R.id.dr_aboisso_map);
        if (drAboissoMapView != null) {
            drAboissoMapView.onCreate(null);drAboissoMapView.onResume();
            drAboissoMapView.getMapAsync(this);
        }
    }
    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        mapLat = Double.parseDouble(getString(R.string.txt_dr_aboisso_point_lat));mapLong = Double.parseDouble(getString(R.string.txt_dr_aboisso_point_long));
        MapsInitializer.initialize(getContext());drAboissoMap = googleMap;
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.getUiSettings().setZoomGesturesEnabled(true);googleMap.getUiSettings().setCompassEnabled(true);
        LatLng ptDrAboisso = new LatLng(mapLat,mapLong);
        LatLng ptcdiAboisso = new LatLng(Double.parseDouble(getString(R.string.txt_dr_aboisso_cdi_aboisso_lat)),Double.parseDouble(getString(R.string.txt_dr_aboisso_cdi_aboisso_long)));
        cdiAboisso = new MarkerOptions();cdiAboisso.position(ptcdiAboisso).icon(setIconColor("#c86331"));
        cdiAboisso.title(getString(R.string.txt_dr_aboisso_cdi_aboisso_titre));cdiAboisso.snippet(getString(R.string.txt_dr_aboisso_cdi_aboisso_adresse));googleMap.addMarker(cdiAboisso);
        LatLng ptcdiAdiake = new LatLng(Double.parseDouble(getString(R.string.txt_dr_aboisso_cdi_adiake_lat)),Double.parseDouble(getString(R.string.txt_dr_aboisso_cdi_adiake_long)));
        cdiAdiake = new MarkerOptions();cdiAdiake.position(ptcdiAdiake).icon(setIconColor("#c86331"));
        cdiAdiake.title(getString(R.string.txt_dr_aboisso_cdi_adiake_titre));cdiAdiake.snippet(getString(R.string.txt_dr_aboisso_cdi_adiake_adresse));googleMap.addMarker(cdiAdiake);
        LatLng ptcdiBonoua = new LatLng(Double.parseDouble(getString(R.string.txt_dr_aboisso_cdi_bonoua_lat)),Double.parseDouble(getString(R.string.txt_dr_aboisso_cdi_bonoua_long)));
        cdiBonoua = new MarkerOptions();cdiBonoua.position(ptcdiBonoua).icon(setIconColor("#c86331"));
        cdiBonoua.title(getString(R.string.txt_dr_aboisso_cdi_bonoua_titre));cdiBonoua.snippet(getString(R.string.txt_dr_aboisso_cdi_bonoua_adresse));googleMap.addMarker(cdiBonoua);
        LatLng ptcdiGrandBassam = new LatLng(Double.parseDouble(getString(R.string.txt_dr_aboisso_cdi_grandbassam_lat)),Double.parseDouble(getString(R.string.txt_dr_aboisso_cdi_grandbassam_long)));
        cdiGrandBassam = new MarkerOptions();cdiGrandBassam.position(ptcdiGrandBassam).icon(setIconColor("#c86331"));
        cdiGrandBassam.title(getString(R.string.txt_dr_aboisso_cdi_grandbassam_titre));cdiGrandBassam.snippet(getString(R.string.txt_dr_aboisso_cdi_grandbassam_adresse));googleMap.addMarker(cdiGrandBassam);
        LatLng ptcdiTiapoum = new LatLng(Double.parseDouble(getString(R.string.txt_dr_aboisso_cdi_tiapoum_lat)),Double.parseDouble(getString(R.string.txt_dr_aboisso_cdi_tiapoum_long)));
        cdiTiapoum = new MarkerOptions();cdiTiapoum.position(ptcdiTiapoum).icon(setIconColor("#c86331"));
        cdiTiapoum.title(getString(R.string.txt_dr_aboisso_cdi_tiapoum_titre));cdiTiapoum.snippet(getString(R.string.txt_dr_aboisso_cdi_tiapoum_adresse));
        googleMap.addMarker(cdiTiapoum);CameraPosition drAboissoPos = CameraPosition.builder().target(ptDrAboisso).zoom(8).bearing(0).tilt(45).build();
        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(drAboissoPos));
    }
    private boolean isGooglePlayServicesAvailable()
    {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            GooglePlayServicesUtil.getErrorDialog(status, getActivity(), 0).show();
            return false;
        }
    }
    public BitmapDescriptor setIconColor(String kol)
    {
        float[] hsv = new float[3];Color.colorToHSV(Color.parseColor(kol), hsv);
        return BitmapDescriptorFactory.defaultMarker(hsv[0]);
    }
}