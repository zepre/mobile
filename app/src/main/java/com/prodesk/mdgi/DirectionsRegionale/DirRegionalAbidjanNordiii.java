package com.prodesk.mdgi.DirectionsRegionale;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import com.prodesk.mdgi.R;
//** Sam 17.08.2019 // 12.26pm **//
public class DirRegionalAbidjanNordiii extends AppCompatActivity
{
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener()
    {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item)
        {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            switch (item.getItemId())
            {
                case R.id.nav_directionsregionales_vuecdi:
                    transaction.replace(R.id.frame_dr_abidjannordiii, new DirRegionalAbidjanNordiiiVuecdi()).commit();
                    return true;
                case R.id.nav_directionsregionales_vuecarte:
                    transaction.replace(R.id.frame_dr_abidjannordiii, new DirRegionalAbidjanNordiiiVuecarte()).commit();
                    return true;
            }
            return false;
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dr_abidjannordiii);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.nav_dr_abidjannordiii);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.frame_dr_abidjannordiii, new DirRegionalAbidjanNordiiiVuecdi()).commit();
    }
}