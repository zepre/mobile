package com.prodesk.mdgi.DirectionsRegionale;
import android.app.FragmentManager;import android.content.Context;
import android.graphics.Color;import android.location.LocationManager;
import android.os.Bundle;import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;import android.view.LayoutInflater;import android.view.View;
import android.view.ViewGroup;import android.widget.Toast;
import com.google.android.gms.common.ConnectionResult;import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;import com.google.android.gms.maps.MapsInitializer;import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;import com.google.android.gms.maps.model.MarkerOptions;import com.prodesk.mdgi.R;
//** Sam 17.08.2019 // 1.29pm **//
public class DirRegionalKorhogoVuecarte extends Fragment implements OnMapReadyCallback
{
    Double mapLat,mapLong;GoogleMap drKorhgoMap;MapView drKorhogoMapView;MarkerOptions cdiBoundiali, cdiFerkessedougou, cdiKong, cdiKorhogo, cdiMbengue, cdiOuangolodougou, cdiTingrela;
    LocationManager locationManager;boolean statusOfGPS;FragmentManager fragmentManager;SupportMapFragment mFragment;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_dr_korhogo_vuecarte, container, false);
        if (!isGooglePlayServicesAvailable()) {
            Toast.makeText(getActivity(), "Service Google Play inaccessible !", Toast.LENGTH_SHORT).show();
        }
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        statusOfGPS = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);return view;
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        drKorhogoMapView = (MapView)view.findViewById(R.id.dr_korhogo_map);
        if (drKorhogoMapView != null) {
            drKorhogoMapView.onCreate(null);drKorhogoMapView.onResume();
            drKorhogoMapView.getMapAsync(this);
        }
    }
    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        mapLat = Double.parseDouble(getString(R.string.txt_dr_korhogo_point_lat));mapLong = Double.parseDouble(getString(R.string.txt_dr_korhogo_point_long));MapsInitializer.initialize(getContext());drKorhgoMap = googleMap;
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.getUiSettings().setZoomGesturesEnabled(true);googleMap.getUiSettings().setCompassEnabled(true);
        LatLng ptDrKorhogo  = new LatLng(mapLat,mapLong);
        LatLng ptcdiBoundiali = new LatLng(Double.parseDouble(getString(R.string.txt_dr_korhogo_cdi_boundiali_lat)),Double.parseDouble(getString(R.string.txt_dr_korhogo_cdi_boundiali_long)));
        cdiBoundiali = new MarkerOptions();cdiBoundiali.position(ptcdiBoundiali).icon(setIconColor("#c86331"));
        cdiBoundiali.title(getString(R.string.txt_dr_korhogo_cdi_boundiali_titre));cdiBoundiali.snippet(getString(R.string.txt_dr_korhogo_cdi_boundiali_adresse));googleMap.addMarker(cdiBoundiali);
        LatLng ptcdiFerkessedougou = new LatLng(Double.parseDouble(getString(R.string.txt_dr_korhogo_cdi_ferkessedougou_lat)),Double.parseDouble(getString(R.string.txt_dr_korhogo_cdi_ferkessedougou_long)));
        cdiFerkessedougou = new MarkerOptions();cdiFerkessedougou.position(ptcdiFerkessedougou).icon(setIconColor("#c86331"));
        cdiFerkessedougou.title(getString(R.string.txt_dr_korhogo_cdi_ferkessedougou_titre));cdiFerkessedougou.snippet(getString(R.string.txt_dr_korhogo_cdi_ferkessedougou_adresse));googleMap.addMarker(cdiFerkessedougou);
        LatLng ptcdiKong = new LatLng(Double.parseDouble(getString(R.string.txt_dr_korhogo_cdi_kong_lat)),Double.parseDouble(getString(R.string.txt_dr_korhogo_cdi_kong_long)));
        cdiKong = new MarkerOptions();cdiKong.position(ptcdiKong).icon(setIconColor("#c86331"));
        cdiKong.title(getString(R.string.txt_dr_korhogo_cdi_kong_titre));cdiKong.snippet(getString(R.string.txt_dr_korhogo_cdi_kong_adresse));googleMap.addMarker(cdiKong);
        LatLng ptcdiKorhogo = new LatLng(Double.parseDouble(getString(R.string.txt_dr_korhogo_cdi_korhogo_lat)),Double.parseDouble(getString(R.string.txt_dr_korhogo_cdi_korhogo_long)));
        cdiKorhogo = new MarkerOptions();cdiKorhogo.position(ptcdiKorhogo).icon(setIconColor("#c86331"));
        cdiKorhogo.title(getString(R.string.txt_dr_korhogo_cdi_korhogo_titre));cdiKorhogo.snippet(getString(R.string.txt_dr_korhogo_cdi_korhogo_adresse));googleMap.addMarker(cdiKorhogo);
        LatLng ptcdiMbengue = new LatLng(Double.parseDouble(getString(R.string.txt_dr_korhogo_cdi_mbengue_lat)),Double.parseDouble(getString(R.string.txt_dr_korhogo_cdi_mbengue_long)));
        cdiMbengue = new MarkerOptions();cdiMbengue.position(ptcdiMbengue).icon(setIconColor("#c86331"));
        cdiMbengue.title(getString(R.string.txt_dr_korhogo_cdi_mbengue_titre));cdiMbengue.snippet(getString(R.string.txt_dr_korhogo_cdi_mbengue_adresse));googleMap.addMarker(cdiMbengue);
        LatLng ptcdiOuangolodougou = new LatLng(Double.parseDouble(getString(R.string.txt_dr_korhogo_cdi_ouangolodoudou_lat)),Double.parseDouble(getString(R.string.txt_dr_korhogo_cdi_ouangolodougou_long)));
        cdiOuangolodougou = new MarkerOptions();cdiOuangolodougou.position(ptcdiOuangolodougou).icon(setIconColor("#c86331"));
        cdiOuangolodougou.title(getString(R.string.txt_dr_korhogo_cdi_ouangolodougou_titre));cdiOuangolodougou.snippet(getString(R.string.txt_dr_korhogo_cdi_ouangolodougou_adresse));googleMap.addMarker(cdiOuangolodougou);
        LatLng ptcdiTingrela = new LatLng(Double.parseDouble(getString(R.string.txt_dr_korhogo_cdi_tingrela_lat)),Double.parseDouble(getString(R.string.txt_dr_korhogo_cdi_tingrela_long)));
        cdiTingrela = new MarkerOptions();cdiTingrela.position(ptcdiTingrela).icon(setIconColor("#c86331"));
        cdiTingrela.title(getString(R.string.txt_dr_korhogo_cdi_tingrela_titre));cdiTingrela.snippet(getString(R.string.txt_dr_korhogo_cdi_tingrela_adresse));
        googleMap.addMarker(cdiTingrela);CameraPosition drKorhogoPos = CameraPosition.builder().target(ptDrKorhogo).zoom(7).bearing(0).tilt(45).build();
        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(drKorhogoPos));
    }
    private boolean isGooglePlayServicesAvailable()
    {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            GooglePlayServicesUtil.getErrorDialog(status, getActivity(), 0).show();
            return false;
        }
    }
    public BitmapDescriptor setIconColor(String kol)
    {
        float[] hsv = new float[3];Color.colorToHSV(Color.parseColor(kol), hsv);return BitmapDescriptorFactory.defaultMarker(hsv[0]);
    }
}