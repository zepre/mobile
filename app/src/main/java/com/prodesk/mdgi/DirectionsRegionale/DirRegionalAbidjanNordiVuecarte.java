package com.prodesk.mdgi.DirectionsRegionale;
import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Color;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.prodesk.mdgi.R;
//** Sam 17.08.2019 // 12.34pm **//
public class DirRegionalAbidjanNordiVuecarte extends Fragment implements OnMapReadyCallback
{
    Double mapLat,mapLong;GoogleMap drAbidjanNordiMap;MapView drAbidjanNordiMapView;
    MarkerOptions cdiCocody, cdiIIPlatDjibi, cdiIIPlatI, cdiIIPlatII, cdiIIPlatIII;
    LocationManager locationManager;boolean statusOfGPS;FragmentManager fragmentManager;SupportMapFragment mFragment;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_dr_abidjannordi_vuecarte, container, false);
        if (!isGooglePlayServicesAvailable()) {
            Toast.makeText(getActivity(), "Service Google Play inaccessible !", Toast.LENGTH_SHORT).show();
        }
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        statusOfGPS = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        return view;
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        drAbidjanNordiMapView = (MapView)view.findViewById(R.id.dr_abidjannordi_map);
        if (drAbidjanNordiMapView != null) {
            drAbidjanNordiMapView.onCreate(null);drAbidjanNordiMapView.onResume();
            drAbidjanNordiMapView.getMapAsync(this);
        }
    }
    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        mapLat = Double.parseDouble(getString(R.string.txt_dr_abidjannordi_point_lat));mapLong = Double.parseDouble(getString(R.string.txt_dr_abidjannordi_point_long));MapsInitializer.initialize(getContext());drAbidjanNordiMap = googleMap;
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.getUiSettings().setZoomControlsEnabled(true);googleMap.getUiSettings().setZoomGesturesEnabled(true);googleMap.getUiSettings().setCompassEnabled(true);
        LatLng ptDrAbNi = new LatLng(mapLat,mapLong);
        LatLng ptcdiCocody = new LatLng(Double.parseDouble(getString(R.string.txt_dr_abidjannordi_cdi_cocody_lat)),Double.parseDouble(getString(R.string.txt_dr_abidjannordi_cdi_cocody_long)));
        cdiCocody = new MarkerOptions();
        cdiCocody.position(ptcdiCocody).icon(setIconColor("#c86331"));cdiCocody.title(getString(R.string.txt_dr_abidjannordi_cdi_cocody_titre));
        cdiCocody.snippet(getString(R.string.txt_dr_abidjannordi_cdi_cocody_adresse));googleMap.addMarker(cdiCocody);
        LatLng ptcdiIIPlatI = new LatLng(Double.parseDouble(getString(R.string.txt_dr_abidjannordi_cdi_iiplati_lat)),Double.parseDouble(getString(R.string.txt_dr_abidjannordi_cdi_iiplati_long)));
        cdiIIPlatI = new MarkerOptions();
        cdiIIPlatI.position(ptcdiIIPlatI).icon(setIconColor("#c86331"));cdiIIPlatI.title(getString(R.string.txt_dr_abidjannordi_cdi_iiplati_titre));
        //cdiIIPlatI.snippet(getString(R.string.txt_dr_abidjannordi_cdi_iiplati_contact));
        cdiIIPlatI.snippet(getString(R.string.txt_dr_abidjannordi_cdi_iiplati_adresse));googleMap.addMarker(cdiIIPlatI);
        LatLng ptcdiIIPlatII = new LatLng(Double.parseDouble(getString(R.string.txt_dr_abidjannordi_cdi_iiplatii_lat)),Double.parseDouble(getString(R.string.txt_dr_abidjannordi_cdi_iiplatii_long)));
        cdiIIPlatII = new MarkerOptions();
        cdiIIPlatII.position(ptcdiIIPlatII).icon(setIconColor("#c86331"));cdiIIPlatII.title(getString(R.string.txt_dr_abidjannordi_cdi_iiplat_ii_titre));
        //cdiIIPlatII.snippet(getString(R.string.txt_dr_abidjannordi_cdi_iiplat_ii_contact1)+" | "+getString(R.string.txt_dr_abidjannordi_cdi_iiplat_ii_contact2));
        cdiIIPlatII.snippet(getString(R.string.txt_dr_abidjannordi_cdi_iiplat_ii_adresse));googleMap.addMarker(cdiIIPlatII);
        LatLng ptcdiIIPlatIII = new LatLng(Double.parseDouble(getString(R.string.txt_dr_abidjannordi_cdi_iiplatiii_lat)),Double.parseDouble(getString(R.string.txt_dr_abidjannordi_cdi_iiplatiii_long)));
        cdiIIPlatIII = new MarkerOptions();
        cdiIIPlatIII.position(ptcdiIIPlatIII).icon(setIconColor("#c86331"));cdiIIPlatIII.title(getString(R.string.txt_dr_abidjannordi_cdi_iiplatiii_titre));
        //cdiIIPlatIII.snippet(getString(R.string.txt_dr_abidjannordi_cdi_iiplatiii_contact));
        cdiIIPlatIII.snippet(getString(R.string.txt_dr_abidjannordi_cdi_iiplatiii_adresse));googleMap.addMarker(cdiIIPlatIII);
        LatLng ptcdiIIPlatDjibi = new LatLng(Double.parseDouble(getString(R.string.txt_dr_abidjannordi_cdi_iiplatdjibi_lat)),Double.parseDouble(getString(R.string.txt_dr_abidjannordi_cdi_iiplatdjibi_long)));
        cdiIIPlatDjibi = new MarkerOptions();
        cdiIIPlatDjibi.position(ptcdiIIPlatDjibi).icon(setIconColor("#c86331"));cdiIIPlatDjibi.title(getString(R.string.txt_dr_abidjannordi_cdi_iiplatdjibi_titre));
        //cdiIIPlatDjibi.snippet(getString(R.string.txt_dr_abidjannordi_cdi_iiplatdjibi_contact));
        cdiIIPlatDjibi.snippet(getString(R.string.txt_dr_abidjannordi_cdi_iiplatdjibi_adresse));googleMap.addMarker(cdiIIPlatDjibi);
        CameraPosition drAbNiPos = CameraPosition.builder().target(ptDrAbNi).zoom(13).bearing(0).tilt(45).build();
        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(drAbNiPos));
    }
    private boolean isGooglePlayServicesAvailable()
    {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            GooglePlayServicesUtil.getErrorDialog(status, getActivity(), 0).show();
            return false;
        }
    }
    public BitmapDescriptor setIconColor(String kol)
    {
        float[] hsv = new float[3];Color.colorToHSV(Color.parseColor(kol), hsv);
        return BitmapDescriptorFactory.defaultMarker(hsv[0]);
    }
}