package com.prodesk.mdgi.DirectionsRegionale;
import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Color;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.prodesk.mdgi.R;
//** Sam 17.08.2019 // 12.41pm **//
public class DirRegionalAbidjanNordviVuecarte extends Fragment implements OnMapReadyCallback
{
    Double mapLat,mapLong;GoogleMap drAbidjanNordviMap;MapView drAbidjanNordviMapView;MarkerOptions cdiBingerville, cdiRiviera;LocationManager locationManager;
    boolean statusOfGPS;FragmentManager fragmentManager;SupportMapFragment mFragment;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_dr_abidjannordvi_vuecarte, container, false);
        if (!isGooglePlayServicesAvailable()) {
            Toast.makeText(getActivity(), "Service Google Play inaccessible !", Toast.LENGTH_SHORT).show();
        }
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        statusOfGPS = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        return view;
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        drAbidjanNordviMapView = (MapView)view.findViewById(R.id.dr_abidjannordvi_map);
        if (drAbidjanNordviMapView != null) {
            drAbidjanNordviMapView.onCreate(null);drAbidjanNordviMapView.onResume();
            drAbidjanNordviMapView.getMapAsync(this);
        }
    }
    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        mapLat = Double.parseDouble(getString(R.string.txt_dr_abidjannordvi_point_lat));mapLong = Double.parseDouble(getString(R.string.txt_dr_abidjannordvi_point_long));MapsInitializer.initialize(getContext());
        drAbidjanNordviMap = googleMap;googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.getUiSettings().setZoomControlsEnabled(true);googleMap.getUiSettings().setZoomGesturesEnabled(true);googleMap.getUiSettings().setCompassEnabled(true);
        LatLng ptDrAbNvi = new LatLng(mapLat,mapLong);
        LatLng ptcdiBingerville = new LatLng(Double.parseDouble(getString(R.string.txt_dr_abidjannordvi_cdi_bingerville_lat)),Double.parseDouble(getString(R.string.txt_dr_abidjannordvi_cdi_bingerville_long)));
        cdiBingerville = new MarkerOptions();cdiBingerville.position(ptcdiBingerville).icon(setIconColor("#c86331"));cdiBingerville.title(getString(R.string.txt_dr_abidjannordvi_cdi_bingerville_titre));
        cdiBingerville.snippet(getString(R.string.txt_dr_abidjannordvi_cdi_bingerville_adresse));googleMap.addMarker(cdiBingerville);
        LatLng ptcdiRiviera = new LatLng(Double.parseDouble(getString(R.string.txt_dr_abidjannordvi_cdi_riviera_lat)),Double.parseDouble(getString(R.string.txt_dr_abidjannordvi_cdi_riviera_long)));cdiRiviera = new MarkerOptions();
        cdiRiviera.position(ptcdiRiviera).icon(setIconColor("#c86331"));cdiRiviera.title(getString(R.string.txt_dr_abidjannordvi_cdi_riviera_title));
        cdiRiviera.snippet(getString(R.string.txt_dr_abidjannordvi_cdi_rivierai_adresse));googleMap.addMarker(cdiRiviera);
        CameraPosition drAbNviPos = CameraPosition.builder().target(ptDrAbNvi).zoom(10).bearing(0).tilt(45).build();
        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(drAbNviPos));
    }
    private boolean isGooglePlayServicesAvailable()
    {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            GooglePlayServicesUtil.getErrorDialog(status, getActivity(), 0).show();
            return false;
        }
    }
    public BitmapDescriptor setIconColor(String kol)
    {
        float[] hsv = new float[3];Color.colorToHSV(Color.parseColor(kol), hsv);
        return BitmapDescriptorFactory.defaultMarker(hsv[0]);
    }
}