package com.prodesk.mdgi.DirectionsRegionale;
import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;
import com.prodesk.mdgi.ContactEmail;
import com.prodesk.mdgi.R;
//** Jeud 05.09.2019 // 9.52pm **//
public class DirRegionalAbidjanNordivVuecdi extends Fragment
{
    String dr_abidjannordiv_num;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_dr_abidjannordiv_vuecdi, container, false);
        Button call_dr_abidjannordiv = (Button)view.findViewById(R.id.dr_abidjannordiv_call);
        call_dr_abidjannordiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                AlertDialog.Builder buildCall = new AlertDialog.Builder(getContext());
                buildCall.setMessage("Veuillez choisir le numéro à appeler !");
                buildCall.setCancelable(true);
                buildCall.setPositiveButton(
                        getString(R.string.txt_dr_abidjannordiv_contact1),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dr_abidjannordiv_num = getString(R.string.txt_dr_abidjannordiv_contact1);
                                apellerDrAbidjanNordiv(dr_abidjannordiv_num);
                            }
                        });

                buildCall.setNegativeButton(
                        getString(R.string.txt_dr_abidjannordiv_contact2),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dr_abidjannordiv_num = getString(R.string.txt_dr_abidjannordiv_contact2);
                                apellerDrAbidjanNordiv(dr_abidjannordiv_num);
                            }
                        });
                AlertDialog alert11 = buildCall.create();
                alert11.show();
            }
        });
        // apeller dr Abidjan Nord iv cdi Yopougon i
        ImageView call_dr_abidjannordiv_cdi_yopi = (ImageView)view.findViewById(R.id.dr_abidjannordiv_cdi_yopi_call);
        call_dr_abidjannordiv_cdi_yopi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_abidjannordiv_cdi_yopougoni_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_abidjannordiv_num = getString(R.string.txt_dr_abidjannordiv_cdi_yopougoni_contact);
                    apellerDrAbidjanNordiv(dr_abidjannordiv_num);
                }
            }
        });
        // apeller dr Abidjan Nord iv cdi Yopougon ii
        ImageView call_dr_abidjannordiv_cdi_yopii = (ImageView)view.findViewById(R.id.dr_abidjannordiv_cdi_yopii_call);
        call_dr_abidjannordiv_cdi_yopii.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_abidjannordiv_cdi_yopougonii_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_abidjannordiv_num = getString(R.string.txt_dr_abidjannordiv_cdi_yopougonii_contact);
                    apellerDrAbidjanNordiv(dr_abidjannordiv_num);
                }
            }
        });
        // envoyer mail dr Abidjan Nord iv
        Button envoi_mail_dr_abidjannordiv = (Button)view.findViewById(R.id.dr_abidjannordiv_message);
        envoi_mail_dr_abidjannordiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                ecrireDirGenerale(getString(R.string.txt_dr_abidjannordiv_titre),"DIRECTION REGIONALE");
            }
        });
        return view;
    }
    public void apellerDrAbidjanNordiv(String dr_abidjannordiv_appel)
    {
        try {
            if(Build.VERSION.SDK_INT > 22) {
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE}, 101);
                    return;
                }
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + dr_abidjannordiv_appel));
                startActivity(callIntent);
            }
            else {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + dr_abidjannordiv_appel));
                startActivity(callIntent);
            }
        }
        catch (Exception ex) {}
    }
    public void ecrireDirGenerale(String title, String categorie)
    {
        Intent intEcrireAuDr = new Intent(getContext(), ContactEmail.class);intEcrireAuDr.putExtra("dataContactTitle", title);
        intEcrireAuDr.putExtra("dataContactCategorie", categorie);intEcrireAuDr.putExtra("dataContactDestinataire", getString(R.string.txt_contact_destinataire_dran4));startActivity(intEcrireAuDr);
    }
}