package com.prodesk.mdgi.DirectionsRegionale;
import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Color;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.prodesk.mdgi.R;
//** Sam 17.08.2019 // 1.06pm **//
public class DirRegionalBouakeVuecarte extends Fragment implements OnMapReadyCallback
{
    Double mapLat,mapLong;GoogleMap drBouakeMap;MapView drBouakeMapView;MarkerOptions cdiBouake, cdiBeoumi, cdiDabakala, cdiKatiola;LocationManager locationManager;
    boolean statusOfGPS;FragmentManager fragmentManager;SupportMapFragment mFragment;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_dr_bouake_vuecarte, container, false);
        if (!isGooglePlayServicesAvailable()) {
            Toast.makeText(getActivity(), "Service Google Play inaccessible !", Toast.LENGTH_SHORT).show();
        }
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        statusOfGPS = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);return view;
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        drBouakeMapView = (MapView)view.findViewById(R.id.dr_bouake_map);
        if (drBouakeMapView != null) {
            drBouakeMapView.onCreate(null);drBouakeMapView.onResume();
            drBouakeMapView.getMapAsync(this);
        }
    }
    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        mapLat = Double.parseDouble(getString(R.string.txt_dr_bouake_lat));mapLong = Double.parseDouble(getString(R.string.txt_dr_bouake_long));MapsInitializer.initialize(getContext());
        drBouakeMap = googleMap;
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.getUiSettings().setZoomGesturesEnabled(true);googleMap.getUiSettings().setCompassEnabled(true);
        LatLng ptDrBouake = new LatLng(mapLat,mapLong);
        LatLng ptcdiBouake = new LatLng(Double.parseDouble(getString(R.string.txt_dr_bouake_cdi_bouake_lat)),Double.parseDouble(getString(R.string.txt_dr_bouake_cdi_bouake_long)));
        cdiBouake = new MarkerOptions();cdiBouake.position(ptcdiBouake).icon(setIconColor("#c86331"));
        cdiBouake.title(getString(R.string.txt_dr_bouake_cdi_bouake_title));cdiBouake.snippet(getString(R.string.txt_dr_bouake_cdi_bouakei_adresse));googleMap.addMarker(cdiBouake);
        LatLng ptcdiBeoumi = new LatLng(Double.parseDouble(getString(R.string.txt_dr_bouake_cdi_beoumi_lat)),Double.parseDouble(getString(R.string.txt_dr_bouake_cdi_beoumi_long)));
        cdiBeoumi = new MarkerOptions();cdiBeoumi.position(ptcdiBeoumi).icon(setIconColor("#c86331"));cdiBeoumi.title(getString(R.string.txt_dr_bouake_cdi_beoumi_titre));
        cdiBeoumi.snippet(getString(R.string.txt_dr_bouake_cdi_beoumi_adresse));googleMap.addMarker(cdiBeoumi);
        LatLng ptcdiDabakala = new LatLng(Double.parseDouble(getString(R.string.txt_dr_bouake_cdi_dabakala_lat)),Double.parseDouble(getString(R.string.txt_dr_bouake_cdi_dabakala_long)));
        cdiDabakala = new MarkerOptions();cdiDabakala.position(ptcdiDabakala).icon(setIconColor("#c86331"));cdiDabakala.title(getString(R.string.txt_dr_bouake_cdi_dabakala_titre));
        cdiDabakala.snippet(getString(R.string.txt_dr_bouake_cdi_dabakala_adresse));googleMap.addMarker(cdiDabakala);
        LatLng ptcdiKatiola = new LatLng(Double.parseDouble(getString(R.string.txt_dr_bouake_cdi_katiola_lat)),Double.parseDouble(getString(R.string.txt_dr_bouake_cdi_katiola_long)));
        cdiKatiola = new MarkerOptions();cdiKatiola.position(ptcdiKatiola).icon(setIconColor("#c86331"));cdiKatiola.title(getString(R.string.txt_dr_bouake_cdi_katiola_titre));
        cdiKatiola.snippet(getString(R.string.txt_dr_bouake_cdi_katiola_adresse));googleMap.addMarker(cdiKatiola);
        CameraPosition drBouakePos = CameraPosition.builder().target(ptDrBouake).zoom(8).bearing(0).tilt(45).build();
        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(drBouakePos));
    }
    private boolean isGooglePlayServicesAvailable()
    {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            GooglePlayServicesUtil.getErrorDialog(status, getActivity(), 0).show();
            return false;
        }
    }
    public BitmapDescriptor setIconColor(String kol)
    {
        float[] hsv = new float[3];Color.colorToHSV(Color.parseColor(kol), hsv);
        return BitmapDescriptorFactory.defaultMarker(hsv[0]);
    }
}