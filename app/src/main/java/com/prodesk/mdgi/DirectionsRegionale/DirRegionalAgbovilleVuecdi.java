package com.prodesk.mdgi.DirectionsRegionale;
import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;
import com.prodesk.mdgi.ContactEmail;
import com.prodesk.mdgi.R;
//** Jeud 05.09.2019 // 10.13pm **//
public class DirRegionalAgbovilleVuecdi extends Fragment
{
    public String dr_agboville_num;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_dr_agboville_vuecdi, container, false);
        Button call_dr_agbovillle = (Button)view.findViewById(R.id.dr_agboville_call);
        call_dr_agbovillle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                dr_agboville_num = getString(R.string.txt_dr_agboville_contact);apellerDrAgbovile(dr_agboville_num);
            }
        });
        ImageView call_dr_agboville_cdi_adzope = (ImageView)view.findViewById(R.id.dr_agboville_cdi_adzope_call);
        call_dr_agboville_cdi_adzope.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_agboville_cdi_adzope_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_agboville_num = getString(R.string.txt_dr_agboville_cdi_adzope_contact);apellerDrAgbovile(dr_agboville_num);
                }
            }
        });
        ImageView call_dr_agboville_cdi_agboville = (ImageView)view.findViewById(R.id.dr_agboville_cdi_adzope_call);
        call_dr_agboville_cdi_agboville.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_agboville_cdi_agboville_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_agboville_num = getString(R.string.txt_dr_agboville_cdi_agboville_contact);apellerDrAgbovile(dr_agboville_num);
                }
            }
        });
        ImageView call_dr_agboville_cdi_akoupe = (ImageView)view.findViewById(R.id.dr_agboville_cdi_akoupe_call);
        call_dr_agboville_cdi_akoupe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_agboville_cdi_akoupe_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_agboville_num = getString(R.string.txt_dr_agboville_cdi_akoupe_contact);apellerDrAgbovile(dr_agboville_num);
                }
            }
        });
        ImageView call_dr_agboville_cdi_taabo = (ImageView)view.findViewById(R.id.dr_agboville_cdi_taabo_call);
        call_dr_agboville_cdi_taabo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_agboville_cdi_taabo_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_agboville_num = getString(R.string.txt_dr_agboville_cdi_taabo_contact);apellerDrAgbovile(dr_agboville_num);
                }
            }
        });
        ImageView call_dr_agboville_cdi_tiassale = (ImageView)view.findViewById(R.id.dr_agboville_cdi_tiassale_call);
        call_dr_agboville_cdi_tiassale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_agboville_cdi_tiassale_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_agboville_num = getString(R.string.txt_dr_agboville_cdi_tiassale_contact);apellerDrAgbovile(dr_agboville_num);
                }
            }
        });
        ImageView call_dr_agboville_cdi_yakasse = (ImageView)view.findViewById(R.id.dr_agboville_cdi_yakasse_call);
        call_dr_agboville_cdi_yakasse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_agboville_cdi_yakasse_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_agboville_num = getString(R.string.txt_dr_agboville_cdi_yakasse_contact);apellerDrAgbovile(dr_agboville_num);
                }
            }
        });
        Button envoi_mail_dr_agbovillle = (Button)view.findViewById(R.id.dr_agboville_message);
        envoi_mail_dr_agbovillle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                ecrireDirGenerale(getString(R.string.txt_dr_agboville_titre),"DIRECTION REGIONALE");
            }
        });return view;
    }
    public void apellerDrAgbovile(String dr_agboville_appel)
    {
        try {
            if(Build.VERSION.SDK_INT > 22) {
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE}, 101);
                    return;
                }
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + dr_agboville_appel));startActivity(callIntent);
            } else {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + dr_agboville_appel));startActivity(callIntent);
            }
        } catch (Exception ex) {}
    }
    public void ecrireDirGenerale(String title, String categorie)
    {
        Intent intEcrireAuDr = new Intent(getContext(), ContactEmail.class);intEcrireAuDr.putExtra("dataContactTitle", title);
        intEcrireAuDr.putExtra("dataContactCategorie", categorie);intEcrireAuDr.putExtra("dataContactDestinataire", getString(R.string.txt_contact_destinataire_agboville));startActivity(intEcrireAuDr);
    }
}