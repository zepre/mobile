package com.prodesk.mdgi.DirectionsRegionale;
import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Color;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.prodesk.mdgi.R;
//** Sam 17.08.2019 // 1.32pm **//
public class DirRegionalManVuecarte extends Fragment implements OnMapReadyCallback
{
    Double mapLat,mapLong;GoogleMap drManMap;MapView drManMapView;MarkerOptions cdiBangolo, cdiBiankouma, cdiDanane, cdiMan;LocationManager locationManager;boolean statusOfGPS;
    FragmentManager fragmentManager;SupportMapFragment mFragment;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_dr_man_vuecarte, container, false);
        if (!isGooglePlayServicesAvailable()) {
            Toast.makeText(getActivity(), "Service Google Play inaccessible !", Toast.LENGTH_SHORT).show();
        }
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        statusOfGPS = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);return view;
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        drManMapView = (MapView)view.findViewById(R.id.dr_man_map);
        if (drManMapView != null) {
            drManMapView.onCreate(null);drManMapView.onResume();
            drManMapView.getMapAsync(this);
        }
    }
    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        mapLat = Double.parseDouble(getString(R.string.txt_dr_man_point_lat));mapLong = Double.parseDouble(getString(R.string.txt_dr_man_point_long));MapsInitializer.initialize(getContext());
        drManMap = googleMap;googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.getUiSettings().setZoomControlsEnabled(true);googleMap.getUiSettings().setZoomGesturesEnabled(true);googleMap.getUiSettings().setCompassEnabled(true);
        LatLng ptDrMan = new LatLng(mapLat,mapLong);
        LatLng ptcdiBangolo = new LatLng(Double.parseDouble(getString(R.string.txt_dr_man_cdi_bangolo_lat)),Double.parseDouble(getString(R.string.txt_dr_man_cdi_bangolo_long)));
        cdiBangolo = new MarkerOptions();cdiBangolo.position(ptcdiBangolo).icon(setIconColor("#c86331"));
        cdiBangolo.title(getString(R.string.txt_dr_man_cdi_bangolo_titre));cdiBangolo.snippet(getString(R.string.txt_dr_man_cdi_bangolo_adresse));googleMap.addMarker(cdiBangolo);
        LatLng ptcdiBiankouma = new LatLng(Double.parseDouble(getString(R.string.txt_dr_man_cdi_biankouma_lat)),Double.parseDouble(getString(R.string.txt_dr_man_cdi_biankouma_long)));
        cdiBiankouma = new MarkerOptions();cdiBiankouma.position(ptcdiBiankouma).icon(setIconColor("#c86331"));
        cdiBiankouma.title(getString(R.string.txt_dr_man_cdi_biankouma_titre));cdiBiankouma.snippet(getString(R.string.txt_dr_man_cdi_biankouma_adresse));googleMap.addMarker(cdiBiankouma);
        LatLng ptcdiDanane = new LatLng(Double.parseDouble(getString(R.string.txt_dr_man_cdi_danane_lat)),Double.parseDouble(getString(R.string.txt_dr_man_cdi_danane_long)));
        cdiDanane = new MarkerOptions();cdiDanane.position(ptcdiDanane).icon(setIconColor("#c86331"));
        cdiDanane.title(getString(R.string.txt_dr_man_cdi_danane_titre));cdiDanane.snippet(getString(R.string.txt_dr_man_cdi_danane_adresse));googleMap.addMarker(cdiDanane);
        LatLng ptcdiMan = new LatLng(Double.parseDouble(getString(R.string.txt_dr_man_cdi_man_lat)),Double.parseDouble(getString(R.string.txt_dr_man_cdi_man_long)));
        cdiMan = new MarkerOptions();cdiMan.position(ptcdiMan).icon(setIconColor("#c86331"));
        cdiMan.title(getString(R.string.txt_dr_man_cdi_man_titre));cdiMan.snippet(getString(R.string.txt_dr_man_cdi_man_adresse));googleMap.addMarker(cdiMan);
        CameraPosition drManPos = CameraPosition.builder().target(ptDrMan).zoom(8).bearing(0).tilt(45).build();googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(drManPos));
    }
    private boolean isGooglePlayServicesAvailable()
    {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            GooglePlayServicesUtil.getErrorDialog(status, getActivity(), 0).show();return false;
        }
    }
    public BitmapDescriptor setIconColor(String kol)
    {
        float[] hsv = new float[3];Color.colorToHSV(Color.parseColor(kol), hsv);
        return BitmapDescriptorFactory.defaultMarker(hsv[0]);
    }
}