package com.prodesk.mdgi.DirectionsRegionale;
import android.Manifest;import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;import android.content.Intent;
import android.content.pm.PackageManager;import android.net.Uri;
import android.os.Build;import android.os.Bundle;
import android.view.LayoutInflater;import android.view.View;
import android.view.ViewGroup;import android.widget.Button;
import android.widget.ImageView;import android.widget.Toast;
import com.prodesk.mdgi.ContactEmail;import com.prodesk.mdgi.R;
//** Jeud 05.09.2019 // 10.18pm **//
public class DirRegionalDabouVuecdi extends Fragment
{
    public String dr_dabou_num;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_dr_dabou_vuecdi, container, false);Button call_dr_dabou = (Button)view.findViewById(R.id.dr_dabou_call);
        call_dr_dabou.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                dr_dabou_num = getString(R.string.txt_dr_dabou_contact);apellerDrDabou(dr_dabou_num);
            }
        });
        ImageView call_dr_dabou_cdi_dabou = (ImageView)view.findViewById(R.id.dr_dabou_cdi_dabou_call);
        call_dr_dabou_cdi_dabou.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_dabou_cdi_dabou_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_dabou_num = getString(R.string.txt_dr_dabou_cdi_dabou_contact);apellerDrDabou(dr_dabou_num);
                }
            }
        });
        ImageView call_dr_dabou_cdi_grandlahou = (ImageView)view.findViewById(R.id.dr_dabou_cdi_grandlahou_call);
        call_dr_dabou_cdi_grandlahou.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_dabou_cdi_grandlahou_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_dabou_num = getString(R.string.txt_dr_dabou_cdi_grandlahou_contact);apellerDrDabou(dr_dabou_num);
                }
            }
        });
        ImageView call_dr_dabou_cdi_jacqueville = (ImageView)view.findViewById(R.id.dr_dabou_cdi_jacqueville_call);
        call_dr_dabou_cdi_jacqueville.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_dabou_cdi_jacqueville_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_dabou_num = getString(R.string.txt_dr_dabou_cdi_jacqueville_contact);apellerDrDabou(dr_dabou_num);
                }
            }
        });
        ImageView call_dr_dabou_cdi_sikensi = (ImageView)view.findViewById(R.id.dr_dabou_cdi_sikensi_call);
        call_dr_dabou_cdi_sikensi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_dabou_cdi_sikensi_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_dabou_num = getString(R.string.txt_dr_dabou_cdi_sikensi_contact);apellerDrDabou(dr_dabou_num);
                }
            }
        });
        Button envoi_mail_dr_dabou = (Button)view.findViewById(R.id.dr_dabou_message);
        envoi_mail_dr_dabou.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                ecrireDirGenerale(getString(R.string.txt_dr_dabou_titre),"DIRECTION REGIONALE");
            }
        });return view;
    }
    public void apellerDrDabou(String dr_dabou_appel)
    {
        try {
            if(Build.VERSION.SDK_INT > 22) {
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE}, 101);return;
                }
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + dr_dabou_appel));startActivity(callIntent);
            }
            else {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + dr_dabou_appel));startActivity(callIntent);
            }
        }
        catch (Exception ex) { }
    }
    public void ecrireDirGenerale(String title, String categorie)
    {
        Intent intEcrireAuDr = new Intent(getContext(), ContactEmail.class);
        intEcrireAuDr.putExtra("dataContactTitle", title);intEcrireAuDr.putExtra("dataContactCategorie", categorie);
        intEcrireAuDr.putExtra("dataContactDestinataire", getString(R.string.txt_contact_destinataire_dabou));startActivity(intEcrireAuDr);
    }
}