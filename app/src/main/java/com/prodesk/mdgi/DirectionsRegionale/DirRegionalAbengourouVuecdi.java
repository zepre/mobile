package com.prodesk.mdgi.DirectionsRegionale;
import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;
import com.prodesk.mdgi.ContactEmail;
import com.prodesk.mdgi.R;
//** Jeud 05.09.2019 // 9.31pm **//
public class DirRegionalAbengourouVuecdi extends Fragment
{
    String dr_abengourou_num;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_dr_abengourou_vuecdi, container, false);
        Button call_dr_abengourou = (Button)view.findViewById(R.id.dr_abengourou_call);
        call_dr_abengourou.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                dr_abengourou_num = getString(R.string.txt_dr_abengourou_contact);
                apellerDrAbengourou(dr_abengourou_num);
            }
        });
        ImageView call_dr_abengourou_cdi_betie = (ImageView)view.findViewById(R.id.dr_abengourou_cdi_betie_call);
        call_dr_abengourou_cdi_betie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_abengourou_cdi_betie_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_abengourou_num = getString(R.string.txt_dr_abengourou_cdi_betie_contact);
                    apellerDrAbengourou(dr_abengourou_num);
                }
            }
        });
        ImageView call_dr_abengourou_cdi_abengourou = (ImageView)view.findViewById(R.id.dr_abengourou_cdi_abengourou_call);
        call_dr_abengourou_cdi_abengourou.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_abengourou_cdi_abengourou_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_abengourou_num = getString(R.string.txt_dr_abengourou_cdi_abengourou_contact);
                    apellerDrAbengourou(dr_abengourou_num);
                }
            }
        });
        ImageView call_dr_abengourou_cdi_agnibilekro = (ImageView)view.findViewById(R.id.dr_abengourou_cdi_agnibilekro_call);
        call_dr_abengourou_cdi_agnibilekro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_abengourou_cdi_agnibilekro_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_abengourou_num = getString(R.string.txt_dr_abengourou_cdi_agnibilekro_contact);
                    apellerDrAbengourou(dr_abengourou_num);
                }
            }
        });
        ImageView call_dr_abengourou_cdi_niable = (ImageView)view.findViewById(R.id.dr_abengourou_cdi_niable_call);
        call_dr_abengourou_cdi_niable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_abengourou_cdi_niable_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_abengourou_num = getString(R.string.txt_dr_abengourou_cdi_niable_contact);
                    apellerDrAbengourou(dr_abengourou_num);
                }
            }
        });
        Button envoi_mail_dr_abengourou = (Button)view.findViewById(R.id.dr_abengourou_message);
        envoi_mail_dr_abengourou.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                ecrireDirGenerale(getString(R.string.txt_dr_abengourou_titre),"DIRECTION REGIONALE");
            }
        });
        return view;
    }
    public void apellerDrAbengourou(String dr_abengourou_appel)
    {
        try {
            if(Build.VERSION.SDK_INT > 22) {
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE}, 101);
                    return;
                }
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + dr_abengourou_appel));
                startActivity(callIntent);
            }
            else {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + dr_abengourou_appel));
                startActivity(callIntent);
            }
        }
        catch (Exception ex) {}
    }
    public void ecrireDirGenerale(String title, String categorie)
    {
        Intent intEcrireAuDr = new Intent(getContext(), ContactEmail.class);
        intEcrireAuDr.putExtra("dataContactTitle", title);intEcrireAuDr.putExtra("dataContactCategorie", categorie);
        intEcrireAuDr.putExtra("dataContactDestinataire", getString(R.string.txt_contact_destinataire_abengourou));startActivity(intEcrireAuDr);
    }
}