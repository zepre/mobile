package com.prodesk.mdgi.DirectionsRegionale;
import android.Manifest;import android.content.Intent;
import android.content.pm.PackageManager;import android.net.Uri;
import android.os.Build;import android.os.Bundle;
import android.support.v4.app.ActivityCompat;import android.support.v4.app.Fragment;
import android.view.LayoutInflater;import android.view.View;
import android.view.ViewGroup;import android.widget.Button;
import android.widget.ImageView;import android.widget.Toast;
import com.prodesk.mdgi.ContactEmail;import com.prodesk.mdgi.R;
//** Jeud 05.09.2019 // 11.03pm **//
public class DirRegionalManVuecdi extends Fragment
{
    String dr_man_num;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_dr_man_vuecdi, container, false);Button call_dr_man = (Button)view.findViewById(R.id.dr_man_call);
        call_dr_man.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_man_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_man_num = getString(R.string.txt_dr_man_contact);apellerDrMan(dr_man_num);
                }
            }
        });
        ImageView call_dr_man_cdi_bangolo = (ImageView)view.findViewById(R.id.dr_man_cdi_bangolo_call);
        call_dr_man_cdi_bangolo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_man_cdi_bangolo_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_man_num = getString(R.string.txt_dr_man_cdi_bangolo_contact);apellerDrMan(dr_man_num);
                }
            }
        });
        ImageView call_dr_man_cdi_biankouma = (ImageView)view.findViewById(R.id.dr_man_cdi_biankouma_call);
        call_dr_man_cdi_biankouma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_man_cdi_biankouma_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_man_num = getString(R.string.txt_dr_man_cdi_biankouma_contact);apellerDrMan(dr_man_num);
                }
            }
        });
        ImageView call_dr_man_cdi_danane = (ImageView)view.findViewById(R.id.dr_man_cdi_danane_call);
        call_dr_man_cdi_danane.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_man_cdi_danane_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_man_num = getString(R.string.txt_dr_man_cdi_danane_contact);apellerDrMan(dr_man_num);
                }
            }
        });
        ImageView call_dr_man_cdi_man = (ImageView)view.findViewById(R.id.dr_man_cdi_man_call);
        call_dr_man_cdi_man.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_dr_man_cdi_man_contact) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dr_man_num = getString(R.string.txt_dr_man_cdi_man_contact);apellerDrMan(dr_man_num);
                }
            }
        });
        Button envoi_mail_dr_man = (Button)view.findViewById(R.id.dr_man_message);
        envoi_mail_dr_man.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                ecrireDirGenerale(getString(R.string.txt_dr_man_titre),"DIRECTION REGIONALE");
            }
        });return view;
    }
    public void apellerDrMan(String dr_man_appel)
    {
        try {
            if(Build.VERSION.SDK_INT > 22) {
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE}, 101);
                    return;
                }
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + dr_man_appel));startActivity(callIntent);
            } else {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + dr_man_appel));startActivity(callIntent);
            }
        } catch (Exception ex) {}
    }
    public void ecrireDirGenerale(String title, String categorie)
    {
        Intent intEcrireAuDr = new Intent(getContext(), ContactEmail.class);intEcrireAuDr.putExtra("dataContactTitle", title);intEcrireAuDr.putExtra("dataContactCategorie", categorie);
        intEcrireAuDr.putExtra("dataContactDestinataire", getString(R.string.txt_contact_destinataire_man));startActivity(intEcrireAuDr);
    }
}