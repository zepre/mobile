package com.prodesk.mdgi.Goto;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.prodesk.mdgi.R;
import com.prodesk.mdgi.WebViewer.WebActivity;
//** Mardi 17.09.2019 // 3.17pm **//
public class GotoFormulaireUnique extends AppCompatActivity
{
    Animation zoomDedans, zoomDehors;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);setContentView(R.layout.activity_formulaireunique);
        final ImageView mainBackground = (ImageView)findViewById(R.id.formulaireunique_img);
        zoomDedans = AnimationUtils.loadAnimation(this,R.anim.zoomin);
        zoomDehors = AnimationUtils.loadAnimation(this,R.anim.zoomout);
        zoomDedans.setAnimationListener(new Animation.AnimationListener()
        {
            @Override
            public void onAnimationStart(Animation arg0) {}
            @Override
            public void onAnimationRepeat(Animation arg0) {}
            @Override
            public void onAnimationEnd(Animation arg0) {
                mainBackground.startAnimation(zoomDehors);
            }
        });
        zoomDehors.setAnimationListener(new Animation.AnimationListener()
        {
            @Override
            public void onAnimationStart(Animation arg0) {}
            @Override
            public void onAnimationRepeat(Animation arg0) {}
            @Override
            public void onAnimationEnd(Animation arg0) {
                mainBackground.startAnimation(zoomDedans);
            }
        });
        mainBackground.startAnimation(zoomDedans);
        LinearLayout gotoFormUniqFudpit = (LinearLayout)findViewById(R.id.open_formuniq_fudpit);
        gotoFormUniqFudpit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openFormulaireUniqueDoc(getString(R.string.txt_formuniq_file_fudpit),getString(R.string.txt_formuniq_file_fudpit_lk),"fudpit");
            }
        });
        LinearLayout gotoFormUniqPrfud = (LinearLayout)findViewById(R.id.open_formuniq_prfud);
        gotoFormUniqPrfud.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openFormulaireUniqueDoc(getString(R.string.txt_formuniq_file_prfud),getString(R.string.txt_formuniq_file_prfud_lk),"docpdf");
            }
        });
        LinearLayout gotoFormUniqcodpit = (LinearLayout)findViewById(R.id.open_formuniq_codpit);
        gotoFormUniqcodpit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openFormulaireUniqueDoc(getString(R.string.txt_formuniq_file_codpit),getString(R.string.txt_formuniq_file_codpit_lk),"docpdf");
            }
        });
    }
    private void openFormulaireUniqueDoc(String title, String link, String icon)
    {
        Intent intFormunik = new Intent(GotoFormulaireUnique.this, WebActivity.class);intFormunik.putExtra("dataWebTitle", title);intFormunik.putExtra("dataWebLink", link);
        intFormunik.putExtra("dataWebIcon", icon);startActivity(intFormunik);
    }
    @Override
    public void finish()
    {
        super.finish();
    }
}