package com.prodesk.mdgi.Goto;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.prodesk.mdgi.Paiements.*;
import com.prodesk.mdgi.R;
//** Lund 23.09.2019 // 1.04pm **//
public class GotoPaiements extends AppCompatActivity
{
    Animation zoomDedans, zoomDehors;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);setContentView(R.layout.activity_paiements);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        final ImageView mainBackground = (ImageView)findViewById(R.id.paiment_mode_img);
        zoomDedans = AnimationUtils.loadAnimation(this,R.anim.zoomin);
        zoomDehors = AnimationUtils.loadAnimation(this,R.anim.zoomout);
        zoomDedans.setAnimationListener(new Animation.AnimationListener()
        {
            @Override
            public void onAnimationStart(Animation arg0) {}
            @Override
            public void onAnimationRepeat(Animation arg0) {}
            @Override
            public void onAnimationEnd(Animation arg0) {
                mainBackground.startAnimation(zoomDehors);
            }
        });
        zoomDehors.setAnimationListener(new Animation.AnimationListener()
        {
            @Override
            public void onAnimationStart(Animation arg0) {}
            @Override
            public void onAnimationRepeat(Animation arg0) {}
            @Override
            public void onAnimationEnd(Animation arg0) {
                mainBackground.startAnimation(zoomDedans);
            }
        });
        mainBackground.startAnimation(zoomDedans);
        LinearLayout gotoPaiementBancaire = (LinearLayout)findViewById(R.id.paiement_bancaire);
        gotoPaiementBancaire.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intPaiBank = new Intent(GotoPaiements.this, PaiementBancaire.class);
                startActivity(intPaiBank);
            }
        });
        LinearLayout gotoPaiementMobile = (LinearLayout)findViewById(R.id.paiement_mobile);
        gotoPaiementMobile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intPaiMob = new Intent(GotoPaiements.this, PaiementMobile.class);
                startActivity(intPaiMob);
            }
        });
    }
    @Override
    public void finish()
    {
        super.finish();
    }
}