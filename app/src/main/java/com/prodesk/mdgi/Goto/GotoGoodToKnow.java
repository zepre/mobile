package com.prodesk.mdgi.Goto;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.prodesk.mdgi.BonasavoirPublications;
import com.prodesk.mdgi.MainActivity;
import com.prodesk.mdgi.R;
import com.prodesk.mdgi.WebViewer.WebActivity;
//** Lund 23.09.2019 // 12.20pm **//
public class GotoGoodToKnow extends AppCompatActivity
{
    Animation zoomDedans, zoomDehors;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);setContentView(R.layout.activity_bonasavoir);
        final ImageView mainBackground = (ImageView)findViewById(R.id.bonasavoir_mode_img);
        zoomDedans = AnimationUtils.loadAnimation(this,R.anim.zoomin);
        zoomDehors = AnimationUtils.loadAnimation(this,R.anim.zoomout);
        zoomDedans.setAnimationListener(new Animation.AnimationListener()
        {
            @Override
            public void onAnimationStart(Animation arg0) {}
            @Override
            public void onAnimationRepeat(Animation arg0) {}
            @Override
            public void onAnimationEnd(Animation arg0) {
                mainBackground.startAnimation(zoomDehors);
            }
        });
        zoomDehors.setAnimationListener(new Animation.AnimationListener()
        {
            @Override
            public void onAnimationStart(Animation arg0) {}
            @Override
            public void onAnimationRepeat(Animation arg0) {}
            @Override
            public void onAnimationEnd(Animation arg0) {
                mainBackground.startAnimation(zoomDedans);
            }
        });
        mainBackground.startAnimation(zoomDedans);
        LinearLayout gotoFaq = (LinearLayout)findViewById(R.id.bonasavoir_faq);
        gotoFaq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openBonASavoirWeb(getString(R.string.txt_bonasavoir_faq),getString(R.string.txt_bonasavoir_faq_link),"website");
            }
        });
        LinearLayout gotoFacturenormalisee = (LinearLayout)findViewById(R.id.bonasavoir_factnormalisée);
        gotoFacturenormalisee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openBonASavoirWeb(getString(R.string.txt_bonasavoir_facturenormalisee),getString(R.string.txt_bonasavoir_facturenormalisee_link),"website");
            }
        });
        LinearLayout gotoCga = (LinearLayout)findViewById(R.id.bonasavoir_lescga);
        gotoCga.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openBonASavoirWeb(getString(R.string.txt_bonasavoir_lescga),getString(R.string.txt_bonasavoir_lescga_link),"website");
            }
        });
        LinearLayout gotoPublications = (LinearLayout)findViewById(R.id.bonasavoir_lespublications);
        gotoPublications.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent lespublicationsint = new Intent(GotoGoodToKnow.this, BonasavoirPublications.class);
                startActivity(lespublicationsint);
            }
        });
        LinearLayout gotoRegimeImposition = (LinearLayout)findViewById(R.id.bonasavoir_regimpositions);
        gotoRegimeImposition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Toast.makeText(GotoGoodToKnow.this,"Option non disponible",Toast.LENGTH_SHORT).show();
            }
        });
        LinearLayout gotoLexfisc = (LinearLayout)findViewById(R.id.bonasavoir_lexiquefiscale);
        gotoLexfisc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openBonASavoirWeb(getString(R.string.txt_bonasavoir_lexiquefiscale),getString(R.string.txt_bonasavoir_lexiquefiscale_link),"website");
            }
        });
    }
    private void openBonASavoirWeb(String title, String link, String icon)
    {
        Intent intPourWeb = new Intent(GotoGoodToKnow.this, WebActivity.class);intPourWeb.putExtra("dataWebTitle", title);
        intPourWeb.putExtra("dataWebLink", link);intPourWeb.putExtra("dataWebIcon", icon);startActivity(intPourWeb);
    }
    @Override
    public void finish()
    {
        Intent intMain = new Intent(this, MainActivity.class);
        intMain.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intMain);
        //super.finish();
    }
    @Override
    public void onBackPressed()
    {
        Intent intMain = new Intent(this, MainActivity.class);
        intMain.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intMain);
        //finish();
    }
}