package com.prodesk.mdgi.Goto;
import android.content.Intent;import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;import android.view.View;
import android.view.animation.Animation;import android.view.animation.AnimationUtils;
import android.widget.ImageView;import android.widget.LinearLayout;
import com.prodesk.mdgi.WebViewer.EserviceWebConnect;import com.prodesk.mdgi.R;
//** Lund 23.09.2019 // 1.05pm **//
public class GotoEservices extends AppCompatActivity
{
    Animation zoomDedans, zoomDehors;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);setContentView(R.layout.activity_eservices);final ImageView mainBackground = (ImageView)findViewById(R.id.eservices_mode_img);
        zoomDedans = AnimationUtils.loadAnimation(this,R.anim.zoomin);zoomDehors = AnimationUtils.loadAnimation(this,R.anim.zoomout);
        zoomDedans.setAnimationListener(new Animation.AnimationListener()
        {
            @Override
            public void onAnimationStart(Animation arg0) {}
            @Override
            public void onAnimationRepeat(Animation arg0) {}
            @Override
            public void onAnimationEnd(Animation arg0) {
                mainBackground.startAnimation(zoomDehors);
            }
        });
        zoomDehors.setAnimationListener(new Animation.AnimationListener()
        {
            @Override
            public void onAnimationStart(Animation arg0) {}
            @Override
            public void onAnimationRepeat(Animation arg0) {}
            @Override
            public void onAnimationEnd(Animation arg0) {
                mainBackground.startAnimation(zoomDedans);
            }
        });
        mainBackground.startAnimation(zoomDedans);
        LinearLayout gotoEavis = (LinearLayout)findViewById(R.id.eservice_eavis);
        gotoEavis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openEserviceWeb(getString(R.string.txt_eservice_eavis),getString(R.string.txt_eservice_eavis_link));
            }
        });
        LinearLayout gotoEavisImpotfoncier = (LinearLayout)findViewById(R.id.eservice_eavisimpotfoncier);
        gotoEavisImpotfoncier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openEserviceWeb(getString(R.string.txt_eservice_eavis_impotfoncier),getString(R.string.txt_eservice_eavis_impotfoncier_link));
            }
        });
        LinearLayout gotoEimpot = (LinearLayout)findViewById(R.id.eservice_eimpot);
        gotoEimpot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openEserviceWeb(getString(R.string.txt_eservice_eimpot),getString(R.string.txt_eservice_eimpot_link));
            }
        });
        LinearLayout gotoEliasse = (LinearLayout)findViewById(R.id.eservice_eliasse);
        gotoEliasse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openEserviceWeb(getString(R.string.txt_eservice_eliasse),getString(R.string.txt_eservice_eliasse_link));
            }
        });
        LinearLayout gotoEncc = (LinearLayout)findViewById(R.id.eservice_encc);
        gotoEncc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openEserviceWeb(getString(R.string.txt_eservice_encc),getString(R.string.txt_eservice_encc_link));
            }
        });
        LinearLayout gotoEquittance = (LinearLayout)findViewById(R.id.eservice_equitance);
        gotoEquittance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openEserviceWeb(getString(R.string.txt_eservice_equitance),getString(R.string.txt_eservice_equitance_link));
            }
        });
    }
    private void openEserviceWeb(String title, String link)
    {
        Intent intEserviceWeb = new Intent(GotoEservices.this, EserviceWebConnect.class);intEserviceWeb.putExtra("dataTitle", title);intEserviceWeb.putExtra("dataLink", link);startActivity(intEserviceWeb);
    }
    @Override
    public void finish() { super.finish(); }
}