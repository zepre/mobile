package com.prodesk.mdgi;
import android.Manifest;import android.app.ActivityManager;
import android.app.AlertDialog;import android.app.ProgressDialog;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;import android.net.ConnectivityManager;
import android.net.NetworkInfo;import android.net.Uri;
import android.os.AsyncTask;import android.os.Build;
import android.os.Bundle;import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;import android.text.SpannableString;
import android.text.style.TextAppearanceSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;import android.view.MenuItem;
import android.view.View;import android.widget.Button;
import android.widget.ImageView;import android.widget.LinearLayout;
import android.widget.RelativeLayout;import android.widget.TextView;import android.widget.Toast;
import com.prodesk.mdgi.Communiques.ListeCommunique;import com.prodesk.mdgi.Communiques.ListeCommuniqueOffline;
import com.prodesk.mdgi.DonneesLocal.DonneeLocalHelper;import com.prodesk.mdgi.Goto.*;
import com.prodesk.mdgi.WebViewer.*;
import com.prodesk.mdgi.myServix.mdgiDataServix;
import com.squareup.picasso.Picasso;
import org.apache.http.HttpEntity;import org.apache.http.HttpResponse;import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;import org.json.JSONObject;import java.io.BufferedReader;
import java.io.InputStream;import java.io.InputStreamReader;
import java.util.ArrayList;import java.util.Timer;
import java.util.TimerTask;import java.util.concurrent.ExecutionException;
import static com.prodesk.mdgi.Utilitaires.*;
//** Mard 03.12.2019 // 2.16pm **//
public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener
{
    private DrawerLayout myDrawer;private ActionBarDrawerToggle myToggle;private long lastBackPress;private Toast toast;
    private ProgressDialog pb_reload, pageLoad;public static FragmentManager fragmentManager;final Context context = this;
    String noumero, oneItemImage, oneItemTitre;String request_url = liste_comslide_url;
    private ViewPager pager, pageroffline;private RelativeLayout sliderOne;private TextView slideOneTitre;private ImageView slideOneImage;
    private LinearLayout slide_erreur;ArrayList<Integer> list = new ArrayList<>();
    DonneeLocalHelper mdgiciDb;long ComDataRows, ComDataRowsOnline, ComDataRowsDirections, ComDataRowsDirectionsOnline, newCommuniques, newDirections;
    HttpPost httpPost;HttpResponse response;HttpClient httpClient;Intent intServix;private mdgiServices monServix;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);setContentView(R.layout.activity_main_plus);
        mdgiciDb = new DonneeLocalHelper(this);setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        ComDataRowsOnline = 0;ComDataRowsDirectionsOnline = 0;pager = (ViewPager) findViewById(R.id.view_pager_main);
        sliderOne = (RelativeLayout) findViewById(R.id.view_pager_one);slide_erreur = (LinearLayout) findViewById(R.id.lay_error_slide);
        pageroffline = (ViewPager) findViewById(R.id.view_pager_offline);slideOneTitre = (TextView) findViewById(R.id.slide_one_text);
        slideOneImage = (ImageView) findViewById(R.id.slide_one_image);Button btn_reload = (Button)findViewById(R.id.btn_refresh_slide);
        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            pageLoad = new ProgressDialog(MainActivity.this);pageLoad.setMessage("Chargement des données, \nVeuillez patienter ...");
            pageLoad.setIndeterminate(true);pageLoad.setCancelable(true);
            pageLoad.show();Handler handler = new Handler();
            handler.postDelayed(new Runnable()
            {
                public void run()
                {
                    localData();
                    pageLoad.dismiss();
                }
            }, 2500);
            sliderOne.setVisibility(View.VISIBLE);slide_erreur.setVisibility(View.GONE);
        } else {
            getRowsAllData();
            if (ComDataRows > 0) {
                pageroffline.setVisibility(View.GONE);slide_erreur.setVisibility(View.VISIBLE);
                OfflineSlideAdapter offlineSlideAdapter = new OfflineSlideAdapter(MainActivity.this,list);slide_erreur.setVisibility(View.GONE);
                pageroffline.setVisibility(View.VISIBLE);CustomSwipeAdapter customSwipeAdapter = new CustomSwipeAdapter(MainActivity.this,list);
                pageroffline.setAdapter(offlineSlideAdapter);Timer tempOffline = new Timer();
                tempOffline.scheduleAtFixedRate(new slideOffline(), 3000, 5000);
            }
        }
        myDrawer = (DrawerLayout) findViewById(R.id.myDrawer);NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        Menu menuView = navigationView.getMenu();MenuItem toolsMenuDoc = menuView.findItem(R.id.menu_tilte_ressourcesdocs);
        SpannableString titleDoc = new SpannableString(toolsMenuDoc.getTitle());titleDoc.setSpan(new TextAppearanceSpan(this, R.style.menuItemTitle), 0, titleDoc.length(),0);
        toolsMenuDoc.setTitle(titleDoc);MenuItem toolsMenuContact = menuView.findItem(R.id.menu_tilte_contacts);
        SpannableString titleContact = new SpannableString(toolsMenuContact.getTitle());
        titleContact.setSpan(new TextAppearanceSpan(this, R.style.menuItemTitle), 0, titleContact.length(),0);
        toolsMenuContact.setTitle(titleContact);MenuItem toolsMenuFollow = menuView.findItem(R.id.menu_tilte_follow);SpannableString titleFollow = new SpannableString(toolsMenuFollow.getTitle());
        titleFollow.setSpan(new TextAppearanceSpan(this, R.style.menuItemTitle), 0, titleFollow.length(),0);toolsMenuFollow.setTitle(titleFollow);
        navigationView.setNavigationItemSelectedListener(this);navigationView.setItemIconTintList(null);
        myToggle = new ActionBarDrawerToggle(this, myDrawer, R.string.nav_open, R.string.nav_close);myDrawer.addDrawerListener(myToggle);
        myToggle.syncState();getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        LinearLayout gotoAbout = (LinearLayout)findViewById(R.id.shortcut_apropos);
        gotoAbout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intAbout = new Intent(MainActivity.this, GotoApropos.class);startActivity(intAbout);
            }
        });
        LinearLayout gotoBonasavoir = (LinearLayout)findViewById(R.id.shortcut_bonasavoir);
        gotoBonasavoir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intBonasavoir = new Intent(MainActivity.this, GotoGoodToKnow.class);startActivity(intBonasavoir);
            }
        });
        LinearLayout gotoEservices = (LinearLayout)findViewById(R.id.shortcut_eservice);
        gotoEservices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intEser = new Intent(MainActivity.this, GotoEservices.class);startActivity(intEser);
            }
        });
        LinearLayout gotoDirections = (LinearLayout)findViewById(R.id.shortcut_directioncdi);
        gotoDirections.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intDir = new Intent(MainActivity.this, DirectionsCartograph.class);startActivity(intDir);
            }
        });
        LinearLayout gotoPaiements = (LinearLayout)findViewById(R.id.shortcut_paiements);
        gotoPaiements.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intPaie = new Intent(MainActivity.this, GotoPaiements.class);startActivity(intPaie);
            }
        });
        LinearLayout gotoForms = (LinearLayout)findViewById(R.id.shortcut_formulaireunique);
        gotoForms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intEser = new Intent(MainActivity.this, GotoFormulaireUnique.class);startActivity(intEser);
            }
        });
        sliderOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (ComDataRowsOnline > 0) {
                    Intent intCom = new Intent(MainActivity.this, ListeCommunique.class);
                    startActivity(intCom);

                } else {
                    if (ComDataRows > 0) {
                        Intent intComOff = new Intent(MainActivity.this, ListeCommuniqueOffline.class);
                        startActivity(intComOff);
                    }
                    else  {
                        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(MainActivity.this);
                        alertBuilder.setMessage("Vous n'avez aucun communiqué enregistré")
                            .setTitle("Erreur !")
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id)
                                {}
                            });
                        AlertDialog erreurInternet = alertBuilder.create();
                        erreurInternet.show();
                    }
                }
            }
        });
        pb_reload = new ProgressDialog(MainActivity.this);
        btn_reload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                pb_reload.setMessage("Rechargement ...");
                pb_reload.show();
                Handler progressHandler = new Handler();
                progressHandler.postDelayed(new Runnable() {
                    @Override
                    public void run()
                    {}
                },3000);
                rechargerPage();
            }
        });
        if (networkInfo != null && networkInfo.isConnected()) {
            // nouveau service
            //lancerJobServix();//prepareServix();
            // ancien service
            monServix = new mdgiServices();intServix = new Intent(this, monServix.getClass());
            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {
                if (!monServiceTourne(monServix.getClass())) {
                    startService(intServix);
                }
                }
            }, 20000);
        }
    }
    /*public void lancerJobServix() {
        ComponentName componentName = new ComponentName(this, mdgiDataServix.class);
        JobInfo jobInfo = new JobInfo.Builder(123, componentName)
            .setRequiresCharging(true)
            .setRequiredNetworkType(JobInfo.NETWORK_TYPE_UNMETERED)
            .setPersisted(true)
            .setPeriodic(15 * 60 * 1000)
            .build();
        JobScheduler scheduler = (JobScheduler) getSystemService(JOB_SCHEDULER_SERVICE);
        int resultJobServix = scheduler.schedule(jobInfo);
        if (resultJobServix == JobScheduler.RESULT_SUCCESS) {
            Log.d("JOBBER", "K=JOb lancer xuxex");
        } else {
            Log.d("JOBBER", "K=JOb lancer erorrrorr");
        }
    }
    private void prepareServix() {
        SharedPreferences preferences = PreferenceManager.
            getDefaultSharedPreferences(this);
        if(!preferences.getBoolean("premierLancement", false)){
            //schedule the job only once.
            lancementServiceMdgi();
            //update shared preference
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean("premierLancement", true);
            editor.commit();
        }
    }
    private void lancementServiceMdgi() {
        JobScheduler jobScheduler = (JobScheduler)getApplicationContext()
            .getSystemService(JOB_SCHEDULER_SERVICE);
        ComponentName componentName = new ComponentName(this, mdgiDataServix.class);
        JobInfo jobInfo = new JobInfo.Builder(1, componentName)
            .setPeriodic(1000)
            .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
            .setPersisted(true)
            .build();
        jobScheduler.schedule(jobInfo);
    }*/
    @Override
    protected void onResume()
    { super.onResume();}
    @Override
    protected void onStart()
    { super.onStart();}
    private void localData()
    {
        getRowsAllData();loadOneSlideItem();
    }
    private void getRowsAllData()
    {
        ComDataRows = mdgiciDb.nombresCommuniquesLocal();
        ComDataRowsDirections = mdgiciDb.nombresDirectionsLocal();
        nombreCommuniquesEnLigne getAllComRows = new nombreCommuniquesEnLigne();
        nombreDirectionsEnLigne getAllDirRows = new nombreDirectionsEnLigne();
        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            try {
                String dataGet = getAllComRows.execute().get();String dataDir = getAllDirRows.execute().get();
                if (dataGet == null) {
                    ComDataRowsOnline = 0;
                } else {
                    ComDataRowsOnline = Long.parseLong(dataGet);
                }
                if (dataDir == null) {
                    ComDataRowsDirectionsOnline = 0;
                } else {
                    ComDataRowsDirectionsOnline = Long.parseLong(dataDir);
                }
            } catch (InterruptedException e) {} catch (ExecutionException e) { }
        }
        newCommuniques = ComDataRowsOnline - ComDataRows;newDirections = ComDataRowsDirectionsOnline - ComDataRowsDirections;
    }
    private void loadOneSlideItem()
    {
        String[] mySlideData = new String[2];
        telechargerComOneItem getOneItemData = new telechargerComOneItem();
        try {
            mySlideData = getOneItemData.execute().get();
        } catch (InterruptedException e) {} catch (ExecutionException e) {}oneItemImage = mySlideData[0];
        oneItemTitre = mySlideData[1];slideOneTitre.setText(oneItemTitre);
        Picasso.get().load(oneItemImage).into(slideOneImage);
    }
    public class slideOffline extends TimerTask
    {
        @Override
        public void run(){
            MainActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run()
                {
                int pageritem = pageroffline.getCurrentItem();
                switch (pageritem) {
                    case 0:
                        pageroffline.setCurrentItem(1);
                    break;
                    case 1:
                        pageroffline.setCurrentItem(2);
                    break;
                    case 2:
                        pageroffline.setCurrentItem(0);
                    break;
                }
                }
            });
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults)
    {
        if (requestCode == 101) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                appelerNumero();
            }
        }
    }
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.nav_communiques:
                ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
                if (networkInfo != null && networkInfo.isConnected()) {
                    if (ComDataRowsOnline == 0) {
                        if (ComDataRows > 0) {
                            Intent comOffline = new Intent(this, ListeCommuniqueOffline.class);startActivity(comOffline);
                        } else  {
                            AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
                            alertBuilder.setMessage("Vous n'avez aucun communiqué enregistré")
                                .setTitle("Erreur !")
                                .setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id)
                                    {}
                                });
                            AlertDialog erreurInternet = alertBuilder.create();erreurInternet.show();
                        }
                    } else {
                        Intent com = new Intent(this, ListeCommunique.class);startActivity(com);
                    }
                } else {
                    if (ComDataRows > 0) {
                        Intent comOffline = new Intent(this, ListeCommuniqueOffline.class);startActivity(comOffline);
                    } else  {
                        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
                        alertBuilder.setMessage("Vous n'avez aucun communiqué enregistré")
                            .setTitle("Erreur !")
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id)
                                {}
                            });
                        AlertDialog erreurInternet = alertBuilder.create();
                        erreurInternet.show();
                    }
                }
            break;
            case R.id.nav_imprimes:
                Intent imp = new Intent(this, Imprimes.class);
                startActivity(imp);
            break;
            case R.id.nav_annexefiscale:
                openWebPage(getString(R.string.activity_annexefisc),getString(R.string.txt_annexe_fiscale_link),"annexefiscale");
            break;
            case R.id.nav_cgi:
                openWebPage(getString(R.string.activity_cgi),getString(R.string.txt_cgi_link),"cgi");
            break;
            case R.id.nav_systemefisc:
                openWebPage(getString(R.string.activity_sfi),getString(R.string.txt_systeme_fiscal_link),"sfi");
            break;
            case R.id.nav_doctrinefiscale:
                openWebPage(getString(R.string.activity_dfi),getString(R.string.txt_doctrine_fiscale_link),"dfi");
            break;
            case R.id.nav_guideprocedure:
                Intent guidp = new Intent(this, GuideProcedure.class);
                startActivity(guidp);
            break;
            case R.id.nav_impotetaxes:
                openWebPage(getString(R.string.activity_impotax),getString(R.string.txt_impotsettaxes_link),"website");
            break;
            case R.id.nav_paiementsacd:
                Toast.makeText(this, "Paiement des ACD | non disponible", Toast.LENGTH_SHORT).show();
                break;
            case R.id.nav_procedurefiscale:
                openWebPage(getString(R.string.activity_profisc),getString(R.string.txt_profisc_link),"website");
            break;
            case R.id.nav_siteweb:
                openWebPage(getString(R.string.activity_website),getString(R.string.txt_website_link),"website");
            break;
            case R.id.nav_numerovert:
                noumero = getString(R.string.number_numerovert);
                appelerNumero();
            break;
            case R.id.nav_telephone:
                noumero = getString(R.string.number_call_phone);
                appelerNumero();
            break;
            case R.id.nav_fax:
                Toast.makeText(this,"Utilisez ce numéro pour nous envoyer un fax",Toast.LENGTH_LONG).show();
            break;
            case R.id.nav_email:
                Intent intContact = new Intent(this,ContactEmail.class);
                intContact.putExtra("dataContactTitle", getString(R.string.txt_contact_destinataire_dgi));intContact.putExtra("dataContactCategorie", getString(R.string.txt_contact_categorie_dgi));
                intContact.putExtra("dataContactDestinataire", getString(R.string.txt_contact_destinataire));startActivity(intContact);
            break;
            case R.id.nav_follow_fb:
                openWebPage("Suivre Facebook DGI",fb_profile,"facebook");
            break;
            case R.id.nav_follow_tw:
                openWebPage("Suivre Twitter DGI","https://twitter.com/dgicitwitt","twitter");
            break;
            case R.id.nav_follow_yt:
                openWebPage("Suivre Youtube DGI","https://www.youtube.com/channel/UClHvfP8lGsSmsP6Fv1qATOw/","youtube");
            break;
        }
        myDrawer.closeDrawer(GravityCompat.START);
        return true;
    }
    public void openWebPage(String title, String link, String icon)
    {
        Intent intPourWeb = new Intent(MainActivity.this, WebActivity.class);
        intPourWeb.putExtra("dataWebTitle", title);intPourWeb.putExtra("dataWebLink", link);intPourWeb.putExtra("dataWebIcon", icon);startActivity(intPourWeb);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.my_menu_home, menu);return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int idd = item.getItemId();
        if (myToggle.onOptionsItemSelected(item)) {
            return true;
        }
        switch (idd)
        {
            case R.id.nav_actualiser:
                recreate();
            break;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onBackPressed()
    {
        if (myDrawer.isDrawerOpen(GravityCompat.START)) {
            myDrawer.closeDrawer(Gravity.LEFT);
        } else {
            if (this.lastBackPress < System.currentTimeMillis() - 3000) {
                toast = Toast.makeText(this, "Appuyez une nouvelle fois pour quitter", Toast.LENGTH_LONG);
                toast.show();this.lastBackPress = System.currentTimeMillis();
            } else {
                if (toast != null) {
                    toast.cancel();
                }
                moveTaskToBack(true);finishAffinity();
                android.os.Process.killProcess(android.os.Process.myPid());System.exit(1);
            }
        }
    }
    public void appelerNumero()
    {
        try {
            if(Build.VERSION.SDK_INT > 22) {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.CALL_PHONE}, 101);
                    return;
                }
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + noumero));startActivity(callIntent);
            } else {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + noumero));startActivity(callIntent);
            }
        } catch (Exception ex) {}
    }
    private void rechargerPage()
    {
        Intent refresh = new Intent(this, MainActivity.class);
        startActivity(refresh);finish();
    }
    private class nombreCommuniquesEnLigne extends AsyncTask<Void, Void, String>
    {
        protected void onPreExecute()
        {
            super.onPreExecute();
        }
        protected String doInBackground(Void...params)
        {
            InputStream inputStream = null;
            String resultat = "";String localComDataRows = "";
            try {
                httpClient = new DefaultHttpClient();httpPost = new HttpPost(liste_com_url);
                response = httpClient.execute(httpPost);HttpEntity entity = response.getEntity();
                inputStream = entity.getContent();
            } catch (Exception ecn){}
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream,"utf-8"),8);
                StringBuilder stringBuilder = new StringBuilder();String ligne = null;
                while ((ligne = reader.readLine()) != null)
                {
                    stringBuilder.append(ligne+"\n");
                }
                inputStream.close();resultat = stringBuilder.toString();
            } catch (Exception ec) {}
            try {
                resultat = resultat.substring(resultat.indexOf("["));
                JSONArray jsonArray = new JSONArray(resultat);localComDataRows = String.valueOf(jsonArray.length());
            } catch (Exception ep) {}
            return localComDataRows;
        }
        protected void onPostExecute(Void result) {}
    }
    private class telechargerComOneItem extends AsyncTask<Void, Void, String[]>
    {
        protected void onPreExecute()
        {
            super.onPreExecute();
        }
        protected String[] doInBackground(Void...params)
        {
            InputStream inputStream = null;
            String resultat = "";String[] localOneItem = new String[2];
            try {
                httpClient = new DefaultHttpClient();
                httpPost = new HttpPost(liste_com_url);response = httpClient.execute(httpPost);
                HttpEntity entity = response.getEntity();inputStream = entity.getContent();
            } catch (Exception ecn){ }
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream,"utf-8"),8);
                StringBuilder stringBuilder = new StringBuilder();String ligne = null;
                while ((ligne = reader.readLine()) != null)
                {
                    stringBuilder.append(ligne+"\n");
                }
                inputStream.close();resultat = stringBuilder.toString();
            } catch (Exception ec) {}
            try {
                resultat = resultat.substring(resultat.indexOf("["));JSONArray jsonArray = new JSONArray(resultat);
                JSONObject jsonObject1 = jsonArray.getJSONObject(0);localOneItem[0] = base_url+jsonObject1.getString("com_nomimage");
                localOneItem[1] = jsonObject1.getString("com_titre");
            } catch (Exception ep) {}return localOneItem;
        }
        protected void onPostExecute(Void result) {}
    }
    private class nombreDirectionsEnLigne extends AsyncTask<Void, Void, String>
    {
        protected void onPreExecute()
        {
            super.onPreExecute();
        }
        protected String doInBackground(Void...params)
        {
            InputStream inputStream = null;String resultat = "";String localDirections = "";
            try {
                httpClient = new DefaultHttpClient();httpPost = new HttpPost(liste_dir_url);response = httpClient.execute(httpPost);
                HttpEntity entity = response.getEntity();inputStream = entity.getContent();
            } catch (Exception ecn){}
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream,"utf-8"),8);
                StringBuilder stringBuilder = new StringBuilder();String ligne = null;
                while ((ligne = reader.readLine()) != null)
                {
                    stringBuilder.append(ligne+"\n");
                }
                inputStream.close();resultat = stringBuilder.toString();
            } catch (Exception ec) {}
            try {
                resultat = resultat.substring(resultat.indexOf("["));JSONArray jsonArray = new JSONArray(resultat);
                localDirections = String.valueOf(jsonArray.length());
            } catch (Exception ep) {}
            return localDirections;
        }
        protected void onPostExecute(Void result) {}
    }
    private boolean monServiceTourne(Class<?> servClass)
    {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE))
        {
            if (servClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }return false;
    }
}