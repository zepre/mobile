package com.prodesk.mdgi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import com.prodesk.mdgi.DonneesLocal.DonneeLocalHelper;
import java.util.ArrayList;
//** Vand 23.08.2019 // 2.22pm **//
public class DirectionsCartograph extends AppCompatActivity implements AdapterView.OnItemClickListener {
    Activity context;private LinearLayout laydirectiongenerale, layresultatrecherche;
    private TextView txtsearchresult, txtsearchavailable;private ProgressDialog pageLoad;
    AdapterRechercheDirections adapter;ListView listDirections;ArrayList<AdminDirection> admindirection;long directionRows;
    DonneeLocalHelper mdgiciData = new DonneeLocalHelper(this);
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener()
    {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item)
        {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            switch (item.getItemId())
            {
                case R.id.nav_directionscdi_gle_ctle:
                    transaction.replace(R.id.directionscdi_content, new DirectionGleFragment()).commit();
                    return true;
                case R.id.nav_directionscdi_rgle:
                    transaction.replace(R.id.directionscdi_content, new DirectionRgleFragment()).commit();
                    return true;
            }
            return false;
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);setContentView(R.layout.activity_directionscdi);context = this;
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.nav_directionscdi);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.directionscdi_content, new DirectionGleFragment()).commit();
        admindirection = new ArrayList<AdminDirection>();
        listDirections = (ListView) findViewById(R.id.adm_resultat_recherchedirections);
        adapter = new AdapterRechercheDirections(context, R.layout.admin_recherche_direction, R.id.adm_rech_dir_titre, admindirection);
        listDirections.setAdapter(adapter);
        listDirections.setClickable(true);
        listDirections.setOnItemClickListener(this);
        pageLoad = new ProgressDialog(DirectionsCartograph.this);
        pageLoad.setMessage("Chargement des cartes, \nVeuillez patienter ...");
        pageLoad.setIndeterminate(true);
        pageLoad.setCancelable(false);
        pageLoad.setCanceledOnTouchOutside(false);
        pageLoad.show();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable()
        {
            public void run()
            {
                pageLoad.dismiss();
            }
        }, 2100);
    }
    @Override
    protected void onStart()
    {
        super.onStart();
        selectDirectionData bts = new selectDirectionData();
        bts.execute();
    }
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        String titleClass;
        TextView txtTitle = (TextView)view.findViewById(R.id.adm_rech_dir_class);
        titleClass = txtTitle.getText().toString();
        openFoundDirection(titleClass);
    }
    private void openFoundDirection(String gotoClass)
    {
        try {
            Class<?> clx = Class.forName(gotoClass);
            Intent intContact = new Intent(this,clx);
            intContact.putExtra("dataContactTitle", getString(R.string.txt_contact_destinataire_dgi));
            intContact.putExtra("dataContactCategorie", getString(R.string.txt_contact_categorie_dgi));
            startActivity(intContact);
        } catch (ClassNotFoundException e) {}
    }
    private class selectDirectionData extends AsyncTask<Void,Void,Void>
    {
        protected void onPreExecute()
        {
            super.onPreExecute();
        }
        protected Void doInBackground(Void...params)
        {
            try {
                directionRows = mdgiciData.nombresDirectionsLocal();
                Cursor getAllDir = mdgiciData.afficheTotalDirections(String.valueOf(directionRows));
                if (getAllDir.getCount() == 0) {
                } else {
                    admindirection.clear();
                    while (getAllDir.moveToNext())
                    {
                        AdminDirection admdir = new AdminDirection();
                        admdir.setDirectionId(getAllDir.getString(0));
                        admdir.setDirectionTitre(getAllDir.getString(1));
                        admdir.setDirectionContact(getAllDir.getString(2));
                        admdir.setDirectionAdresse(getAllDir.getString(3));
                        admdir.setDirectionCategorie(getAllDir.getString(4));
                        admdir.setDirectionLocalite(getAllDir.getString(5));
                        admdir.setDirectionLocaliteprincipale(getAllDir.getString(6));
                        admdir.setDirectionClass(getAllDir.getString(7));
                        admindirection.add(admdir);
                    }
                    notifyDataSetChanged();
                }
            } catch (Exception ep) {}
            return null;
        }
        protected void onPostExecute(Void result)
        {
            adapter.notifyDataSetChanged();
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.recherche_directions, menu);
        laydirectiongenerale = (LinearLayout)findViewById(R.id.lay_directionsgenerale_view);
        layresultatrecherche = (LinearLayout)findViewById(R.id.lay_directionsgenerale_searchresult);
        txtsearchresult = (TextView)findViewById(R.id.txt_directiongenerale_searchresult);
        txtsearchavailable = (TextView)findViewById(R.id.adm_resultat_null);
        MenuItem item = menu.findItem(R.id.recherche_direction_cdi);
        SearchView searchView = (SearchView)item.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener()
        {
            @Override
            public boolean onQueryTextSubmit(String query)
            {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String newText)
            {
                if (newText.trim().isEmpty()) {
                    laydirectiongenerale.setVisibility(View.VISIBLE);
                    layresultatrecherche.setVisibility(View.GONE);
                    listDirections.clearTextFilter();
                } else {
                    laydirectiongenerale.setVisibility(View.GONE);
                    layresultatrecherche.setVisibility(View.VISIBLE);
                    txtsearchresult.setText(newText);
                    adapter.getFilter().filter(newText);
                    if (adapter.isEmpty())
                    {
                        txtsearchavailable.setVisibility(View.VISIBLE);
                        listDirections.setVisibility(View.GONE);
                    } else {
                        listDirections.setVisibility(View.VISIBLE);
                        txtsearchavailable.setVisibility(View.GONE);
                    }
                }
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }
    public void notifyDataSetChanged() {adapter.notifyDataSetChanged();}
}