package com.prodesk.mdgi;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.prodesk.mdgi.WebViewer.WebActivity;
//** Sam 17.08.2019 // 2.30pm **//
public class Imprimes extends AppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);setContentView(R.layout.activity_imprimes);
        TextView tx = (TextView) findViewById(R.id.imprimes_toolbar_title);
        tx.setText(getString(R.string.activity_imprimes));
        Toolbar toolbar = (Toolbar) findViewById(R.id.imprimes_toolbar_read);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        ImageView goback = (ImageView) findViewById(R.id.imprimes_retour);
        goback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                finish();
            }
        });
        final TextView readAdrstibic_clic = (TextView) findViewById(R.id.readAdrstibic);
        readAdrstibic_clic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openImprimeRead(getString(R.string.txt_imprimes_file_readAdrstibic),getString(R.string.txt_imprimes_file_readAdrstibic_lk),"docpdf");
            }
        });
        final TextView readAdrstibnc_clic = (TextView) findViewById(R.id.readAdrstibnc);
        readAdrstibnc_clic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openImprimeRead(getString(R.string.txt_imprimes_file_readAdrstibnc),getString(R.string.txt_imprimes_file_readAdrstibnc_lk),"docpdf");
            }
        });
        final TextView readDrrvaplsao_clic = (TextView) findViewById(R.id.readDrrvaplsao);
        readDrrvaplsao_clic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openImprimeRead(getString(R.string.txt_imprimes_file_readDrrvaplsao),getString(R.string.txt_imprimes_file_readDrrvaplsao_lk),"docpdf");
            }
        });
        final TextView readDf_clic = (TextView) findViewById(R.id.readDf);
        readDf_clic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openImprimeRead(getString(R.string.txt_imprimes_file_readDf),getString(R.string.txt_imprimes_file_readDf_lk),"docpdf");
            }
        });
        final TextView readRstarl_clic = (TextView) findViewById(R.id.readRstarl);
        readRstarl_clic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openImprimeRead(getString(R.string.txt_imprimes_file_readRstarl),getString(R.string.txt_imprimes_file_readRstarl_lk),"docpdf");
            }
        });
        final TextView readArstarl_clic = (TextView) findViewById(R.id.readArstarl);
        readArstarl_clic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openImprimeRead(getString(R.string.txt_imprimes_file_readArstarl),getString(R.string.txt_imprimes_file_readArstarl_lk),"docpdf");
            }
        });
        final TextView readAirstirvm_clic = (TextView) findViewById(R.id.readAirstirvm);
        readAirstirvm_clic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openImprimeRead(getString(R.string.txt_imprimes_file_readAirstirvm),getString(R.string.txt_imprimes_file_readAirstirvm_lk),"docpdf");
            }
        });
        final TextView readAetvaemepstep_clic = (TextView) findViewById(R.id.readAetvaemepstep);
        readAetvaemepstep_clic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openImprimeRead(getString(R.string.txt_imprimes_file_readAetvaemepstep),getString(R.string.txt_imprimes_file_readAetvaemepstep_lk),"docpdf");
            }
        });
        final TextView readDaauetva_clic = (TextView) findViewById(R.id.readDaauetva);
        readDaauetva_clic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openImprimeRead(getString(R.string.txt_imprimes_file_readDaauetva),getString(R.string.txt_imprimes_file_readDaauetva_lk),"docpdf");
            }
        });
        final TextView readErmbsfepmetva_clic = (TextView) findViewById(R.id.readErmbsfepmetva);
        readErmbsfepmetva_clic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openImprimeRead(getString(R.string.txt_imprimes_file_readErmbsfepmetva),getString(R.string.txt_imprimes_file_readErmbsfepmetva_lk),"docpdf");
            }
        });
        final TextView readErtbsaempstep_clic = (TextView) findViewById(R.id.readErtbsaempstep);
        readErtbsaempstep_clic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openImprimeRead(getString(R.string.txt_imprimes_file_readErtbsaempstep),getString(R.string.txt_imprimes_file_readErtbsaempstep_lk),"docpdf");
            }
        });
        final TextView readDde_clic = (TextView) findViewById(R.id.readDde);
        readDde_clic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openImprimeRead(getString(R.string.txt_imprimes_file_readDde),getString(R.string.txt_imprimes_file_readDde_lk),"docpdf");
            }
        });
        final TextView readDdpf_clic = (TextView) findViewById(R.id.readDdpf);
        readDdpf_clic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openImprimeRead(getString(R.string.txt_imprimes_file_readDdpf),getString(R.string.txt_imprimes_file_readDdpf_lk),"docpdf");
            }
        });
        final TextView readArstairsi_clic = (TextView) findViewById(R.id.readArstairsi);
        readArstairsi_clic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openImprimeRead(getString(R.string.txt_imprimes_file_readArstairsi),getString(R.string.txt_imprimes_file_readArstairsi_lk),"docpdf");
            }
        });
        final TextView readAirstairsi_clic = (TextView) findViewById(R.id.readAirstairsi);
        readAirstairsi_clic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openImprimeRead(getString(R.string.txt_imprimes_file_readAirstairsi),getString(R.string.txt_imprimes_file_readAirstairsi_lk),"docpdf");
            }
        });
        final TextView readIdilezfbtic_clic = (TextView) findViewById(R.id.readIdilezfbtic);
        readIdilezfbtic_clic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openImprimeRead(getString(R.string.txt_imprimes_file_readIdilezfbtic),getString(R.string.txt_imprimes_file_readIdilezfbtic_lk),"docpdf");
            }
        });
        final TextView readBavilezfbtic_clic = (TextView) findViewById(R.id.readBavilezfbtic);
        readBavilezfbtic_clic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openImprimeRead(getString(R.string.txt_imprimes_file_readBavilezfbtic),getString(R.string.txt_imprimes_file_readBavilezfbtic_lk),"docpdf");
            }
        });
        final TextView readEtc_clic = (TextView) findViewById(R.id.readEtc);
        readEtc_clic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openImprimeRead(getString(R.string.txt_imprimes_file_readEtc),getString(R.string.txt_imprimes_file_readEtc_lk),"docpdf");
            }
        });
        final TextView readSmt_clic = (TextView) findViewById(R.id.readSmt);
        readSmt_clic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openImprimeRead(getString(R.string.txt_imprimes_file_readSmt),getString(R.string.txt_imprimes_file_readSmt_lk),"docpdf");
            }
        });
        final TextView readFdgeue_clic = (TextView) findViewById(R.id.readFdgeue);
        readFdgeue_clic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openImprimeRead(getString(R.string.txt_imprimes_file_readFdgeue),getString(R.string.txt_imprimes_file_readFdgeue_lk),"docpdf");
            }
        });
        final TextView readItvbg_clic = (TextView) findViewById(R.id.readItvbg);
        readItvbg_clic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openImprimeRead(getString(R.string.txt_imprimes_file_readItvbg),getString(R.string.txt_imprimes_file_readItvbg_lk),"docpdf");
            }
        });
        final TextView readDfepmdfec_clic = (TextView) findViewById(R.id.readDfepmdfec);
        readDfepmdfec_clic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openImprimeRead(getString(R.string.txt_imprimes_file_readDfepmdfec),getString(R.string.txt_imprimes_file_readDfepmdfec_lk),"docpdf");
            }
        });
        final TextView readDfepmdfecn_clic = (TextView) findViewById(R.id.readDfepmdfecn);
        readDfepmdfecn_clic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openImprimeRead(getString(R.string.txt_imprimes_file_readDfepmdfecn),getString(R.string.txt_imprimes_file_readDfepmdfecn_lk),"docpdf");
            }
        });
        final TextView readDfepmdfmcec_clic = (TextView) findViewById(R.id.readDfepmdfmcec);
        readDfepmdfmcec_clic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openImprimeRead(getString(R.string.txt_imprimes_file_readDfepmdfmcec),getString(R.string.txt_imprimes_file_readDfepmdfmcec_lk),"docpdf");
            }
        });
        final TextView readDfepmdfmcecn_clic = (TextView) findViewById(R.id.readDfepmdfmcecn);
        readDfepmdfmcecn_clic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openImprimeRead(getString(R.string.txt_imprimes_file_readDfepmdfmcecn),getString(R.string.txt_imprimes_file_readDfepmdfmcecn_lk),"docpdf");
            }
        });
        //
        final TextView readDfeppdfec_clic = (TextView) findViewById(R.id.readDfeppdfec);
        readDfeppdfec_clic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openImprimeRead(getString(R.string.txt_imprimes_file_readDfeppdfec),getString(R.string.txt_imprimes_file_readDfeppdfec_lk),"docpdf");
            }
        });
        final TextView readDfeppdfecn_clic = (TextView) findViewById(R.id.readDfeppdfecn);
        readDfeppdfecn_clic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openImprimeRead(getString(R.string.txt_imprimes_file_readDfeppdfecn),getString(R.string.txt_imprimes_file_readDfeppdfecn_lk),"docpdf");
            }
        });
        final TextView readDfeppdfmcec_clic = (TextView) findViewById(R.id.readDfeppdfmcec);
        readDfeppdfmcec_clic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openImprimeRead(getString(R.string.txt_imprimes_file_readDfeppdfmcec),getString(R.string.txt_imprimes_file_readDfeppdfmcec_lk),"docpdf");
            }
        });
        final TextView readDfeppdfmcecn_clic = (TextView) findViewById(R.id.readDfeppdfmcecn);
        readDfeppdfmcecn_clic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openImprimeRead(getString(R.string.txt_imprimes_file_readDfeppdfmcecn),getString(R.string.txt_imprimes_file_readDfeppdfmcecn_lk),"docpdf");
            }
        });
        //
        final TextView readDfccapp_clic = (TextView) findViewById(R.id.readDfccapp);
        readDfccapp_clic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openImprimeRead(getString(R.string.txt_imprimes_file_readDfccapp),getString(R.string.txt_imprimes_file_readDfccapp_lk),"docpdf");
            }
        });
        final TextView readDfccapm_clic = (TextView) findViewById(R.id.readDfccapm);
        readDfccapm_clic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openImprimeRead(getString(R.string.txt_imprimes_file_readDfccapm),getString(R.string.txt_imprimes_file_readDfccapm_lk),"docpdf");
            }
        });
        final TextView readEtii_clic = (TextView) findViewById(R.id.readEtii);
        readEtii_clic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openImprimeRead(getString(R.string.txt_imprimes_file_readEtii),getString(R.string.txt_imprimes_file_readEtii_lk),"docpdf");
            }
        });
    }
    private void openImprimeRead(String title, String link, String icon)
    {
        Intent intImpRead = new Intent(Imprimes.this, WebActivity.class);
        intImpRead.putExtra("dataWebTitle", title);intImpRead.putExtra("dataWebLink", link);
        intImpRead.putExtra("dataWebIcon", icon);startActivity(intImpRead);
    }
}