package com.prodesk.mdgi.WebViewer;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.prodesk.mdgi.R;
//** Sam 17.08.2019 // 1.11pm **//
public class EserviceWebConnect extends AppCompatActivity
{
    private WebView navigateur;private ProgressDialog progressDialog;String titlen, linkin;Boolean menuEstOuvert = false;
    FloatingActionButton fab_files_ass_iard, fab_files_ass_vie, fab_files_sys_cmpt, fab_files_sys_allg, fab_files_sys_minimal, fab_files_sys_normal;
    LinearLayout fab_files_ass_iard_lay, fab_files_ass_vie_lay, fab_files_sys_cmpt_lay, fab_files_sys_allg_lay, fab_files_sys_minimal_lay, fab_files_sys_normal_lay;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);setContentView(R.layout.activity_eserviceweb);
        Intent intEserWeb = getIntent();
        titlen = intEserWeb.getStringExtra("dataTitle");
        linkin = intEserWeb.getStringExtra("dataLink");
        TextView tx = (TextView) findViewById(R.id.eserviceweb_toolbar_title);
        tx.setText(titlen);Toolbar toolbar = (Toolbar) findViewById(R.id.eserviceweb_toolbar_read);
        setSupportActionBar(toolbar);getSupportActionBar().setTitle(null);
        LinearLayout goback = (LinearLayout) findViewById(R.id.eserviceweb_retour);
        goback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                finish();
            }
        });
        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            openEserviceWeb(linkin);
        } else {
            AlertDialog.Builder alertEserv = new AlertDialog.Builder(EserviceWebConnect.this);
            alertEserv.setTitle("Erreur Internet !");
            alertEserv.setMessage("Il vous faut internet pour lire cette page");
            alertEserv.setCancelable(false);
            alertEserv.setPositiveButton(
                    "Actualiser",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id)
                        {
                            recreate();
                        }
                    });
            alertEserv.setNegativeButton(
                    "Quitter",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id)
                        {
                            finish();
                        }
                    });
            AlertDialog alertEser = alertEserv.create();
            alertEser.setIcon(R.drawable.ic_network_check);
            alertEser.setCanceledOnTouchOutside(false);
            alertEser.show();
        }
        FloatingActionButton fab_eliasse = findViewById(R.id.floatbut_eliasse_files);
        fab_files_ass_iard = findViewById(R.id.floatbut_download_iard);
        fab_files_ass_vie = findViewById(R.id.floatbut_download_vie);
        fab_files_sys_cmpt = findViewById(R.id.floatbut_download_systcomptbanc);
        fab_files_sys_allg = findViewById(R.id.floatbut_download_systallege);
        fab_files_sys_minimal = findViewById(R.id.floatbut_download_systminimal);
        fab_files_sys_normal = findViewById(R.id.floatbut_download_systnormal);
        fab_files_ass_iard_lay = findViewById(R.id.floatbut_download_iard_lay);
        fab_files_ass_vie_lay = findViewById(R.id.floatbut_download_vie_lay);
        fab_files_sys_cmpt_lay = findViewById(R.id.floatbut_download_systcomptbanc_lay);
        fab_files_sys_allg_lay = findViewById(R.id.floatbut_download_systallege_lay);
        fab_files_sys_minimal_lay = findViewById(R.id.floatbut_download_systminimal_lay);
        fab_files_sys_normal_lay = findViewById(R.id.floatbut_download_systnormal_lay);
        fab_eliasse.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
            if (!menuEstOuvert) {
                ouvreLeMenu();
            } else {
                fermeLeMenu();
            }
            }
        });
    }
    private void openEserviceWeb(String url)
    {
        navigateur = (WebView) findViewById(R.id.web_view_eserviceweb);
        WebSettings settings = navigateur.getSettings();settings.setJavaScriptEnabled(true);
        navigateur.setScrollBarStyle(View.SCROLLBARS_OUTSIDE_OVERLAY);navigateur.getSettings().setBuiltInZoomControls(true);
        navigateur.getSettings().setUseWideViewPort(true);navigateur.getSettings().setLoadWithOverviewMode(true);navigateur.canGoBack();
        navigateur.setWebChromeClient(new WebChromeClient());
        progressDialog = new ProgressDialog(EserviceWebConnect.this);
        progressDialog.setMessage("Chargement ...");
        progressDialog.show();
        navigateur.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url)
            {
                view.loadUrl(url);
                return true;
            }
            @Override
            public void onPageFinished(WebView view, String url)
            {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            }
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl)
            {
                Toast.makeText(EserviceWebConnect.this, "Erreur de lecture: " + description, Toast.LENGTH_SHORT).show();
            }
        });
        navigateur.loadUrl(url);
    }
    public void downloadEliasseFile(String title, String link)
    {
        Intent intEliass = new Intent(this, EliasseWeb.class);
        intEliass.putExtra("dataEliasseTitle", title);intEliass.putExtra("dataEliasseLink", link);
        startActivity(intEliass);
    }
    private void ouvreLeMenu()
    {
        menuEstOuvert = true;
        fab_files_ass_iard_lay.setVisibility(View.VISIBLE);fab_files_ass_vie_lay.setVisibility(View.VISIBLE);
        fab_files_sys_cmpt_lay.setVisibility(View.VISIBLE);fab_files_sys_allg_lay.setVisibility(View.VISIBLE);
        fab_files_sys_minimal_lay.setVisibility(View.VISIBLE);fab_files_sys_normal_lay.setVisibility(View.VISIBLE);
        fab_files_sys_normal_lay.animate().translationY(-getResources().getDimension(R.dimen.translateur_fab1));fab_files_sys_minimal_lay.animate().translationY(-getResources().getDimension(R.dimen.translateur_fab2));
        fab_files_sys_allg_lay.animate().translationY(-getResources().getDimension(R.dimen.translateur_fab3));fab_files_sys_cmpt_lay.animate().translationY(-getResources().getDimension(R.dimen.translateur_fab4));
        fab_files_ass_vie_lay.animate().translationY(-getResources().getDimension(R.dimen.translateur_fab5));fab_files_ass_iard_lay.animate().translationY(-getResources().getDimension(R.dimen.translateur_fab6));
    }
    private void fermeLeMenu()
    {
        menuEstOuvert = false;
        fab_files_ass_iard_lay.setVisibility(View.GONE);fab_files_ass_vie_lay.setVisibility(View.GONE);
        fab_files_sys_cmpt_lay.setVisibility(View.GONE);fab_files_sys_allg_lay.setVisibility(View.GONE);
        fab_files_sys_minimal_lay.setVisibility(View.GONE);fab_files_sys_normal_lay.setVisibility(View.GONE);
        fab_files_sys_normal_lay.animate().translationY(0);fab_files_sys_minimal_lay.animate().translationY(0);
        fab_files_sys_allg_lay.animate().translationY(0);fab_files_sys_cmpt_lay.animate().translationY(0);
        fab_files_ass_vie_lay.animate().translationY(0);fab_files_ass_iard_lay.animate().translationY(0);
    }
    @Override
    public void onBackPressed()
    {
        if (navigateur.isFocused() && navigateur.canGoBack()) {
            navigateur.goBack();
        } else {
            if (!menuEstOuvert) {
                super.onBackPressed();
            } else {
                fermeLeMenu();
            }
        }
    }
    public void telechargerIardFile(View view)
    {
        downloadEliasseFile(getString(R.string.txt_eservice_eliasse_assurance_iard),getString(R.string.txt_eservice_eliasse_assurance_iard_link));
    }
    public void telechargerVieFile(View view)
    {
        downloadEliasseFile(getString(R.string.txt_eservice_eliasse_assurance_vie),getString(R.string.txt_eservice_eliasse_assurance_vie_link));
    }
    public void telechargerBancFile(View view)
    {
        downloadEliasseFile(getString(R.string.txt_eservice_eliasse_banque),getString(R.string.txt_eservice_eliasse_banque_link));
    }
    public void telechargerAllgFile(View view)
    {
        downloadEliasseFile(getString(R.string.txt_eservice_eliasse_systemeallege),getString(R.string.txt_eservice_eliasse_systemeallege_link));
    }
    public void telechargerMiniFile(View view)
    {
        downloadEliasseFile(getString(R.string.txt_eservice_eliasse_systememinimal),getString(R.string.txt_eservice_eliasse_systememinimal_link));
    }
    public void telechargerNormalFile(View view)
    {
        downloadEliasseFile(getString(R.string.txt_eservice_eliasse_systemenormal),getString(R.string.txt_eservice_eliasse_systemenormal_link));
    }
    @Override
    public void finish()
    {
        super.finish();
    }
}