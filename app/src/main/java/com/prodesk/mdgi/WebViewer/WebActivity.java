package com.prodesk.mdgi.WebViewer;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.prodesk.mdgi.R;
//** Sam 17.08.2019 // 1.52pm **//
public class WebActivity extends AppCompatActivity
{
    private WebView bdkfwsytem;private ProgressDialog pxwheDdldkE;String web_title,web_link,web_icon;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);setContentView(R.layout.activity_web);Intent intWebAct = getIntent();
        web_link = intWebAct.getStringExtra("dataWebLink");
        web_title = intWebAct.getStringExtra("dataWebTitle");web_icon = intWebAct.getStringExtra("dataWebIcon");
        TextView tx = (TextView) findViewById(R.id.web_toolbar_title);
        tx.setText(web_title);
        Toolbar toolbar = (Toolbar) findViewById(R.id.web_toolbar_read);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        ImageView webIcon = (ImageView) findViewById(R.id.web_icon_toolbar);
        switch (web_icon)
        {
            case "facebook":
                webIcon.setImageResource(R.drawable.ic_facebook);
            break;
            case "youtube":
                webIcon.setImageResource(R.drawable.ic_youtube);
            break;
            case "twitter":
                webIcon.setImageResource(R.drawable.ic_twitter);
            break;
            case "fudpit":
                webIcon.setImageResource(R.drawable.ic_excel);
            break;
            case "docpdf":
                webIcon.setImageResource(R.drawable.ic_pdf);
            break;
            case "annexefiscale":
                webIcon.setImageResource(R.drawable.ic_ebook);
            break;
            case "cgi":
                webIcon.setImageResource(R.drawable.icon_ebook);
            break;
            case "sfi":
                webIcon.setImageResource(R.drawable.cogs);
            break;
            case "dfi":
                webIcon.setImageResource(R.drawable.icon_ebook);
            break;
            case "website":
                webIcon.setImageResource(R.drawable.website);
            break;
        }
        LinearLayout goback = (LinearLayout) findViewById(R.id.webactivity_retour);
        goback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                finish();
            }
        });
        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            openWebDgi(web_link);
        } else {
            AlertDialog.Builder ListComfocri = new AlertDialog.Builder(WebActivity.this);
            ListComfocri.setTitle("Erreur Internet !");
            ListComfocri.setMessage("Il vous faut internet pour lire cette page");
            ListComfocri.setCancelable(false);
            ListComfocri.setPositiveButton(
                    "Actualiser",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id)
                        {
                            recreate();
                        }
                    });

            ListComfocri.setNegativeButton(
                    "Quitter",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id)
                        {
                            finish();
                        }
                    });
            AlertDialog alertLisCom = ListComfocri.create();
            alertLisCom.setIcon(R.drawable.ic_network_check);
            alertLisCom.setCanceledOnTouchOutside(false);
            alertLisCom.show();
        }
    }
    private void openWebDgi(String url)
    {
        bdkfwsytem = (WebView) findViewById(R.id.web_view_websitedgi);
        WebSettings settings = bdkfwsytem.getSettings();settings.setJavaScriptEnabled(true);
        bdkfwsytem.setScrollBarStyle(View.SCROLLBARS_OUTSIDE_OVERLAY);
        bdkfwsytem.getSettings().setBuiltInZoomControls(true);bdkfwsytem.getSettings().setUseWideViewPort(true);
        bdkfwsytem.getSettings().setLoadWithOverviewMode(true);bdkfwsytem.canGoBack();
        bdkfwsytem.setWebChromeClient(new WebChromeClient());
        pxwheDdldkE = new ProgressDialog(WebActivity.this);
        pxwheDdldkE.setMessage("Chargement ...");
        pxwheDdldkE.show();
        bdkfwsytem.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url)
            {
                view.loadUrl(url);
                return true;
            }
            @Override
            public void onPageFinished(WebView view, String url)
            {
                if (pxwheDdldkE.isShowing()) {
                    pxwheDdldkE.dismiss();
                }
            }
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl)
            {
                AlertDialog.Builder focri = new AlertDialog.Builder(WebActivity.this);
                focri.setTitle("Erreur !");
                focri.setMessage("Une erreur est survenue, veuillez réessayer.");
                focri.setCancelable(true);
                focri.setPositiveButton(
                        "Continuer",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id)
                            {
                                recreate();
                            }
                        });

                focri.setNegativeButton(
                        "Quitter",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id)
                            {
                                finish();
                            }
                        });
                AlertDialog alertWebDgi = focri.create();
                alertWebDgi.setIcon(R.drawable.ic_network_check);
                alertWebDgi.show();
            }
        });
        bdkfwsytem.loadUrl(url);
    }
    @Override
    public void onBackPressed()
    {
        if (bdkfwsytem.isFocused() && bdkfwsytem.canGoBack()) {
            bdkfwsytem.goBack();
        } else {
            super.onBackPressed();
        }
    }
    @Override
    public void finish() {super.finish();}
}