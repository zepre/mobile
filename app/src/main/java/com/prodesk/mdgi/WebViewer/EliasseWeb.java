package com.prodesk.mdgi.WebViewer;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.prodesk.mdgi.R;
//** Sam 17.08.2019 // 1.49pm **//
public class EliasseWeb extends AppCompatActivity
{
    private WebView navigateur;
    private ProgressDialog progressDialog;
    String eliasse_title, eliasse_link;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);setContentView(R.layout.activity_eliasse);
        Intent intWebAct = getIntent();Intent intEserWeb = getIntent();
        eliasse_title = intEserWeb.getStringExtra("dataEliasseTitle");
        eliasse_link = intEserWeb.getStringExtra("dataEliasseLink");
        TextView tx = (TextView) findViewById(R.id.eliasse_toolbar_title);
        tx.setText(eliasse_title);
        Toolbar toolbar = (Toolbar) findViewById(R.id.eliasse_toolbar_read);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        LinearLayout goback = (LinearLayout) findViewById(R.id.eliasse_retour);
        goback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                finish();
            }
        });
        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            openWebEliasse(eliasse_link);
        } else {
            AlertDialog.Builder alertEserv = new AlertDialog.Builder(EliasseWeb.this);
            alertEserv.setTitle("Erreur Internet !");
            alertEserv.setMessage("Il vous faut internet télécharger ce document");
            alertEserv.setCancelable(false);
            alertEserv.setPositiveButton(
                    "Actualiser",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id)
                        {
                            recreate();
                        }
                    });

            alertEserv.setNegativeButton(
                    "Quitter",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id)
                        {
                            finish();
                        }
                    });
            AlertDialog alertEser = alertEserv.create();
            alertEser.setIcon(R.drawable.ic_network_check);
            alertEser.setCanceledOnTouchOutside(false);
            alertEser.show();
        }
    }
    private void openWebEliasse(String url)
    {
        navigateur = (WebView) findViewById(R.id.eliasse_view_web);
        WebSettings settings = navigateur.getSettings();settings.setJavaScriptEnabled(true);
        navigateur.setScrollBarStyle(View.SCROLLBARS_OUTSIDE_OVERLAY);navigateur.getSettings().setBuiltInZoomControls(true);
        navigateur.getSettings().setUseWideViewPort(true);navigateur.getSettings().setLoadWithOverviewMode(true);
        navigateur.setWebChromeClient(new WebChromeClient());
        progressDialog = new ProgressDialog(EliasseWeb.this);
        progressDialog.setMessage("Chargement ...");
        progressDialog.show();
        navigateur.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url)
            {
                view.loadUrl(url);
                return true;
            }
            @Override
            public void onPageFinished(WebView view, String url)
            {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            }
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl)
            {
                Toast.makeText(EliasseWeb.this, "Erreur de lecture: " + description, Toast.LENGTH_SHORT).show();
            }
        });
        navigateur.loadUrl(url);
    }
    @Override
    public void finish()
    {
        super.finish();
    }
}