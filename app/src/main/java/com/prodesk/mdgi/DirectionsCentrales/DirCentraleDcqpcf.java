package com.prodesk.mdgi.DirectionsCentrales;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import com.prodesk.mdgi.ContactEmail;
import com.prodesk.mdgi.R;
//** Jeud 05.09.2019 // 9.01pm **//
public class DirCentraleDcqpcf extends AppCompatActivity
{
    String txt_dir_centrale, txt_dir_latitude, txt_dir_longitude;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);setContentView(R.layout.activity_dircentrale_dcqpcf);
        Toolbar toolbar = (Toolbar) findViewById(R.id.dcqpcf_toolbar_read);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        Intent intDir = getIntent();
        txt_dir_latitude = getString(R.string.txt_dir_dcqpcf_latitude);
        txt_dir_longitude = getString(R.string.txt_dir_dcqpcf_longitude);
        txt_dir_centrale = getString(R.string.txt_dir_dcqpcf_titre);
        Button btn_call_dcqpcf = (Button) findViewById(R.id.dcqpcf_appeler_num);
        btn_call_dcqpcf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                appelerDcqpcf();
            }
        });
        Button btn_envoi_suggestdcqpcf = (Button) findViewById(R.id.dcqpcf_ecrire_mail);
        btn_envoi_suggestdcqpcf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                ecrireDirCentrale(txt_dir_centrale,"DIRECTION CENTRALE");
            }
        });
        LinearLayout goback = (LinearLayout) findViewById(R.id.dcqpcf_retour);
        goback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                finish();
            }
        });
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Fragment fragment = DirCentraleMap.newInstance(txt_dir_latitude, txt_dir_longitude, txt_dir_centrale);
        fragmentTransaction.add(R.id.dcqpcf_map_content, fragment);
        fragmentTransaction.commit();
    }
    public void appelerDcqpcf()
    {
        final Intent callIntent = new Intent(Intent.ACTION_CALL);
        try {
            if(Build.VERSION.SDK_INT > 22) {
                if (ActivityCompat.checkSelfPermission(DirCentraleDcqpcf.this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(DirCentraleDcqpcf.this, new String[]{android.Manifest.permission.CALL_PHONE}, 101);
                    return;
                }
                callIntent.setData(Uri.parse("tel:" + getString(R.string.txt_dir_dcqpcf_tel)));
                startActivity(callIntent);
            } else {
                if (ActivityCompat.checkSelfPermission(DirCentraleDcqpcf.this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(DirCentraleDcqpcf.this, new String[]{android.Manifest.permission.CALL_PHONE}, 101);
                    return;
                }
                callIntent.setData(Uri.parse("tel:" + getString(R.string.txt_dir_dcqpcf_tel)));
                startActivity(callIntent);
            }
        } catch (Exception ex) {}
    }
    public void ecrireDirCentrale(String title, String categorie)
    {
        Intent intEcrireAuDr = new Intent(DirCentraleDcqpcf.this, ContactEmail.class);
        intEcrireAuDr.putExtra("dataContactTitle", title);intEcrireAuDr.putExtra("dataContactCategorie", categorie);
        intEcrireAuDr.putExtra("dataContactDestinataire", getString(R.string.txt_contact_destinataire_dcqpcf));startActivity(intEcrireAuDr);
    }
}