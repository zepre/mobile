package com.prodesk.mdgi.DirectionsCentrales;
import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Color;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.prodesk.mdgi.R;
//** Jeud 10.10.2019 // 3.02pm **//
public class DirCentraleRgiVuecarte extends Fragment implements OnMapReadyCallback
{
    Double mapLat,mapLong;GoogleMap dcRgiMap;
    MapView dcRgiMapView;MarkerOptions rpiDran1, rpiDran2, rpiDran3, rpiDran4, rpiDras1, rpiDras2, rpiDras3, rpiAbengourou, rpiBouake, rpiDaloa, rpiSanpedro, rpiYamoussoukro;
    LocationManager locationManager;boolean statusOfGPS;FragmentManager fragmentManager;
    SupportMapFragment mFragment;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_dircentrale_rgi_vuecarte, container, false);
        if (!isGooglePlayServicesAvailable()) {
            Toast.makeText(getActivity(), "Service Google Play inaccessible !", Toast.LENGTH_SHORT).show();
        }
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        statusOfGPS = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        return view;
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        dcRgiMapView = (MapView)view.findViewById(R.id.dircentrale_rgi_map);
        if (dcRgiMapView != null) {
            dcRgiMapView.onCreate(null);dcRgiMapView.onResume();
            dcRgiMapView.getMapAsync(this);
        }
    }
    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        mapLat = Double.parseDouble(getString(R.string.txt_rgi_point_lat));mapLong = Double.parseDouble(getString(R.string.txt_rgi_point_lng));MapsInitializer.initialize(getContext());dcRgiMap = googleMap;
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.getUiSettings().setZoomGesturesEnabled(true);
        googleMap.getUiSettings().setCompassEnabled(true);
        LatLng ptDcRGI = new LatLng(mapLat,mapLong);
        LatLng ptrpiDran1 = new LatLng(Double.parseDouble(getString(R.string.txt_rpi_dran1_lat)),Double.parseDouble(getString(R.string.txt_rpi_dran1_long)));
        rpiDran1 = new MarkerOptions();rpiDran1.position(ptrpiDran1).icon(setIconColor("#c86331"));
        rpiDran1.title(getString(R.string.txt_rpi_dran1_titre));rpiDran1.snippet(getString(R.string.txt_rpi_dran1_adresse));googleMap.addMarker(rpiDran1);
        LatLng ptrpiDran2 = new LatLng(Double.parseDouble(getString(R.string.txt_rpi_dran2_lat)),Double.parseDouble(getString(R.string.txt_rpi_dran2_long)));
        rpiDran2 = new MarkerOptions();rpiDran2.position(ptrpiDran2).icon(setIconColor("#c86331"));rpiDran2.title(getString(R.string.txt_rpi_dran2_titre));
        rpiDran2.snippet(getString(R.string.txt_rpi_dran2_adresse));googleMap.addMarker(rpiDran2);
        LatLng ptrpiDran3 = new LatLng(Double.parseDouble(getString(R.string.txt_rpi_dran3_lat)),Double.parseDouble(getString(R.string.txt_rpi_dran3_long)));
        rpiDran3 = new MarkerOptions();rpiDran3.position(ptrpiDran3).icon(setIconColor("#c86331"));rpiDran3.title(getString(R.string.txt_rpi_dran3_titre));
        rpiDran3.snippet(getString(R.string.txt_rpi_dran3_adresse));googleMap.addMarker(rpiDran3);
        LatLng ptrpiDran4 = new LatLng(Double.parseDouble(getString(R.string.txt_rpi_dran4_lat)),Double.parseDouble(getString(R.string.txt_rpi_dran4_long)));
        rpiDran4 = new MarkerOptions();rpiDran4.position(ptrpiDran4).icon(setIconColor("#c86331"));
        rpiDran4.title(getString(R.string.txt_rpi_dran4_titre));rpiDran4.snippet(getString(R.string.txt_rpi_dran4_adresse));googleMap.addMarker(rpiDran4);
        LatLng ptrpiDras1 = new LatLng(Double.parseDouble(getString(R.string.txt_rpi_dras1_lat)),Double.parseDouble(getString(R.string.txt_rpi_dras1_long)));
        rpiDras1 = new MarkerOptions();rpiDras1.position(ptrpiDras1).icon(setIconColor("#c86331"));rpiDras1.title(getString(R.string.txt_rpi_dras1_titre));
        rpiDras1.snippet(getString(R.string.txt_rpi_dras1_adresse));googleMap.addMarker(rpiDras1);
        LatLng ptrpiDras2 = new LatLng(Double.parseDouble(getString(R.string.txt_rpi_dras2_lat)),Double.parseDouble(getString(R.string.txt_rpi_dras2_long)));
        rpiDras2 = new MarkerOptions();rpiDras2.position(ptrpiDras2).icon(setIconColor("#c86331"));
        rpiDras2.title(getString(R.string.txt_rpi_dras2_titre));rpiDras2.snippet(getString(R.string.txt_rpi_dras2_adresse));
        googleMap.addMarker(rpiDras2);
        LatLng ptrpiDras3 = new LatLng(Double.parseDouble(getString(R.string.txt_rpi_dras3_lat)),Double.parseDouble(getString(R.string.txt_rpi_dras3_long)));
        rpiDras3 = new MarkerOptions();rpiDras3.position(ptrpiDras3).icon(setIconColor("#c86331"));rpiDras3.title(getString(R.string.txt_rpi_dras3_titre));
        rpiDras3.snippet(getString(R.string.txt_rpi_dras3_adresse));googleMap.addMarker(rpiDras3);
        LatLng ptrpiAbengourou = new LatLng(Double.parseDouble(getString(R.string.txt_rpi_abengourou_lat)),Double.parseDouble(getString(R.string.txt_rpi_abengourou_long)));
        rpiAbengourou = new MarkerOptions();rpiAbengourou.position(ptrpiAbengourou).icon(setIconColor("#c86331"));rpiAbengourou.title(getString(R.string.txt_rpi_abengourou_titre));
        rpiAbengourou.snippet(getString(R.string.txt_rpi_abengourou_adresse));googleMap.addMarker(rpiAbengourou);
        LatLng ptrpiBouake = new LatLng(Double.parseDouble(getString(R.string.txt_rpi_bouake_lat)),Double.parseDouble(getString(R.string.txt_rpi_bouake_long)));
        rpiBouake = new MarkerOptions();rpiBouake.position(ptrpiBouake).icon(setIconColor("#c86331"));
        rpiBouake.title(getString(R.string.txt_rpi_bouake_titre));rpiBouake.snippet(getString(R.string.txt_rpi_bouake_adresse));googleMap.addMarker(rpiBouake);
        LatLng ptrpiDaloa = new LatLng(Double.parseDouble(getString(R.string.txt_rpi_daloa_lat)),Double.parseDouble(getString(R.string.txt_rpi_daloa_long)));
        rpiDaloa = new MarkerOptions();rpiDaloa.position(ptrpiDaloa).icon(setIconColor("#c86331"));
        rpiDaloa.title(getString(R.string.txt_rpi_daloa_titre));rpiDaloa.snippet(getString(R.string.txt_rpi_daloa_adresse));googleMap.addMarker(rpiDaloa);
        LatLng ptrpiSanpedro = new LatLng(Double.parseDouble(getString(R.string.txt_rpi_sanpedro_lat)),Double.parseDouble(getString(R.string.txt_rpi_sanpedro_long)));
        rpiSanpedro = new MarkerOptions();rpiSanpedro.position(ptrpiSanpedro).icon(setIconColor("#c86331"));
        rpiSanpedro.title(getString(R.string.txt_rpi_sanpedro_titre));rpiSanpedro.snippet(getString(R.string.txt_rpi_sanpedro_adresse));googleMap.addMarker(rpiSanpedro);
        LatLng ptrpiYamoussoukro = new LatLng(Double.parseDouble(getString(R.string.txt_rpi_yamoussoukro_lat)),Double.parseDouble(getString(R.string.txt_rpi_yamoussoukro_long)));
        rpiYamoussoukro = new MarkerOptions();rpiYamoussoukro.position(ptrpiYamoussoukro).icon(setIconColor("#c86331"));
        rpiYamoussoukro.title(getString(R.string.txt_rpi_yamoussoukro_titre));rpiYamoussoukro.snippet(getString(R.string.txt_rpi_yamoussoukro_adresse));googleMap.addMarker(rpiYamoussoukro);
        CameraPosition dcRgiPos = CameraPosition.builder().target(ptDcRGI).zoom(6).bearing(0).tilt(45).build();
        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(dcRgiPos));
    }
    private boolean isGooglePlayServicesAvailable()
    {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            GooglePlayServicesUtil.getErrorDialog(status, getActivity(), 0).show();
            return false;
        }
    }
    public BitmapDescriptor setIconColor(String kol)
    {
        float[] hsv = new float[3];Color.colorToHSV(Color.parseColor(kol), hsv);return BitmapDescriptorFactory.defaultMarker(hsv[0]);
    }
}