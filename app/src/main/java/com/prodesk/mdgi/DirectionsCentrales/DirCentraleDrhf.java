package com.prodesk.mdgi.DirectionsCentrales;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import com.prodesk.mdgi.ContactEmail;
import com.prodesk.mdgi.R;
//** Jeud 05.09.2019 // 9.07pm **//
public class DirCentraleDrhf extends AppCompatActivity
{
    String txt_dir_centrale, txt_dir_latitude, txt_dir_longitude;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);setContentView(R.layout.activity_dircentrale_drhf);
        Toolbar toolbar = (Toolbar) findViewById(R.id.drhf_toolbar_read);
        setSupportActionBar(toolbar);getSupportActionBar().setTitle(null);Intent intDir = getIntent();
        txt_dir_latitude = getString(R.string.txt_dir_drhf_latitude);txt_dir_longitude = getString(R.string.txt_dir_drhf_longitude);
        txt_dir_centrale = getString(R.string.txt_dir_drhf_titre);
        Button btn_call = (Button) findViewById(R.id.drhf_appeler_num);
        btn_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                appelerDrhf();
            }
        });
        Button btn_envoi_suggest = (Button) findViewById(R.id.drhf_ecrire_mail);
        btn_envoi_suggest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                ecrireDirCentrale(txt_dir_centrale,"DIRECTION CENTRALE");
            }
        });
        LinearLayout goback = (LinearLayout) findViewById(R.id.drhf_retour);
        goback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                finish();
            }
        });
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Fragment fragment = DirCentraleMap.newInstance(txt_dir_latitude, txt_dir_longitude, txt_dir_centrale);
        fragmentTransaction.add(R.id.drhf_map_content, fragment);
        fragmentTransaction.commit();
    }
    public void appelerDrhf()
    {
        final Intent callIntent = new Intent(Intent.ACTION_CALL);
        try {
            if(Build.VERSION.SDK_INT > 22) {
                if (ActivityCompat.checkSelfPermission(DirCentraleDrhf.this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(DirCentraleDrhf.this, new String[]{android.Manifest.permission.CALL_PHONE}, 101);
                    return;
                }
                callIntent.setData(Uri.parse("tel:" + getString(R.string.txt_dir_drhf_tel)));
                startActivity(callIntent);
            } else {
                if (ActivityCompat.checkSelfPermission(DirCentraleDrhf.this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(DirCentraleDrhf.this, new String[]{android.Manifest.permission.CALL_PHONE}, 101);
                    return;
                }
                callIntent.setData(Uri.parse("tel:" + getString(R.string.txt_dir_drhf_tel)));
                startActivity(callIntent);
            }
        } catch (Exception ex) {}
    }
    public void ecrireDirCentrale(String title, String categorie)
    {
        Intent intEcrireAuDr = new Intent(DirCentraleDrhf.this, ContactEmail.class);
        intEcrireAuDr.putExtra("dataContactTitle", title);intEcrireAuDr.putExtra("dataContactCategorie", categorie);
        intEcrireAuDr.putExtra("dataContactDestinataire", getString(R.string.txt_contact_destinataire_drhf));startActivity(intEcrireAuDr);
    }
}