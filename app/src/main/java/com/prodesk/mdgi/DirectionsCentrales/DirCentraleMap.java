package com.prodesk.mdgi.DirectionsCentrales;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.prodesk.mdgi.R;
//** Lun 19.08.2019 // 12.30pm **//
public class DirCentraleMap extends Fragment implements OnMapReadyCallback
{
    GoogleMap dirCentraleMap;MapView dirCentraleMapView;private String latitudeDir;
    private String longitudeDir;private String titleDir;Double newLat, newLong;
    public static DirCentraleMap newInstance(String lat, String lng, String tit)
    {
        Bundle bundle = new Bundle();bundle.putString("latitude", lat);
        bundle.putString("longitude", lng);bundle.putString("title", tit);
        DirCentraleMap fragmentDirMap = new DirCentraleMap();
        fragmentDirMap.setArguments(bundle);
        return fragmentDirMap;
    }
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_dircentralemap, container, false);
        readBundle(getArguments());return view;
    }
    private void readBundle(Bundle bundle)
    {
        if (bundle != null) {
            latitudeDir = bundle.getString("latitude");longitudeDir = bundle.getString("longitude");titleDir = bundle.getString("title");
        }
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        dirCentraleMapView = (MapView)view.findViewById(R.id.direction_centrale_map);
        if (dirCentraleMapView != null) {
            dirCentraleMapView.onCreate(null);dirCentraleMapView.onResume();
            dirCentraleMapView.getMapAsync(this);
        }
    }
    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        newLat = Double.parseDouble(latitudeDir);newLong = Double.parseDouble(longitudeDir);MapsInitializer.initialize(getContext());
        dirCentraleMap = googleMap;
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.getUiSettings().setZoomGesturesEnabled(true);
        googleMap.getUiSettings().setCompassEnabled(true);
        googleMap.addMarker(new MarkerOptions().position(new LatLng(newLat, newLong)).icon(setIconColor("#c86331")).title(titleDir));
        CameraPosition dirCentrale = CameraPosition.builder().target(new LatLng(newLat, newLong)).zoom(13).bearing(0).tilt(45).build();
        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(dirCentrale));
    }
    public BitmapDescriptor setIconColor(String kol)
    {
        float[] hsv = new float[3];Color.colorToHSV(Color.parseColor(kol), hsv);
        return BitmapDescriptorFactory.defaultMarker(hsv[0]);
    }
}