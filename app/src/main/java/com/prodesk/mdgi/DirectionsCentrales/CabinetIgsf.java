package com.prodesk.mdgi.DirectionsCentrales;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import com.prodesk.mdgi.ContactEmail;
import com.prodesk.mdgi.R;
//** Jeud 19.09.2019 // 1.52pm **//
public class CabinetIgsf extends AppCompatActivity
{
    String txt_igsf_titre, txt_dir_latitude, txt_dir_longitude;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);setContentView(R.layout.activity_dirigsf);
        Toolbar toolbar = (Toolbar) findViewById(R.id.igsf_toolbar_read);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        Intent intDir = getIntent();
        txt_dir_latitude = getString(R.string.txt_cabinet_igsf_lat);
        txt_dir_longitude = getString(R.string.txt_cabinet_igsf_long);
        txt_igsf_titre = getString(R.string.txt_cabinet_igsf_titre);
        Button btn_call_igsf = (Button) findViewById(R.id.igsf_appeler_num);
        btn_call_igsf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appelerIgsf();
            }
        });
        Button btn_envoi_suggestigsf = (Button) findViewById(R.id.igsf_ecrire_mail);
        btn_envoi_suggestigsf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ecrireDirCentrale(txt_igsf_titre,"CABINET DU DIRECTEUR",getString(R.string.txt_contact_destinataire_igsf));
            }
        });
        LinearLayout goback = (LinearLayout) findViewById(R.id.igsf_retour);
        goback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Fragment fragment = DirCentraleMap.newInstance(txt_dir_latitude, txt_dir_longitude, txt_igsf_titre);
        fragmentTransaction.add(R.id.igsf_map_content, fragment);
        fragmentTransaction.commit();
    }
    public void appelerIgsf()
    {
        final Intent callIntent = new Intent(Intent.ACTION_CALL);
        try {
            if(Build.VERSION.SDK_INT > 22) {
                AlertDialog.Builder buildCall = new AlertDialog.Builder(CabinetIgsf.this);
                buildCall.setMessage("Veuillez choisir le numéro à appeler !");
                buildCall.setCancelable(true);
                buildCall.setPositiveButton(
                        getString(R.string.txt_cabinet_igsf_secretariat_tel1),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                if (ActivityCompat.checkSelfPermission(CabinetIgsf.this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                    ActivityCompat.requestPermissions(CabinetIgsf.this, new String[]{android.Manifest.permission.CALL_PHONE}, 101);
                                    return;
                                }
                                callIntent.setData(Uri.parse("tel:" + getString(R.string.txt_cabinet_igsf_secretariat_tel1)));
                                startActivity(callIntent);
                            }
                        });
                buildCall.setNegativeButton(
                        getString(R.string.txt_cabinet_igsf_secretariat_tel2),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                if (ActivityCompat.checkSelfPermission(CabinetIgsf.this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                    ActivityCompat.requestPermissions(CabinetIgsf.this, new String[]{android.Manifest.permission.CALL_PHONE}, 101);
                                    return;
                                }
                                callIntent.setData(Uri.parse("tel:" + getString(R.string.txt_cabinet_igsf_secretariat_tel2)));
                                startActivity(callIntent);
                            }
                        });
                AlertDialog alert11 = buildCall.create();
                alert11.show();
            } else {
                AlertDialog.Builder buildCall = new AlertDialog.Builder(CabinetIgsf.this);
                buildCall.setMessage("Veuillez choisir le numéro à appeler !");
                buildCall.setCancelable(true);
                buildCall.setPositiveButton(
                        getString(R.string.txt_cabinet_igsf_secretariat_tel1),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                if (ActivityCompat.checkSelfPermission(CabinetIgsf.this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                    ActivityCompat.requestPermissions(CabinetIgsf.this, new String[]{android.Manifest.permission.CALL_PHONE}, 101);
                                    return;
                                }
                                callIntent.setData(Uri.parse("tel:" + getString(R.string.txt_cabinet_igsf_secretariat_tel1)));
                                startActivity(callIntent);
                            }
                        });
                buildCall.setNegativeButton(
                        getString(R.string.txt_cabinet_igsf_secretariat_tel2),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                if (ActivityCompat.checkSelfPermission(CabinetIgsf.this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                    ActivityCompat.requestPermissions(CabinetIgsf.this, new String[]{android.Manifest.permission.CALL_PHONE}, 101);
                                    return;
                                }
                                callIntent.setData(Uri.parse("tel:" + getString(R.string.txt_cabinet_igsf_secretariat_tel2)));
                                startActivity(callIntent);
                            }
                        });
                AlertDialog alert11 = buildCall.create();
                alert11.show();
            }
        } catch (Exception ex) {}
    }
    public void ecrireDirCentrale(String title, String categorie, String email)
    {
        Intent intEcrireAuDr = new Intent(CabinetIgsf.this, ContactEmail.class);
        intEcrireAuDr.putExtra("dataContactTitle", title);intEcrireAuDr.putExtra("dataContactCategorie", categorie);
        intEcrireAuDr.putExtra("dataContactDestinataire", email);
        startActivity(intEcrireAuDr);
    }
}