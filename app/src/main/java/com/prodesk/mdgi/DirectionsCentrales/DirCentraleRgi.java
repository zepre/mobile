package com.prodesk.mdgi.DirectionsCentrales;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import com.prodesk.mdgi.R;
//** Jeud 10.10.2019 // 10.37am **//
public class DirCentraleRgi extends AppCompatActivity
{
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener()
    {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item)
        {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            switch (item.getItemId())
            {
                case R.id.nav_rgi_vuerpi:
                    transaction.replace(R.id.frame_dir_centrale_rgi, new DirCentraleRgiVueRpi()).commit();
                return true;
                case R.id.nav_rgi_vuecarte:
                    transaction.replace(R.id.frame_dir_centrale_rgi, new DirCentraleRgiVuecarte()).commit();
                return true;
            }
            return false;
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dircentrale_rgi);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.nav_dir_centrale_rgi);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.frame_dir_centrale_rgi, new DirCentraleRgiVueRpi()).commit();
    }
}