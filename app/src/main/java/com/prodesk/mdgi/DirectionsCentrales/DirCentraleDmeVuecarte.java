package com.prodesk.mdgi.DirectionsCentrales;
import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Color;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.prodesk.mdgi.R;
//** Lund 14.10.2019 // 3.02pm **//
public class DirCentraleDmeVuecarte extends Fragment implements OnMapReadyCallback
{
    Double mapLat,mapLong;GoogleMap dcDmeMap;
    MapView dcDmeMapView;MarkerOptions cmeDjibi, cmeMarcory, cmePortBouet, cmePlateau;
    LocationManager locationManager;boolean statusOfGPS;FragmentManager fragmentManager;
    SupportMapFragment mFragment;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_dircentrale_dme_vuecarte, container, false);
        if (!isGooglePlayServicesAvailable()) {
            Toast.makeText(getActivity(), "Service Google Play inaccessible !", Toast.LENGTH_SHORT).show();
        }
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        statusOfGPS = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        return view;
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        dcDmeMapView = (MapView)view.findViewById(R.id.dircentrale_dme_map);
        if (dcDmeMapView != null) {
            dcDmeMapView.onCreate(null);dcDmeMapView.onResume();
            dcDmeMapView.getMapAsync(this);
        }
    }
    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        mapLat = Double.parseDouble(getString(R.string.txt_dme_point_lat));mapLong = Double.parseDouble(getString(R.string.txt_dme_point_lng));MapsInitializer.initialize(getContext());dcDmeMap = googleMap;
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.getUiSettings().setZoomGesturesEnabled(true);
        googleMap.getUiSettings().setCompassEnabled(true);
        LatLng ptDcDME = new LatLng(mapLat,mapLong);
        LatLng ptcmeDjibi = new LatLng(Double.parseDouble(getString(R.string.txt_cme_djibi_lat)),Double.parseDouble(getString(R.string.txt_cme_djibi_long)));
        cmeDjibi = new MarkerOptions();
        cmeDjibi.position(ptcmeDjibi).icon(setIconColor("#c86331"));
        cmeDjibi.title(getString(R.string.txt_cme_djibi_titre));
        cmeDjibi.snippet(getString(R.string.txt_cme_djibi_adresse));
        googleMap.addMarker(cmeDjibi);
        LatLng ptcmeMarcory = new LatLng(Double.parseDouble(getString(R.string.txt_cme_marcory_lat)),Double.parseDouble(getString(R.string.txt_cme_marcory_long)));
        cmeMarcory = new MarkerOptions();
        cmeMarcory.position(ptcmeMarcory).icon(setIconColor("#c86331"));
        cmeMarcory.title(getString(R.string.txt_cme_marcory_titre));
        cmeMarcory.snippet(getString(R.string.txt_cme_marcory_adresse));
        googleMap.addMarker(cmeMarcory);
        LatLng ptcmePortBouet = new LatLng(Double.parseDouble(getString(R.string.txt_cme_portbouet_lat)),Double.parseDouble(getString(R.string.txt_cme_portbouet_long)));
        cmePortBouet = new MarkerOptions();
        cmePortBouet.position(ptcmePortBouet).icon(setIconColor("#c86331"));
        cmePortBouet.title(getString(R.string.txt_cme_portbouet_titre));
        cmePortBouet.snippet(getString(R.string.txt_cme_portbouet_adresse));
        googleMap.addMarker(cmePortBouet);
        LatLng ptcmePlateau = new LatLng(Double.parseDouble(getString(R.string.txt_cme_plateau_lat)),Double.parseDouble(getString(R.string.txt_cme_plateau_long)));
        cmePlateau = new MarkerOptions();
        cmePlateau.position(ptcmePlateau).icon(setIconColor("#c86331"));
        cmePlateau.title(getString(R.string.txt_cme_plateau_titre));
        cmePlateau.snippet(getString(R.string.txt_cme_plateau_adresse));
        googleMap.addMarker(cmePlateau);
        CameraPosition dcDmePos = CameraPosition.builder().target(ptDcDME).zoom(12).bearing(0).tilt(45).build();
        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(dcDmePos));
    }
    private boolean isGooglePlayServicesAvailable()
    {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            GooglePlayServicesUtil.getErrorDialog(status, getActivity(), 0).show();
            return false;
        }
    }
    public BitmapDescriptor setIconColor(String kol)
    {
        float[] hsv = new float[3];Color.colorToHSV(Color.parseColor(kol), hsv);return BitmapDescriptorFactory.defaultMarker(hsv[0]);
    }
}