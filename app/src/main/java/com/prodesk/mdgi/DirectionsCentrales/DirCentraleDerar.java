package com.prodesk.mdgi.DirectionsCentrales;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.prodesk.mdgi.ContactEmail;
import com.prodesk.mdgi.R;
//** Jeud 05.09.2019 // 9.02pm **//
public class DirCentraleDerar extends AppCompatActivity
{
    String txt_dir_centrale, txt_dir_latitude, txt_dir_longitude;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);setContentView(R.layout.activity_dircentrale_derar);
        Toolbar toolbar = (Toolbar) findViewById(R.id.derar_toolbar_read);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        Intent intDir = getIntent();
        txt_dir_latitude = getString(R.string.txt_dir_derar_latitude);
        txt_dir_longitude = getString(R.string.txt_dir_derar_longitude);
        txt_dir_centrale = getString(R.string.txt_dir_derar_titre);
        Button btn_call_derar = (Button) findViewById(R.id.derar_appeler_num);
        btn_call_derar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                appelerDerar();
            }
        });
        Button btn_envoi_suggestderar = (Button) findViewById(R.id.derar_ecrire_mail);
        btn_envoi_suggestderar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                ecrireDirCentrale(txt_dir_centrale,"DIRECTION CENTRALE");
            }
        });
        LinearLayout goback = (LinearLayout) findViewById(R.id.derar_retour);
        goback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                finish();
            }
        });
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Fragment fragment = DirCentraleMap.newInstance(txt_dir_latitude, txt_dir_longitude, txt_dir_centrale);
        fragmentTransaction.add(R.id.derar_map_content, fragment);
        fragmentTransaction.commit();
    }
    public void appelerDerar()
    {
        Toast.makeText(DirCentraleDerar.this,getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
        return;
    }
    public void ecrireDirCentrale(String title, String categorie)
    {
        Intent intEcrireAuDr = new Intent(DirCentraleDerar.this, ContactEmail.class);
        intEcrireAuDr.putExtra("dataContactTitle", title);intEcrireAuDr.putExtra("dataContactCategorie", categorie);
        intEcrireAuDr.putExtra("dataContactDestinataire", getString(R.string.txt_contact_destinataire_derar));startActivity(intEcrireAuDr);
    }
}