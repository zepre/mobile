package com.prodesk.mdgi.DirectionsCentrales;
import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;
import com.prodesk.mdgi.ContactEmail;
import com.prodesk.mdgi.R;
//** Jeud 10.10.2019 // 3.01pm **//
public class DirCentraleRgiVueRpi extends Fragment
{
    String dircentrale_rgi_num;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_dircentrale_rgi_vuerpi, container, false);
        Button call_dircentrale_rgi = (Button)view.findViewById(R.id.rgi_appeler_num);
        call_dircentrale_rgi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                dircentrale_rgi_num = getString(R.string.txt_dir_rgi_tel);
                apellerDirCentraleRgi(dircentrale_rgi_num);
            }
        });
        ImageView call_rpi_dran1 = (ImageView)view.findViewById(R.id.rpi_dran1_call);
        call_rpi_dran1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
            if (getString(R.string.txt_rpi_dran1_tel) == getString(R.string.txt_dr_numero_null)) {
                Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
            } else {
                dircentrale_rgi_num = getString(R.string.txt_rpi_dran1_tel);
                apellerDirCentraleRgi(dircentrale_rgi_num);
            }
            }
        });
        ImageView call_rpi_dran2 = (ImageView)view.findViewById(R.id.rpi_dran2_call);
        call_rpi_dran2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_rpi_dran2_tel) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dircentrale_rgi_num = getString(R.string.txt_rpi_dran2_tel);
                    apellerDirCentraleRgi(dircentrale_rgi_num);
                }
            }
        });
        ImageView call_rpi_dran3 = (ImageView)view.findViewById(R.id.rpi_dran3_call);
        call_rpi_dran3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_rpi_dran3_tel) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dircentrale_rgi_num = getString(R.string.txt_rpi_dran3_tel);
                    apellerDirCentraleRgi(dircentrale_rgi_num);
                }
            }
        });
        ImageView call_rpi_dran4 = (ImageView)view.findViewById(R.id.rpi_dran4_call);
        call_rpi_dran4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_rpi_dran4_tel) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dircentrale_rgi_num = getString(R.string.txt_rpi_dran4_tel);
                    apellerDirCentraleRgi(dircentrale_rgi_num);
                }
            }
        });
        ImageView call_rpi_dras1 = (ImageView)view.findViewById(R.id.rpi_dras1_call);
        call_rpi_dras1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_rpi_dras1_tel) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dircentrale_rgi_num = getString(R.string.txt_rpi_dras1_tel);
                    apellerDirCentraleRgi(dircentrale_rgi_num);
                }
            }
        });
        ImageView call_rpi_dras2 = (ImageView)view.findViewById(R.id.rpi_dras2_call);
        call_rpi_dras2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_rpi_dras2_tel) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dircentrale_rgi_num = getString(R.string.txt_rpi_dras2_tel);
                    apellerDirCentraleRgi(dircentrale_rgi_num);
                }
            }
        });
        ImageView call_rpi_dras3 = (ImageView)view.findViewById(R.id.rpi_dras3_call);
        call_rpi_dras3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_rpi_dras3_tel) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    AlertDialog.Builder buildCall = new AlertDialog.Builder(getContext());
                    buildCall.setMessage("Veuillez choisir le numéro à appeler !");
                    buildCall.setCancelable(true);
                    buildCall.setPositiveButton(
                        getString(R.string.txt_rpi_dras3_tel1),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                            dircentrale_rgi_num = getString(R.string.txt_rpi_dras3_tel1);
                            apellerDirCentraleRgi(dircentrale_rgi_num);
                            }
                        });

                    buildCall.setNegativeButton(
                        getString(R.string.txt_rpi_dras3_tel2),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                            dircentrale_rgi_num = getString(R.string.txt_rpi_dras3_tel2);
                            apellerDirCentraleRgi(dircentrale_rgi_num);
                            }
                        });
                    AlertDialog alert11 = buildCall.create();
                    alert11.show();
                    return;
                }
            }
        });
        ImageView call_rpi_abengourou = (ImageView)view.findViewById(R.id.rpi_abengourou_call);
        call_rpi_abengourou.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_rpi_abengourou_tel) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dircentrale_rgi_num = getString(R.string.txt_rpi_abengourou_tel);
                    apellerDirCentraleRgi(dircentrale_rgi_num);
                }
            }
        });
        ImageView call_rpi_bouake = (ImageView)view.findViewById(R.id.rpi_bouake_call);
        call_rpi_bouake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_rpi_bouake_tel) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dircentrale_rgi_num = getString(R.string.txt_rpi_bouake_tel);
                    apellerDirCentraleRgi(dircentrale_rgi_num);
                }
            }
        });
        ImageView call_rpi_daloa = (ImageView)view.findViewById(R.id.rpi_daloa_call);
        call_rpi_daloa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_rpi_daloa_tel) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dircentrale_rgi_num = getString(R.string.txt_rpi_daloa_tel);
                    apellerDirCentraleRgi(dircentrale_rgi_num);
                }
            }
        });
        ImageView call_rpi_sanpedro = (ImageView)view.findViewById(R.id.rpi_sanpedro_call);
        call_rpi_sanpedro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_rpi_sanpedro_tel) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dircentrale_rgi_num = getString(R.string.txt_rpi_sanpedro_tel);
                    apellerDirCentraleRgi(dircentrale_rgi_num);
                }
            }
        });
        ImageView call_rpi_yamoussoukro = (ImageView)view.findViewById(R.id.rpi_yamoussoukro_call);
        call_rpi_yamoussoukro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (getString(R.string.txt_rpi_yamoussoukro_tel) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(),getString(R.string.txt_dr_cannot_call_alert),Toast.LENGTH_LONG).show();
                } else {
                    dircentrale_rgi_num = getString(R.string.txt_rpi_yamoussoukro_tel);
                    apellerDirCentraleRgi(dircentrale_rgi_num);
                }
            }
        });
        Button envoi_mail_rgi = (Button)view.findViewById(R.id.rgi_ecrire_mail);
        envoi_mail_rgi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
            ecrireDirGenerale(getString(R.string.txt_dir_rgi_titre),"DIRECTION CENTRALE");
            }
        });
        return view;
    }
    public void apellerDirCentraleRgi(String rgi_appel)
    {
        try {
            if(Build.VERSION.SDK_INT > 22) {
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE}, 101);
                    return;
                }
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + rgi_appel));
                startActivity(callIntent);
            }
            else {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + rgi_appel));
                startActivity(callIntent);
            }
        }
        catch (Exception ex) {}
    }
    public void ecrireDirGenerale(String title, String categorie)
    {
        Intent intEcrireAuDr = new Intent(getContext(), ContactEmail.class);
        intEcrireAuDr.putExtra("dataContactTitle", title);intEcrireAuDr.putExtra("dataContactCategorie", categorie);
        intEcrireAuDr.putExtra("dataContactDestinataire", getString(R.string.txt_contact_destinataire));startActivity(intEcrireAuDr);
    }
}