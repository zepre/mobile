package com.prodesk.mdgi.DirectionsCentrales;
import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;
import com.prodesk.mdgi.ContactEmail;
import com.prodesk.mdgi.R;
//** Lund 14.10.2019 // 3.25pm **//
public class DirCentraleDmeVueCme extends Fragment
{
    String dircentrale_dme_num;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dircentrale_dme_vuecme, container, false);
        Button call_dircentrale_dme = (Button) view.findViewById(R.id.dme_appeler_num);
        call_dircentrale_dme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            if (getString(R.string.txt_dir_dme_tel) == getString(R.string.txt_dr_numero_null)) {
                Toast.makeText(getActivity(), getString(R.string.txt_dr_cannot_call_alert), Toast.LENGTH_LONG).show();
            } else {
                AlertDialog.Builder buildCall = new AlertDialog.Builder(getContext());
                buildCall.setMessage("Veuillez choisir le numéro à appeler !");
                buildCall.setCancelable(true);
                buildCall.setPositiveButton(
                    getString(R.string.txt_dir_dme_tel1),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dircentrale_dme_num = getString(R.string.txt_dir_dme_tel1);
                            apellerDirCentraleDme(dircentrale_dme_num);
                        }
                    });
                buildCall.setNegativeButton(
                    getString(R.string.txt_dir_dme_tel2),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dircentrale_dme_num = getString(R.string.txt_dir_dme_tel2);
                            apellerDirCentraleDme(dircentrale_dme_num);
                        }
                    });
                AlertDialog alert11 = buildCall.create();
                alert11.show();
                return;
            }
            }
        });
        ImageView call_cme_djibi = (ImageView) view.findViewById(R.id.cme_djibi_call);
        call_cme_djibi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getString(R.string.txt_cme_djibi_tel) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(), getString(R.string.txt_dr_cannot_call_alert), Toast.LENGTH_LONG).show();
                } else {
                    dircentrale_dme_num = getString(R.string.txt_cme_djibi_tel);
                    apellerDirCentraleDme(dircentrale_dme_num);
                }
            }
        });
        ImageView call_cme_marcory = (ImageView) view.findViewById(R.id.cme_marcory_call);
        call_cme_marcory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getString(R.string.txt_cme_marcory_tel) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(), getString(R.string.txt_dr_cannot_call_alert), Toast.LENGTH_LONG).show();
                } else {
                    dircentrale_dme_num = getString(R.string.txt_cme_marcory_tel);
                    apellerDirCentraleDme(dircentrale_dme_num);
                }
            }
        });
        ImageView call_cme_portbouet = (ImageView) view.findViewById(R.id.cme_portbouet_call);
        call_cme_portbouet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getString(R.string.txt_cme_portbouet_tel) == getString(R.string.txt_dr_numero_null)) {
                    Toast.makeText(getActivity(), getString(R.string.txt_dr_cannot_call_alert), Toast.LENGTH_LONG).show();
                } else {
                    AlertDialog.Builder buildCall = new AlertDialog.Builder(getContext());
                    buildCall.setMessage("Veuillez choisir le numéro à appeler !");
                    buildCall.setCancelable(true);
                    buildCall.setPositiveButton(
                        getString(R.string.txt_cme_portbouet_tel1),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dircentrale_dme_num = getString(R.string.txt_cme_portbouet_tel1);
                                apellerDirCentraleDme(dircentrale_dme_num);
                            }
                        });
                    buildCall.setNegativeButton(
                        getString(R.string.txt_cme_portbouet_tel2),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dircentrale_dme_num = getString(R.string.txt_cme_portbouet_tel2);
                                apellerDirCentraleDme(dircentrale_dme_num);
                            }
                        });
                    AlertDialog alert11 = buildCall.create();
                    alert11.show();
                    return;
                }
            }
        });
        Button envoi_mail_dme = (Button)view.findViewById(R.id.dme_ecrire_mail);
        envoi_mail_dme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                ecrireDirGenerale(getString(R.string.txt_dir_dme_titre),"DIRECTION CENTRALE");
            }
        });
        return view;
    }
    public void apellerDirCentraleDme(String rgi_appel)
    {
        try {
            if(Build.VERSION.SDK_INT > 22) {
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE}, 101);
                    return;
                }
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + rgi_appel));
                startActivity(callIntent);
            }
            else {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + rgi_appel));
                startActivity(callIntent);
            }
        }
        catch (Exception ex) {}
    }
    public void ecrireDirGenerale(String title, String categorie)
    {
        Intent intEcrireAuDr = new Intent(getContext(), ContactEmail.class);
        intEcrireAuDr.putExtra("dataContactTitle", title);intEcrireAuDr.putExtra("dataContactCategorie", categorie);
        intEcrireAuDr.putExtra("dataContactDestinataire", getString(R.string.txt_contact_destinataire));startActivity(intEcrireAuDr);
    }
}