package com.prodesk.mdgi.DirectionsCentrales;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import com.prodesk.mdgi.ContactEmail;
import com.prodesk.mdgi.R;
//** Merc 18.09.2019 // 10.15am **//
public class CabinetDirGalAdjoint extends AppCompatActivity
{
    String txt_dga1_titre, txt_dga2_titre,txt_dga_titre, txt_dir_latitude, txt_dir_longitude;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);setContentView(R.layout.activity_dirgaladjoint);
        Toolbar toolbar = (Toolbar) findViewById(R.id.dga_toolbar_read);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        Intent intDir = getIntent();
        txt_dir_latitude = getString(R.string.txt_cabinet_dga_lat);
        txt_dir_longitude = getString(R.string.txt_cabinet_dga_long);
        txt_dga_titre = getString(R.string.txt_cabinet_dga_titre);
        txt_dga1_titre = getString(R.string.txt_cabinet_dga_titre1);
        txt_dga2_titre = getString(R.string.txt_cabinet_dga_titre2);
        Button btn_call_dga1 = (Button) findViewById(R.id.dga1_appeler_num);
        btn_call_dga1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appelerDga1();
            }
        });
        Button btn_call_dga2 = (Button) findViewById(R.id.dga2_appeler_num);
        btn_call_dga2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appelerDga2();
            }
        });
        Button btn_envoi_suggestdga1 = (Button) findViewById(R.id.dga1_ecrire_mail);
        btn_envoi_suggestdga1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ecrireDirCentrale(txt_dga1_titre,"CABINET DU DIRECTEUR",getString(R.string.txt_contact_destinataire_dga1));
            }
        });
        Button btn_envoi_suggestdga2 = (Button) findViewById(R.id.dga2_ecrire_mail);
        btn_envoi_suggestdga2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ecrireDirCentrale(txt_dga2_titre,"CABINET DU DIRECTEUR",getString(R.string.txt_contact_destinataire_dga2));
            }
        });
        LinearLayout goback = (LinearLayout) findViewById(R.id.dga_retour);
        goback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Fragment fragment = DirCentraleMap.newInstance(txt_dir_latitude, txt_dir_longitude, txt_dga_titre);
        fragmentTransaction.add(R.id.dga_map_content, fragment);
        fragmentTransaction.commit();
    }
    public void appelerDga1()
    {
        final Intent callIntent = new Intent(Intent.ACTION_CALL);
        try {
            if(Build.VERSION.SDK_INT > 22) {
                if (ActivityCompat.checkSelfPermission(CabinetDirGalAdjoint.this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(CabinetDirGalAdjoint.this, new String[]{android.Manifest.permission.CALL_PHONE}, 101);
                    return;
                }
                callIntent.setData(Uri.parse("tel:" + getString(R.string.txt_cabinet_dga1_secretariat_tel)));
                startActivity(callIntent);
            } else {
                if (ActivityCompat.checkSelfPermission(CabinetDirGalAdjoint.this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(CabinetDirGalAdjoint.this, new String[]{android.Manifest.permission.CALL_PHONE}, 101);
                    return;
                }
                callIntent.setData(Uri.parse("tel:" + getString(R.string.txt_cabinet_dga1_secretariat_tel)));
                startActivity(callIntent);
            }
        } catch (Exception ex) {}
    }
    public void appelerDga2()
    {
        final Intent callIntent = new Intent(Intent.ACTION_CALL);
        try {
            if(Build.VERSION.SDK_INT > 22) {
                if (ActivityCompat.checkSelfPermission(CabinetDirGalAdjoint.this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(CabinetDirGalAdjoint.this, new String[]{android.Manifest.permission.CALL_PHONE}, 101);
                    return;
                }
                callIntent.setData(Uri.parse("tel:" + getString(R.string.txt_cabinet_dga2_secretariat_tel)));
                startActivity(callIntent);
            } else {
                if (ActivityCompat.checkSelfPermission(CabinetDirGalAdjoint.this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(CabinetDirGalAdjoint.this, new String[]{android.Manifest.permission.CALL_PHONE}, 101);
                    return;
                }
                callIntent.setData(Uri.parse("tel:" + getString(R.string.txt_cabinet_dga2_secretariat_tel)));
                startActivity(callIntent);
            }
        } catch (Exception ex) {}
    }
    public void ecrireDirCentrale(String title, String categorie, String email)
    {
        Intent intEcrireAuDr = new Intent(CabinetDirGalAdjoint.this, ContactEmail.class);
        intEcrireAuDr.putExtra("dataContactTitle", title);intEcrireAuDr.putExtra("dataContactCategorie", categorie);
        intEcrireAuDr.putExtra("dataContactDestinataire", email);startActivity(intEcrireAuDr);
    }
}