package com.prodesk.mdgi;
/**
 * Created by Candidell on 08 aout 2019.
 */
public class AdminDirection
{
    private String directionId, directionTitre, directionContact, directionAdresse, directionCategorie, directionLocalite, directionLocaliteprincipale, directionClass;

    public void setDirectionId(String directionId) { this.directionId = directionId;}
    public String getDirectionId(){return directionId;}

    public void setDirectionTitre(String directionTitre) { this.directionTitre = directionTitre;}
    public String getDirectionTitre(){return directionTitre;}

    public void setDirectionContact(String directionContact){ this.directionContact = directionContact;}
    public String getDirectionContact(){return directionContact;}

    public void setDirectionAdresse(String directionAdresse) {this.directionAdresse = directionAdresse;}
    public String getDirectionAdresse(){return directionAdresse;}

    public void setDirectionCategorie(String directionCategorie) {this.directionCategorie = directionCategorie;}
    public String getDirectionCategorie(){return directionCategorie;}

    public void setDirectionLocalite(String directionLocalite) {this.directionLocalite = directionLocalite;}
    public String getDirectionLocalite(){return directionLocalite;}

    public void setDirectionLocaliteprincipale(String directionLocaliteprincipale) {this.directionLocaliteprincipale = directionLocaliteprincipale;}
    public String getDirectionLocaliteprincipale(){return directionLocaliteprincipale;}

    public void setDirectionClass(String directionClass) {this.directionClass = directionClass;}
    public String getDirectionClass(){return directionClass;}
}