package com.prodesk.mdgi;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;
import com.prodesk.mdgi.Communiques.*;
import com.prodesk.mdgi.DonneesLocal.DonneeLocalHelper;
/**
 * Created by Candidell on 07/07/2019.
 */
public class OfflineSlideAdapter extends PagerAdapter
{
    private ArrayList<Integer> Images;
    private int [] image_resource = {1,2,3};private Context ctx;private LayoutInflater inflater;
    final String[] slideDataPicture = new String[image_resource.length];
    final String[] slideDataTitre = new String[image_resource.length];DonneeLocalHelper mdgiciData;
    public OfflineSlideAdapter(Context ctx, ArrayList<Integer> Images)
    {
        this.ctx = ctx;this.Images = Images;
        inflater = LayoutInflater.from(ctx);mdgiciData = new DonneeLocalHelper(ctx);
    }
    @Override
    public Object instantiateItem(ViewGroup container, int position)
    {
        inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View item_view = inflater.inflate(R.layout.slide_element_offline, null);
        assert item_view != null;
        final ImageView imageSlide = (ImageView)item_view.findViewById(R.id.slide_image_item_offline);
        final TextView titreSlide = (TextView)item_view.findViewById(R.id.slide_title_text_offline);
        getAllSlide();
        titreSlide.setText(slideDataTitre[position]);
        imageSlide.setImageBitmap(mdgiciData.afficheImagesHorsLigne(Integer.parseInt(slideDataPicture[position])));
        container.addView(item_view, 0);
        ImageView gotoListCom = (ImageView)item_view.findViewById(R.id.slide_image_item_offline);
        gotoListCom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intComList = new Intent(v.getContext(), ListeCommuniqueOffline.class);
                v.getContext().startActivity(intComList);
            }
        });
        return item_view;
    }
    @Override
    public void destroyItem(ViewGroup container, int position, Object object)
    {
        ViewPager vp = (ViewPager) container;View item_view = (View) object;vp.removeView(item_view);
    }
    @Override
    public int getCount()
    {
        return image_resource.length;
    }
    @Override
    public boolean isViewFromObject(View view, Object object)
    {
        return view == object;
    }
    private class offlineSlideTask extends AsyncTask<Void,Void,Void>
    {
        Context ctx;
        protected void onPreExecute()
        {
            super.onPreExecute();
        }
        protected Void doInBackground(Void...params)
        {
            slideDataTitre[0] = mdgiciData.afficheSlideCom1();slideDataTitre[1] = mdgiciData.afficheSlideCom2();slideDataTitre[2] = mdgiciData.afficheSlideCom3();return null;
        }
        protected void onPostExecute(Void result)
        {}
    }
    void getAllSlide()
    {
        slideDataTitre[0] = mdgiciData.afficheSlideCom1();slideDataTitre[1] = mdgiciData.afficheSlideCom2();slideDataTitre[2] = mdgiciData.afficheSlideCom3();
        slideDataPicture[0] = mdgiciData.afficheIdCom1();slideDataPicture[1] = mdgiciData.afficheIdCom2();slideDataPicture[2] = mdgiciData.afficheIdCom3();
    }
}