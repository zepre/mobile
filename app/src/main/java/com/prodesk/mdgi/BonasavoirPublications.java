package com.prodesk.mdgi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
//** Sam 17.08.2019 // 2.04pm **//
public class BonasavoirPublications extends AppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);setContentView(R.layout.activity_bonasavoirpublications);
        TextView tx = (TextView) findViewById(R.id.publications_toolbar_title);
        tx.setText(getString(R.string.txt_bonasavoir_publications_title));
        Toolbar toolbar = (Toolbar) findViewById(R.id.publications_toolbar_read);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        ImageView goback = (ImageView) findViewById(R.id.publications_retour);
        goback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                finish();
            }
        });
    }
}