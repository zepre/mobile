package com.prodesk.mdgi.Communiques;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import com.prodesk.mdgi.R;
import com.squareup.picasso.Picasso;
//** Sam 17.08.2019 // 2.39pm **//
public class Communique extends AppCompatActivity
{
    private String com_title, com_contenu, com_date, com_image;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);setContentView(R.layout.activity_communique);
        Intent intWebAct = getIntent();com_title = intWebAct.getStringExtra("dataComTitle");
        com_contenu = intWebAct.getStringExtra("dataComContenu");com_date = intWebAct.getStringExtra("dataComDate");
        com_image = intWebAct.getStringExtra("dataComImage");TextView txtTitle = (TextView)findViewById(R.id.communique_titre);
        TextView txtContenu = (TextView)findViewById(R.id.communique_contenu);TextView txtDate = (TextView)findViewById(R.id.communique_date);
        ImageView imgCommunique = (ImageView)findViewById(R.id.communique_image);
        this.setTitle(com_date);txtTitle.setText(com_title);txtContenu.setText(com_contenu);
        txtDate.setText(com_date);Picasso.get().load(com_image).into(imgCommunique);
    }
    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
    }
}