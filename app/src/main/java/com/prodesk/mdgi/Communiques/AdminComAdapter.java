package com.prodesk.mdgi.Communiques;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import com.prodesk.mdgi.DonneesLocal.DonneeLocalHelper;
import com.prodesk.mdgi.R;
import com.squareup.picasso.Picasso;
import java.text.Normalizer;
import java.util.ArrayList;
//** Lun 19.08.2019 // 2.47pm **//
public class AdminComAdapter extends BaseAdapter implements Filterable
{
    int comid;ArrayList<AdminCommunique> admincommuniques, getFilter;Context context;DonneeLocalHelper mdgiciData;
    public AdminComAdapter(Context context, int vg, int id, ArrayList<AdminCommunique> admincommuniques)
    {
        super();this.context = context;comid = vg;
        this.admincommuniques = admincommuniques;
        mdgiciData = new DonneeLocalHelper(context);
    }
    public class AdminCommmuniqueHolder
    {
        TextView title, contenu, date, imagelink;ImageView image;
    }
    public Filter getFilter()
    {
        return new Filter()
        {
            @Override
            protected FilterResults performFiltering(CharSequence constraint)
            {
                final FilterResults oReturn = new FilterResults();
                final ArrayList<AdminCommunique> resultats = new ArrayList<AdminCommunique>();
                if (getFilter == null)
                    getFilter = admincommuniques;
                if (constraint != null) {
                    if (getFilter != null && getFilter.size() > 0) {
                        for (final AdminCommunique rechcom : getFilter) {
                            if (convertAccent(rechcom.getCommuniqueTitle().toLowerCase()).contains(convertAccent(constraint.toString().toLowerCase())))
                                resultats.add(rechcom);
                        }
                    }
                    oReturn.values = resultats;
                }
                return oReturn;
            }
            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results)
            {
                admincommuniques = (ArrayList<AdminCommunique>) results.values;
                notifyDataSetChanged();
            }
        };
    }
    public String convertAccent(String toconv)
    {
        toconv = Normalizer.normalize(toconv, Normalizer.Form.NFD);
        return toconv.replaceAll("[^\\p{ASCII}]","");
    }
    public void notifyDataSetChanged()
    {
        super.notifyDataSetChanged();
    }
    @Override
    public int getCount()
    {
        return admincommuniques.size();
    }
    @Override
    public Object getItem(int position)
    {
        return admincommuniques.get(position);
    }
    public long getItemId(int position)
    {
        return position;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        AdminCommmuniqueHolder holderCom;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.admin_com_item,parent,false);
            holderCom = new AdminCommmuniqueHolder();
            holderCom.title = (TextView)convertView.findViewById(R.id.adm_com_title);
            holderCom.contenu = (TextView)convertView.findViewById(R.id.adm_com_contenu);
            holderCom.date = (TextView)convertView.findViewById(R.id.adm_com_date);
            holderCom.image = (ImageView) convertView.findViewById(R.id.adm_com_image);
            holderCom.imagelink = (TextView)convertView.findViewById(R.id.adm_com_image_link);
            convertView.setTag(holderCom);
        } else {
            holderCom = (AdminComAdapter.AdminCommmuniqueHolder) convertView.getTag();
        }
        holderCom.title.setText(admincommuniques.get(position).getCommuniqueTitle());holderCom.contenu.setText(admincommuniques.get(position).getCommuniqueContenu());
        holderCom.date.setText(admincommuniques.get(position).getCommuniqueDate());Picasso.get().load(admincommuniques.get(position).getCommuniqueImage()).into(holderCom.image);
        holderCom.imagelink.setText(admincommuniques.get(position).getCommuniqueImageLink());
        return convertView;
    }
}