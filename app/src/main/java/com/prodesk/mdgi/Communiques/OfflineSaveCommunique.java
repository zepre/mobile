package com.prodesk.mdgi.Communiques;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;
import com.prodesk.mdgi.DonneesLocal.DonneeLocalHelper;
import com.prodesk.mdgi.R;
public class OfflineSaveCommunique extends AppCompatActivity
{
    DonneeLocalHelper mdgiciDb;
    long nbdir;
    private String dirtitre, dircontact1, dircontact2, diradresse, dircategorie, dirlocalite, dirlocaliteprincipale;
    private EditText txtdirtitre, txtdircontact1, txtdircontact2, txtdiradresse, txtdircategorie, txtdirlocalite, txtdirlocaliteprincipale;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.offline_save_communique);
        mdgiciDb = new DonneeLocalHelper(this);
        txtdirtitre = (EditText)findViewById(R.id.adm_savedir_titre);
        txtdircontact1 = (EditText)findViewById(R.id.adm_savedir_contact1);
        txtdircontact2 = (EditText)findViewById(R.id.adm_savedir_contact2);
        txtdiradresse = (EditText)findViewById(R.id.adm_savedir_adresse);
        txtdircategorie = (EditText)findViewById(R.id.adm_savedir_categorie);
        txtdirlocalite = (EditText)findViewById(R.id.adm_savedir_localite);
        txtdirlocaliteprincipale = (EditText)findViewById(R.id.adm_savedir_localite_principale);
    }
    public void enregistreDirection(View view)
    {
        getData();
        Toast.makeText(this,"ok "+dirtitre+"nombres sauvé=="+nbdir,Toast.LENGTH_LONG).show();
    }
    public void effaceDirection(View view)
    {
        txtdirtitre.setText("");
        txtdircontact1.setText("");
        txtdircontact2.setText("");
        txtdiradresse.setText("");
        txtdircategorie.setText("");
        txtdirlocalite.setText("");
        txtdirlocaliteprincipale.setText("");
        prendsleFocus(txtdirtitre);
    }
    public void getData()
    {
        dirtitre = txtdirtitre.getText().toString();
        dircontact1 = txtdircontact1.getText().toString();
        dircontact2 = txtdircontact2.getText().toString();
        diradresse = txtdiradresse.getText().toString();
        dircategorie = txtdircategorie.getText().toString();
        dirlocalite = txtdirlocalite.getText().toString();
        dirlocaliteprincipale = txtdirlocaliteprincipale.getText().toString();
        nbdir = mdgiciDb.nombresDirectionsLocal();
    }
    private void prendsleFocus(View view)
    {
        if (view.requestFocus())
        {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }
}