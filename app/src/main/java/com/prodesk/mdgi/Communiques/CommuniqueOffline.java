package com.prodesk.mdgi.Communiques;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.GestureDetector;
import android.widget.ImageView;
import android.widget.TextView;

import com.prodesk.mdgi.DonneesLocal.DonneeLocalHelper;
import com.prodesk.mdgi.R;
//** Sam 17.08.2019 // 2.39pm **//
public class CommuniqueOffline extends AppCompatActivity
{
    ProgressDialog progressDialog;private GestureDetector mouvement;
    private String com_title, com_contenu, com_date, com_position;
    DonneeLocalHelper mdgiciData;String[] nextCom = new String[4];
    String[] prevCom = new String[4];int idComPrev, idComSuiv;float allCommuniques, curPos;int com_image;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_communique_offline);
        Intent intWebAct = getIntent();Bundle bnd = getIntent().getExtras();curPos = bnd.getFloat("dataComPosition");
        com_title = bnd.getString("dataComTitle");com_contenu = bnd.getString("dataComContenu");
        com_date = bnd.getString("dataComDate");com_image = bnd.getInt("dataComImage");com_position = String.valueOf(curPos);
        mdgiciData = new DonneeLocalHelper(this);
        this.setTitle(com_date);TextView txtTitle = (TextView)findViewById(R.id.communique_titre_offline);
        TextView txtContenu = (TextView)findViewById(R.id.communique_contenu_offline);TextView txtDate = (TextView)findViewById(R.id.communique_date_offline);
        ImageView imgCommunique = (ImageView)findViewById(R.id.communique_image_offline);
        txtTitle.setText(com_title);txtContenu.setText(com_contenu);
        txtDate.setText(com_date);allCommuniques = mdgiciData.nombresCommuniquesLocal();
        imgCommunique.setImageBitmap(mdgiciData.afficheImagesHorsLigne(com_image));
    }
}
