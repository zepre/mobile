package com.prodesk.mdgi.Communiques;
import static com.prodesk.mdgi.Utilitaires.*;
//** Sam 17.08.2019 // 2.48pm **//
public class AdminCommunique
{
    private String communiqueId, communiqueTitle, communiqueDate, communiqueImage, communiqueContenu, communiqueImageLink;
    public void setCommuniqueId(String communiqueId) { this.communiqueId = communiqueId;}
    public String getCommuniqueId(){return communiqueId;}
    public void setCommuniqueTitle(String communiqueTitle) { this.communiqueTitle = communiqueTitle;}
    public String getCommuniqueTitle(){return communiqueTitle;}
    public void setCommuniqueContenu(String communiqueContenu){ this.communiqueContenu = communiqueContenu;}
    public String getCommuniqueContenu(){return communiqueContenu;}
    public void setCommuniqueImageLink(String communiqueImageLink) {this.communiqueImageLink = base_url+communiqueImageLink;}
    public String getCommuniqueImageLink(){return communiqueImageLink;}
    public void setCommuniqueDate(String communiqueDate)
    {
        this.communiqueDate = communiqueDate;
    }
    public String getCommuniqueDate(){return communiqueDate;}
    public void setCommuniqueImage(String communiqueImage) {this.communiqueImage = base_url+communiqueImage;}
    public String getCommuniqueImage(){return communiqueImage;}
}