package com.prodesk.mdgi.Communiques;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.TextView;
import com.prodesk.mdgi.R;
import com.squareup.picasso.Picasso;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import static com.prodesk.mdgi.Utilitaires.*;
//** Sam 17.08.2019 // 2.31pm **//
public class ListeCommunique extends AppCompatActivity implements AdapterView.OnItemClickListener, SearchView.OnQueryTextListener
{
    Activity context;HttpPost httpPost;StringBuffer buffer;
    HttpResponse response;HttpClient httpClient;ProgressDialog progressDialog;AdminComAdapter adapter;
    ListView listCommuniques;ArrayList<AdminCommunique> admincommuniques;
    String[] mainCom = new String[4];ImageView mainComImage;TextView mainComTitle, mainComDate;
    RelativeLayout com_premier;long ComDataRowsOnline;private SearchView mRechercheOnline;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);setContentView(R.layout.activity_admin_listecommunique);
        mRechercheOnline = (SearchView)findViewById(R.id.recherche_communique);
        context = this;
        mainComImage = (ImageView)findViewById(R.id.adm_com_main_image);
        mainComTitle = (TextView)findViewById(R.id.adm_com_main_title);
        mainComDate = (TextView)findViewById(R.id.adm_com_main_date);
        com_premier = (RelativeLayout)findViewById(R.id.adm_com_premier);
        admincommuniques = new ArrayList<AdminCommunique>();
        listCommuniques = (ListView) findViewById(R.id.adm_com_list);
        adapter = new AdminComAdapter(context, R.layout.admin_com_item, R.id.adm_com_title, admincommuniques);
        listCommuniques.setAdapter(adapter);
        listCommuniques.setClickable(true);
        listCommuniques.setOnItemClickListener(this);
        listCommuniques.setTextFilterEnabled(true);
        firstComTask getCom = new firstComTask();
        try {
            mainCom = getCom.execute().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        Picasso.get().load(mainCom[0]).into(mainComImage);
        mainComTitle.setText(mainCom[1]);
        mainComDate.setText(mainCom[2]);
        com_premier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openCommunique(mainCom[1],mainCom[3],mainCom[2],mainCom[0]);
            }
        });
    }
    private void setRecherchOnline()
    {
        mRechercheOnline.setIconifiedByDefault(true);mRechercheOnline.setOnQueryTextListener(this);
        mRechercheOnline.setSubmitButtonEnabled(true);mRechercheOnline.setQueryHint("Recherche ...");
        mRechercheOnline.clearFocus();
    }
    @Override
    public boolean onQueryTextChange(String rechTxt)
    {
        if (TextUtils.isEmpty(rechTxt)) {
            listCommuniques.clearTextFilter();
        } else {
            listCommuniques.setFilterText(rechTxt);
        }
        return true;
    }
    @Override
    public boolean onQueryTextSubmit(String requ)
    {
        return false;
    }
    public void notifyDataSetChanged()
    {
        adapter.notifyDataSetChanged();
    }
    public void onStart()
    {
        super.onStart();
        selectCommuniques bts = new selectCommuniques();
        bts.execute();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.recherche_communiques, menu);
        MenuItem item = menu.findItem(R.id.recherche_communiques_all);
        SearchView searchView = (SearchView)item.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener()
        {
            @Override
            public boolean onQueryTextSubmit(String query)
            {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String newText)
            {
                adapter.getFilter().filter(newText);
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        String titlecom, contenucom, imagelinkcom, datecom;
        TextView txtTitle = (TextView)view.findViewById(R.id.adm_com_title);
        titlecom = txtTitle.getText().toString();
        TextView txtContenu = (TextView)view.findViewById(R.id.adm_com_contenu);
        contenucom = txtContenu.getText().toString();
        TextView txtImageLink = (TextView)view.findViewById(R.id.adm_com_image_link);
        imagelinkcom = txtImageLink.getText().toString();
        TextView txtDate = (TextView)view.findViewById(R.id.adm_com_date);
        datecom = txtDate.getText().toString();
        openCommunique(titlecom,contenucom,datecom,imagelinkcom);
    }
    private class selectCommuniques extends AsyncTask<Void,Void,Void>
    {
        protected void onPreExecute()
        {
            super.onPreExecute();
            ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isConnected())
            {
                progressDialog = new ProgressDialog(context);
                progressDialog.setTitle("Chargement des communiqués");
                progressDialog.setMessage("Veuillez patienter");
                progressDialog.setCancelable(true);
                progressDialog.show();
            } else {
                AlertDialog.Builder ListComfocri = new AlertDialog.Builder(ListeCommunique.this);
                ListComfocri.setTitle("Erreur Internet !");
                ListComfocri.setMessage("Il vous faut internet pour lire les communiqués");
                ListComfocri.setCancelable(true);
                ListComfocri.setPositiveButton(
                    "Actualiser",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id)
                        {
                            Intent intComList = new Intent(getBaseContext(), ListeCommuniqueOffline.class);
                            startActivity(intComList);
                        }
                    });
                ListComfocri.setNegativeButton(
                    "Quitter",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id)
                        {
                            finish();
                        }
                    });
                AlertDialog alertLisCom = ListComfocri.create();
                alertLisCom.setIcon(R.drawable.ic_network_check);
                alertLisCom.show();
            }
        }
        protected Void doInBackground(Void...params)
        {
            InputStream inputStream = null;
            String resultat = "";
            try {
                httpClient = new DefaultHttpClient();
                httpPost = new HttpPost(liste_com_url);
                response = httpClient.execute(httpPost);
                HttpEntity entity = response.getEntity();
                inputStream = entity.getContent();
            } catch (Exception ecn){
                if (progressDialog!=null)
                    progressDialog.dismiss();
            }
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream,"utf-8"),8);
                StringBuilder stringBuilder = new StringBuilder();
                String ligne = null;
                while ((ligne = reader.readLine()) != null)
                {
                    stringBuilder.append(ligne+"\n");
                }
                inputStream.close();
                resultat = stringBuilder.toString();
            } catch (Exception ec) {}
            try {
                resultat = resultat.substring(resultat.indexOf("["));
                JSONArray jsonArray = new JSONArray(resultat);
                admincommuniques.clear();
                for (int i = 1; i < jsonArray.length(); i++)
                {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    AdminCommunique admcom = new AdminCommunique();admcom.setCommuniqueTitle(jsonObject.getString("com_titre"));
                    admcom.setCommuniqueContenu(jsonObject.getString("com_contenu"));admcom.setCommuniqueDate(jsonObject.getString("com_date"));
                    admcom.setCommuniqueImage(jsonObject.getString("com_nomimage"));admcom.setCommuniqueImageLink(jsonObject.getString("com_nomimage"));
                    admincommuniques.add(admcom);
                }
                notifyDataSetChanged();
            } catch (Exception ep) {}
            return null;
        }
        protected void onPostExecute(Void result)
        {
            if (progressDialog != null)
                progressDialog.dismiss();
            adapter.notifyDataSetChanged();
        }
    }
    private class firstComTask extends AsyncTask<Void,Void,String[]>
    {
        protected void onPreExecute()
        {
            super.onPreExecute();
        }
        protected String[] doInBackground(Void...params)
        {
            InputStream inputStream = null;
            String resultat = "";
            String[] mainComTask = new String[4];
            try {
                httpClient = new DefaultHttpClient();
                httpPost = new HttpPost(liste_comslide_url);
                response = httpClient.execute(httpPost);
                HttpEntity entity = response.getEntity();
                inputStream = entity.getContent();
            } catch (Exception ecn){}
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream,"utf-8"),8);
                StringBuilder stringBuilder = new StringBuilder();
                String ligne = null;
                while ((ligne = reader.readLine()) != null)
                {
                    stringBuilder.append(ligne+"\n");
                }
                inputStream.close();
                resultat = stringBuilder.toString();
            } catch (Exception ec) {}
            try {
                resultat = resultat.substring(resultat.indexOf("["));
                JSONArray jsonArray = new JSONArray(resultat);
                JSONObject jsonObject1 = jsonArray.getJSONObject(0);
                mainComTask[0] = base_url+jsonObject1.getString("com_nomimage");
                mainComTask[1] = jsonObject1.getString("com_titre");
                mainComTask[2] = jsonObject1.getString("com_date");
                mainComTask[3] = jsonObject1.getString("com_contenu");
            } catch (Exception ep) {}
            return mainComTask;
        }
        protected void onPostExecute(Void result) {}
    }
    private class nombreCommuniquesEnLigne extends AsyncTask<Void, Void, String>
    {
        protected void onPreExecute()
        {
            super.onPreExecute();
        }
        protected String doInBackground(Void...params)
        {
            InputStream inputStream = null;String resultat = "";
            String localComDataRows = "";Boolean dataValid = true;
            try {
                httpClient = new DefaultHttpClient();httpPost = new HttpPost(liste_com_url);response = httpClient.execute(httpPost);
                HttpEntity entity = response.getEntity();inputStream = entity.getContent();
            } catch (Exception ecn){
                dataValid = false;
            }
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream,"utf-8"),8);
                StringBuilder stringBuilder = new StringBuilder();
                String ligne = null;
                while ((ligne = reader.readLine()) != null)
                {
                    stringBuilder.append(ligne+"\n");
                }
                inputStream.close();
                resultat = stringBuilder.toString();
            } catch (Exception ec) {
                dataValid = false;
            }
            try {
                resultat = resultat.substring(resultat.indexOf("["));
                JSONArray jsonArray = new JSONArray(resultat);
                localComDataRows = String.valueOf(jsonArray.length());
            } catch (Exception ep) {
                dataValid = false;
            }
            if (!dataValid) {
                return null;
            } else {
                return localComDataRows;
            }
        }
        protected void onPostExecute(Void result) {}
    }
    private void openCommunique(String titre, String contenu, String date, String image)
    {
        Intent intCom = new Intent(this,Communique.class);intCom.putExtra("dataComTitle", titre);
        intCom.putExtra("dataComContenu", contenu);intCom.putExtra("dataComDate", date);
        intCom.putExtra("dataComImage", image);startActivity(intCom);
    }
    private void openOfflineCom()
    {
        Intent intComList = new Intent(getBaseContext(), ListeCommuniqueOffline.class);
        startActivity(intComList);
    }
}