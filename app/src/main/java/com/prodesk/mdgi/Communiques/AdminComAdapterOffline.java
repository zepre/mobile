package com.prodesk.mdgi.Communiques;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import com.prodesk.mdgi.DonneesLocal.DonneeLocalHelper;
import com.prodesk.mdgi.R;
import java.text.Normalizer;
import java.util.ArrayList;
//** Lun 19.08.2019 // 2.49pm **//
public class AdminComAdapterOffline extends BaseAdapter implements Filterable
{
    int comid;ArrayList<AdminCommunique> admincommuniques;
    ArrayList<AdminCommunique> forFilter;Context context;DonneeLocalHelper mdgiciData;
    public AdminComAdapterOffline(Context context, int vg, int id, ArrayList<AdminCommunique> admincommuniques)
    {
        super();this.context = context;comid = vg;this.admincommuniques = admincommuniques;mdgiciData = new DonneeLocalHelper(context);
    }
    public class AdminComHolder
    {
        TextView title;TextView contenu;TextView date;ImageView image;TextView imagelink;
    }
    public Filter getFilter()
    {
        return new Filter()
        {
            @Override
            protected FilterResults performFiltering(CharSequence constraint)
            {
                final FilterResults oReturn = new FilterResults();
                final ArrayList<AdminCommunique> resultats = new ArrayList<AdminCommunique>();
                if (forFilter == null)
                    forFilter = admincommuniques;
                if (constraint != null) {
                    if (forFilter != null && forFilter.size() > 0) {
                        for (final AdminCommunique rechcom : forFilter) {
                            if (convertAccent(rechcom.getCommuniqueTitle().toLowerCase()).contains(convertAccent(constraint.toString().toLowerCase())))
                                resultats.add(rechcom);
                        }
                    }
                    oReturn.values = resultats;
                }
                return oReturn;
            }
            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results)
            {
                admincommuniques = (ArrayList<AdminCommunique>) results.values;
                notifyDataSetChanged();
            }
        };
    }
    public String convertAccent(String toconv)
    {
        toconv = Normalizer.normalize(toconv, Normalizer.Form.NFD);
        return toconv.replaceAll("[^\\p{ASCII}]","");
    }
    public void notifyDataSetChanged()
    {
        super.notifyDataSetChanged();
    }
    @Override
    public int getCount()
    {
        return admincommuniques.size();
    }
    @Override
    public Object getItem(int position)
    {
        return admincommuniques.get(position);
    }
    public long getItemId(int position)
    {
        return position;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        AdminComHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.admin_offline_item,parent,false);
            holder = new AdminComHolder();
            holder.title = (TextView)convertView.findViewById(R.id.adm_com_title_offline);
            holder.contenu = (TextView)convertView.findViewById(R.id.adm_com_contenu_offline);
            holder.date = (TextView)convertView.findViewById(R.id.adm_com_date_offline);
            holder.image = (ImageView) convertView.findViewById(R.id.adm_com_image_offline);
            holder.imagelink = (TextView)convertView.findViewById(R.id.adm_com_image_link_offline);
            convertView.setTag(holder);
        } else {
            holder = (AdminComHolder) convertView.getTag();
        }
        holder.title.setText(admincommuniques.get(position).getCommuniqueTitle());
        holder.contenu.setText(admincommuniques.get(position).getCommuniqueContenu());
        holder.date.setText(admincommuniques.get(position).getCommuniqueDate());
        holder.image.setImageBitmap(mdgiciData.afficheImagesHorsLigne(Integer.parseInt(admincommuniques.get(position).getCommuniqueId())));
        holder.imagelink.setText(admincommuniques.get(position).getCommuniqueId());
        return convertView;
    }
}