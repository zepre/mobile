package com.prodesk.mdgi.Communiques;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.TextView;
import com.prodesk.mdgi.R;
import java.util.ArrayList;
import com.prodesk.mdgi.DonneesLocal.DonneeLocalHelper;
//** Sam 17.08.2019 // 2.39pm **//
public class ListeCommuniqueOffline extends AppCompatActivity implements AdapterView.OnItemClickListener, SearchView.OnQueryTextListener
{
    Activity context;ProgressDialog progressDialog;AdminComAdapterOffline adapter;
    ListView listCommuniques;ArrayList<AdminCommunique> admincommuniques;String[] mainCom = new String[4];
    ImageView mainComImage;TextView mainComTitle, mainComDate;RelativeLayout com_premier;long OfflineRowsCount;
    private SearchView mRecherche;DonneeLocalHelper mdgiciData = new DonneeLocalHelper(this);
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);setContentView(R.layout.activity_offline_listecommunique);
        mRecherche = (SearchView)findViewById(R.id.recherche_communique_offline);
        context = this;
        mainComImage = (ImageView)findViewById(R.id.adm_com_main_image_offine);
        mainComTitle = (TextView)findViewById(R.id.adm_com_main_title_offline);
        mainComDate = (TextView)findViewById(R.id.adm_com_main_date_offline);
        com_premier = (RelativeLayout)findViewById(R.id.adm_com_premier_offline);
        admincommuniques = new ArrayList<AdminCommunique>();
        listCommuniques = (ListView) findViewById(R.id.adm_com_list_offline);
        adapter = new AdminComAdapterOffline(context, R.layout.admin_offline_item, R.id.adm_com_title_offline, admincommuniques);
        listCommuniques.setAdapter(adapter);
        listCommuniques.setClickable(true);
        listCommuniques.setOnItemClickListener(this);
        listCommuniques.setTextFilterEnabled(true);
        getFirstData();
        mainComImage.setImageBitmap(mdgiciData.afficheImagesHorsLigne(Integer.parseInt(mainCom[0])));
        mainComTitle.setText(mainCom[1]);
        mainComDate.setText(mainCom[2]);
        com_premier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openCommunique(mainCom[1],mainCom[3],mainCom[2], Integer.parseInt(mainCom[0]),1);
            }
        });
    }
    private void setRechercheView()
    {
        mRecherche.setIconifiedByDefault(true);
        mRecherche.setOnQueryTextListener(this);mRecherche.setSubmitButtonEnabled(true);
        mRecherche.setQueryHint("Recherche ...");mRecherche.clearFocus();
    }
    @Override
    public boolean onQueryTextChange(String rechText)
    {
        if (TextUtils.isEmpty(rechText)) {
            listCommuniques.clearTextFilter();
        } else {
            listCommuniques.setFilterText(rechText);
        }
        return true;
    }
    @Override
    public boolean onQueryTextSubmit(String requete)
    {
        return false;
    }
    public void onStart()
    {
        super.onStart();
        selectCommuniquesOffline bts = new selectCommuniquesOffline();bts.execute();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.recherche_communiques, menu);
        MenuItem item = menu.findItem(R.id.recherche_communiques_all);
        SearchView searchView = (SearchView)item.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener()
        {
            @Override
            public boolean onQueryTextSubmit(String query)
            {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String newText)
            {
                adapter.getFilter().filter(newText);
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }
    public void notifyDataSetChanged()
    {
        adapter.notifyDataSetChanged();
    }
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        String titlecom, contenucom, datecom;int imagelinkcom;
        TextView txtTitle = (TextView)view.findViewById(R.id.adm_com_title_offline);
        titlecom = txtTitle.getText().toString();
        TextView txtContenu = (TextView)view.findViewById(R.id.adm_com_contenu_offline);
        contenucom = txtContenu.getText().toString();
        TextView txtImageLink = (TextView)view.findViewById(R.id.adm_com_image_link_offline);
        imagelinkcom = Integer.parseInt(txtImageLink.getText().toString());
        TextView txtDate = (TextView)view.findViewById(R.id.adm_com_date_offline);
        datecom = txtDate.getText().toString();
        String posiz = String.valueOf(position);
        openCommunique(titlecom,contenucom,datecom,imagelinkcom, position+1);
    }
    private void openCommunique(String titre, String contenu, String date, int image, float posiz)
    {
        Intent intCom = new Intent(this,CommuniqueOffline.class);
        intCom.putExtra("dataComTitle", titre);intCom.putExtra("dataComContenu", contenu);
        intCom.putExtra("dataComDate", date);intCom.putExtra("dataComImage", image);
        intCom.putExtra("dataComPosition", posiz);startActivity(intCom);
    }
    private void getFirstData()
    {
        mainCom[0] = mdgiciData.afficheMainComId();mainCom[1] = mdgiciData.afficheMainComTitre();
        mainCom[2] = mdgiciData.afficheMainComDate();mainCom[3] = mdgiciData.afficheMainComContenu();
    }
    private class selectCommuniquesOffline extends AsyncTask<Void,Void,Void>
    {
        protected void onPreExecute()
        {
            super.onPreExecute();
        }
        protected Void doInBackground(Void...params)
        {
            try {
                OfflineRowsCount = mdgiciData.nombresCommuniquesLocal();
                Cursor getAllCom = mdgiciData.afficheTotalCommuniques(String.valueOf(OfflineRowsCount));
                if (getAllCom.getCount() == 0) {
                } else {
                    admincommuniques.clear();
                    while (getAllCom.moveToNext())
                    {
                        AdminCommunique admcom = new AdminCommunique();
                        admcom.setCommuniqueId(getAllCom.getString(0));admcom.setCommuniqueTitle(getAllCom.getString(1));
                        admcom.setCommuniqueContenu(getAllCom.getString(2));admcom.setCommuniqueDate(getAllCom.getString(3));admcom.setCommuniqueImage(getAllCom.getString(5));
                        admcom.setCommuniqueImageLink(getAllCom.getString(5));admincommuniques.add(admcom);
                    }
                    notifyDataSetChanged();
                }
            } catch (Exception ep) {}
            return null;
        }
        protected void onPostExecute(Void result)
        {
            if (progressDialog != null)
                progressDialog.dismiss();
            adapter.notifyDataSetChanged();
        }
    }
    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
    }
}