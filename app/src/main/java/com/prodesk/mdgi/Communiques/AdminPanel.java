package com.prodesk.mdgi.Communiques;
import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.prodesk.mdgi.R;
public class AdminPanel extends AppCompatActivity {
    String link;
    Double myLat, myLong;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adminpanel);
        LinearLayout listCom = (LinearLayout) findViewById(R.id.open_liste_communique);
        listCom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intAbout = new Intent(AdminPanel.this, ListeCommunique.class);
                startActivity(intAbout);
            }
        });
        LinearLayout listTest = (LinearLayout) findViewById(R.id.open_liste_communiqueTest);
        listTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intJson = new Intent(AdminPanel.this, testJSON.class);
                startActivity(intJson);
            }
        });
        LinearLayout startDownload = (LinearLayout) findViewById(R.id.open_download_file);
        startDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                link = getString(R.string.txt_imprimes_file_readEtii_lk);
            }
        });
        LinearLayout startLocationGetter = (LinearLayout) findViewById(R.id.open_get_location);
        startLocationGetter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intLoc = new Intent(AdminPanel.this, AdminLocation.class);
                startActivity(intLoc);
            }
        });
    }
    public void getMyLocation() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
        {
            return;
        }
        Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        myLat = location.getLatitude();
        myLong = location.getLongitude();
        Toast.makeText(this,"my latitue = "+myLat+"\n my longitude = "+myLong,Toast.LENGTH_LONG).show();
    }
}