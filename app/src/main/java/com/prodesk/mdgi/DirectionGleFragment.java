package com.prodesk.mdgi;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.prodesk.mdgi.DirectionsCentrales.*;
//** Lun 23.09.2019 // 9.55am **//
public class DirectionGleFragment extends Fragment
{
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_directiongle, container, false);
        final TextView openDirCabinetActivity = (TextView) view.findViewById(R.id.goto_dir_cabinet_dg);
        openDirCabinetActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openDirGleActivity(DirCabinetDirecteur.class);
            }
        });
        final TextView openDirDI = (TextView) view.findViewById(R.id.goto_dir_di);
        openDirDI.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openDirCentrale(DirCentraleDi.class);
            }
        });
        final TextView openDirDMGE = (TextView) view.findViewById(R.id.goto_dir_dmge);
        openDirDMGE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openDirCentrale(DirCentraleDmge.class);
            }
        });
        final TextView openDirDCQPCF = (TextView) view.findViewById(R.id.goto_dir_dcqpcf);
        openDirDCQPCF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openDirCentrale(DirCentraleDcqpcf.class);
            }
        });
        final TextView openDirDLCD = (TextView) view.findViewById(R.id.goto_dir_dlcd);
        openDirDLCD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openDirCentrale(DirCentraleDlcd.class);
            }
        });
        final TextView openDirDPESF = (TextView) view.findViewById(R.id.goto_dir_dpesf);
        openDirDPESF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openDirCentrale(DirCentraleDpesf.class);
            }
        });
        final TextView openDirDERAR = (TextView) view.findViewById(R.id.goto_dir_derar);
        openDirDERAR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openDirCentrale(DirCentraleDerar.class);
            }
        });
        final TextView openDirDGE = (TextView) view.findViewById(R.id.goto_dir_dge);
        openDirDGE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openDirCentrale(DirCentraleDge.class);
            }
        });
        final TextView openDirDME = (TextView) view.findViewById(R.id.goto_dir_dme);
        openDirDME.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openDirCentrale(DirCentraleDme.class);
            }
        });
        final TextView openDirDOA = (TextView) view.findViewById(R.id.goto_dir_doa);
        openDirDOA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openDirCentrale(DirCentraleDoa.class);
            }
        });
        final TextView openDirDRHF = (TextView) view.findViewById(R.id.goto_dir_drhf);
        openDirDRHF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openDirCentrale(DirCentraleDrhf.class);
            }
        });
        final TextView openDirDVN = (TextView) view.findViewById(R.id.goto_dir_dvn);
        openDirDVN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openDirCentrale(DirCentraleDvn.class);
            }
        });
        final TextView openDirDCADASTRE = (TextView) view.findViewById(R.id.goto_dir_dcadastre);
        openDirDCADASTRE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openDirCentrale(DirCentraleDc.class);
            }
        });
        final TextView openDirDDCFET = (TextView) view.findViewById(R.id.goto_dir_ddcfet);
        openDirDDCFET.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openDirCentrale(DirCentraleDdcfet.class);
            }
        });
        final TextView openDirRGI = (TextView) view.findViewById(R.id.goto_dir_rgi);
        openDirRGI.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openDirCentrale(DirCentraleRgi.class);
            }
        });
        return view;
    }
    public void openDirGleActivity(Class item)
    {
        Intent intAct = new Intent(getActivity(), item);startActivity(intAct);
    }
    public void openDirCentrale(Class DirCentrale)
    {
        Intent intDir = new Intent(getActivity(), DirCentrale);startActivity(intDir);
    }
}