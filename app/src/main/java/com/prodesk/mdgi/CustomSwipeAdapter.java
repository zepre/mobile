package com.prodesk.mdgi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.prodesk.mdgi.Communiques.ListeCommunique;
import com.squareup.picasso.Picasso;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import static com.prodesk.mdgi.Utilitaires.*;
//** 28/02/2019 // 11.36am **//
public class CustomSwipeAdapter extends PagerAdapter
{
    private ArrayList<Integer> Images;private int [] image_resource = {1,2,3};
    private Context ctx;private LayoutInflater inflater;
    HttpPost httpPost;StringBuffer buffer;
    HttpResponse response;HttpClient httpClient;ProgressDialog progressDialog;
    final String[] slideDataPicture = new String[image_resource.length];
    final String[] slideDataTitre = new String[image_resource.length];
    public CustomSwipeAdapter(Context ctx, ArrayList<Integer> Images)
    {
        this.ctx = ctx;this.Images = Images;inflater = LayoutInflater.from(ctx);
    }
    @Override
    public Object instantiateItem(ViewGroup container, int position)
    {
        inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View item_view = inflater.inflate(R.layout.slide_element_container, null);
        assert item_view != null;
        final ImageView imageSlide = (ImageView)item_view.findViewById(R.id.slide_image_item);
        final TextView titreSlide = (TextView)item_view.findViewById(R.id.slide_title_text);
        SlideTask getSlide = new SlideTask();
        getSlide.execute();
        Picasso.get().load(slideDataPicture[position]).into(imageSlide);
        titreSlide.setText(slideDataTitre[position]);
        container.addView(item_view, 0);
        ImageView gotoListCom = (ImageView)item_view.findViewById(R.id.slide_image_item);
        gotoListCom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intComList = new Intent(v.getContext(), ListeCommunique.class);
                v.getContext().startActivity(intComList);
            }
        });
        return item_view;
    }
    @Override
    public void destroyItem(ViewGroup container, int position, Object object)
    {
        ViewPager vp = (ViewPager) container;
        View item_view = (View) object;vp.removeView(item_view);
    }
    @Override
    public int getCount()
    {
        return image_resource.length;
    }
    @Override
    public boolean isViewFromObject(View view, Object object)
    {
        return view == object;
    }
    private class SlideTask extends AsyncTask<Void,Void,Void>
    {
        protected void onPreExecute()
        {
            super.onPreExecute();
        }
        protected Void doInBackground(Void...params)
        {
            InputStream inputStream = null;
            String resultat = "";
            try {
                httpClient = new DefaultHttpClient();
                httpPost = new HttpPost(liste_comslide_url);
                response = httpClient.execute(httpPost);
                HttpEntity entity = response.getEntity();
                inputStream = entity.getContent();
            } catch (Exception ecn){
                if (progressDialog!=null)
                    progressDialog.dismiss();
            }
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream,"utf-8"),8);
                StringBuilder stringBuilder = new StringBuilder();
                String ligne = null;
                while ((ligne = reader.readLine()) != null)
                {
                    stringBuilder.append(ligne+"\n");
                }
                inputStream.close();
                resultat = stringBuilder.toString();
            } catch (Exception ec) {}
            try {
                resultat = resultat.substring(resultat.indexOf("["));
                JSONArray jsonArray = new JSONArray(resultat);
                JSONObject jsonObject1 = jsonArray.getJSONObject(0);
                slideDataPicture[0] = base_url+jsonObject1.getString("nomImage");
                slideDataTitre[0] = jsonObject1.getString("titre");
                JSONObject jsonObject2 = jsonArray.getJSONObject(1);
                slideDataPicture[1] = base_url+jsonObject2.getString("nomImage");
                slideDataTitre[1] = jsonObject2.getString("titre");
                JSONObject jsonObject3 = jsonArray.getJSONObject(2);
                slideDataPicture[2] = base_url+jsonObject3.getString("nomImage");
                slideDataTitre[2] = jsonObject3.getString("titre");
            } catch (Exception ep) {}
            return null;
        }
        protected void onPostExecute(Void result) {}
    }
}