package com.prodesk.mdgi;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import com.prodesk.mdgi.DonneesLocal.DonneeLocalHelper;
import java.text.Normalizer;
import java.util.ArrayList;
//** Lun 19.08.2019 // 2.45pm **//
public class AdapterRechercheDirections extends BaseAdapter implements Filterable
{
    int dirid;ArrayList<AdminDirection> admindirection;ArrayList<AdminDirection> forFilter;
    Context context;DonneeLocalHelper mdgiciData;
    public AdapterRechercheDirections(Context context, int vg, int id, ArrayList<AdminDirection> admindirection)
    {
        super();this.context = context;
        dirid = vg;this.admindirection = admindirection;
        mdgiciData = new DonneeLocalHelper(context);
    }
    public class AdminRechercheHolder
    {
        TextView title;TextView contact;TextView adresse;TextView localite;
        TextView categorie;TextView localiteprincipale;TextView classjava;
    }
    public Filter getFilter()
    {
        return new Filter()
        {
            @Override
            protected FilterResults performFiltering(CharSequence constraint)
            {
                final FilterResults oReturn = new FilterResults();
                final ArrayList<AdminDirection> resultats = new ArrayList<AdminDirection>();
                if (forFilter == null)
                    forFilter = admindirection;
                if (constraint != null) {
                    if (forFilter != null && forFilter.size() > 0) {
                        for (final AdminDirection rechdir : forFilter) {
                            if (convertAccent(rechdir.getDirectionTitre().toLowerCase()).contains(convertAccent(constraint.toString().toLowerCase())) || convertAccent(rechdir.getDirectionAdresse().toLowerCase()).contains(convertAccent(constraint.toString().toLowerCase())) || convertAccent(rechdir.getDirectionLocalite().toLowerCase()).contains(convertAccent(constraint.toString().toLowerCase())) || convertAccent(rechdir.getDirectionLocaliteprincipale().toLowerCase()).contains(convertAccent(constraint.toString().toLowerCase())))
                                resultats.add(rechdir);
                        }
                    }
                    oReturn.values = resultats;
                }
                return oReturn;
            }
            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results)
            {
                admindirection = (ArrayList<AdminDirection>) results.values;
                notifyDataSetChanged();
            }
        };
    }
    public void notifyDataSetChanged()
    {
        super.notifyDataSetChanged();
    }
    @Override
    public int getCount()
    {
        return admindirection.size();
    }
    @Override
    public Object getItem(int position)
    {
        return admindirection.get(position);
    }
    public long getItemId(int position)
    {
        return position;
    }
    public String convertAccent(String toconv)
    {
        toconv = Normalizer.normalize(toconv, Normalizer.Form.NFD);
        return toconv.replaceAll("[^\\p{ASCII}]","");
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        AdapterRechercheDirections.AdminRechercheHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.admin_recherche_direction,parent,false);
            holder = new AdapterRechercheDirections.AdminRechercheHolder();
            holder.title = (TextView)convertView.findViewById(R.id.adm_rech_dir_titre);
            holder.contact = (TextView)convertView.findViewById(R.id.adm_rech_dir_contact);
            holder.adresse = (TextView)convertView.findViewById(R.id.adm_rech_dir_adresse);
            holder.localite = (TextView) convertView.findViewById(R.id.adm_rech_dir_localite);
            holder.categorie = (TextView)convertView.findViewById(R.id.adm_rech_dir_categorie);
            holder.localiteprincipale = (TextView)convertView.findViewById(R.id.adm_rech_dir_localiteprincipale);
            holder.classjava = (TextView)convertView.findViewById(R.id.adm_rech_dir_class);
            convertView.setTag(holder);
        } else {
            holder = (AdapterRechercheDirections.AdminRechercheHolder) convertView.getTag();
        }
        holder.title.setText(admindirection.get(position).getDirectionTitre());
        holder.contact.setText(admindirection.get(position).getDirectionContact());
        holder.adresse.setText(admindirection.get(position).getDirectionAdresse());
        holder.localite.setText(admindirection.get(position).getDirectionLocalite());
        holder.categorie.setText(admindirection.get(position).getDirectionCategorie());
        holder.localiteprincipale.setText(admindirection.get(position).getDirectionLocaliteprincipale());
        holder.classjava.setText(admindirection.get(position).getDirectionClass());
        return convertView;
    }
}