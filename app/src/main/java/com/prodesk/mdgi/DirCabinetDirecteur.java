package com.prodesk.mdgi;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.LocationManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.prodesk.mdgi.DirectionsCentrales.*;
//** Mard 17.09.2019 // 12.21pm **//
public class DirCabinetDirecteur extends AppCompatActivity implements OnMapReadyCallback
{
    String numero_dir_cab, txt_dir_centrale, txt_dir_latitude, txt_dir_longitude;
    Double mapLat,mapLong;
    GoogleMap drDircabMap;MapView drDircabMapView;MarkerOptions drOption, dgaOption, igsfOption;
    LocationManager locationManager;boolean statusOfGPS;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);setContentView(R.layout.activity_dir_cabinetdirecteur);
        if (!isGooglePlayServicesAvailable()) {
            Toast.makeText(this, "Service Google Play inaccessible !", Toast.LENGTH_SHORT).show();
        }
        drDircabMapView = (MapView)findViewById(R.id.dircab_map_content);
        if (drDircabMapView != null) {
            drDircabMapView.onCreate(null);
            drDircabMapView.onResume();
            drDircabMapView.getMapAsync((OnMapReadyCallback)this);
        }
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        statusOfGPS = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        txt_dir_centrale = getString(R.string.txt_cabinet_dir_titre);
        txt_dir_latitude = getString(R.string.txt_cabinet_lat);
        txt_dir_longitude = getString(R.string.txt_cabinet_long);
        TextView gotoDircab = (TextView)findViewById(R.id.goto_dircab_dc);
        gotoDircab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intDc = new Intent(DirCabinetDirecteur.this, CabinetDirecteur.class);
                startActivity(intDc);
            }
        });
        TextView gotoDga = (TextView)findViewById(R.id.goto_dircab_dga);
        gotoDga.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intDga = new Intent(DirCabinetDirecteur.this, CabinetDirGalAdjoint.class);
                startActivity(intDga);
            }
        });
        TextView gotoIgsf = (TextView)findViewById(R.id.goto_dircab_igsf);
        gotoIgsf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intIgsf = new Intent(DirCabinetDirecteur.this, CabinetIgsf.class);
                startActivity(intIgsf);
            }
        });
    }
    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        mapLat = Double.parseDouble("5.328052");mapLong = Double.parseDouble("-4.019818");
        MapsInitializer.initialize(this);drDircabMap = googleMap;
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.getUiSettings().setZoomGesturesEnabled(true);
        googleMap.getUiSettings().setCompassEnabled(true);
        LatLng ptDrDircab = new LatLng(mapLat,mapLong);
        LatLng ptdgaOption = new LatLng(Double.parseDouble("5.334398"),Double.parseDouble("-4.023052"));
        dgaOption = new MarkerOptions();
        dgaOption.position(ptdgaOption).icon(setIconColor("#c86331"));
        dgaOption.title(getString(R.string.txt_cabinet_dga_titre));
        dgaOption.snippet(getString(R.string.txt_cabinet_dga1_adresse));
        googleMap.addMarker(dgaOption);
        LatLng ptigsfOption= new LatLng(Double.parseDouble("5.321851"),Double.parseDouble("-4.015344"));
        igsfOption = new MarkerOptions();
        igsfOption.position(ptigsfOption).icon(setIconColor("#c86331"));
        igsfOption.title(getString(R.string.txt_cabinet_igsf_titre));
        igsfOption.snippet(getString(R.string.txt_cabinet_igsf_adresse));
        googleMap.addMarker(igsfOption);
        CameraPosition drAbengourouPos = CameraPosition.builder().target(ptDrDircab).zoom(15).bearing(0).tilt(45).build();
        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(drAbengourouPos));
    }
    private boolean isGooglePlayServicesAvailable()
    {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            GooglePlayServicesUtil.getErrorDialog(status, this, 0).show();
            return false;
        }
    }
    public BitmapDescriptor setIconColor(String kol)
    {
        float[] hsv = new float[3];
        Color.colorToHSV(Color.parseColor(kol), hsv);return BitmapDescriptorFactory.defaultMarker(hsv[0]);
    }
}