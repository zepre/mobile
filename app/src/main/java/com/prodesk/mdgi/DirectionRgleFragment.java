package com.prodesk.mdgi;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.prodesk.mdgi.DirectionsRegionale.*;
//** Sam 17.08.2019 // 2.21pm **//
public class DirectionRgleFragment extends Fragment implements OnMapReadyCallback
{
    public static final String TAG = DirectionRgleFragment.class.getSimpleName();
    public static final int REQUEST_CODE_FOR_PERMISSIONS = 1;GoogleApiClient mGoogleApiClient;
    MarkerOptions drAbidjanNordi,drAbidjanNordii,drAbidjanNordiii,drAbidjanNordiv,drAbidjanNordvi,drAbidjanSudi,drAbidjanSudii,drAbengourou,drAboisso,drAgboville,drBondoukou,drBouake,drDabou,drDaloa,drDimbokro,drGagnoa,drGuiglo,drKorhogo,drOdiene,drMan,drSanPedro,drYamoussoukro;
    LocationManager locationManager;boolean statusOfGPS;private Dialog mDialogGPS;View view;
    FragmentManager fragmentManager;Double mapLat,mapLong;GoogleMap dirRegionaleMap;MapView dirRegionaleMapView;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_directionrgle, container, false);
        fragmentManager=getChildFragmentManager();
        if (!isGooglePlayServicesAvailable()) {
            Toast.makeText(getActivity(), "Service Google Play inaccessible !", Toast.LENGTH_SHORT).show();
        }
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        statusOfGPS = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        final TextView openDirRegAbengourou = (TextView) view.findViewById(R.id.goto_dr_abengourou);
        openDirRegAbengourou.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openDirCentraleActivity(DirRegionalAbengourou.class);
            }
        });
        final TextView openDirRegAbidjanNordi = (TextView) view.findViewById(R.id.goto_dr_abidjannordi);
        openDirRegAbidjanNordi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openDirCentraleActivity(DirRegionalAbidjanNordi.class);
            }
        });
        final TextView openDirRegAbidjanNordii = (TextView) view.findViewById(R.id.goto_dr_abidjannordii);
        openDirRegAbidjanNordii.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openDirCentraleActivity(DirRegionalAbidjanNordii.class);
            }
        });
        final TextView openDirRegAbidjanNordiii = (TextView) view.findViewById(R.id.goto_dr_abidjannordiii);
        openDirRegAbidjanNordiii.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openDirCentraleActivity(DirRegionalAbidjanNordiii.class);
            }
        });
        final TextView openDirRegAbidjanNordiv = (TextView) view.findViewById(R.id.goto_dr_abidjannordiv);
        openDirRegAbidjanNordiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openDirCentraleActivity(DirRegionalAbidjanNordiv.class);
            }
        });
        final TextView openDirRegAbidjanNordv = (TextView) view.findViewById(R.id.goto_dr_abidjannordv);
        openDirRegAbidjanNordv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openDirCentraleActivity(DirRegionalAbidjanNordv.class);
            }
        });
        final TextView openDirRegAbidjanNordvi = (TextView) view.findViewById(R.id.goto_dr_abidjannordvi);
        openDirRegAbidjanNordvi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openDirCentraleActivity(DirRegionalAbidjanNordvi.class);
            }
        });
        final TextView openDirRegAbidjanSudi = (TextView) view.findViewById(R.id.goto_dr_abidjansudi);
        openDirRegAbidjanSudi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openDirCentraleActivity(DirRegionalAbidjanSudi.class);
            }
        });
        final TextView openDirRegAbidjanSudii = (TextView) view.findViewById(R.id.goto_dr_abidjansudii);
        openDirRegAbidjanSudii.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openDirCentraleActivity(DirRegionalAbidjanSudii.class);
            }
        });
        final TextView openDirRegAboisso = (TextView) view.findViewById(R.id.goto_dr_aboisso);
        openDirRegAboisso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openDirCentraleActivity(DirRegionalAboisso.class);
            }
        });
        final TextView openDirRegAgboville = (TextView) view.findViewById(R.id.goto_dr_agboville);
        openDirRegAgboville.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openDirCentraleActivity(DirRegionalAgboville.class);
            }
        });
        final TextView openDirRegBondoukou = (TextView) view.findViewById(R.id.goto_dr_bondoukou);
        openDirRegBondoukou.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openDirCentraleActivity(DirRegionalBondoukou.class);
            }
        });
        final TextView openDirRegBouake = (TextView) view.findViewById(R.id.goto_dr_bouake);
        openDirRegBouake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openDirCentraleActivity(DirRegionalBouake.class);
            }
        });
        final TextView openDirRegDabou = (TextView) view.findViewById(R.id.goto_dr_dabou);
        openDirRegDabou.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openDirCentraleActivity(DirRegionalDabou.class);
            }
        });
        final TextView openDirRegDaloa = (TextView) view.findViewById(R.id.goto_dr_daloa);
        openDirRegDaloa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openDirCentraleActivity(DirRegionalDaloa.class);
            }
        });
        final TextView openDirRegDimbokro = (TextView) view.findViewById(R.id.goto_dr_dimbokro);
        openDirRegDimbokro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openDirCentraleActivity(DirRegionalDimbokro.class);
            }
        });
        final TextView openDirRegGagnoa = (TextView) view.findViewById(R.id.goto_dr_gagnoa);
        openDirRegGagnoa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openDirCentraleActivity(DirRegionalGagnoa.class);
            }
        });
        final TextView openDirRegGuiglo = (TextView) view.findViewById(R.id.goto_dr_guiglo);
        openDirRegGuiglo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openDirCentraleActivity(DirRegionalGuiglo.class);
            }
        });
        final TextView openDirRegKorhogo = (TextView) view.findViewById(R.id.goto_dr_korhogo);
        openDirRegKorhogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openDirCentraleActivity(DirRegionalKorhogo.class);
            }
        });
        final TextView openDirRegMan = (TextView) view.findViewById(R.id.goto_dr_man);
        openDirRegMan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openDirCentraleActivity(DirRegionalMan.class);
            }
        });
        final TextView openDirRegOdiene = (TextView) view.findViewById(R.id.goto_dr_odiene);
        openDirRegOdiene.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openDirCentraleActivity(DirRegionalOdiene.class);
            }
        });
        final TextView openDirRegSanpedro = (TextView) view.findViewById(R.id.goto_dr_sanpedro);
        openDirRegSanpedro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openDirCentraleActivity(DirRegionalSanPedro.class);
            }
        });
        final TextView openDirRegYamoussoukro = (TextView) view.findViewById(R.id.goto_dr_yamoussoukro);
        openDirRegYamoussoukro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openDirCentraleActivity(DirRegionalYamoussoukro.class);
            }
        });
        return view;
    }
    public void openDirCentraleActivity(Class dRclax)
    {
        Intent intDr = new Intent(getActivity(), dRclax);
        startActivity(intDr);
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        dirRegionaleMapView = (MapView)view.findViewById(R.id.direction_rgle_map);
        if (dirRegionaleMapView != null) {
            dirRegionaleMapView.onCreate(null);dirRegionaleMapView.onResume();
            dirRegionaleMapView.getMapAsync(this);
        }
    }
    @SuppressLint("ResourceType")
    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        mapLat = Double.parseDouble(getString(R.string.txt_map_cotedivoire_lat));mapLong = Double.parseDouble(getString(R.string.txt_map_cotedivoire_lng));MapsInitializer.initialize(getContext());
        dirRegionaleMap = googleMap;
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.getUiSettings().setZoomGesturesEnabled(true);googleMap.getUiSettings().setCompassEnabled(true);
        BitmapDrawable bitmapDrawable = (BitmapDrawable)getResources().getDrawable(R.drawable.icon_map);
        Bitmap bt = bitmapDrawable.getBitmap();
        Bitmap markerIcon = Bitmap.createScaledBitmap(bt, 85, 85, false);
        LatLng ptDrAbNi = new LatLng(Double.parseDouble(getString(R.string.txt_dr_abidjannordi_lat)), Double.parseDouble(getString(R.string.txt_dr_abidjannordi_long)));
        drAbidjanNordi = new MarkerOptions();drAbidjanNordi.icon(BitmapDescriptorFactory.fromBitmap(markerIcon));
        drAbidjanNordi.position(ptDrAbNi).icon(setIconColor("#c86331"));drAbidjanNordi.title(getString(R.string.txt_dr_abidjannordi_titre));
        drAbidjanNordi.snippet(getString(R.string.txt_dr_abidjannordi_adresse));googleMap.addMarker(drAbidjanNordi);
        LatLng ptDrAbNii = new LatLng(Double.parseDouble(getString(R.string.txt_dr_abidjannordii_lat)), Double.parseDouble(getString(R.string.txt_dr_abidjannordii_long)));
        drAbidjanNordii = new MarkerOptions();drAbidjanNordii.icon(BitmapDescriptorFactory.fromBitmap(markerIcon));
        drAbidjanNordii.position(ptDrAbNii).icon(setIconColor("#c86331"));drAbidjanNordii.title(getString(R.string.txt_dr_abidjannordii_titre));
        drAbidjanNordii.snippet(getString(R.string.txt_dr_abidjannordii_adresse));googleMap.addMarker(drAbidjanNordii);
        LatLng ptDrAbNiii = new LatLng(Double.parseDouble(getString(R.string.txt_dr_abidjannordiii_lat)), Double.parseDouble(getString(R.string.txt_dr_abidjannordiii_long)));
        drAbidjanNordiii = new MarkerOptions();drAbidjanNordiii.icon(BitmapDescriptorFactory.fromBitmap(markerIcon));
        drAbidjanNordiii.position(ptDrAbNiii).icon(setIconColor("#c86331"));drAbidjanNordiii.title(getString(R.string.txt_dr_abidjannordiii_titre));
        drAbidjanNordiii.snippet(getString(R.string.txt_dr_abidjannordiii_adresse));googleMap.addMarker(drAbidjanNordiii);
        LatLng ptDrAbNiv = new LatLng(Double.parseDouble(getString(R.string.txt_dr_abidjannordivv_lat)), Double.parseDouble(getString(R.string.txt_dr_abidjannordivv_long)));
        drAbidjanNordiv = new MarkerOptions();drAbidjanNordiv.icon(BitmapDescriptorFactory.fromBitmap(markerIcon));
        drAbidjanNordiv.position(ptDrAbNiv).icon(setIconColor("#c86331"));drAbidjanNordiv.title(getString(R.string.txt_dr_abidjannordivv_title));
        drAbidjanNordiv.snippet(getString(R.string.txt_dr_abidjannordiv_adresse));googleMap.addMarker(drAbidjanNordiv);
        LatLng ptDrAbNvi = new LatLng(Double.parseDouble(getString(R.string.txt_dr_abidjannordvi_lat)), Double.parseDouble(getString(R.string.txt_dr_abidjannordvi_long)));
        drAbidjanNordvi = new MarkerOptions();drAbidjanNordvi.icon(BitmapDescriptorFactory.fromBitmap(markerIcon));
        drAbidjanNordvi.position(ptDrAbNvi).icon(setIconColor("#c86331"));drAbidjanNordvi.title(getString(R.string.txt_dr_abidjannordvi_titre));
        drAbidjanNordvi.snippet(getString(R.string.txt_dr_abidjannordvi_adresse));googleMap.addMarker(drAbidjanNordvi);
        LatLng ptDrAbSi = new LatLng(Double.parseDouble(getString(R.string.txt_dr_abidjansudi_lat)), Double.parseDouble(getString(R.string.txt_dr_abidjansudi_long)));
        drAbidjanSudi = new MarkerOptions();drAbidjanSudi.icon(BitmapDescriptorFactory.fromBitmap(markerIcon));
        drAbidjanSudi.position(ptDrAbSi).icon(setIconColor("#c86331"));drAbidjanSudi.title(getString(R.string.txt_dr_abidjansudi_titre));
        drAbidjanSudi.snippet(getString(R.string.txt_dr_abidjansudi_adresse));googleMap.addMarker(drAbidjanSudi);
        LatLng ptDrAbSii = new LatLng(Double.parseDouble(getString(R.string.txt_dr_abidjansudii_lat)), Double.parseDouble(getString(R.string.txt_dr_abidjansudii_long)));
        drAbidjanSudii = new MarkerOptions();drAbidjanSudii.icon(BitmapDescriptorFactory.fromBitmap(markerIcon));
        drAbidjanSudii.position(ptDrAbSii).icon(setIconColor("#c86331"));drAbidjanSudii.title(getString(R.string.txt_dr_abidjansudii_titre));
        drAbidjanSudii.snippet(getString(R.string.txt_dr_abidjansudii_adresse));googleMap.addMarker(drAbidjanSudii);
        LatLng ptDrAbeng = new LatLng(Double.parseDouble(getString(R.string.txt_dr_abengourou_lat)), Double.parseDouble(getString(R.string.txt_dr_abengourou_long)));
        drAbengourou = new MarkerOptions();drAbengourou.icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_map));
        drAbengourou.position(ptDrAbeng).icon(setIconColor("#c86331"));drAbengourou.title(getString(R.string.txt_dr_abengourou_titre));
        drAbengourou.snippet(getString(R.string.txt_dr_abengourou_adresse));googleMap.addMarker(drAbengourou);
        LatLng ptDrAbsso = new LatLng(Double.parseDouble(getString(R.string.txt_dr_aboisso_lat)), Double.parseDouble(getString(R.string.txt_dr_aboisso_long)));
        drAboisso = new MarkerOptions();drAboisso.icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_map));
        drAboisso.position(ptDrAbsso).icon(setIconColor("#c86331"));drAboisso.title(getString(R.string.txt_dr_aboisso_titre));
        drAboisso.snippet(getString(R.string.txt_dr_aboisso_adresse));googleMap.addMarker(drAboisso);
        LatLng ptDrAgbv = new LatLng(Double.parseDouble(getString(R.string.txt_dr_agboville_lat)), Double.parseDouble(getString(R.string.txt_dr_agboville_long)));
        drAgboville = new MarkerOptions();drAgboville.icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_map));
        drAgboville.position(ptDrAgbv).icon(setIconColor("#c86331"));drAgboville.title(getString(R.string.txt_dr_agboville_titre));
        drAgboville.snippet(getString(R.string.txt_dr_agboville_adresse));googleMap.addMarker(drAgboville);
        LatLng ptDrBdk = new LatLng(Double.parseDouble(getString(R.string.txt_dr_bondoukou_lat)), Double.parseDouble(getString(R.string.txt_dr_bondoukou_long)));
        drBondoukou = new MarkerOptions();drBondoukou.icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_map));
        drBondoukou.position(ptDrBdk).icon(setIconColor("#c86331"));drBondoukou.title(getString(R.string.txt_dr_bondoukou_titre));
        drBondoukou.snippet(getString(R.string.txt_dr_bondoukou_adresse));googleMap.addMarker(drBondoukou);
        LatLng ptDrBke = new LatLng(Double.parseDouble(getString(R.string.txt_dr_bouake_lat)), Double.parseDouble(getString(R.string.txt_dr_bouake_long)));
        drBouake = new MarkerOptions();drBouake.icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_map));
        drBouake.position(ptDrBke).icon(setIconColor("#c86331"));drBouake.title(getString(R.string.txt_dr_bouake_titre));drBouake.snippet(getString(R.string.txt_dr_bouake_adresse));
        googleMap.addMarker(drBouake);
        LatLng ptDrDab = new LatLng(Double.parseDouble(getString(R.string.txt_dr_dabou_lat)), Double.parseDouble(getString(R.string.txt_dr_dabou_long)));
        drDabou = new MarkerOptions();drDabou.icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_map));
        drDabou.position(ptDrDab).icon(setIconColor("#c86331"));drDabou.title(getString(R.string.txt_dr_dabou_titre));
        drDabou.snippet(getString(R.string.txt_dr_dabou_adresse));googleMap.addMarker(drDabou);
        LatLng ptDrDal = new LatLng(Double.parseDouble(getString(R.string.txt_dr_daloa_lat)), Double.parseDouble(getString(R.string.txt_dr_daloa_long)));
        drDaloa = new MarkerOptions();drDaloa.icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_map));
        drDaloa.position(ptDrDal).icon(setIconColor("#c86331"));drDaloa.title(getString(R.string.txt_dr_daloa_titre));
        drDaloa.snippet(getString(R.string.txt_dr_daloa_adresse));googleMap.addMarker(drDaloa);
        LatLng ptDrDim = new LatLng(Double.parseDouble(getString(R.string.txt_dr_dimbokro_lat)), Double.parseDouble(getString(R.string.txt_dr_dimbokro_long)));
        drDimbokro = new MarkerOptions();drDimbokro.icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_map));
        drDimbokro.position(ptDrDim).icon(setIconColor("#c86331"));drDimbokro.title(getString(R.string.txt_dr_dimbokro_titre));
        drDimbokro.snippet(getString(R.string.txt_dr_dimbokro_adresse));googleMap.addMarker(drDimbokro);
        LatLng ptDrGagnoa = new LatLng(Double.parseDouble(getString(R.string.txt_dr_gagnoa_lat)), Double.parseDouble(getString(R.string.txt_dr_gagnoa_long)));
        drGagnoa = new MarkerOptions();drGagnoa.icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_map));
        drGagnoa.position(ptDrGagnoa).icon(setIconColor("#c86331"));drGagnoa.title(getString(R.string.txt_dr_gagnoa_titre));
        drGagnoa.snippet(getString(R.string.txt_dr_gagnoa_adresse));googleMap.addMarker(drGagnoa);
        LatLng ptDrGui = new LatLng(Double.parseDouble(getString(R.string.txt_dr_guiglo_lat)), Double.parseDouble(getString(R.string.txt_dr_guiglo_long)));
        drGuiglo = new MarkerOptions();drGuiglo.icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_map));
        drGuiglo.position(ptDrGui).icon(setIconColor("#c86331"));drGuiglo.title(getString(R.string.txt_dr_guiglo_titre));
        drGuiglo.snippet(getString(R.string.txt_dr_guiglo_adresse));googleMap.addMarker(drGuiglo);
        LatLng ptDrKgo = new LatLng(Double.parseDouble(getString(R.string.txt_dr_korhogo_lat)), Double.parseDouble(getString(R.string.txt_dr_korhogo_long)));
        drKorhogo = new MarkerOptions();drKorhogo.icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_map));
        drKorhogo.position(ptDrKgo).icon(setIconColor("#c86331"));drKorhogo.title(getString(R.string.txt_dr_korhogo_titre));
        drKorhogo.snippet(getString(R.string.txt_dr_korhogo_adresse));googleMap.addMarker(drKorhogo);
        LatLng ptDrOdi = new LatLng(Double.parseDouble(getString(R.string.txt_dr_odiene_lat)), Double.parseDouble(getString(R.string.txt_dr_odiene_long)));
        drOdiene = new MarkerOptions();drOdiene.icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_map));
        drOdiene.position(ptDrOdi).icon(setIconColor("#c86331"));drOdiene.title(getString(R.string.txt_dr_odiene_titre));
        drOdiene.snippet(getString(R.string.txt_dr_odiene_adresse));googleMap.addMarker(drOdiene);
        LatLng ptDrMan = new LatLng(Double.parseDouble(getString(R.string.txt_dr_man_lat)), Double.parseDouble(getString(R.string.txt_dr_man_long)));
        drMan = new MarkerOptions();drMan.icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_map));
        drMan.position(ptDrMan).icon(setIconColor("#c86331"));drMan.title(getString(R.string.txt_dr_man_titre));
        drMan.snippet(getString(R.string.txt_dr_man_adresse));googleMap.addMarker(drMan);
        LatLng ptDrSp = new LatLng(Double.parseDouble(getString(R.string.txt_dr_sanpedro_lat)), Double.parseDouble(getString(R.string.txt_dr_sanpedro_long)));
        drSanPedro = new MarkerOptions();drSanPedro.icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_map));
        drSanPedro.position(ptDrSp).icon(setIconColor("#c86331"));drSanPedro.title(getString(R.string.txt_dr_sanpedro_titre));
        drSanPedro.snippet(getString(R.string.txt_dr_sanpedro_adresse));googleMap.addMarker(drSanPedro);
        LatLng ptDrYam = new LatLng(Double.parseDouble(getString(R.string.txt_dr_yamoussoukro_lat)), Double.parseDouble(getString(R.string.txt_dr_yamoussoukro_long)));
        drYamoussoukro = new MarkerOptions();drYamoussoukro.icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_map));
        drYamoussoukro.position(ptDrYam).icon(setIconColor("#c86331"));drYamoussoukro.title(getString(R.string.txt_dr_yamoussoukro_titre));
        drYamoussoukro.snippet(getString(R.string.txt_dr_yamoussoukro_adresse));googleMap.addMarker(drYamoussoukro);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mapLat,mapLong), 5f));
    }
    private boolean isGooglePlayServicesAvailable()
    {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            GooglePlayServicesUtil.getErrorDialog(status, getActivity(), 0).show();
            return false;
        }
    }
    public BitmapDescriptor setIconColor(String kol)
    {
        float[] hsv = new float[3];Color.colorToHSV(Color.parseColor(kol), hsv);return BitmapDescriptorFactory.defaultMarker(hsv[0]);
    }
}