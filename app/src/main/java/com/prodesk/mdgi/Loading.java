package com.prodesk.mdgi;
import android.os.Bundle;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.app.Activity;
public class Loading extends Activity
{
    ProgressBar progressB;
    TextView textV;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loading);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        try {
            this.getActionBar().hide();
        } catch (Exception e) {}
        progressB = (ProgressBar)findViewById(R.id.progressBar);
        textV = (TextView)findViewById(R.id.text_view);
        progressB.setMax(100);progressB.setScaleY(3f);
        RunAnimation();progressAnimation();
    }
    public void progressAnimation()
    {
        LoadingAnimation anim = new LoadingAnimation(this, progressB, textV, 0f, 100f);
        anim.setDuration(3100);progressB.setAnimation(anim);
    }
    private void RunAnimation()
    {
        TextView tv = (TextView) findViewById(R.id.wellcomeText);
        Animation firstAnimation = AnimationUtils.loadAnimation(this, R.anim.blink);
        firstAnimation.setRepeatCount(Animation.INFINITE);tv.startAnimation(firstAnimation);
    }
}