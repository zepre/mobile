package com.prodesk.mdgi;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.prodesk.mdgi.WebViewer.WebActivity;
//** Sam 17.08.2019 // 2.25pm **//
public class GuideProcedure extends AppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);setContentView(R.layout.activity_guideprocedure);
        TextView tx = (TextView) findViewById(R.id.guideprocedure_toolbar_title);
        tx.setText(getString(R.string.activity_guideprocedures));
        Toolbar toolbar = (Toolbar) findViewById(R.id.guideprocedure_toolbar_read);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        ImageView goback = (ImageView) findViewById(R.id.guideprocedure_retour);
        goback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                finish();
            }
        });
        TextView readGuidpaaa_clic = (TextView) findViewById(R.id.readGuidpaaa);
        readGuidpaaa_clic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openGuideProcedureRead(getString(R.string.txt_guidepro_file_aaa),getString(R.string.txt_guidepro_file_aaa_lk),"docpdf");
            }
        });
        TextView readGuidparf_clic = (TextView) findViewById(R.id.readGuidparf);
        readGuidparf_clic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openGuideProcedureRead(getString(R.string.txt_guidepro_file_arf),getString(R.string.txt_guidepro_file_arf_lk),"docpdf");
            }
        });
        TextView readGuidpasf_clic = (TextView) findViewById(R.id.readGuidpasf);
        readGuidpasf_clic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openGuideProcedureRead(getString(R.string.txt_guidepro_file_asf),getString(R.string.txt_guidepro_file_asf_lk),"docpdf");
            }
        });
        TextView readGuidpaeit_clic = (TextView) findViewById(R.id.readGuidpaeit);
        readGuidpaeit_clic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openGuideProcedureRead(getString(R.string.txt_guidepro_file_aeit),getString(R.string.txt_guidepro_file_aeit_lk),"docpdf");
            }
        });
        TextView readGuidpcrf_clic = (TextView) findViewById(R.id.readGuidpcrf);
        readGuidpcrf_clic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openGuideProcedureRead(getString(R.string.txt_guidepro_file_crf),getString(R.string.txt_guidepro_file_crf_lk),"docpdf");
            }
        });
        TextView readGuidpcsg_clic = (TextView) findViewById(R.id.readGuidpcsg);
        readGuidpcsg_clic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openGuideProcedureRead(getString(R.string.txt_guidepro_file_csg),getString(R.string.txt_guidepro_file_csg_lk),"docpdf");
            }
        });
        TextView readGuidpcdd_clic = (TextView) findViewById(R.id.readGuidpcdd);
        readGuidpcdd_clic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openGuideProcedureRead(getString(R.string.txt_guidepro_file_cdd),getString(R.string.txt_guidepro_file_cdd_lk),"docpdf");
            }
        });
        TextView readGuidppdt_clic = (TextView) findViewById(R.id.readGuidppdt);
        readGuidppdt_clic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openGuideProcedureRead(getString(R.string.txt_guidepro_file_pdt),getString(R.string.txt_guidepro_file_pdt_lk),"docpdf");
            }
        });
    }
    private void openGuideProcedureRead(String title, String link, String icon)
    {
        Intent intGuideRead = new Intent(GuideProcedure.this, WebActivity.class);
        intGuideRead.putExtra("dataWebTitle", title);intGuideRead.putExtra("dataWebLink", link);
        intGuideRead.putExtra("dataWebIcon", icon);startActivity(intGuideRead);
    }
}