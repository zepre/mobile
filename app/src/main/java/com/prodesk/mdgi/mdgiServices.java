package com.prodesk.mdgi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.telephony.TelephonyManager;
import android.widget.Toast;
import com.prodesk.mdgi.Communiques.ListeCommunique;
import com.prodesk.mdgi.Communiques.ListeCommuniqueOffline;
import com.prodesk.mdgi.DonneesLocal.DonneeLocalHelper;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;
import static com.prodesk.mdgi.Utilitaires.*;
//** Mard 08.10.2019 // 12.16am **//
public class mdgiServices extends Service
{
    ArrayList<Integer> list = new ArrayList<>();
    DonneeLocalHelper mdgiciDb;
    long ComDataRows, ComDataRowsOnline, ComDataRowsDirections, ComDataRowsDirectionsOnline, newCommuniques, newDirections;
    ArrayList<String> ComDataTitre = new ArrayList<String>();ArrayList<String> ComDataContenu = new ArrayList<String>();
    ArrayList<String> ComDataDate = new ArrayList<String>();ArrayList<String> ComDataImage = new ArrayList<String>();
    ArrayList<String> ComDirTitre = new ArrayList<String>();ArrayList<String> ComDirContact = new ArrayList<String>();
    ArrayList<String> ComDirAdresse = new ArrayList<String>();ArrayList<String> ComDirLocalite = new ArrayList<String>();
    ArrayList<String> ComDirLocaliteprincipale = new ArrayList<String>();ArrayList<String> ComDirCategorie = new ArrayList<String>();
    ArrayList<String> ComDirClassjava = new ArrayList<String>();
    HttpPost httpPost;HttpResponse response;HttpClient httpClient;
    public static final int notifyIntervall = 150000;
    private Handler serviceHandler = new Handler();
    private Timer serviceTimer = null;
    @Override
    public void onCreate() {
        mdgiciDb = new DonneeLocalHelper(this);
        if (serviceTimer != null)
            serviceTimer.cancel();
        else
            serviceTimer = new Timer();
        serviceTimer.scheduleAtFixedRate(new TimeDisplay(), 0, notifyIntervall);
    }
    @Override
    public void onDestroy() {
        super.onDestroy();serviceTimer.cancel();
        Toast.makeText(this, "Service detruit", Toast.LENGTH_SHORT).show();
    }
    class TimeDisplay extends TimerTask {
        @Override
        public void run() {
            serviceHandler.post(new Runnable() {
                @Override
                public void run() {
                    //localData();
                    /*ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo info = Connectivity.getNetworkInfo(this);
                    if (info.getType() == ConnectivityManager.TYPE_WIFI) {
                        // do something
                        Toast.makeText(getBaseContext(), "wifi connection", Toast.LENGTH_SHORT).show();
                    } else if (info.getType() == ConnectivityManager.TYPE_MOBILE) {
                        // check NetworkInfo subtype
                        if (info.getSubtype() == TelephonyManager.NETWORK_TYPE_GPRS) {
                            // Bandwidth between 100 kbps and below
                            Toast.makeText(getBaseContext(), "gprs connection", Toast.LENGTH_SHORT).show();
                        } else if (info.getSubtype() == TelephonyManager.NETWORK_TYPE_EDGE) {
                            // Bandwidth between 50-100 kbps
                            Toast.makeText(getBaseContext(), "edge connection", Toast.LENGTH_SHORT).show();
                        } else if (info.getSubtype() == TelephonyManager.NETWORK_TYPE_EVDO_0) {
                            // Bandwidth between 400-1000 kbps
                            Toast.makeText(getBaseContext(), "evd0 connection", Toast.LENGTH_SHORT).show();
                        } else if (info.getSubtype() == TelephonyManager.NETWORK_TYPE_EVDO_A) {
                            // Bandwidth between 600-1400 kbps
                            Toast.makeText(getBaseContext(), "evd0a connection", Toast.LENGTH_SHORT).show();
                        }
                    }*/
                    ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
                    if (networkInfo != null && networkInfo.isConnected()) {
                        localData();
                    } else {
                        Toast.makeText(getBaseContext(), "Pas de connexion internet !", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }
    private void localData()
    {
        getRowsAllData();
        if (ComDataRowsOnline > ComDataRows)
        {
            loadDataSqlite();
            for (int i = 0; i < newCommuniques; i++)
            {
                Bitmap imageToSave = null;
                try {
                    imageToSave = new telechargeComImageBitmap().execute(ComDataImage.get(i)).get();
                } catch (InterruptedException e) {
                } catch (ExecutionException e) {}
                mdgiciDb.enregistreCommuniques(ComDataTitre.get(i),ComDataContenu.get(i),ComDataDate.get(i),ComDataImage.get(i),imageToSave);
            }
        }
        if (ComDataRowsDirectionsOnline > ComDataRowsDirections)
        {
            mdgiciDb.effacerDirectionsLocal();
            loadDirSqlite();
            for (int i = 0; i < newDirections; i++) {
                mdgiciDb.enregistreDirection(ComDirTitre.get(i),ComDirContact.get(i),ComDirAdresse.get(i),ComDirCategorie.get(i),ComDirLocalite.get(i),ComDirLocaliteprincipale.get(i),ComDirClassjava.get(i));
            }
        }
        if (newCommuniques > 0) {
            notifierCommuniques(newCommuniques);
        }
    }
    private void getRowsAllData()
    {
        ComDataRows = mdgiciDb.nombresCommuniquesLocal();ComDataRowsDirections = mdgiciDb.nombresDirectionsLocal();
        nombreCommuniquesEnLigne getAllComRows = new nombreCommuniquesEnLigne();
        nombreDirectionsEnLigne getAllDirRows = new nombreDirectionsEnLigne();
        try {
            String dataGet = getAllComRows.execute().get();
            String dataDir = getAllDirRows.execute().get();
            if (dataGet == null) {
                ComDataRowsOnline = 0;
            } else {
                ComDataRowsOnline = Long.parseLong(dataGet);
            }
            if (dataDir == null) {
                ComDataRowsDirectionsOnline = 0;
            } else {
                ComDataRowsDirectionsOnline = Long.parseLong(dataDir);
            }
        } catch (InterruptedException e) {
        } catch (ExecutionException e) {}
        /*ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {

        } else {
            Toast.makeText(getBaseContext(), "Pas de connexion internet !", Toast.LENGTH_SHORT).show();
        }*/
        newCommuniques = ComDataRowsOnline - ComDataRows;
        newDirections = ComDataRowsDirectionsOnline - ComDataRowsDirections;
    }
    private void loadDataSqlite()
    {
        telechargerComTitre getAllComTitre = new telechargerComTitre();
        try {
            ComDataTitre = getAllComTitre.execute().get();
        } catch (InterruptedException e) {
        } catch (ExecutionException e) {}
        telechargerComContenu getAllComContenu = new telechargerComContenu();
        try {
            ComDataContenu = getAllComContenu.execute().get();
        } catch (InterruptedException e) {
        } catch (ExecutionException e) {}
        telechargerComDate getAllComDate = new telechargerComDate();
        try {
            ComDataDate = getAllComDate.execute().get();
        } catch (InterruptedException e) {
        } catch (ExecutionException e) {}
        telechargerComImage getAllComImage = new telechargerComImage();
        try {
            ComDataImage = getAllComImage.execute().get();
        } catch (InterruptedException e) {
        } catch (ExecutionException e) {}
    }
    private void loadDirSqlite()
    {
        telechargerDirTitre getAllDirTitre = new telechargerDirTitre();
        try {
            ComDirTitre = getAllDirTitre.execute().get();
        } catch (InterruptedException e) {
        } catch (ExecutionException e) {
        }
        telechargerDirContacts getAllDirContacts = new telechargerDirContacts();
        try {
            ComDirContact = getAllDirContacts.execute().get();
        } catch (InterruptedException e) {
        } catch (ExecutionException e) {}
        telechargerDirAdresse getAllDirAdresse = new telechargerDirAdresse();
        try {
            ComDirAdresse = getAllDirAdresse.execute().get();
        } catch (InterruptedException e) {
        } catch (ExecutionException e) {}
        telechargerDirLocalite getAllDirLocalite = new telechargerDirLocalite();
        try {
            ComDirLocalite = getAllDirLocalite.execute().get();
        } catch (InterruptedException e) {
        } catch (ExecutionException e) {}
        telechargerDirCategorie getAllDirCategorie = new telechargerDirCategorie();
        try {
            ComDirCategorie = getAllDirCategorie.execute().get();
        } catch (InterruptedException e) {
        } catch (ExecutionException e) {}
        telechargerDirLocaliteprincipale getAllDirLocaliteprincipale = new telechargerDirLocaliteprincipale();
        try {
            ComDirLocaliteprincipale = getAllDirLocaliteprincipale.execute().get();
        } catch (InterruptedException e) {
        } catch (ExecutionException e) {}
        telechargerDirClassjava getAllDirClassjava = new telechargerDirClassjava();
        try {
            ComDirClassjava = getAllDirClassjava.execute().get();
        } catch (InterruptedException e) {
        } catch (ExecutionException e) {}
    }
    private void notifierCommuniques(long nouvo)
    {
        int nc = (int) nouvo;Intent intCom;
        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            intCom = new Intent(getApplicationContext(), ListeCommunique.class);
        } else {
            intCom = new Intent(getApplicationContext(), ListeCommuniqueOffline.class);
        }
        PendingIntent intNotif = PendingIntent.getActivity(getApplicationContext(), (int) System.currentTimeMillis(), intCom, 0);
        NotificationCompat.Builder mBuilder =
            new NotificationCompat.Builder(getApplicationContext())
                .setAutoCancel(true)
                .setSmallIcon(R.mipmap.logo_notif)
                .setContentTitle("Communiqués MDGI")
                .setContentText("Vous avez "+nc+" nouveaux communiqués non lu(s)!")
                .setDefaults(Notification.DEFAULT_SOUND|Notification.DEFAULT_LIGHTS|Notification.DEFAULT_VIBRATE)
                .setContentIntent(intNotif);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, mBuilder.build());
    }
    private class telechargerComTitre extends AsyncTask<Void, Void, ArrayList<String>>
    {
        protected void onPreExecute()
        {
            super.onPreExecute();
        }
        protected ArrayList<String> doInBackground(Void...params)
        {
            InputStream inputStream = null;
            String resultat = "";
            ArrayList<String> localComDataTitre = new ArrayList<String>();
            try {
                httpClient = new DefaultHttpClient();
                httpPost = new HttpPost(liste_com_url);response = httpClient.execute(httpPost);
                HttpEntity entity = response.getEntity();inputStream = entity.getContent();
            } catch (Exception ecn){}
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream,"utf-8"),8);
                StringBuilder stringBuilder = new StringBuilder();
                String ligne = null;
                while ((ligne = reader.readLine()) != null)
                {
                    stringBuilder.append(ligne+"\n");
                }
                inputStream.close();
                resultat = stringBuilder.toString();
            } catch (Exception ec) {}
            try {
                resultat = resultat.substring(resultat.indexOf("["));
                JSONArray jsonArray = new JSONArray(resultat);
                for (int i = 0; i < jsonArray.length(); i++)
                {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    localComDataTitre.add(jsonObject.getString("com_titre"));
                }
            } catch (Exception ep) {}
            return localComDataTitre;
        }
        protected void onPostExecute(Void result) {}
    }
    private class telechargerComContenu extends AsyncTask<Void, Void, ArrayList<String>>
    {
        protected void onPreExecute()
        {
            super.onPreExecute();
        }
        protected ArrayList<String> doInBackground(Void...params)
        {
            InputStream inputStream = null;
            String resultat = "";
            ArrayList<String> localComDataContenu = new ArrayList<String>();
            try {
                httpClient = new DefaultHttpClient();
                httpPost = new HttpPost(liste_com_url);response = httpClient.execute(httpPost);
                HttpEntity entity = response.getEntity();inputStream = entity.getContent();
            } catch (Exception ecn){}
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream,"utf-8"),8);
                StringBuilder stringBuilder = new StringBuilder();
                String ligne = null;
                while ((ligne = reader.readLine()) != null)
                {
                    stringBuilder.append(ligne+"\n");
                }
                inputStream.close();
                resultat = stringBuilder.toString();
            } catch (Exception ec) {}
            try {
                resultat = resultat.substring(resultat.indexOf("["));
                JSONArray jsonArray = new JSONArray(resultat);
                for (int i = 0; i < jsonArray.length(); i++)
                {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    localComDataContenu.add(jsonObject.getString("com_contenu"));
                }
            } catch (Exception ep) {}
            return localComDataContenu;
        }
        protected void onPostExecute(Void result) {}
    }
    private class telechargerComDate extends AsyncTask<Void, Void, ArrayList<String>>
    {
        protected void onPreExecute()
        {
            super.onPreExecute();
        }
        protected ArrayList<String> doInBackground(Void...params)
        {
            InputStream inputStream = null;
            String resultat = "";
            ArrayList<String> localComDataDate = new ArrayList<String>();
            try {
                httpClient = new DefaultHttpClient();
                httpPost = new HttpPost(liste_com_url);response = httpClient.execute(httpPost);
                HttpEntity entity = response.getEntity();inputStream = entity.getContent();
            } catch (Exception ecn){}
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream,"utf-8"),8);
                StringBuilder stringBuilder = new StringBuilder();
                String ligne = null;
                while ((ligne = reader.readLine()) != null)
                {
                    stringBuilder.append(ligne+"\n");
                }
                inputStream.close();
                resultat = stringBuilder.toString();
            } catch (Exception ec) {}
            try {
                resultat = resultat.substring(resultat.indexOf("["));
                JSONArray jsonArray = new JSONArray(resultat);
                for (int i = 0; i < jsonArray.length(); i++)
                {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    localComDataDate.add(jsonObject.getString("com_date"));
                }
            } catch (Exception ep) {}
            return localComDataDate;
        }
        protected void onPostExecute(Void result) {}
    }
    private class telechargerComImage extends AsyncTask<Void, Void, ArrayList<String>>
    {
        protected void onPreExecute()
        {
            super.onPreExecute();
        }
        protected ArrayList<String> doInBackground(Void...params)
        {
            InputStream inputStream = null;
            String resultat = "";
            ArrayList<String> localComDataImage = new ArrayList<String>();
            try {
                httpClient = new DefaultHttpClient();
                httpPost = new HttpPost(liste_com_url);response = httpClient.execute(httpPost);
                HttpEntity entity = response.getEntity();inputStream = entity.getContent();
            } catch (Exception ecn){}
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream,"utf-8"),8);
                StringBuilder stringBuilder = new StringBuilder();
                String ligne = null;
                while ((ligne = reader.readLine()) != null)
                {
                    stringBuilder.append(ligne+"\n");
                }
                inputStream.close();
                resultat = stringBuilder.toString();
            } catch (Exception ec) {}
            try {
                resultat = resultat.substring(resultat.indexOf("["));
                JSONArray jsonArray = new JSONArray(resultat);
                for (int i = 0; i < jsonArray.length(); i++)
                {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    localComDataImage.add(base_url+jsonObject.getString("com_nomimage"));
                }
            } catch (Exception ep) {}
            return localComDataImage;
        }
        protected void onPostExecute(Void result) {}
    }
    private class telechargeComImageBitmap extends AsyncTask<String, Void, Bitmap>
    {
        protected void onPreExecute()
        {
            super.onPreExecute();
        }
        protected Bitmap doInBackground(String...params)
        {
            Bitmap localComDataImageBit = null;
            try {
                URL link = new URL(params[0]);
                HttpURLConnection con = (HttpURLConnection)link.openConnection();con.setDoInput(true);
                con.connect();InputStream is = con.getInputStream();
                final BitmapFactory.Options bitOption = new BitmapFactory.Options();
                bitOption.inJustDecodeBounds = false;bitOption.inSampleSize = 4;
                localComDataImageBit = BitmapFactory.decodeStream(is,null,bitOption);
                is.close();
            } catch (Exception ecn){}
            return localComDataImageBit;
        }
        protected void onPostExecute(Void result) {}
    }
    private class nombreCommuniquesEnLigne extends AsyncTask<Void, Void, String>
    {
        protected void onPreExecute()
        {
            super.onPreExecute();
        }
        protected String doInBackground(Void...params)
        {
            InputStream inputStream = null;
            String resultat = "";
            String localComDataRows = "";
            try {
                httpClient = new DefaultHttpClient();
                httpPost = new HttpPost(liste_com_url);
                response = httpClient.execute(httpPost);
                HttpEntity entity = response.getEntity();
                inputStream = entity.getContent();
            } catch (Exception ecn){}
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream,"utf-8"),8);
                StringBuilder stringBuilder = new StringBuilder();
                String ligne = null;
                while ((ligne = reader.readLine()) != null)
                {
                    stringBuilder.append(ligne+"\n");
                }
                inputStream.close();
                resultat = stringBuilder.toString();
            } catch (Exception ec) {}
            try {
                resultat = resultat.substring(resultat.indexOf("["));
                JSONArray jsonArray = new JSONArray(resultat);
                localComDataRows = String.valueOf(jsonArray.length());
            } catch (Exception ep) {}
            return localComDataRows;
        }
        protected void onPostExecute(Void result) {}
    }
    private class telechargerDirTitre extends AsyncTask<Void, Void, ArrayList<String>>
    {
        protected void onPreExecute()
        {
            super.onPreExecute();
        }
        protected ArrayList<String> doInBackground(Void...params)
        {
            InputStream inputStream = null;
            String resultat = "";
            ArrayList<String> localDirTitre = new ArrayList<String>();
            try {
                httpClient = new DefaultHttpClient();
                httpPost = new HttpPost(liste_dir_url);
                response = httpClient.execute(httpPost);
                HttpEntity entity = response.getEntity();
                inputStream = entity.getContent();
            } catch (Exception ecn){}
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream,"utf-8"),8);
                StringBuilder stringBuilder = new StringBuilder();
                String ligne = null;
                while ((ligne = reader.readLine()) != null)
                {
                    stringBuilder.append(ligne+"\n");
                }
                inputStream.close();
                resultat = stringBuilder.toString();
            } catch (Exception ec) {}
            try {
                resultat = resultat.substring(resultat.indexOf("["));
                JSONArray jsonArray = new JSONArray(resultat);
                for (int i = 0; i < jsonArray.length(); i++)
                {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    localDirTitre.add(jsonObject.getString("titre"));
                }
            } catch (Exception ep) {}
            return localDirTitre;
        }
        protected void onPostExecute(Void result) {}
    }
    private class telechargerDirContacts extends AsyncTask<Void, Void, ArrayList<String>>
    {
        protected void onPreExecute()
        {
            super.onPreExecute();
        }
        protected ArrayList<String> doInBackground(Void...params)
        {
            InputStream inputStream = null;
            String resultat = "";
            ArrayList<String> localDirContact = new ArrayList<String>();
            try {
                httpClient = new DefaultHttpClient();
                httpPost = new HttpPost(liste_dir_url);
                response = httpClient.execute(httpPost);
                HttpEntity entity = response.getEntity();
                inputStream = entity.getContent();
            } catch (Exception ecn){}
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream,"utf-8"),8);
                StringBuilder stringBuilder = new StringBuilder();
                String ligne = null;
                while ((ligne = reader.readLine()) != null)
                {
                    stringBuilder.append(ligne+"\n");
                }
                inputStream.close();
                resultat = stringBuilder.toString();
            } catch (Exception ec) {}
            try {
                resultat = resultat.substring(resultat.indexOf("["));
                JSONArray jsonArray = new JSONArray(resultat);
                for (int i = 0; i < jsonArray.length(); i++)
                {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    localDirContact.add(jsonObject.getString("contacts"));
                }
            } catch (Exception ep) {}
            return localDirContact;
        }
        protected void onPostExecute(Void result) {}
    }
    private class telechargerDirAdresse extends AsyncTask<Void, Void, ArrayList<String>>
    {
        protected void onPreExecute()
        {
            super.onPreExecute();
        }
        protected ArrayList<String> doInBackground(Void...params)
        {
            InputStream inputStream = null;
            String resultat = "";
            ArrayList<String> localDirAdresse = new ArrayList<String>();
            try {
                httpClient = new DefaultHttpClient();
                httpPost = new HttpPost(liste_dir_url);
                response = httpClient.execute(httpPost);
                HttpEntity entity = response.getEntity();
                inputStream = entity.getContent();
            } catch (Exception ecn){}
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream,"utf-8"),8);
                StringBuilder stringBuilder = new StringBuilder();
                String ligne = null;
                while ((ligne = reader.readLine()) != null)
                {
                    stringBuilder.append(ligne+"\n");
                }
                inputStream.close();
                resultat = stringBuilder.toString();
            } catch (Exception ec) {}
            try {
                resultat = resultat.substring(resultat.indexOf("["));
                JSONArray jsonArray = new JSONArray(resultat);
                for (int i = 0; i < jsonArray.length(); i++)
                {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    localDirAdresse.add(jsonObject.getString("adresse"));
                }
            } catch (Exception ep) {}
            return localDirAdresse;
        }
        protected void onPostExecute(Void result) {}
    }
    private class telechargerDirCategorie extends AsyncTask<Void, Void, ArrayList<String>>
    {
        protected void onPreExecute()
        {
            super.onPreExecute();
        }
        protected ArrayList<String> doInBackground(Void...params)
        {
            InputStream inputStream = null;
            String resultat = "";
            ArrayList<String> localDirCategorie = new ArrayList<String>();
            try {
                httpClient = new DefaultHttpClient();httpPost = new HttpPost(liste_dir_url);
                response = httpClient.execute(httpPost);HttpEntity entity = response.getEntity();
                inputStream = entity.getContent();
            } catch (Exception ecn){}
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream,"utf-8"),8);
                StringBuilder stringBuilder = new StringBuilder();
                String ligne = null;
                while ((ligne = reader.readLine()) != null)
                {
                    stringBuilder.append(ligne+"\n");
                }
                inputStream.close();
                resultat = stringBuilder.toString();
            } catch (Exception ec) {}
            try {
                resultat = resultat.substring(resultat.indexOf("["));
                JSONArray jsonArray = new JSONArray(resultat);
                for (int i = 0; i < jsonArray.length(); i++)
                {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    localDirCategorie.add(jsonObject.getString("categorie"));
                }
            } catch (Exception ep) {}
            return localDirCategorie;
        }
        protected void onPostExecute(Void result) {}
    }
    private class telechargerDirLocalite extends AsyncTask<Void, Void, ArrayList<String>>
    {
        protected void onPreExecute()
        {
            super.onPreExecute();
        }
        protected ArrayList<String> doInBackground(Void...params)
        {
            InputStream inputStream = null;
            String resultat = "";
            ArrayList<String> localDirLocalite = new ArrayList<String>();
            try {
                httpClient = new DefaultHttpClient();httpPost = new HttpPost(liste_dir_url);response = httpClient.execute(httpPost);
                HttpEntity entity = response.getEntity();inputStream = entity.getContent();
            } catch (Exception ecn){}
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream,"utf-8"),8);
                StringBuilder stringBuilder = new StringBuilder();
                String ligne = null;
                while ((ligne = reader.readLine()) != null)
                {
                    stringBuilder.append(ligne+"\n");
                }
                inputStream.close();
                resultat = stringBuilder.toString();
            } catch (Exception ec) {}
            try {
                resultat = resultat.substring(resultat.indexOf("["));
                JSONArray jsonArray = new JSONArray(resultat);
                for (int i = 0; i < jsonArray.length(); i++)
                {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    localDirLocalite.add(jsonObject.getString("localite"));
                }
            } catch (Exception ep) {}
            return localDirLocalite;
        }
        protected void onPostExecute(Void result) {}
    }
    private class telechargerDirLocaliteprincipale extends AsyncTask<Void, Void, ArrayList<String>>
    {
        protected void onPreExecute()
        {
            super.onPreExecute();
        }
        protected ArrayList<String> doInBackground(Void...params)
        {
            InputStream inputStream = null;
            String resultat = "";
            ArrayList<String> localDirLocaliteprincipale = new ArrayList<String>();
            try {
                httpClient = new DefaultHttpClient();httpPost = new HttpPost(liste_dir_url);
                response = httpClient.execute(httpPost);HttpEntity entity = response.getEntity();
                inputStream = entity.getContent();
            } catch (Exception ecn){}
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream,"utf-8"),8);
                StringBuilder stringBuilder = new StringBuilder();
                String ligne = null;
                while ((ligne = reader.readLine()) != null)
                {
                    stringBuilder.append(ligne+"\n");
                }
                inputStream.close();
                resultat = stringBuilder.toString();
            } catch (Exception ec) {}
            try {
                resultat = resultat.substring(resultat.indexOf("["));
                JSONArray jsonArray = new JSONArray(resultat);
                for (int i = 0; i < jsonArray.length(); i++)
                {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    localDirLocaliteprincipale.add(jsonObject.getString("localite_principale"));
                }
            } catch (Exception ep) {}
            return localDirLocaliteprincipale;
        }
        protected void onPostExecute(Void result) {}
    }
    private class telechargerDirClassjava extends AsyncTask<Void, Void, ArrayList<String>>
    {
        protected void onPreExecute()
        {
            super.onPreExecute();
        }
        protected ArrayList<String> doInBackground(Void...params)
        {
            InputStream inputStream = null;
            String resultat = "";
            ArrayList<String> localDirClassjava = new ArrayList<String>();
            try {
                httpClient = new DefaultHttpClient();httpPost = new HttpPost(liste_dir_url);
                response = httpClient.execute(httpPost);HttpEntity entity = response.getEntity();
                inputStream = entity.getContent();
            } catch (Exception ecn){
                Toast.makeText(getApplicationContext(),"Erreur de traitement !",Toast.LENGTH_SHORT).show();
            }
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream,"utf-8"),8);
                StringBuilder stringBuilder = new StringBuilder();
                String ligne = null;
                while ((ligne = reader.readLine()) != null)
                {
                    stringBuilder.append(ligne+"\n");
                }
                inputStream.close();
                resultat = stringBuilder.toString();
            } catch (Exception ec) {
                Toast.makeText(getApplicationContext(),"Erreur de traitement !",Toast.LENGTH_SHORT).show();
            }
            try {
                resultat = resultat.substring(resultat.indexOf("["));
                JSONArray jsonArray = new JSONArray(resultat);
                for (int i = 0; i < jsonArray.length(); i++)
                {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    localDirClassjava.add(jsonObject.getString("classjava"));
                }
            } catch (Exception ep) {
                Toast.makeText(getApplicationContext(),"Erreur de traitement !",Toast.LENGTH_SHORT).show();
            }
            return localDirClassjava;
        }
        protected void onPostExecute(Void result) {}
    }
    private class nombreDirectionsEnLigne extends AsyncTask<Void, Void, String>
    {
        protected void onPreExecute()
        {
            super.onPreExecute();
        }
        protected String doInBackground(Void...params)
        {
            InputStream inputStream = null;
            String resultat = "";
            String localDirections = "";
            try {
                httpClient = new DefaultHttpClient();httpPost = new HttpPost(liste_dir_url);
                response = httpClient.execute(httpPost);HttpEntity entity = response.getEntity();
                inputStream = entity.getContent();
            } catch (Exception ecn){}
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream,"utf-8"),8);
                StringBuilder stringBuilder = new StringBuilder();
                String ligne = null;
                while ((ligne = reader.readLine()) != null)
                {
                    stringBuilder.append(ligne+"\n");
                }
                inputStream.close();
                resultat = stringBuilder.toString();
            } catch (Exception ec) {}
            try {
                resultat = resultat.substring(resultat.indexOf("["));
                JSONArray jsonArray = new JSONArray(resultat);
                localDirections = String.valueOf(jsonArray.length());
            } catch (Exception ep) {}
            return localDirections;
        }
        protected void onPostExecute(Void result) {}
    }
    @Nullable
    @Override
    public IBinder onBind(Intent intent)
    {
        return null;
    }
}