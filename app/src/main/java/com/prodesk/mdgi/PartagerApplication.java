package com.prodesk.mdgi;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.Normalizer;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;
import javax.net.ssl.HttpsURLConnection;
import static com.prodesk.mdgi.Utilitaires.email_url;
//** Lund 11.11.2019 // 4.35pm **//
public class PartagerApplication extends AppCompatActivity
{
    String partageAdmin,partageNom,partageEmail,partageEmailDestinataire,partageMessage,partageDate,partageSujet;
    private EditText partageUsername, partageUserdestinataire, partageUsermessage;
    private TextInputLayout partageLayUsername, partageLayUserdestinataire, partageLayUsermessage;
    private Button partageBtnsend, partageBtncancel;ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_partager_application);
        partageLayUsername = (TextInputLayout) findViewById(R.id.partagerapp_text_username);
        partageLayUserdestinataire = (TextInputLayout) findViewById(R.id.partagerapp_text_usermail_destinataire);
        partageLayUsermessage = (TextInputLayout) findViewById(R.id.partagerapp_text_usermessage);
        partageUsername = (EditText) findViewById(R.id.partagerapp_username);
        partageUserdestinataire = (EditText) findViewById(R.id.partagerapp_usermail_destinataire);
        partageUsermessage = (EditText) findViewById(R.id.partagerapp_usermessage);
        partageBtnsend = (Button) findViewById(R.id.partagerapp_btn_send);
        partageBtncancel = (Button) findViewById(R.id.partagerapp_btn_cancel);
        partageUsername.addTextChangedListener(new PartagerApplication.verificateurTexte(partageUsername));
        partageUserdestinataire.addTextChangedListener(new PartagerApplication.verificateurTexte(partageUserdestinataire));
        partageUsermessage.addTextChangedListener(new PartagerApplication.verificateurTexte(partageUsermessage));
        ImageView goback = (ImageView) findViewById(R.id.partagerapp_toolbar_icon);
        goback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        TextView networkState = (TextView)findViewById(R.id.partage_network_error);
        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            networkState.setVisibility(View.INVISIBLE);
        } else {
            partageBtnsend.setEnabled(false);
            partageBtncancel.setEnabled(false);
        }
        progressDialog = new ProgressDialog(PartagerApplication.this);
    }
    public void partagerApplication(View view)
    {
        if (!verifications()) {
            return;
        } else {
            prendsLesDonnees();
            new SendPartageRequest().execute();
            annulerPartage(view);
        }
    }
    public void annulerPartage(View view)
    {
        partageUsername.setText("");partageUserdestinataire.setText("");
        partageLayUsername.setErrorEnabled(false);partageLayUserdestinataire.setErrorEnabled(false);
        obtainFocus(partageUsername);
    }
    public boolean verifications()
    {
        if (!validateUserName()) {
            return false;
        }
        if (!validateUserDestinataire()) {
            return false;
        }
        if (!validateUserMessage()) {
            return false;
        }
        return true;
    }
    public void prendsLesDonnees()
    {
        Date dTime = new Date();
        java.text.DateFormat dfl = java.text.DateFormat.getDateInstance(java.text.DateFormat.FULL,new Locale("FR","fr"));
        String dJour = dfl.format(dTime);
        String heure = new java.text.SimpleDateFormat("HH:mm:ss").format(new Date());
        partageAdmin = getString(R.string.txt_contact_destinataire);
        partageNom = partageUsername.getText().toString();
        partageEmail = String.valueOf(R.string.txt_contact_destinataire);
        partageEmailDestinataire = partageUserdestinataire.getText().toString();
        partageMessage = partageUsermessage.getText().toString();
        partageSujet = "PARTAGE MDGICI à "+partageEmailDestinataire;partageDate = dJour+" "+heure;
    }
    private boolean validateUserName()
    {
        if (partageUsername.getText().toString().trim().isEmpty()) {
            partageLayUsername.setError(getString(R.string.err_contactemail_username));
            obtainFocus(partageUsername);
            return false;
        } else {
            partageLayUsername.setErrorEnabled(false);
        }
        return true;
    }
    private boolean validateUserDestinataire()
    {
        String userdestinataire = partageUserdestinataire.getText().toString();
        if (userdestinataire.isEmpty() || !estEmailValid(userdestinataire)) {
            partageLayUserdestinataire.setError(getString(R.string.err_contactemail_usermail));
            obtainFocus(partageUserdestinataire);
            return false;
        } else {
            partageLayUserdestinataire.setErrorEnabled(false);
        }
        return true;
    }
    private boolean validateUserMessage()
    {
        String usermessage = partageUsermessage.getText().toString();
        if (usermessage.isEmpty()) {
            partageLayUsermessage.setError(getString(R.string.err_contactemail_usermessage));
            obtainFocus(partageUsermessage);
            return false;
        } else {
            partageLayUsermessage.setErrorEnabled(false);
        }
        return true;
    }
    private static boolean estEmailValid(String xemail)
    {
        return !TextUtils.isEmpty(xemail) && android.util.Patterns.EMAIL_ADDRESS.matcher(xemail).matches();
    }
    private void obtainFocus(View view)
    {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }
    private class verificateurTexte implements TextWatcher
    {
        private View view;
        private verificateurTexte(View view)
        {
            this.view = view;
        }
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
        public void afterTextChanged(Editable editable)
        {
            switch (view.getId())
            {
                case R.id.partagerapp_username:
                    validateUserName();
                break;
                case R.id.partagerapp_usermail_destinataire:
                    validateUserDestinataire();
                break;
                case R.id.partagerapp_usermessage:
                    validateUserMessage();
                break;
            }
        }
    }
    public class SendPartageRequest extends AsyncTask<String, Void, String>
    {
        protected void onPreExecute()
        {
            progressDialog.setMessage(getString(R.string.txt_wait_until_contact_send_message));
            progressDialog.setIndeterminate(true);progressDialog.setCancelable(false);progressDialog.show();
        }
        protected String doInBackground(String... arg0)
        {
            try {
                URL url = new URL(email_url);
                JSONObject postDataParams = new JSONObject();
                postDataParams.put("client_name", convertAccent(partageNom));
                postDataParams.put("client_email", convertAccent(partageEmail));
                postDataParams.put("client_sujet", convertAccent(partageSujet));
                postDataParams.put("client_message", convertAccent(partageMessage));
                postDataParams.put("client_destinataire_email", convertAccent(partageAdmin));
                postDataParams.put("client_categorie", convertAccent("Partage Application"));
                postDataParams.put("client_destinataire", convertAccent("MDGCI Admin"));
                postDataParams.put("client_date", convertAccent(partageDate));
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();conn.setReadTimeout(15000 );
                conn.setConnectTimeout(15000 );conn.setRequestMethod("POST");
                conn.setDoInput(true);conn.setDoOutput(true);
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(postDataParams));
                writer.flush();writer.close();os.close();
                int responseCode=conn.getResponseCode();
                if (responseCode == HttpsURLConnection.HTTP_OK) {
                    BufferedReader in=new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    StringBuffer sb = new StringBuffer("");
                    String line="";
                    while((line = in.readLine()) != null) {
                        sb.append(line);
                        break;
                    }
                    in.close();
                    return sb.toString();
                } else {
                    return new String("false : "+responseCode);
                }
            } catch(Exception e){
                return new String("Exception: " + e.getMessage());
            }
        }
        @Override
        protected void onPostExecute(String result)
        {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
                Toast.makeText(getApplicationContext(), "Votre recommandation a été envoyée. \n La DGI vous remercie!", Toast.LENGTH_LONG).show();
            }
        }
    }
    public String getPostDataString(JSONObject params) throws Exception
    {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        Iterator<String> itr = params.keys();
        while(itr.hasNext()){
            String key= itr.next();
            Object value = params.get(key);
            if (first) {
                first = false;
            } else {
                result.append("&");
            }
            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));
        }
        return result.toString();
    }
    public String convertAccent(String toconv)
    {
        toconv = Normalizer.normalize(toconv, Normalizer.Form.NFD);
        return toconv.replaceAll("[^\\p{ASCII}]","");
    }
}